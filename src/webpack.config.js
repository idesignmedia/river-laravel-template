const path = require("path");

module.exports = {
    resolve: {
        extensions: ["*", ".js", ".jsx", ".vue", ".ts", ".tsx"],
        alias: {
            "@": path.resolve("resources"),
            "~": path.resolve("resources/inertia"),

            // Prevent loading vue from npm packages when linked via `npm link`
            // When this setting is not present, you may encounter the error
            // `Cannot read property 'isCE'`. This happens when a vue component
            // renders a slot using a different Vue version. Read more here:
            // https://github.com/vuejs/core/issues/4344
            vue: path.resolve("./node_modules/vue"),

            "ziggy-js": path.resolve("./vendor/tightenco/ziggy/src/js/index.js"),
        },
    },
    // Load in ambient Ripple Design System setup
    module: {
     rules: [
        {
           test: /\.scss$/,
           loader: "sass-loader",
           options: {
              additionalData: `
                    @import "./resources/styles/design-system/index.scss";
              `
           }
        }
     ]
    },
    plugins: [
        //
    ],
};
