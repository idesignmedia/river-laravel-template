module.exports = {
    root: true,
    env: {
        browser: true,
        node: true,
        es2020: true,
    },
    extends: ["plugin:vue/essential", "airbnb-base", "prettier"],
    plugins: [
        // "vue",
        "prettier",
    ],
    parserOptions: {
        parser: "@babel/eslint-parser",
        requireConfigFile: false,
    },
    rules: {
        "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
        "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
        "import/prefer-default-export": "off",
        "no-param-reassign": "off",
        "vue/no-multiple-template-root": "off",
    },
};
