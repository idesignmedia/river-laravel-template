module.exports = {
    extends: "stylelint-config-standard",
    plugins: ["stylelint-scss"],
    rules: {
        indentation: 4,
        "at-rule-no-unknown": null,
        "scss/at-rule-no-unknown": true,
        "scss/selector-no-redundant-nesting-selector": true,
        "declaration-block-trailing-semicolon": [
            "always",
            {
                ignore: ["single-declaration"],
            },
        ],
    },
};
