@servers(['web' => $user.'@'.$host,'localhost' => '127.0.0.1'])

@setup
// Sanity checks
if (empty($host)) {
exit('ERROR: $host var empty or not defined');
}
if (empty($user)) {
exit('ERROR: $user var empty or not defined');
}
if (empty($path)) {
exit('ERROR: $path var empty or not defined');
}
if (empty($build)) {
exit('ERROR: $build var empty or not defined');
}
if (empty($commit)) {
exit('ERROR: $commit var empty or not defined');
}

if (file_exists($path) || is_writable($path)) {
exit("ERROR: cannot access $path");
}

// Ensure given $path is an application directory (/container/application)
if (!(preg_match('/\/container\/application/i', $path) === 1)) {
exit('ERROR: $path provided doesn\'t look like an application directory path?');
}

$current_release_dir = $path . '/current';
$releases_dir = $path . '/releases';
$new_release_dir = $releases_dir . '/' . $build . '_' . $commit;
$remote = $user . '@' . $host . ':' . $new_release_dir;
$remote_host = $user . '@' . $host;

// Command or path to invoke PHP
$php = empty($php) ? 'php' : $php;
@endsetup

@story('deploy')
copy_nginx
create_release_folder
rsync
manifest_file
setup_symlinks
verify_install
activate_release
optimise
migrate
cleanup
@endstory

@task('create_release_folder', ['on' => 'web'])
mkdir -p {{ $releases_dir }}
@endtask

@task('debug', ['on' => 'localhost'])
ls -la {{ $dir }}
@endtask

@task('rsync', ['on' => 'localhost'])
echo "* Deploying code from {{ $dir }}/src to {{ $remote }} *"
#
https://explainshell.com/explain?cmd=rsync+-zrSlh+--exclude-from%3Ddeployment-exclude-list.txt+.%2F.+%7B%7B+%24remote+%7D%7D
rsync -zrSlh --stats --exclude-from=storage/deployment-exclude-list.txt {{ $dir }}/src/ {{ $remote }}
@endtask

@task('copy_nginx', ['on' => 'localhost'])
rsync -zrSlh --stats {{ $dir }}/platform/sitehost/nginx/ {{$remote_host}}:/container/config/nginx/sites-available/
@endtask

@task('manifest_file', ['on' => 'web'])
echo "* Writing deploy manifest file *"
echo -e "{\"build\":\""{{ $build }}"\", \"commit\":\""{{ $commit }}"\",
\"branch\":\""{{ $branch }}"\"}" > {{ $new_release_dir }}/deploy-manifest.json
@endtask

@task('setup_symlinks', ['on' => 'web'])
echo "* Ensuring storage paths exist *"
mkdir -p {{ $path }}/storage
mkdir -p {{ $path }}/storage/app/public
mkdir -p {{ $path }}/storage/framework
mkdir -p {{ $path }}/storage/framework/cache
mkdir -p {{ $path }}/storage/framework/sessions
mkdir -p {{ $path }}/storage/framework/testing
mkdir -p {{ $path }}/storage/framework/views
mkdir -p {{ $path }}/storage/logs

echo "* Linking .env file to new release dir ({{ $path }}/.env -> {{ $new_release_dir }}/.env) *"
ln -nfs {{ $path }}/.env {{ $new_release_dir }}/.env

if [ -f {{ $new_release_dir }}/storage ]; then
echo "* Moving existing storage dir *"
mv {{ $new_release_dir }}/storage {{ $new_release_dir }}/storage.orig 2>/dev/null
fi

echo "* Linking storage directory to new release dir ({{ $path }}/storage -> {{ $new_release_dir }}/storage)
*"
ln -nfs {{ $path }}/storage {{ $new_release_dir }}/storage
@endtask

@task('verify_install', ['on' => 'web'])
echo "* Verifying install ({{ $new_release_dir }}) *"
cd {{ $new_release_dir }}
ls -al
{{ $php }} artisan --version
@endtask

@task('activate_release', ['on' => 'web'])
echo "* Activating new release ({{ $new_release_dir }} -> {{ $current_release_dir }}) *"
ln -nfs {{ $new_release_dir }} {{ $current_release_dir }}
@endtask

@task('optimise', ['on' => 'web'])
cd {{ $new_release_dir }}

{{ $php }} artisan down

echo '* Dumping autoload *'
composer dumpautoload

echo '* Clearing cache *'
{{ $php }} artisan cache:clear
{{ $php }} artisan config:clear
{{ $php }} artisan route:clear
{{ $php }} artisan view:clear

echo '* Optimising *'
{{ $php }} artisan config:cache
{{ $php }} artisan route:cache
{{ $php }} artisan event:cache
{{ $php }} artisan storage:link

echo '* Restarting queues *'
{{ $php }} artisan queue:restart
{{-- {{ $php }} artisan horizon:purge --}}
{{-- {{ $php }} artisan horizon:terminate --}}
{{-- {{ $php }} artisan horizon:background-terminate --}}

{{ $php }} artisan up
@endtask

@task('migrate', ['on' => 'web'])
echo '* Running migrations *'
cd {{ $new_release_dir }}
{{ $php }} artisan migrate --force
@endtask

@task('cleanup', ['on' => 'web'])
echo "* Executing cleanup command in {{ $releases_dir }} *"
ls -dt {{ $releases_dir }}/*/ | tail -n +4 | xargs rm -rf
@endtask
