@component('mail::message')
# New Customer Account

A new customer registered an account

**Name**<br>
{{ $user->name }}

**Email**<br>
{{ $user->email }}

**Registered At**<br>
{{ $user->created_at->toDateTimeString() }}

<br><br>

Thanks,<br>
{{ config('app.name') }} Website
@endcomponent