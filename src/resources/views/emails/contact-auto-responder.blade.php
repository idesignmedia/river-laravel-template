@component('mail::message')
# Message Received

Thank you for your message. We will get back to you as soon as possible.
<br><br>

Thanks,<br>
{{ config('app.name') }} Website
@endcomponent