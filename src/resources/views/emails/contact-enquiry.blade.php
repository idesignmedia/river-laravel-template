@component('mail::message')
# New Contact Enquiry

A new message has been received from the website contact form. Please review and reply when ready.

**Name**<br>
{{ $form['name'] }}

**Email**<br>
{{ $form['email'] }}

**Message**<br>
{{ $form['message'] }}

<br><br>

Thanks,<br>
{{ config('app.name') }} Website
@endcomponent