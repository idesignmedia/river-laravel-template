{{-- Contact Form --}}
<x-form.form action="contact" redirect="contact/success">

    @if (session()->has('success'))
    <x-form.message success message="{{ session()->get('success') ?? null }}" mb="small" />
    @endif

    {{-- Inputs --}}

    <x-form.control name="name" label="Name" placeholder="Your Name..." :value="old('name')" :error="$errors->first('name')" />
    <x-form.control name="email" label="Email Address" placeholder="Email Address..." :value="old('email')" :error="$errors->first('email')" />
    <x-form.control name="message" type="textarea" label="Message" placeholder="Your Message..." :value="old('message')" :error="$errors->first('message')" />

    {{-- Submit --}}
    <x-form.submit text="Submit Message" />
</x-form.form>
