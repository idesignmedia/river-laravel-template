{{-- Hero: create a hero banner with text and an image --}}
@props([
    'title' => null, // null, string, slot
    'subtitle' => null, // null, string, slot
    'img' => null, // null, string, slot
    'scope' => null, // 'reverse' if there is an $img, false otherwise
    'cover' => null, // null (true if an image is provided, false otherwise), bool, slot
    'fallbackColor' => null, // hex color value
    'variation' => null, // null, 'fullscreen', 'large', 'small'
    'more' => null, // null, string selector to scroll to (eg, '#intro'), slot
])
@php
    // Defaults
    if ($variation === null && ! $img) {
        $variation = 'small';
    }
    if ($cover === null) {
        $cover = ( $img ? true : false );
    }
    if ($scope === null) {
        $scope = ( $img ? 'reverse' : null );
    }

    // Apply the 'display-hero' component class
    $attributes = \Attr::applyComponent(
        class: 'display-hero',
        variations: [$variation, 'img' => $img],
        variationAttributes: ['fullscreen', 'large', 'small'],
        attributes: $attributes,
    );

    // Fallback Color style
    if ($fallbackColor) {
        $attributes = $attributes->merge(['style' => "background-color: {$fallbackColor};"]);
    }

    // Options
    $titleSize = 2;
    $titleSizeLarge = 1;
    $subtitleSize = 'xl';
    $spacerSize = 'normal';

@endphp
<x-block {{ $attributes->merge(['scope' => $scope]) }} class="js-hero-parallax">

    {{-- Image / Background --}}
    <div class="display-hero__bg display-cover js-hero-parallax__bg">
        @if($img && gettype($img) == 'string')
            <x-image childClass="display-cover display-fit" src="{{ $img }}"></x-image>
        @elseif($img)
            {{ $img }} {{-- slot --}}
        @endif
    </div>

    {{-- Cover --}}
    @if($cover === true)
        <div class="display-hero__cover display-cover"></div>
    @elseif($cover)
        {!! $cover !!} {{-- slot --}}
    @endif

    {{-- Content --}}
    <x-container class="display-hero__container js-hero-parallax__content" position="relative">
        <div class="display-hero__body">
            {{-- Title --}}
            @if($title && gettype($title) == 'string')
            <x-title size="{{ $variation == 'large' ? $titleSizeLarge : $titleSize }}" mb="{{ $subtitle ? 'xs' : 'none' }}">
                {!! $title !!}
            </x-title>
            @elseif($title)
            {{ $title }} {{-- slot --}}
            @endif
            {{-- Subtitle --}}
            @if($subtitle && gettype($subtitle) == 'string')
            <x-text size="{{ $subtitleSize }}" mb="none">
                {!! $subtitle !!}
            </x-text>
            @elseif($subtitle)
            {{ $subtitle }} {{-- slot --}}
            @endif
            {{-- Spacer --}}
            @if( ($title || $subtitle) && ! empty((string)$slot))
            <x-space size="{{ $spacerSize }}" />
            @endif
            {{-- Content Slot --}}
            {{ $slot }}
        </div>
    </x-container>

    {{-- More Link --}}
    @if($more && gettype($more) == 'string')
    <div class="display-hero__more">
        <a href="{{ $more }}">
            <x-icon name="ui/down"></x-icon>
        </a>
    </div>
    @elseif($more)
    <div class="display-hero__more">
        {{ $more }} {{-- slot --}}
    </div>
    @endif

</x-block>
