{{-- RIVER Website Template: Component --}}
{{-- Content: Recent Posts --}}
@props([
    'emptyMessage' => 'We are currently updating our website, and will be adding new posts shortly...',
])
@php
    // Grab "Recent Posts", checking cache accordingly
    $cacheService = config('river.universalcontent.services.UniversalContentCaching');
    $latestCacheTime = $cacheService::latestCache();
    $cacheKey = 'content@recent-posts';

    $postsData = $cacheService::cachingEnabled()
        ? \Cache::get($cacheKey)
        : null;

    // Calculate and store the latests posts,
    // if none are defined, or UCM has been updated since our last cache
    if (! $postsData || ($latestCacheTime && $latestCacheTime > $postsData['timestamp'])) {

        // 1x each of Updates, Events, Resources
        $posts = [];

        // Recent Update
        $postQuery = \App\Models\ContentUpdate::repository()
            ->getEntitiesQuery(['orderBy' => ['published_at' => 'desc']])
            ->limit(1);
        $posts[] = $postQuery->first();

        // Recent Event
        $eventQuery = \App\Models\ContentEvent::repository()
            ->getEntitiesQuery(['orderBy' => ['published_at' => 'desc']])
            ->limit(1);
        $posts[] = $eventQuery->first();

        // Recent Resource
        $resourceQuery = \App\Models\ContentResource::repository()
            ->getEntitiesQuery(['orderBy' => ['published_at' => 'desc']])
            ->limit(1);
        $posts[] = $resourceQuery->first();

        // Transform into cards
        $posts = array_filter($posts);
        $cards = [];
        foreach($posts as $post) {
            $cards[] = $post->toCardData();
        }

        // Define; cache with current timestamp
        $postsData = [
            'timestamp' => \Illuminate\Support\Carbon::now(),
            'data' => $cards,
        ];

        if ($cacheService::cachingEnabled()) {
            \Cache::put($cacheKey, $postsData, $cacheService::defaultCacheTime());
        }
    }

    $cards = $postsData['data'] ?? [];
    shuffle($cards);
@endphp
<x-box {{ $attributes }} class="content-recent-posts">
    @if(count($cards))
        <x-grid columns="1 sm:2 md:3" gapX="normal" gapY="normal md:2xl" class="ucm-cards shared-cards">
            @foreach($cards as $card)
                <x-universalcontent.card :entity="$card" />
            @endforeach
        </x-grid>
    @else
        <x-caption my="normal md:2xl">{{ $emptyMessage }}</x-caption>
    @endif
</x-box>