{{-- Menu PageData: a menu item created from a UniversalContent PageData DTO --}}
@props([
    'pageData' => null,
    'includeChildren' => true,
    'highlightActive' => true,
    'highlightActiveChildren' => true,
])
@if($pageData)
    @if($includeChildren && $pageData->hasChildren())
        {{-- Dropdown for children --}}
        <x-dropdown-menu variation="right inline" scope="reverse-cancel">
            {{-- Dropdown trigger button --}}
            <x-slot name="trigger">
                <x-link variation="button plain icon-down">
                    {{ $pageData->getName() }}
                    <x-icon name="ui/down"></x-icon>
                </x-link>
            </x-slot>
            {{-- The underlying page itself (as the trigger simply opens the menu) --}}
            <x-link :href="$pageData->getUrl()" :active="$highlightActive && $pageData->isActive()" variation="button block">
                {{ $pageData->getName() }}
            </x-link>
            {{-- The child pages --}}
            @foreach($pageData->getChildren() as $pageDataChild)
            <x-link :href="$pageData->getUrl()" :active="$highlightActive && $highlightActiveChildren && $pageDataChild->isActive()" variation="button block">
                {{ $pageDataChild->getName() }}
            </x-link>
            @endforeach()
        </x-dropdown-menu>
    @else
        {{-- Direct page link --}}
        <x-link :href="$pageData->getUrl()" :active="$highlightActive && $pageData->isActive()" variation="button subtle">
            {{ $pageData->getName() }}
        </x-link>
    @endif
@endif