{{-- Layout / Environment Banner: banner message for the current environment --}}
@props([
    'theme' => 'warning',
])
@php
    $environment = app()->environment();
    $tag = config('project.features.tag');

    $message = $environment !== 'production' ? "This is a preview site. You are currently viewing the <strong>{$tag}</strong> version of this site." : null;
@endphp
@if ($message)
    <x-banner theme="{{ $theme }}" :container="true" :closable="false" key="environment-banner">
        {!! $message !!}
    </x-banner>
@endif