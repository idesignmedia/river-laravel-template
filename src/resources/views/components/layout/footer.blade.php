{{-- Layout / Footer: Render the website footer --}}
@props([
    'as' => 'footer',
    'variation' => null,
])
@php
    // Apply the 'layout-footer' component class
    $attributes = \Attr::applyComponent(class: 'layout-footer', variations: $variation, attributes: $attributes);

    // Define
    // Note, we have multiple approaches to footer navigation...
    $pageHelper = config('river.pages.helper_class');

    // NAVIGATION 1 - using Navigation Groups
    $footerNavigation = \App\Models\NavigationGroup::getNavigationGroup('footer');
    $footerPages = $footerNavigation->getNavigationPageData();
    // $models = $footerNavigation->getModels();
    // dd($models);
    // $footerPages = $footerNavigation->getPageLinks();

    // NAVIGATION 2 - config based menu
    // This grabs pages based on the config (eg, where data.menu or data.more is true), often used for the main menu
    // $footerPages = $pageHelper::configMenu(['data.footer' => true]);

@endphp
<x-box :as="$as" {{ $attributes }} id="footer">
    <x-container class="layout-footer__container">
        <x-grid columns="1 md:2">

            {{-- Footer Logo / Info --}}
            <x-box class="layout-footer__info">
                <x-logo variation="footer" class="layout-header__logo" mx="auto" mb="xs" />
                <x-clear />
                <x-text size="xs" mb="none" display="flex" alignItems="center" gap="tiny">
                    &copy; {{ config('project.owner.name') }} {{ now()->format('Y') }}
                    <x-inline mx="subtle md:tiny">|</x-inline>
                    <x-text>Site by <x-logo-river :reverse="false" ml="tiny" /></x-text>
                </x-text>
            </x-box>

            {{-- Footer Menu --}}
            <x-stack class="layout-footer__nav" gap="xs" alignItems="center sm:start md:end">
                <x-social-links gap="sm" />
                <x-row class="layout-footer__menu" wrap="wrap" gapX="sm" gapY="xs" justifyContent="center md:end" >
                    {{-- Footer Menu Pages --}}
                    @if(count($footerPages))
                        @foreach ($footerPages as $pageData)
                            <x-link href="{{ $pageData->getUrl() }}" active="{{ $pageData->isActive() }}" variation="subtle">{{ $pageData->getName() }}</x-link>
                        @endforeach()
                    @endif
                    {{-- Demo Button --}}
                    @if (config('project.features.demo_pages'))
                        <x-button href="{{ url('demo') }}" variation="small">Demo</x-button>
                    @endif
                </x-row>
            </x-stack>

        </x-grid>
    </x-container>
</x-box>