{{-- Mobile Menu: Render the full-page website mobile menu --}}
@php
    // Define mobile menu
    // Powered by Alipe JS using 'mobileMenu' property on
    $class="mobile-menu";

    // Define
    // Note, we multiple approaches to navigation...
    $pageHelper = config('river.pages.helper_class');
    $moreName = 'More';
    $moreDescription = null;

    // NAVIGATION 1 - using Navigation Groups
    // This grabs pages dynamically based on the editable "NavigationGroups" from the DMS
    $mainNavigation = \App\Models\NavigationGroup::getNavigationGroup('main');
    $mainPages = $mainNavigation->getNavigationPageData();
    $moreNavigation = \App\Models\NavigationGroup::getNavigationGroup('more');
    $moreDescription = $moreNavigation->getContent();
    $morePages = $moreNavigation->getNavigationPageData();

    // NAVIGATION 2 - config based menu
    // This grabs pages based on the config (eg, where data.menu or data.more is true), often used for the main menu
    // $mainPages = $pageHelper::configMenu(['data.menu' => true]);
    // $morePages = $pageHelper::configMenu(['data.more' => true]);
@endphp
<div class="{{ $class }}" x-bind:class="{ 'is-open': $store.mobileMenu.open }" x-on:keydown.escape.window="$store.mobileMenu.open = false">
    <x-flex class="mobile-menu__body" direction="col" alignItems="center" padding="normal">

        {{-- Close Icon --}}
        <x-icon class="mobile-menu__close" x-on:click="$store.mobileMenu.open = false" name="ui/close" />

        {{-- Logo --}}
        <x-logo variation="mobile" class="layout-header__logo" mb="sm"></x-logo>

        {{-- Menu Card --}}
        <x-box class="mobile-menu__card" mt="lg">

            {{-- Menu Pages --}}
            <x-stack as="menu" class="mobile-menu__menu" gap="sm md:normal" alignItems="center">
                @foreach($mainPages as $pageData)
                    <x-link :href="$pageData->getUrl()" :active="$pageData->isActive()" variation="button subtle">{{ $pageData->getName() }}</x-link>
                @endforeach()
            </x-stack>

            {{-- More Pages --}}
            <x-stack as="menu" class="mobile-menu__more" gap="sm md:normal" alignItems="center">
                @foreach($morePages as $pageData)
                    <x-link :href="$pageData->getUrl()" :active="$pageData->isActive()" variation="button subtle">{{ $pageData->getName() }}</x-link>
                @endforeach()
            </x-stack>

            {{-- Auth Links --}}
            @if(config('project.features.auth.enabled'))
                <x-clear />
                <x-stack as="menu" class="mobile-menu__auth" gap="sm md:normal" alignItems="center">
                    @if(Auth::guard(config('auth.defaults.guard'))->check())
                        <x-button variation="default" :href="route('app.dashboard')">Dashboard</x-button>
                    @else
                        <x-button variation="default" :href="route('login')">Login</x-button>
                    @endif
                </x-stack>
            @endif

        </x-box>

        {{-- Social --}}
        <x-social-links mt="lg" />

    </x-flex>
</div>