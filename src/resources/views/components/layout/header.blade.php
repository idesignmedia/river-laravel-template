{{-- Layout / Header: Render the website header --}}
@props([
    'as' => 'header',
    'floating' => false,
    'variation' => null,
])
@php
    // Apply the 'layout-header' component class
    // Note, you can set 'floating' to true for the homepage only using: request()->is('/')
    $attributes = \Attr::applyComponent(class: 'layout-header', variations: [$variation, 'floating' => $floating], attributes: $attributes);

    // Apply fixed header; PENDING fix
    // Powered by @river-common/scripts/fixedHeader.js
    // $attributes = $attributes->class('js-fixed-header');

    // Define
    // Note, we have multiple approaches to navigation...
    $pageHelper = config('river.pages.helper_class');
    $moreName = 'More';
    $moreDescription = null;

    // NAVIGATION 1 - using Navigation Groups
    // This grabs pages dynamically based on the editable "NavigationGroups" from the DMS
    $mainNavigation = \App\Models\NavigationGroup::getNavigationGroup('main');
    $mainPages = $mainNavigation->getNavigationPageData();

    $moreNavigation = \App\Models\NavigationGroup::getNavigationGroup('more');
    $moreDescription = $moreNavigation->getContent();
    $morePages = $moreNavigation->getNavigationPageData();

    // NAVIGATION 2 - config based menu
    // This grabs pages based on the config (eg, where data.menu or data.more is true), often used for the main menu
    // $mainPages = $pageHelper::configMenu(['data.menu' => true]);
    // $morePages = $pageHelper::configMenu(['data.more' => true]);

@endphp
<x-box :as="$as" {{ $attributes }} id="header" :scope="['reverse' => $floating]">
    <x-container class="layout-header__container" display="flex" direction="row" items="center">

        {{-- Logo --}}
        <x-logo variation="header" :reverse="$floating" class="layout-header__logo"></x-logo>

        {{-- Header Menu --}}
        <x-box class="layout-header__menu" ml="auto" display="none! sm:block!">
            <x-row wrap="wrap" gap="normal" alignItems="center">

                {{-- Main Menu Pages --}}
                @if(count($mainPages))
                    @foreach ($mainPages as $pageData)
                        <x-menu-pagedata :pageData="$pageData" :includeChildren="true" :highlightActive="true" :highlightActiveChildren="false" />
                    @endforeach()
                @endif

                {{-- More Pages --}}
                @if (count($morePages))
                    <x-dropdown-menu variation="right inline" scope="reverse-cancel">
                        <x-slot name="trigger">
                            <x-link variation="button plain icon-down">
                                {{ $moreName }} <x-icon name="ui/down"></x-icon>
                            </x-link>
                        </x-slot>
                        @if($moreDescription)
                            <x-box text="xs" px="sm" style="max-width: 30rem;">
                                {!! $moreDescription !!}
                            </x-box>
                        @endif
                        @foreach ($morePages as $pageData)
                            <x-link :href="$pageData->url" :active="$pageData->isActive()" variation="button block">
                                {{ $pageData->name }}
                            </x-link>
                        @endforeach()
                    </x-dropdown-menu>
                @endif

                {{-- Auth Links --}}
                @if(config('project.features.auth.enabled'))
                    <x-row wrap="wrap" gap="sm">
                        @if(Auth::guard(config('auth.defaults.guard'))->check())
                            <x-button variation="default" :href="route('app.dashboard')">Dashboard</x-button>
                        @else
                            <x-button variation="default" :href="route('login')">Login</x-button>
                        @endif
                    </x-row>
                @endif

            </x-row>
        </x-box>

        {{-- Mobile Menu Toggle --}}
        <x-box class="layout-header__menu" ml="auto" display="block! sm:none!">
            <x-layout.mobile-menu-toggle />
        </x-box>

    </x-container>
</x-box>