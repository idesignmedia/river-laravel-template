{{-- Mobile Menu Toggle: Toggled open the mobile menu, powered by Alpine JS --}}
@props([
    'variation' => null, // null, false, or specific animation style
])
@php
    // Apply the animation class
    // Powered by styles/components/mobile-menu-toggle.scss
    $class = 'mobile-menu-toggle';
    $animations = ['offset']; //['offset', 'inverse', 'plus', 'up', 'arrow'];
    if ($variation === null) {
        $variation = $animations[array_rand($animations)];
    }
    if ($variation) {
        $class = "{$class} {$class}--{$variation}";
    }
@endphp
<x-box as="a"
    {{ $attributes->class($class) }}
    {{-- We register mobileMenu Alpine states in main.js --}}
    x-bind:class="{ 'is-active': $store.mobileMenu.open }"
    x-on:click="$store.mobileMenu.toggle()"
><span></span><span></span><span></span></x-box>