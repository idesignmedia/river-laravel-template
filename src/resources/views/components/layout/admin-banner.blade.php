{{-- Layout / Admin Banner: banner with messages for the admin around content versions --}}
@props([
    'theme' => 'warning',
    'entity' => null, // CMS entity to check
])
@php
    // CMS Messages for Admin (if entity is a UCM model)
    $messages = [];
    if ($entity) {
        $admin = request()->user(config('river.users.guards.admin'));
        // Admin CMS Messages (if valid admin, and entity is a UCM model)
        if ($admin?->isAdmin() && $entity instanceof \River\DMS\Contracts\UniversalContent) {
            // Publication Status (status)
            if (! $entity->isPublic()) {
                $messages[] = "You are previewing a <strong>{$entity->getStatusLabel()}</strong> {$entity->getSingularLabel()}, which is not currently visible to the public.";
            }
            // Publication Date (published_at)
            if ($entity->published_at && $entity->published_at->startOfDay()->gt(today())) {
                $messages[] = "This {$entity->getSingularLabel()} is scheduled to be public on <strong>{$entity->published_at->toDateString()}</strong> (via the Published Date).";
            }
            // Content Versions
            if ($entity->contentVersionsEnabled() && $entity->getVersionType()) {
                $noun = $entity->isMasterVersion() ? 'previewing' : 'viewing';
                $message = "You are {$noun} the " . \River\DMS\Enums\ContentVersion::getVersionLabel($entity->getVersionType()) . " of the {$entity->getSingularLabel()}.";
                if ($entity->isMasterVersion()) {
                    $message .= " <a href='{$entity->getPublishedUrl()}'>View Published Version</a>";
                }
                if ($entity->isPublishedVersion()) {
                    $message .= " <a href='{$entity->getPublishedUrl()}?preview'>Preview Editable Version</a>";
                    // Note, we don't use $entity->getPublishedUrl(['preview' => 1]), as we don't want the "=1" visible in the url
                }
                $messages[] = $message;
            }
        }
    }
@endphp
@if(! empty($messages))
    <x-banner :theme="$theme" :messages="$messages" :container="true" :closable="true" key="admin-banner" />
@endif