{{-- Form Message: A simple form message --}}
@props([
    'success' => null,
    'message' => null,
    'successTitle' => 'Success',
    'errorTitle' => 'Error',
    // Remote Message settings
    'remote' => false,
    'remoteSuccess' => 'success', // Alpine JS variables for success status
    'remoteMessage' => 'message', // Alpine JS variable for the message
])
@if($remote)
    <template x-if="typeof {{ $remoteSuccess }} !== 'undefined' && {{ $remoteSuccess }} !== null">
        <x-box
            {{ $attributes }}
            class="form-message style-notification"
            {{-- Remote Data via Alpine JS --}}
            x-bind:class="{
                'style-notification--success': typeof {{ $remoteSuccess }} !== 'undefined' && {{ $remoteSuccess }},
                'style-notification--error': typeof {{ $remoteSuccess }} !== 'undefined' && ! {{ $remoteSuccess }}
                }"
            >
            <x-title size="6" mb="none" x-text="typeof {{ $remoteSuccess }} !== 'undefined' && {{ $remoteSuccess }} ? '{{ $successTitle }}' : '{{ $errorTitle }}'"/>
            <x-text x-text="typeof {{ $remoteMessage }} !== 'undefined' && {{ $remoteMessage }} ? {{ $remoteMessage }} : null"/>
        </x-box>
    </template>
@elseif($message)
    <x-box
        {{ $attributes }}
        class="form-message style-notification {{ 'style-notification--' . ( $success ? 'success' : 'error' ) }}"
        >
        @if($message && is_string($message))
            <x-title size="6" mb="none">{{ $success ? $successTitle : $errorTitle }}</x-title>
            <x-text>{{ $message }}</x-text>
        @endif
{{-- TODO: implement $slot as a plain message
        @if(! empty( (string)$slot) )
            {{ $slot }}
        @endif
--}}
    </x-box>
@endif