{{-- Form Submit: A simple form submit button --}}
@props([
'text' => 'Submit',
'icon' => 'ui/right',
'variation' => 'default icon-right'
])

<x-button {{ $attributes }} type="submit" variation="{{ $variation }}">
    {{ $text }}
    @if($icon)
    <x-icon :name="$icon" />
    @endif
</x-button>
