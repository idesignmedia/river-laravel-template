{{-- Form Control: A control for a form, that includes a label, input, and message --}}
{{-- Slots: label, icon, button, description, options --}}
@props([
    // Control settings
    'icon' => null, // src file of an icon if being used; eg, 'ui/down'
    'button' => null, // src file of a button icon if being used; eg, 'ui/search'
    'variation' => null,

    // Message settings
    'description' => null,
    'success' => null,
    'error' => null,

    // Input settings
    'name' => null,
    'id' => null, // defaults to $name
    'type' => 'text', // 'text', 'select', 'checkbox', 'radio', 'textarea', ...
    'required' => false,
    'value' => null,
    'placeholder' => "",
    'label' => null,
    'options' => collect(), // select, checkbox, radio
    'checked' => false, // checkbox, radio
])
@php
// Defaults
$id = ( $name && $id === null ? $name : $id );

// Input Type
switch($type) {
case 'select':
$icon = $icon ?? 'ui/down';
break;
}

// Variations
if ($icon) {
$variation = $variation ? [$variation, 'icon'] : 'icon';
}
if ($button) {
$variation = $variation ? [$variation, 'button'] : 'button';
}

// Apply the 'form-control' component class
$attributes = \Attr::applyComponent(
class: 'form-control',
variations: [$variation, $type],
attributes: $attributes,
);
@endphp
{{-- Form Control --}}
<x-box {{ $attributes }}>

    {{-- Label --}}
    @if($label instanceof \Illuminate\Support\HtmlString)
    {{ $label }} {{-- slot --}}
    @elseif($label)
    <x-box as="label" class="form-control__label" :for="$id">
        <x-box as="span">{{ $label }}</x-box>
        @if($required)
        <x-box as="span" color="error.normal">*</x-box>
        @endif
    </x-box>
    @endif

    {{-- Body --}}
    <div class="form-control__body">

        {{-- Input --}}
        <x-form.input {{-- Class --}} :attributes="null" class="form-control__input" :name="$name" :id="$id" :label="$label" :type="$type" :value="$value" :placeholder="$placeholder" :options="$options" :checked="$checked" />

        {{-- Input Icon --}}
        @if($icon instanceof \Illuminate\Support\HtmlString)
        {{ $icon }} {{-- slot --}}
        @elseif($icon)
        <x-icon name="{{ $icon }}" />
        @endif

        {{-- Input Button --}}
        @if($button instanceof \Illuminate\Support\HtmlString)
        {{ $button }} {{-- slot --}}
        @elseif($button)
        <button type="submit">
            <x-icon name="{{ $button }}" />
        </button>
        @endif

    </div>

    {{-- Input Description (checkbox) --}}
    @if($description instanceof \Illuminate\Support\HtmlString && $type == 'checkbox')
    {{ $description }} {{-- slot --}}
    @elseif($description && $type == 'checkbox')
    <x-text class="form-control__description">{{ $description }}</x-text>
    @endif

    {{-- Input Description (default) --}}
    @if($description instanceof \Illuminate\Support\HtmlString && $type != 'checkbox')
    {{ $description }} {{-- slot --}}
    @elseif($description && $type != 'checkbox')
    <x-text class="form-control__description">{{ $description }}</x-text>
    @endif

    {{-- Messages (Success, Error) --}}
    <x-text class="form-control__success" x-text="(typeof successes !== 'undefined' ? successes.{{ $name }} : '{{ $success }}')"></x-text>
    <x-text class="form-control__error" x-text="(typeof errors !== 'undefined' ? errors.{{ $name }} : '{{ $error }}')"></x-text>

</x-box>
