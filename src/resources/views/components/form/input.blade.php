{{-- Form Input: An input for a form --}}
@props([
    // Input settings
    'attributes' => null,
    'class' => null, // eg, 'form-control__input'
    'name' => null,
    'id' => null, // defaults to $name
    'type' => 'text', // 'text', 'select', 'checkbox', 'radio', 'textarea', ...
    'value' => null,
    'placeholder' => null, // defaults to "{$label}..."
    'label' => null,
    'options' => null, // select, checkboxes, radio; slot, or array of ['key' => 'label'] pairs
    'checked' => null, // checkbox
])
@php
    // Defaults
    $id = ( $name && $id === null ? $name : $id );
    $placeholder = ( $label && $placeholder === null ? "{$label}..." : $placeholder );
    $class = 'form-input' . ( $class ? " {$class}" : '' );

    // Normalise Options
    if(in_array($type, ['select', 'checkboxes', 'radio'])) {
        $options = $options ?? [];
    }
@endphp
@switch($type)
    {{-- Textarea --}}
    @case('textarea')
        <x-box
            as="textarea"
            :attributes="$attributes"
            :name="$name"
            :id="$id"
            :type="$type"
            :placeholder="$placeholder"
            :class="$class"
        >{{ $value }}</x-box>
        @break
    {{-- Select --}}
    @case('select')
        <x-box
            as="select"
            :attributes="$attributes"
            :name="$name"
            :id="$id"
            :class="$class"
        >
            @if($options instanceof \Illuminate\Support\HtmlString)
                {{ $options }} {{-- slot --}}
            @else
                @foreach($options ?? [] as $optionKey => $optionLabel)
                    <x-box
                        as="option"
                        :value="$optionKey"
                        :data-index="$loop->index"
                        :selected="$value == $optionKey">
                        {{ $optionLabel }}
                    </x-box>
                @endforeach
            @endif
        </x-box>
        @break
    {{-- Checkbox --}}
    @case('checkbox')
        <x-box
            as="input"
            :attributes="$attributes"
            :name="$name"
            :id="$id"
            type="checkbox"
            :value="$value ?? 1"
            :checked="(bool)$checked"
            :class="$class" />
        @break
    {{-- Checkboxes (Multiple) --}}
    @case('checkboxes')
        @if($options instanceof \Illuminate\Support\HtmlString)
            {{ $options }} {{-- slot --}}
        @else
            @foreach($options ?? [] as $optionKey => $optionLabel)
                <x-box
                    as="input"
                    :name="$optionKey"
                    :id="( $id ?? 'checkbox' ) . '-' . $optionKey"
                    type="checkbox"
                    value="1"
                    :checked="(bool)($value == $optionKey)"
                    :class="$class" />
                <x-box as="label" :for="( $id ?? 'checkbox' ) . '-' . $optionKey">{{ $optionLabel }}</x-box>
            @endforeach
        @endif
        @break
    {{-- Radio Buttons --}}
    @case('radio') 
        @if($options instanceof \Illuminate\Support\HtmlString)
            {{ $options }} {{-- slot --}}
        @else
            @foreach($options ?? [] as $optionKey => $optionLabel)
                <x-box
                    as="input"
                    :name="$name"
                    :id="( $id ?? 'radio' ) . '-' . $optionKey"
                    type="radio"
                    :value="$optionKey"
                    :checked="(bool)($value == $optionKey)"
                    :class="$class" />
                <x-box as="label" :for="( $id ?? 'radio' ) . '-' . $optionKey">{{ $optionLabel }}</x-box>
            @endforeach
        @endif
        @break
    {{-- Text / Default --}}
    @case('text')
    @default
        <x-box
            as="input"
            :attributes="$attributes"
            :name="$name"
            :id="$id"
            :type="$type"
            :value="$value"
            :placeholder="$placeholder"
            :class="$class"
        ></x-box>
        @break
@endswitch