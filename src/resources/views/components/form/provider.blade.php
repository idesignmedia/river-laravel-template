{{-- Form: A default form implementation --}}
@props([
'method' => 'POST',
'action' => null,
'remote' => false,
])
@if($remote)
<x-box {{ $attributes }} as="form" :method="$method" :action="$action" class="form-container" {{-- Remote Data via Alpine JS --}} x-data="{isLoading: false, message: null, successes: {}, errors: {}}" x-bind:class="{ 'is-loading': isLoading }" x-on:submit.prevent="" {{-- override with your local Alpine method --}}>
    @csrf
    {{ $slot }}
</x-box>
@else
<x-box {{ $attributes }} as="form" :method="$method" :action="$action" class="form-container">
    @csrf
    {{ $slot }}
</x-box>
@endif
