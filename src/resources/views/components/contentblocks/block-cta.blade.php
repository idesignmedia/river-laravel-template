{{-- RIVER Template: Website --}}
{{-- Content Blocks: Project CTA --}}
@props([
    'block' => null,
])
@php
    // Render a custom CTA for this Project (example Content Block)
    if (! $block) {
        throw new \DomainException('Required $block was not passed into the component view.');
    }
    $blockName = $block->getBlockName(); // 'block-cta'
    $key = $block->getKey(); // random string

    // Content
    $title = $block->title;
    $content = $block->content;
    $imageSrc = $block->getImageUrl();

    // Links
    $links = $block->getLinks();
    $link1 = $links->count() > 0 ? $links->get(0) : null;
    $link2 = $links->count() > 1 ? $links->get(1) : null;

    // Styling
    $attributes = $attributes->merge([
        'class' => $imageSrc ? 'block-cta--image' : null,
        'width' => ! $imageSrc ? '12/12 md:10/12' : null,
    ]);

    // dd('Project CTA block:', $title, $content, $linkText, $linkUrl, $linkTarget);
@endphp
@if($block->isValid())
    <x-box {{ $attributes }} class="content-block {{ $blockName }}">
        @if($imageSrc)
            <x-image :src="$imageSrc" :alt="$title"/>
        @endif
        <x-stack gap="xs" class="block-cta__body">
            <x-contentblocks.partial.title :blockName="$blockName" :title="$title" mb="none" />
            <x-contentblocks.partial.content :blockName="$blockName" :content="$content" />
            @if($links->count())
                <x-row wrap="wrap" gap="normal" mt="sm" class="block-cta__actions">
                    @if($link1)
                        <x-button variation="primary {{ $link1->isExternal() ? 'icon-external' : 'icon-right' }}"
                            :href="$link1->url" :target="$link1->getLinkTarget()"
                            :icon="$link1->isExternal() ? 'ui/external' : 'ui/right'"
                            >{{ $link1->text }}</x-button>
                    @endif
                    @if($link2)
                        <x-button variation="outline {{ $link2->isExternal() ? 'icon-external' : 'icon-right' }}"
                            :href="$link2->url" :target="$link2->getLinkTarget()"
                            :icon="$link2->isExternal() ? 'ui/external' : 'ui/right'"
                            >{{ $link2->text }}</x-button>
                    @endif
                </x-row>
            @endif
        </x-stack>
    </x-box>
@endif