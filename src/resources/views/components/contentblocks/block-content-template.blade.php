{{-- RIVER Template: Website --}}
{{-- Content Blocks: Content Template --}}
@props([
    'block' => null,

    // Defaults
    'gap' => 'lg',
])
@php
    // Render the Content Blocks for the given Content Template
    if (! $block) {
        throw new \DomainException('Required $block was not passed into the component view.');
    }
    $blockName = $block->getBlockName(); // 'block-cta'
    $key = $block->getKey(); // random string

    // Model
    $model = $block->getModel(); // ContentTemplate
    $blockshtml = $model && $model->isPublic()
        ? $model->getContentBlocksHtml()
        : null;

    // dd('Content Template block:', $model, $blockshtml);
@endphp
@if($blockshtml)
    <x-stack {{ $attributes }} class="ucm-content__contentblocks cms-blocks" gap="{{ $gap }}">
        {!! $blockshtml !!}
    </x-stack>
@endif