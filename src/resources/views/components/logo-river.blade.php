{{-- Logo RIVER: The vendor logo and link --}}
@props([
    'display' => 'inline-block',
    'href' => 'https://weareriver.nz/',
    'reverse' => false,
])
@php
    $src = 'river/river-logo' . ( $reverse ? '-reverse' : '' ) . '.svg';
@endphp
<x-link class="river-logo-link" variation="plain" external {{ $attributes->merge(['display' => $display, 'href' => $href]) }}
    ><x-image display="block" src="{{ $src }}" style="height: 0.9em; transform: translateY(2px);"  /></x-link>