{{-- Social: Social Links --}}
@props([
    'size' => '1.6rem',
    'gap' => 'normal'
])
@php
    $links = config('project.owner.social', []);
    // eg, ['facebook' => 'https://www.facebook.com/', ...]
@endphp
@if(! empty($links))
    <x-row class="social-links" {{ $attributes }} :gap="$gap">
        @foreach($links as $key => $href)
            <x-link class="social-links__link" variation="plain" external :href="$href"
                ><x-icon name="social/{{ $key }}" :size="$size" /></x-link>
        @endforeach
    </x-row>
@endif