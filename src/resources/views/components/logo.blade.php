{{-- Logo: Project logo with optional link --}}
@props([
    'href' => '/', // false
    'reverse' => false,
    'variation' => null,
])
@php
    $src = 'company/logo' . ( $reverse ? '-reverse' : '' ) . '.svg';
    $class = 'style-logo' . ( $variation ? " style-logo--{$variation}" : '' );
    // Style managed in project/project.scss
@endphp
@if($href)
    <x-link class="web-logo" variation="plain"
        {{ $attributes->merge(['href' => $href]) }}
        ><x-image class="{{ $class }}" src="{{ $src }}" /></x-link>
@else
    <x-box class="web-logo"
        {{ $attributes }}
        ><x-image class="{{ $class }}" src="{{ $src }}" /></x-box>
@endif