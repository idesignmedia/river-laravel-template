{{-- Banner: A customisable banner --}}
@props([
    'theme' => 'text', // 'text' (gray), 'success' (green), 'info' (blue), 'warning' (yellow), 'error' (red)
    'messages' => [], // array of messages to display (instead of or before our content slot)
    'container' => true, // whether to wrap the content in a container
    'closable' => false, // whether this banner can be closed (dismissed)
    'key' => 'banner', // key used for the banner cookie
    'variation' => null,
])
@php
    // Apply the 'display-banner' component class
    $attributes = \Attr::applyComponent(
        class: 'display-banner',
        variations: $variation,
        attributes: $attributes,
    );
    $attributes = $attributes->class([
        'style-notification',
        "style-notification--{$theme}",
    ]);
    $containerClass = "display-banner__container" . ( $container ? ' layout-container' : '' );

    // Note the current path for our banner cookie (managed via js below)
    $displayPath = request()->path() ?? null;
@endphp
{{-- Banner output --}}
@if(! empty($messages) || ! empty((string)$slot))
<x-box {{ $attributes }} x-data="{ isOpen: getCookie('{{ $key }}') !== '{{ $displayPath }}' }" x-bind:class="{ 'is-open': isOpen, 'is-closed': ! isOpen }">

    {{-- Container --}}
    <div class="{{ $containerClass }}">

        {{-- Close Button --}}
        @if($closable)
        <x-icon class="display-banner__close" x-on:click="isOpen = false; setCookie('{{ $key }}', '{{ $displayPath }}')" name="ui/close" />
        @endif

        {{-- Messages --}}
        @if(! empty($messages))
        <x-box as="ul">
            @foreach($messages as $message)
            <li>{!! $message !!}</li>
            @endforeach
        </x-box>
        @endif

        {{-- Content Slot --}}
        {{ $slot }}

    </div>
</x-box>
@endif
{{-- Manage Close --}}
@once
<script>
    // When we close the banner, note this page so that we don't show the banner again until we navigate away
    function getCookie(name) {
        var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
        return v ? v[2] : null;
    }

    function setCookie(name, value, days = 1) {
        var d = new Date;
        d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * days);
        document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
    }

    function deleteCookie(name) {
        setCookie(name, '', -1);
    }
</script>
@endonce
@if($closable && $key && $displayPath)
<script>
    // Clear the banner cookie if we navigate to a new page
    if (getCookie('{{ $key }}') !== '{{ $displayPath }}') {
        deleteCookie('{{ $key }}');
    }
</script>
@endif
