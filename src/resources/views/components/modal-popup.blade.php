{{-- Modal Poup: a simple full-screen modal popup powered by Alpine JS --}}
{{-- Note, $attributes are passed straight to the .modal-popup__body --}}
@props([
'open' => 'Open Modal', // false, string, or slot
'close' => null, // null, false, or slot
'variation' => null, // null, 'inline'
'inline' => false, // alias for $variation = 'inline'
'clickoutEnabled' => true, // whether the modal can be closed by clicking outside
'escapeEnabled' => true, // whether the modal can be closed by pressing escape
])
@php
$class = 'modal-popup';
if ($variation === null && $inline) {
$variation = 'inline';
}
if ($variation) {
$class = "{$class} {$class}--{$variation}";
}
@endphp

{{-- Modal Wrapper --}}
<div class="modal-wrapper" x-data="{
        isOpen: false,
        clickoutEnabled: {{ $clickoutEnabled ? 'true' : 'false' }},
        escapeEnabled: {{ $escapeEnabled ? 'true' : 'false' }},
    }">

    {{-- Open Modal --}}
    @if($open && is_string($open))
    <x-button x-on:click="isOpen = $store.modalPopups.openModal()">{{ $open }}</x-button>
    @elseif($open)
    {{{ $open }}}
    @endif

    {{-- Modal Container --}}
    <div class="{{ $class }}" x-bind:class="{ 'is-open': isOpen }" x-show="isOpen" x-on:keydown.escape.window.prevent="if (escapeEnabled) { isOpen = $store.modalPopups.closeModal() }">

        {{-- Modal Popup --}}
        <x-block {{ $attributes }} class="modal-popup__body" x-on:click.away="if (clickoutEnabled) { isOpen = $store.modalPopups.closeModal() }">

            {{-- Close --}}
            @if($close === null)
            <x-icon class="modal-popup__close" x-on:click="isOpen = $store.modalPopups.closeModal()" name="ui/close" />
            @elseif($close)
            {{ $close }}
            @endif

            {{-- Content --}}
            {{ $slot }}

        </x-block>
    </div>

</div>
