{{-- RIVER Template (Webiste) --}}
{{-- Universal Content: Resources Filter Results --}}
@props([
    'model' => null,
    'results' => null,
    'hasCategories' => false,
])
@php
    // Based off river-laravel-dms\resources\components\index\filter-results.blade.php
    // Accounts for whether any Resource Categories have been passed in

    // Define
    $model = $model ?? null;
    $results = $results ?? null;

    // Manage filter string
    $filterResults = null;
    if ($results && $model) {
        $pluralLabel = $model::pluralLabel();
        $keyword = request()->get('keyword');

        // Filtering
        $filtering = [];
        if ($keyword) {
            $filtering[] = "the search '<strong>{$keyword}</strong>'";
        }
        if ($hasCategories) {
            $filtering[] = "any of the selected categories";
        }
        if($filtering) {
            $filterResults = "Showing all {$pluralLabel} for " . join(' and ', $filtering);
        } else {
            $filterResults = "Showing all {$pluralLabel}";
        }

        // Count and paging
        if($filterResults) {
            // Result Counts
            $total = $results instanceof \Illuminate\Pagination\LengthAwarePaginator
                ? $results->total()
                : $results->count();
            $pageCurrent = $results instanceof \Illuminate\Pagination\LengthAwarePaginator
                ? $results->currentPage()
                : null;
            $pageLast = $results instanceof \Illuminate\Pagination\LengthAwarePaginator
                ? $results->lastPage()
                : null;
            $filterResultLabel = ( $total == 1 ? 'result' : 'results' );
            // Filter String
            $filterResults .= " ({$total} {$filterResultLabel}";
            $filterResults .= $pageCurrent && $pageLast
                ? ", page {$pageCurrent} of {$pageLast}"
                : '';
            $filterResults .= ')';
        }
    }
@endphp
@if($filterResults)
    <x-caption {{ $attributes }}>{!! $filterResults !!}</x-caption>
@endif