{{-- RIVER Template (Webiste) --}}
{{-- Universal Content: Resource Card; based off river-laravel-dms\resources\components\card.blade.php --}}
@props([
    // NOTE, we need to ensure we pass the full model in, to use model specific methods
    'entity' => null,
])
@if($entity)
    <x-box as="a" href="{{ $entity->getPublishedUrl() }}" class="group ucm-card resource-card">
        {{-- Image --}}
        <div class="ucm-card__image">
            @if($entity->hasThumbnail())
                <x-image
                    childClass="display-cover display-fit"
                    src="{{ $entity->getThumbnail() }}"
                    alt="{{ $entity->getName() }}"
                />
            @endif
        </div>
        {{-- Body --}}
        <x-box class="ucm-card__body" pt="sm">
            <x-label class="ucm-card__caption" class="overflow-ellipsis" mb="tiny">{{ $entity->getResourceCategoriesLabel() }}</x-label>
            <x-title class="ucm-card__title" size="5" mb="xs">{{ $entity->getName() }}</x-title>
            <x-text class="ucm-card__summary">{{ $entity->getShortSummary() }}</x-text>
            <x-text class="ucm-card__more" mt="xs" color="type.title"><x-link as="div" variation="button current no-border">View Resource</x-link></x-text>
        </x-box>
    </x-box>
@endif