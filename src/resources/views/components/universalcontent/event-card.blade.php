{{-- RIVER Template (Webiste) --}}
{{-- Universal Content: Event Card; based off update-featured-card.blade.php --}}
@props([
    // NOTE, we need to ensure we pass the full model in, to use model specific methods
    'entity' => null,
])
@if($entity)
    <x-box class="event-card">
        <x-columns small>
            <x-column width="5/12 md:4/12">
                <x-box as="a" href="{{ $entity->getPublishedUrl() }}" class="group ucm-card">
                    {{-- Body --}}
                    <x-box class="ucm-card__body" my="auto">
                        <x-label class="ucm-card__caption" class="overflow-ellipsis" mb="tiny">{{ $entity->getCategory()->isValid() ? (string)$entity->getCategory() : '-' }}</x-subtitle>
                        <x-title class="ucm-card__title" size="5" mb="tiny">{{ $entity->getName() }}</x-title>
                        <x-adjunct mb="sm">{{ $entity->getDisplayDate() }}, {{ $entity->getDisplayRegion() }}</x-adjunct>
                        <x-text class="ucm-card__summary">{{ $entity->getSummary() }}</x-text>
                        <x-text class="ucm-card__more" mt="xs" color="type.title"><x-link as="div" variation="button current no-border">View Event</x-link></x-text>
                    </x-box>
                </x-box>
            </x-column>
            <x-column width="7/12 md:7/12" offset="0 md:1/12">
                {{-- Image --}}
                <div class="ucm-card__image">
                    @if($entity->hasThumbnail())
                        <x-image
                            childClass="display-cover display-fit"
                            src="{{ $entity->getThumbnail() }}"
                            alt=""
                        />
                    @endif
                </div>
            </x-column>
        </x-columns>
    </x-box>
@endif