{{-- RIVER Template (Website) --}}
{{-- Universal Content: Card (default) --}}
@props([
    // The Universal Content entity, or CardData, that this card represents
    // NOTE, if a CardData has been passed from the cache, entity functions will not be available
    'entity' => null,

    // An optional class to add to the card (alongside 'group ucm-card')
    'class' => null,

    // Defaults
    'titleSize' => 5,//config('river.universalcontent.defaults.card-title-size'), // 6; set to false to use text instead
])
@php
    // Render a generic Universal Content card
    // RIVER Template - ensure a default caption, and add a "Read More" button added to bottom of card
    $attributes = $attributes->class([
        'group',
        'ucm-card',
        $class,
        'has-image' => $entity->hasThumbnail(),
    ]);

    // Caption
    $caption = $entity->getCaption() ?? '-';

    // Note, consider adding "overflow-ellipsis" class to captions/names you don't want to overflow
@endphp
@if($entity)
    <x-box as="a" href="{{ $entity->getPublishedUrl() }}" {{ $attributes }}>
        {{-- Image --}}
        <div class="ucm-card__image">
            @if($entity->hasThumbnail())
                <x-image
                    childClass="display-cover display-fit"
                    src="{{ $entity->getThumbnail() }}"
                    alt="{{ $entity->getName() }}"
                />
            @endif
        </div>
        {{-- Body --}}
        <x-box class="ucm-card__body" pt="sm">
            @if($caption)
                <x-label class="ucm-card__caption" mb="tiny">{{ $caption }}</x-label>
            @endif
            @if($titleSize)
                <x-title :size="$titleSize" class="ucm-card__title" mb="xs">{{ $entity->getName() }}</x-title>
            @else
                <x-text class="ucm-card__title" mb="xs">{{ $entity->getName() }}</x-text>
            @endif
            <x-text class="ucm-card__summary">{{ $entity->getSummary() }}</x-text>
            {{-- Read More --}}
            <x-text mt="xs" color="type.title"><x-link as="div" variation="button current no-border">View {{ $entity->getDisplayType() }}</x-link></x-text>
        </x-box>
    </x-box>
@endif