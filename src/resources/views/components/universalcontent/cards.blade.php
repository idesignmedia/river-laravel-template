{{-- RIVER Template (Website) --}}
{{-- Universal Content: Cards (default) --}}
@props([
    'model' => null,
    'entities' => null,
    'emptyMessage' => null, // html slot, or null for the default "No results found..."

    // Component
    'cardComponent' => 'universalcontent.card',

    // Defaults
    'gridSize' => config('river.universalcontent.defaults.cards-grid-size'), // '1 sm:2 md:3 lg:3'
    'gridGap' => config('river.universalcontent.defaults.cards-grid-gap'), // 'normal'
])
@php
    // Render a collection of Universal Content cards
@endphp
@if($entities && $entities->count())
    {{-- Cards --}}
    <x-grid {{ $attributes }} :columns="$gridSize" :gap="$gridGap" class="ucm-cards">
        @foreach($entities as $entity)
            {{-- <x-universalcontent.card :entity="$entity" /> --}}
            <x-dynamic-component :component="$cardComponent" :entity="$entity" />
        @endforeach
    </x-grid>
@else
    {{-- No Results --}}
    @if($emptyMessage)
        {!! $emptyMessage !!}
    @elseif($emptyMessage === null)
        <x-caption {{ $attributes }}>No results found...</x-caption>
    @endif
@endif