[river-laravel-dms] Universal Content
--

Top-level component views for Universal Content.
These are generally shared components, such as cards and content-type details.

Universal Content views are loaded from three sources by default, in this order:
- src/resources/views/components/contentblocks (PROJECT, aka this folder)
- src/resources/views/vendor/contentblocks (VENDOR)
- [river-laravel-dms]/resources/contentblocks/components (PACKAGE)

This is defined in the [river-laravel-dms] UniversalContentServiceProvider.php class.

When you add Universal Content for this project (Updates, Resources, etc), you need to:
- Create a Migration, Model, Resource and HTPP Controller for the associated content type
- Create an index and show file inside "/resources/views/pages/universalcontent/...", which the HTPP Controller references
- Have the index and show file either point to their defaults, or customise them using individual components
