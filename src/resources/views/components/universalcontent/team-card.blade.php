{{-- RIVER Template (Webiste) --}}
{{-- Universal Content: Team Card; based off river-laravel-dms\resources\components\card.blade.php --}}
@props([
    // NOTE, we need to ensure we pass the full model in, to use model specific methods
    'entity' => null,
])
@if($entity)
    <x-box as="a" href="{{ $entity->getPublishedUrl() }}" class="group ucm-card team-card">
        {{-- Image --}}
        <div class="ucm-card__image">
            @if($entity->hasThumbnail())
                <x-image
                    childClass="display-cover display-fit"
                    src="{{ $entity->getThumbnail() }}"
                    alt="{{ $entity->getName() }}"
                />
            @endif
        </div>
        {{-- Body --}}
        <x-box class="ucm-card__body" pt="sm">
            <x-title class="ucm-card__caption" size="5" mb="tiny">{{ $entity->getName() }}</x-title>
            <x-label class="ucm-card__title" mb="xs" class="overflow-ellipsis">{{ $entity->getPosition() ?? '-' }}</x-label>
            <x-text class="ucm-card__summary">{{ $entity->getShortSummary() }}</x-text>
            <x-text class="ucm-card__more" mt="xs" color="type.title"><x-link as="div" variation="button current no-border">View Bio</x-link></x-text>
        </x-box>
    </x-box>
@endif