@props([
    'media' => null,
    'placeholder' => null,
    'lazy' => true,
    'conversion' => null,
    'color' => 'var(--colors-gray-light)',
])

@php
if (!is_a($media, Spatie\MediaLibrary\MediaCollections\Models\Media::class)) {
    throw new Exception('Media prop must be an instance of ' . Spatie\MediaLibrary\MediaCollections\Models\Media::class);
}

$url = $conversion ? $media->getUrl($conversion) : $media->getUrl();

$placeholder = $placeholder ?? ($media->getCustomProperty('blur') ?? '');

$width = $media->getCustomProperty('width');
$height = $media->getCustomProperty('height');
$altText = $media->getCustomProperty('alt_text') ?? $media->name;

if ($lazy) {
    $attributes = $attributes->class('lozad');
    $attributes = $attributes->merge([
        'data-src' => $url,
        'data-placeholder-color' => $color,
        'src' => $placeholder,
    ]);
} else {
    $attributes = $attributes->merge([
        'src' => $url,
        'style' => "background-color: $color;",
    ]);
}
@endphp

<img
    {{ $attributes }}
    class="style-image"
    width="{{ $width }}px"
    height="{{ $height }}px"
    alt="{{ $altText }}"
/>
