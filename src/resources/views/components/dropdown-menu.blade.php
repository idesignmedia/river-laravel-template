{{-- Dropdown Menu: A simple dropdown menu powered by Alpine JS --}}
@props([
    'variation' => null, // null, 'right' (fix submenu to the right)
])
@php
    // Inspiration: https://alpinejs.dev/component/dropdown

    // Apply the 'dropdown-menu' component class
    $attributes = \Attr::applyComponent(
        class: 'dropdown-menu',
        variations: $variation,
        variationAttributes: ['right'],
        attributes: $attributes,
    );
@endphp
<x-box
    {{ $attributes }}
    x-data="{ isOpen: false }"
    >
    {{-- Trigger --}}
    <div
        class="dropdown-menu__trigger"
        x-bind:class="{ 'is-active': isOpen }"
        x-on:click.stop.prevent="if (! isOpen) { $dispatch('menu-button-open') }; $nextTick(() => { isOpen = ! isOpen })"
        x-on:menu-button-open.window="isOpen = false"
        >{{ $trigger ?? 'More' }}</div>
    {{-- Drodown --}}
    <div
        class="dropdown-menu__submenu"
        x-bind:class="{ 'is-open': isOpen }"
        x-on:keydown.escape.window="isOpen = false"
        x-on:click.outside="isOpen = false"
        {{-- x-on:resize.window="isOpen = false" --}}
        >
{{ $slot }}
    </div>
</x-box>