<x-page title="Verify" bg="primary.bg" view="layouts.page-headless">
    <x-section>
        <x-container>

            <x-box class="auth-form">
                <x-title size="4">{{ __('Verify Your Email Address') }}</x-title>

                @if(Session::has('success'))
                    <x-notification variation="success" my="xs">
                        {{ Session::get('success') }}
                    </x-notification>
                @endif
                @if (Session::has('resent'))
                    <x-notification variation="success" my="xs">
                        {{ __('A fresh verification link has been sent to your email address.') }}
                    </x-notification>
                @endif

                <x-text size="sm" mt="tiny">
                    {{ __('Before proceeding, please check your email for a verification link.') }}
                </x-text>

                <form method="POST" action="{{ route('verification.resend') }}">
                    @csrf
                    <x-button type="submit" variation="default" size="small">Request a new link</x-button>
                </form>
            </x-box>

        </x-container>
    </x-section>
</x-page>