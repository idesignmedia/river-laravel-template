<x-page title="Login" class="auth-page" bg="primary.bg" view="layouts.page-headless">

    {{-- Auth Background --}}
{{-- 
    <x-box class="auth-cover">
        <x-image src="/assets/images/hero/login.jpg" />
        <x-box class="style-cover" />
    </x-box>
 --}}

    {{-- Login Form --}}
    <x-section>
        <x-container>

            <x-box class="auth-form">
                <x-logo variation="auth" mb="lg" />

                {{-- <x-title size="4">{{ __('Login') }}</x-title> --}}
                <x-title size="4" mb="sm">Account Login</x-title>

                @if($notification = Session::get('notification'))
                    <x-notification :variation="$notification['state']" my="xs">
                        {{ $notification['message'] }}
                    </x-notification>
                @endif

                <x-form.provider action="{{ route('login') }}">
                    <x-form.control
                        label="Email Address"
                        placeholder="Email..."
                        name="email"
                        type="email"
                        value="{{ old('email') }}"
                        error="{{ $errors->first('email') }}"
                        autofocus
                        autocomplete="email"
                    />

                    <x-form.control
                        label="Password"
                        placeholder="Password..."
                        name="password"
                        type="password"
                        error="{{ $errors->first('password') }}"
                    />

                    <x-button type="submit" variation="default" width="full">
                        {{ __('Login') }}
                    </x-button>
                </x-form.provider>

                <x-stack mt="lg" gap="xs">
                    @if(config('project.features.auth.password_resets'))
                        <x-text size="sm">
                            Forgot your password?
                            <x-link variation="hover-border" href="{{ route('password.request') }}">Request a reset</x-link>
                        </x-text>
                    @endif       
                    @if(config('project.features.auth.registration'))
                        <x-text size="sm">
                            Don't have an account?
                            <x-link variation="hover-border" :href="url('/register')">Register</x-link>
                        </x-text>
                    @endif
{{--                <x-text size="sm">
                        Are you an admin?
                        <x-link variation="hover-border" :href="url(config('nova.path') . '/login')">Admin Log In</x-link>
                    </x-text>
--}}
                </x-stack>
            </x-box>

        </x-container>
    </x-section>
    
</x-page>