<x-page title="Reset Passowrd" bg="primary.bg" view="layouts.page-headless">
    <x-section>
        <x-container>

            <x-box class="auth-form">
                <x-title as="h2" size="4">Reset Password</x-title>

                <x-form.provider action="{{ route('password.update') }}" mb="normal">
                    <input type="hidden" name="token" value="{{ $token }}">

                        <x-form.control
                            label="Email"
                            name="email"
                            type="email"
                            value="{{ old('email') }}"
                            error="{{ $errors->first('email') }}"
                        />

                        <x-form.control
                            label="Password"
                            name="password"
                            type="password"
                            error="{{ $errors->first('password') }}"
                        />

                        <x-form.control
                            label="Confirm Password"
                            name="password_confirmation"
                            type="password"
                            error="{{ $errors->first('password_confirmation') }}"
                        />

                        <x-button type="submit" variation="default" width="full">
                            {{ __('Reset Password') }}
                        </x-button>
                </x-form.provider>
            </x-box>

        </x-container>
    </x-section>
</x-page>
