<x-page title="Request Passowrd Reset" bg="primary.bg" view="layouts.page-headless">
    <x-section>
        <x-container>

            <x-box class="auth-form">
                <x-title size="4">Request Password Reset</x-title>
                <x-text size="sm" mt="tiny">Enter your email below and we'll send you an email to reset your password.</x-text>

                @if($status = Session::get('status'))
                    <x-notification variation="success" my="xs">
                        {{ $status }}
                    </x-notification>
                @endif

                <x-form.provider mt="sm" action="{{ route('password.email') }}">
                    <x-form.control
                        label="Email"
                        name="email"
                        type="email"
                        value="{{ old('email') }}"
                        error="{{ $errors->first('email') }}"
                    />

                    <x-button type="submit" variation="default" width="full">
                        Send password reset link
                    </x-button>
                </x-form.provider>

                <x-stack mt="lg" gap="sm">
                    <x-text size="sm">
                        Remember your password?
                        <x-link variation="hover-border" :href="url('/login')">Log In</x-link>
                    </x-text>
                </x-stack>
            </x-box>

        </x-container>
    </x-section>
</x-page>
