<x-page title="Register" class="auth-page" bg="primary.bg" view="layouts.page-headless">
    <x-section>
        <x-container>

            <x-box class="auth-form">
                <x-title as="h2" size="4">{{ __('Register') }}</x-title>
                <x-text size="sm" mt="tiny">Enter your details below to create an account.</x-text>

                <x-form.provider mt="sm" action="{{ route('register') }}">
                    <x-form.control
                        label="Name"
                        placeholder="Your Name..."
                        name="name"
                        type="text"
                        value="{{ old('name') }}"
                        error="{{ $errors->first('name') }}"
                        autofocus
                    />

                    <x-form.control
                        label="Email"
                        placeholder="Your Email..."
                        name="email"
                        type="email"
                        value="{{ old('email') }}"
                        error="{{ $errors->first('email') }}"
                    />

                    <x-form.control
                        label="Password"
                        placeholder="Your Password..."
                        name="password"
                        type="password"
                        error="{{ $errors->first('password') }}"
                    />

                    <x-button type="submit" variation="default" width="full">
                        Register
                    </x-button>
                </x-form.provider>

                <x-stack mt="lg" gap="sm">
                    @if(isset($termsLink))
                        <x-text size="sm">
                            By registering you agree to our
                            <x-link variation="hover-border" external :href="$termsLink">Terms</x-link>.
                        </x-text>
                    @endif
                    <x-text size="sm">
                        Already have an account?
                        <x-link variation="hover-border" :href="url('/login')">Log In</x-link>
                    </x-text>
                </x-stack>
            </x-box>

        </x-container>
    </x-section>
</x-page>