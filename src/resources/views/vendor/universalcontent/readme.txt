Universal Content readme
------------------------

Add content universal content component views here to have them load instead of the default universal content component views.

For example, add 'cards.blade.php', to customise how the default cards are rendered.

You can find the the full list of Universal Content component view files in the Vendor src folder:
- src\vendor\river\river-laravel-universal-content
- src\vendor\river\river-laravel-universal-content\resources\components
