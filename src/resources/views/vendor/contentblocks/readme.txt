[river-laravel-dms] Content Blocks
--

Top-level component views for content blocks.

Content block views are loaded from three sources by default, in this order:
- src/resources/views/components/contentblocks (PROJECT)
- src/resources/views/vendor/contentblocks (VENDOR, aka this folder)
- [river-laravel-dms]/resources/contentblocks/components (PACKAGE)

This is defined in the [river-laravel-dms] ContentBlocksServiceProvider.php class.

If you use custom Content Blocks for this project, you will want to place views in the PROJECT folder using the same filename as the $name attribute:
- eg, 'src/resources/views/components/contentblocks/block-customprojectblock.blade.php'.

You can override package views by placing local component views in either the PROJECT or VENDOR folder.
The PROJECT folder is useful if you want to utilise custom components alongside overriding package components.
The VENDOR folder is useful if you want to clearly demarcate exactly what views you are overriding.
