@if (config('project.features.tag') !== 'production')
<tr>
    <td class="banner">
        <strong>Test Email:</strong> This message was sent from the {{ config('project.features.tag', 'staging') }}
        environment.
    </td>
</tr>
@endif
<tr>
    <td class="header">
        <a href="{{ $url }}" style="display: inline-block;">
            <img src="{{ asset('assets/images/company/logo.png') }}" class="logo" alt="{{ config('app.name') }} Logo">
        </a>
    </td>
</tr>
