{{-- RIVER Template (Website) --}}
{{-- Contact page --}}
<x-page :page="$page">

    {{-- Hero --}}
    <x-hero
        title="{{ $page->getContentTitle() }}"
        subtitle="{{ $page->getSummary() }}"
        img="{{ $page->hasHero() ? $page->getHero() : 'hero/contact.jpg' }}"
        fallbackColor="{{ $page->getFallbackColor() }}"
        />

    {{-- Body --}}
    <x-container>
        <x-section small>

            {{-- Content & Related --}}
            <x-universalcontent.layout.page-content-and-related-column :entity="$page">

                {{-- Custom Content: Contact Form --}}
                <x-title size="3">Let's Talk</x-title>
                <x-paragraph>Fill in your details below to contact us.</x-paragraph>
                <x-block py="sm" px="normal" border="normal">
                    @include('forms.contact')
                </x-block>

            </x-universalcontent.layout.page-content-and-related-column>

            {{-- End: Custom Content --}}
            @if ($page->getContentBlocksHtml() || $page->hasMedia('additional_images'))
                <x-divide />
            @endif

            {{-- Content Blocks --}}
            <x-universalcontent.content.contentblocks :entity="$page" />

            {{-- Additional Images --}}
            <x-universalcontent.content.additional-images :entity="$page" />

            {{-- Footer --}}
            {{-- <x-universalcontent.layout.page-footer-return :entity="$page" /> --}}

        </x-section>
    </x-container>

    <x-space size="xl" />
    
</x-page>