{{-- RIVER Template (Website) --}}
{{-- Home page --}}
<x-page :page="$page">

    {{-- Hero --}}
    @if(! ($page->data_json['promo_enabled'] ?? false))
        <x-hero variation="large"
            img="{{ $page->getHero() ?? 'hero/home.jpg' }}"
            fallbackColor="{{ $page->getFallbackColor() }}"
            >

            {{-- Hero Content --}}
            <x-animate initial type="slidein-up">
                <x-title size="2" fontSize="title-3 md:title-2" mb="tiny">{{ config('app.name') }}</x-title>
                <x-text size="xl" mb="lg">{{ $page->getSummary() }}</x-text>
            </x-animate>

            <x-animate initial type="slidein-down" delay="400">
                <x-row wrap="wrap" gap="normal">
                    <x-button href="#intro" scope="reverse" variation="icon-down" icon="ui/down">Learn More</x-button>
                    <x-button href="{{ url('about') }}" scope="reverse" variation="outline icon-right" icon="ui/right">About Us</x-button>
                </x-row>
            </x-animate>

            {{-- Hero Slot: More Arrow --}}
            <x-slot name="more">
                <x-animate initial type="slidein-down" delay="800">
                    <a href="#intro">
                        <x-icon name="ui/down" />
                    </a>
                </x-animate>
            </x-slot>

        </x-hero>
    @endif

    {{-- Hero - Promo --}}
    @if($page->data_json['promo_enabled'] ?? false)
        <x-hero variation="large"
            img="{{ $page->hasMedia('promo') ? $page->getFirstMedia('promo')?->getUrl('medium') : 'hero/home.jpg' }}"
            fallbackColor="{{ $page->getFallbackColor() }}"
            >

            {{-- Hero Content --}}
            <x-animate initial type="slidein-up">
                @if($page->data_json['promo_text'] ?? null)
                    <x-title size="2" fontSize="title-3 md:title-2" mb="tiny">{{ $page->data_json['promo_text'] }}</x-title>
                @endif
                @if($page->data_json['promo_text2'] ?? null)
                    <x-text size="xl" mb="lg">{{ $page->data_json['promo_text2'] }}</x-text>
                @endif
            </x-animate>

            <x-animate initial type="slidein-down" delay="400">
                <x-row wrap="wrap" gap="normal">
                    @if(($page->data_json['promo_button_url'] ?? null) && ($page->data_json['promo_button_name'] ?? null))
                        <x-button href="{{ $page->data_json['promo_button_url'] }}" scope="reverse"
                            :external="substr($page->data_json['promo_button_url'], 0, 1) !== '/'">{{ $page->data_json['promo_button_name'] }}</x-button>
                    @endif
                    @if(($page->data_json['promo_button2_url'] ?? null) && ($page->data_json['promo_button2_name'] ?? null))
                        <x-button href="{{ $page->data_json['promo_button2_url'] }}" scope="reverse" variation="outline"
                            :external="substr($page->data_json['promo_button2_url'], 0, 1) !== '/'">{{ $page->data_json['promo_button2_name'] }}</x-button>
                    @endif
                </x-row>
            </x-animate>

            {{-- Hero Slot: More Arrow --}}
            <x-slot name="more">
                <x-animate initial type="slidein-down" delay="800">
                    <a href="#intro">
                        <x-icon name="ui/down" />
                    </a>
                </x-animate>
            </x-slot>
            
        </x-hero>
    @endif

    {{-- Body --}}
    <x-container>

        {{-- Introduction --}}
        <a id="intro" name="intro"></a>
        <x-section>
            <x-grid columns="1 md:2" width="12/12 xl:10/12" mx="auto">
                <x-grid-cell display="flex" direction="col" justify="center">
                    {{-- Animation for content --}}
                    <x-animate scroll reverse type="slidein-up">

                        {{-- Title --}}
                        <x-title size="2">Overview</x-title>

                        {{-- Intro --}}
                        @if($page->hasContentIntroduction())
                            <x-text size="xl" mb="sm">{{ $page->getContentIntroduction() }}</x-text>
                        @endif

                        {{-- Content --}}
                        <x-paragraph>
                            This is our default template for all Laravel projects. This template incorporates our core Laravel packages, along with our design system and opinionated default setup.
                        </x-paragraph>

                        {{-- Demo Link --}}
                        @if(config('project.features.demo_pages'))
                            <x-row mt="sm">
                                <x-button href="{{ url('demo') }}" variation="outline icon-right" icon="ui/right">Demo</x-button>
                            </x-row>
                        @endif

                    </x-animate>
                </x-grid-cell>
                <x-grid-cell>
                    <x-image src="content/sample.jpg" alt="RIVER"></x-image>
                </x-grid-cell>
            </x-grid>
        </x-section>

        {{-- Featured Posts --}}
        @if(count($featuredPosts = $page->getRelatedPostsCardData()))
            <x-section>
                <x-title size="2" mb="tiny">Featured Posts</x-title>
                <x-caption mb="lg">Shows posts that have been attached to the Homepage in Nova.</x-caption>
                <x-grid columns="2 md:4">
@foreach($featuredPosts as $post)
                    <x-grid-cell span="2" start="{{ count($featuredPosts) == 1 ? '1 md:2' : '' }}">
                        <x-universalcontent.card :entity="$post" />
                    </x-grid-cell>
@endforeach
                </x-grid>
            </x-section>
        @endif

        {{-- Latest Updates --}}
        <x-section>
            <x-title size="2" mb="tiny">Latest Updates</x-title>
            <x-caption mb="lg">Shows the latest three Updates using the "universal-entities" component.</x-caption>
            <x-universal-entities :model="\App\Models\ContentUpdate::class" :limit="3" />
        </x-section>

        {{-- Recent Posts --}}
        <x-section>
            <x-title size="2" mb="tiny">Recent Posts</x-title>
            <x-caption mb="lg">Shows the latest of each Update, Resource, and Event (in a shuffled order).</x-caption>
            <x-content.recent-posts />
        </x-section>

    </x-container>

    <x-space size="20rem" />

</x-page>