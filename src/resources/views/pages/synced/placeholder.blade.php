{{-- RIVER Template (Website) --}}
{{-- Placeholder (synced page example) --}}
<x-page :page="$page">

    {{-- Hero --}}
    <x-hero
        title="{{ $page->getContentTitle() }}"
        subtitle="{{ $page->getSummary() }}"
        img="{{ $page->hasHero() ? $page->getHero() : null }}"
        fallbackColor="{{ $page->getFallbackColor() }}"
        />

    {{-- Body --}}
    <x-container>
        <x-section small>

            {{--  Video Embed, Content & Related --}}
            <x-universalcontent.layout.page-content-and-related-column :entity="$page" />

            {{-- Content Blocks --}}
            <x-universalcontent.content.contentblocks :entity="$page" />

            {{-- Additional Images --}}
            <x-universalcontent.content.additional-images :entity="$page" />

            {{-- Footer --}}
            {{-- <x-universalcontent.layout.page-footer-return :entity="$page" /> --}}

        </x-section>
    </x-container>

    <x-space size="xl" />
    
</x-page>