{{-- RIVER Template (Website) --}}
{{-- About page --}}
<x-page :page="$page">

    {{-- Hero --}}
    <x-hero
        title="{{ $page->getContentTitle() }}"
        subtitle="{{ $page->getSummary() }}"
        img="{{ $page->hasHero() ? $page->getHero() : 'hero/about.jpg' }}"
        fallbackColor="{{ $page->getFallbackColor() }}"
        />

    {{-- Body --}}
    <x-container>
        <x-section small>

            {{-- Content & Related --}}
            <x-universalcontent.layout.page-content-and-related-column :entity="$page">

                {{-- Custom Content --}}
                @php
                    $teamPage = config('river.pages.model')::where('slug', 'team')->masterVersion()->first();
                @endphp
                <x-title size="3" my="normal">Meet our team</x-title>
                <x-text mb="normal">Meet the team behind the journey!</x-text>
                <x-button href="{{ $teamPage?->getPublishedUrl() }}" variation="default">Meet The Team</x-button>

            </x-universalcontent.layout.page-content-and-related-column>

            {{-- End: Custom Content --}}
            @if ($page->getContentBlocksHtml() || $page->hasMedia('additional_images'))
                <x-divide />
            @endif

            {{-- Content Blocks --}}
            <x-universalcontent.content.contentblocks :entity="$page" />

            {{-- Additional Images --}}
            <x-universalcontent.content.additional-images :entity="$page" />

            {{-- Footer --}}
            {{-- <x-universalcontent.layout.page-footer-return :entity="$page" /> --}}

        </x-section>
    </x-container>

    <x-space size="xl" />
    
</x-page>