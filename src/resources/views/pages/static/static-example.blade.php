{{-- Example Static Page --}}
{{-- Static pages are loaded via \App\Http\Controllers\FallbackController --}}
{{-- You can access this example page via '/static-example' --}}

<x-page title="Static Page Example">
    <x-container>
        <x-title size="2">Static Page</x-title>
        <x-paragraph>
            This is an example of a Static Page,
            accessed via <x-link href="{{ url('static-example') }}">/static-example</x-link>...
        </x-paragraph>

        <x-text>
            <x-link href="{{ url('/') }}">Return Home</x-link>
        </x-text>
    </x-container>
</x-page>
