{{-- RIVER Template (Website) --}}
{{-- Universal Content / Content Templates: Show page --}}
<x-page :page="$entity">

    {{-- Body --}}
    <x-container>

        {{-- Combined Hero Text + Hero Image, Video Embed, Content & Related --}}
        <x-title size="2" mt="lg">Content Template: {{ $entity->getName() }}</x-title>
        <x-divide />

        {{-- Content Blocks --}}
        <x-universalcontent.content.contentblocks :entity="$entity" />

    </x-container>

</x-page>