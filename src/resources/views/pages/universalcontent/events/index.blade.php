{{-- RIVER Template (Website) --}}
{{-- Universal Content / Events: Index page --}}
<x-page :page="$page ?? $model">

    {{-- Hero --}}
    <x-hero variation="{{ $page->hasHero() ? '' : 'small' }}"
        title="{{ $model::indexLabel() }}"
        subtitle="{{ $page->getSummary() }}"
        img="{{ $page->hasHero() ? $page->getHero() : null }}" />

    {{-- Body --}}
    <x-container>

        {{-- Filter Tools --}}
        <x-universalcontent.index.tools py="sm" :model="$model" />
        <x-divide my="none" />

        {{-- Event Cards; river-laravel-dms\resources\components\cards.blade.php --}}
        @if($model::repository()->getEntitiesCount())
            <x-section small pt="normal">
                <x-universalcontent.index.filter-results :model="$model" :results="$entities" mb="normal" />

                @if($entities && $entities->count())
                    <x-stack gap="normal: 2xl" class="ucm-cards event-cards">
                        @foreach($entities as $entity)
                            <x-universalcontent.event-card  :entity="$entity" />
                            <x-divide my="none" />
                        @endforeach
                    </x-stack>
                @else
                    <x-caption>No results found...</x-caption>
                @endif

                <x-universalcontent.index.pagination :results="$entities" mt="normal md:2xl" />
            </x-section>
        @else
            <x-text emphasis my="xl">
                We are currently updating our website, and will be adding new {{ $model::pluralLabel() }} shortly.
            </x-text>
        @endif

    </x-container>
</x-page>