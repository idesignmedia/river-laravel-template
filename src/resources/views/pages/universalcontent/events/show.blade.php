{{-- RIVER Template (Website) --}}
{{-- Universal Content / Events: Show page --}}
<x-page :page="$entity">

    <x-divide color="{{ $entity->getFallbackColor() }}" my="none" />

    {{-- Body --}}
    <x-container>

        {{-- Hero Text & Category --}}
        <x-universalcontent.layout.post-hero-category py="normal" :entity="$entity" />

        {{-- Hero Image, Video Embed, Content & Related --}}
        <x-universalcontent.layout.grid-content-and-related-column :entity="$entity">

            {{-- Custom Content --}}
            @if($entity->hasRegistrationLink())
                <x-box my="normal">
                    <x-button href="{{ $entity->getRegistrationLink() }}" variation="default" external>{{ $entity->getRegistrationLabel() }}</x-button>
                </x-box>
            @endif

        </x-universalcontent.layout.grid-content-and-related-column>

        {{-- Content Blocks --}}
        <x-universalcontent.content.contentblocks :entity="$entity" />

        {{-- Additional Images --}}
        <x-universalcontent.content.additional-images :entity="$entity" />

        {{-- Event Address --}}
        @if($entity->showAddress() && $entity->hasStreetAddress())
            <x-box my="normal">
                <x-divide mt="none" mb="normal" />

                <x-title size="5">Address</x-title>
                <x-link href="https://maps.google.com/maps?q={{ $entity->getGoogleMapAddress() }}" variation="plain" external>{{ $entity->getFormattedAddress() }}</x-link>

                <x-box class="event-map" mt="sm">
                    <iframe title="Event Map" width="100%" height="600"
                    src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q={{ $entity->getGoogleMapAddress() }}&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                </x-box>

            </x-box>
        @endif

        {{-- Footer --}}
        <x-universalcontent.layout.post-footer-return :entity="$entity" />

    </x-container>

    <x-space size="xl" />

</x-page>