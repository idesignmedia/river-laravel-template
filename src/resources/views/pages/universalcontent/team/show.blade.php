{{-- RIVER Template (Website) --}}
{{-- Universal Content / Team: Show page --}}
<x-page :page="$entity">

    <x-divide color="{{ $entity->getFallbackColor() }}" my="none" />

    {{-- Body --}}
    <x-container>

        {{-- Hero Text & Category; inspired by x-universalcontent.layout.post-hero-category  --}}
        <x-box py="normal">

            {{-- Hero Title --}}
            <x-title size="2" mb="tiny">{{ $entity->getName() }}</x-title>

            {{-- Post Details --}}
            <x-subtitle>
                {{-- About Page --}}
                @php
                    $aboutPage = config('river.pages.model')::where('slug', 'about')->masterVersion()->first();
                @endphp
                @if($aboutPage)
                    <x-link href="{{ $aboutPage->getPublishedUrl() }}" variation="plain">{{ $aboutPage->getName() }}</x-link> /
                @endif
                {{-- Content Type --}}
                @if($entity->indexLabel())
                    <x-link href="{{ url($entity->getroutePrefix()) }}" variation="plain">{{ $entity->indexLabel() }}</x-link>
                @endif
                {{-- Category; link directly to the relevant Team Section via #category --}}
                @if($entity->getCategory()->isValid())
                    @if($entity->indexLabel())/@endif
                    <x-link
                        href="{{ url($entity->getroutePrefix() . '#' . $entity->getCategory()->getKey()) }}"
                        variation="plain">{{ (string)$entity->getCategory() }}</x-link
                        >@if($entity->getPublishedDate()->isValid()),@endif
                @endif
            </x-subtitle>

        </x-box>

        {{-- Hero Image, Video Embed, Content & Related --}}
        <x-universalcontent.layout.grid-content-and-related-column :entity="$entity" />

        {{-- Content Blocks --}}
        <x-universalcontent.content.contentblocks :entity="$entity" />

        {{-- Additional Images --}}
        <x-universalcontent.content.additional-images :entity="$entity" />

        {{-- Footer --}}
        <x-universalcontent.layout.post-footer-return :entity="$entity" />

    </x-container>

    <x-space size="xl" />

</x-page>