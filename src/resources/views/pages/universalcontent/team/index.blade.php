{{-- RIVER Template (Website) --}}
{{-- About/Team Index page; Synced page, we manually show entities underneath CMS content --}}
<x-page :page="$page">

    {{-- Hero --}}
    <x-hero variation="{{ $page->hasHero() ? '' : 'small' }}"
        title="{{ $model::indexLabel() }}"
        subtitle="{{ $page->getSummary() }}"
        img="{{ $page->hasHero() ? $page->getHero() : null }}"
        fallbackColor="{{ $page->getFallbackColor() }}"
        />

    {{-- Body --}}
    <x-container>
        <x-section small>

            {{-- Team Categories; river-laravel-dms\resources\components\index\tools.blade.php --}}
            @if(count($categories))
                <x-row color="gray.dark" wrap="wrap" gapX="sm md:lg" gapY="sm" alignItems="center" mb="normal">
                    {{-- Categories; river-laravel-dms\resources\components\index\tools-categories.blade.php --}}
                    @foreach($categories as $category)
                        @continue(! $category->isValid())
                        <div><x-link
                            href="#{{ $category->getKey() }}"
                            variation="button subtle"
                            >{{ $category->getLabel() }}</x-link></div>
                    @endforeach
                </x-row>
            @endif

            {{-- Content & Related --}}
            <x-universalcontent.layout.page-content-and-related-column :entity="$page" />
            <x-divide />

            {{-- Cards, grouped by Category --}}
            @if($model::repository()->getEntitiesCount())
                @foreach($categories as $category)
                    @continue(! $category->isValid())
                    <x-box mt="normal" mb="xl">
                        <a id="{{ $category->getKey() }}" name="{{ $category->getKey() }}"></a>
                        <x-link href="#{{ $category->getKey() }}" variation="plain">
                            <x-title size="4" mb="sm" >{{ $category->getLabel() }}</x-title>
                        </x-link>
                        <x-grid columns="1 sm:2 md:3 lg:4" gapX="normal" gapY="normal md:lg" class="ucm-cards update-cards">
                            @foreach($entities->where('category', $category->getKey()) as $entity)
                                <x-universalcontent.team-card  :entity="$entity" />
                            @endforeach
                        </x-grid>
                    </x-box>
                    @if(! $loop->last)
                        {{-- <x-divide my="none" /> --}}
                    @endif
                @endforeach
            @else 
                <x-text emphasis my="xl">
                    We are currently updating our website, and will be adding our Team Members shortly.
                </x-text>
            @endif

            {{-- End Team --}}
            @if ($page->getContentBlocksHtml() || $page->hasMedia('additional_images'))
                <x-divide />
            @endif

            {{-- Content Blocks --}}
            <x-universalcontent.content.contentblocks :entity="$page" />

            {{-- Additional Images --}}
            <x-universalcontent.content.additional-images :entity="$page" />

            {{-- Footer --}}
            <x-universalcontent.layout.page-footer-return :entity="$page" />

        </x-section>
    </x-container>

    <x-space size="xl" />
    
</x-page>