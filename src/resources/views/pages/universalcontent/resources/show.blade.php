{{-- RIVER Template (Website) --}}
{{-- Universal Content / Resources: Show page --}}
<x-page :page="$entity">

    <x-divide color="{{ $entity->getFallbackColor() }}" my="none" />

    {{-- Body --}}
    <x-container>

        {{-- Hero Text & Category; inspired by x-universalcontent.layout.post-hero-category  --}}
        <x-box py="normal">

            {{-- Hero Title --}}
            <x-title size="2" mb="tiny">{{ $entity->getName() }}</x-title>

            {{-- Post Details --}}
            <x-subtitle>
                {{-- Content Type --}}
                @if($entity->indexLabel())
                    <x-link href="{{ url($entity->getroutePrefix()) }}" variation="plain">{{ $entity->indexLabel() }}</x-link
                        >@if($entity->hasResourceCategories()): @endif
                @endif
                {{-- Resource Categories --}}
                @if($entity->hasResourceCategories())
                    @foreach($entity->getResourceCategories() as $key => $label)
                        <span>{{ $label }}</span>@if(! $loop->last), @endif
                    @endforeach
                @endif
            </x-subtitle>

        </x-box>

        {{-- Hero Image, Video Embed, Content & Related --}}
        <x-universalcontent.layout.grid-content-and-related-column :entity="$entity" />

        {{-- Content Blocks --}}
        <x-universalcontent.content.contentblocks :entity="$entity" />

        {{-- Additional Images --}}
        <x-universalcontent.content.additional-images :entity="$entity" />

        {{-- Footer --}}
        <x-universalcontent.layout.post-footer-return :entity="$entity" />

    </x-container>

    <x-space size="xl" />

</x-page>