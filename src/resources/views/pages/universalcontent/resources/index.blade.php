{{-- RIVER Template (Website) --}}
{{-- Universal Content / Resources: Index page --}}
<x-page :page="$page ?? $model">

    {{-- Hero --}}
    <x-hero variation="{{ $page->hasHero() ? '' : 'small' }}"
        title="{{ $model::indexLabel() }}"
        subtitle="{{ $page->getSummary() }}"
        img="{{ $page->hasHero() ? $page->getHero() : null }}"
        fallbackColor="{{ $page->getFallbackColor() }}"
        />

    {{-- Body --}}
    <x-container>

        {{-- Filters & Resources --}}
        <x-columns variation="small" mb="normal md:2xl" py="normal">

            {{-- Filters --}}
            <x-column width="12/12 md:4/12 lg:3/12">
                <x-title size="5" mb="sm">Filter Resources</x-title>
                <x-divide />

                {{-- Filter Tools (Search & Categories); river-laravel-dms\resources\components\index\tools.blade.php --}}
                {{-- <x-universalcontent.index.tools py="sm" :model="$model" /> --}}
                @php
                    $categories = \App\Models\ContentResource::resourceCategories();
                    $requestKeyword = request()->get('keyword');
                @endphp

                <x-form.form class="resource-filters-form" method="GET" :csrf="false" mb="normal">
                    <x-grid columns="1 sm:2 md:1" gap="normal md:lg">

                        {{-- Filter Tools: Search; river-laravel-dms\resources\components\index\tools-search.blade.php --}}
                        <x-box class="form-control form-control--button" mb="none">
                            <x-subtitle as="label" for="form-search" display="block" mb="2xs">Search</x-subtitle>
                            <x-form.control name="keyword" id="form-search" placeholder="Search..." value="{{ request()->get('keyword') }}" button="ui/search" type="text" />
                        </x-box>

                        {{-- Filter Tools: Categories; river-laravel-dms\resources\components\index\tools-categories.blade.php --}}
                        @foreach($categories as $groupLabel => $group)
                            <x-box>
                                <x-subtitle mb="none">{{ $groupLabel }}</x-subtitle>
                                <x-divide my="xs" />
                                <x-stack gap="xs">
                                @foreach($group as $key => $label)
                                    <x-form.control name="{{ $key }}" label="{{ $label }}" type="checkbox" checked="{{ request()->has($key) ? true : false }}" value="" />
                                @endforeach
                                </x-stack>
                            </x-box>
                        @endforeach

                        {{-- Filter Tools: Submit --}}
                        <x-grid columns="1 sm:2 md:1" gap="sm">
                            <div><x-form.submit text="Apply Filters" variation="primary small" :icon="false"/></div>
                            <div><x-button href="{{ url(\App\Models\ContentResource::routePrefix()) }}" variation="default small">Clear Filters</x-button></div>
                        </x-grid>

                    </x-grid>
                </x-form.form>

            </x-column>

            {{-- Resource Cards --}}
            <x-column width="12/12 md:8/12 lg:9/12">
                <x-title size="5" mb="sm">Browse through Resources</x-title>
                <x-divide />

                {{-- Results --}}
                @if($model::repository()->getEntitiesCount())

                    {{-- Filter Results --}}
                    <x-universalcontent.resource-filter-results :model="$model" :results="$entities" :hasCategories="$hasCategories" mt="sm" mb="none" />

                    {{-- Resource Cards; river-laravel-dms\resources\components\cards.blade.php --}}
                    <x-box my="sm">
                        @if($entities && $entities->count())
                            <x-grid columns="1 md:2 lg:3" gapX="normal" gapY="normal md:2xl" class="ucm-cards update-cards">
                                @foreach($entities as $entity)
                                    <x-universalcontent.resource-card :entity="$entity" />
                                @endforeach
                            </x-grid>
                        @else
                            <x-caption>No results found...</x-caption>
                        @endif

                        <x-universalcontent.index.pagination :results="$entities" mt="normal" />
                    </x-box>
                @else
                    <x-text emphasis my="xl">
                        We are currently updating our website, and will be adding new {{ $model::pluralLabel() }} shortly.
                    </x-text>
                @endif

            </x-column>

        </x-columns>

    </x-container>
</x-page>