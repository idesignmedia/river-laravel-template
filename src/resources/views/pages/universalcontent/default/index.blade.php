{{-- Universal Content: Index page (default) --}}
<x-page :page="$page ?? $model">

    {{-- Hero --}}
    <x-hero variation="{{ $page->hasHero() ? '' : 'small' }}"
        title="{{ $page->getName() ?? $model::indexLabel() }}"
        subtitle="{{ $page->getSummary() }}"
        img="{{ $page->hasHero() ? $page->getHero() : null }}"
        fallbackColor="{{ $page->getFallbackColor() }}"
        />

    {{-- Body --}}
    <x-container>

        {{-- Filter Tools --}}
        <x-universalcontent.index.tools py="sm" :model="$model" />
        <x-divide my="none" />

        {{-- Cards --}}
        @if($model::repository()->getEntitiesCount())
            <x-section small pt="normal">
                <x-universalcontent.index.filter-results :model="$model" :results="$entities" mb="normal" />
                <x-universalcontent.cards :entities="$entities" :model="$model" mb="large" />
                <x-universalcontent.index.pagination :results="$entities" />
            </x-section>
        @else
            <x-text emphasis my="xl">
                We are currently updating our website, and will be adding new {{ $model::pluralLabel() }} shortly.
            </x-text>
        @endif

    </x-container>
</x-page>