{{-- Universal Content: Show page (default) --}}
<x-page :page="$entity">

    <x-divide color="{{ $entity->getFallbackColor() }}" my="none" />

    {{-- Body --}}
    <x-container>

        {{-- Hero Text & Category --}}
        <x-universalcontent.layout.post-hero-category py="normal" :entity="$entity" />

        {{-- Hero Image, Video Embed, Content & Related --}}
        <x-universalcontent.layout.grid-content-and-related-column :entity="$entity" />

        {{-- Content Blocks --}}
        <x-universalcontent.content.contentblocks :entity="$entity" />

        {{-- Additional Images --}}
        <x-universalcontent.content.additional-images :entity="$entity" />

        {{-- Footer --}}
        <x-universalcontent.layout.post-footer-return :entity="$entity" />

    </x-container>

    <x-space size="xl" />

</x-page>