<x-page title="Demo">

    {{-- Hero --}}
    <x-hero variation="small" title="Demo / Remote Data" subtitle="Remote data examples using Alpine JS..." />

    {{-- Body --}}
    <x-container>
        <x-section small>

            {{-- Remote Data --}}
            {{-- TODO: define createRemoteDataState() alpine state in @river-agency/support --}}
            <div x-data="remoteData()" x-init="fetchData()" x-on:remoteForm-submit.window="fetchData(true)">

                {{-- Heading --}}
                <x-title size="3" mb="none">News Content</x-title>
                <x-paragraph emphasis>Search for news content here...</x-paragraph>

                {{-- Filters --}}
                <x-columns size="2 md:4" mb="normal" gap="small">

                    {{-- Filter Keyword --}}
                    <x-form.control mb="none" x-ref="inputSearch" x-model="filterKeyword" x-on:keydown.debounce.150="fetchData()" x-on:change="fetchData()" name="keyword" type="text" button="ui/search" placeholder="Search..." />

                    {{-- Filter Status --}}
                    <x-form.control mb="none" x-ref="inputStatus" x-model="filterStatus" x-on:change="fetchData()" name="status" type="select">
                        <x-slot name="options">
                            <option value="">Any Status</option>
                            @foreach(\River\DMS\Enums\ContentStatus::getValueLabels() as $key => $label)
                            <option value="{{ $key }}">{{ $label }}</option>
                            @endforeach
                        </x-slot>
                    </x-form.control>

                    {{-- Clear Filters --}}
                    <x-grid-cell class="flex-center-vertical">
                        <div>
                            <x-link plain x-on:click="clearFilters()">
                                <x-icon name="ui/cancel" size="3rem"></x-icon>
                            </x-link>
                        </div>
                    </x-grid-cell>

                </x-columns>

                {{-- Results --}}
                <table class="style-table my--normal" x-bind:class="{ 'is-loading style-loading': isLoading }">
                    <thead class="mb--normal">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Summmary</th>
                        </tr>
                    </thead>
                    <tbody>
                        <template x-for="(model, index) in models" :key="index">
                            <tr>
                                <td x-text="model.id"></td>
                                <td>
                                    <x-link x-bind:href="'/' + model.path" variation="default no-border icon-external" external><span x-text="model.name"></span>
                                        <x-icon name="ui/external" />
                                    </x-link>
                                </td>
                                <td x-text="model.status"></td>
                                <td x-text="model.summary"></td>
                            </tr>
                        </template>
                    </tbody>
                </table>

                {{-- Pagination --}}
                <template x-if="paginated">
                    <x-block class="pagination">
                        {{-- Previous --}}
                        <a x-on:click="fetchPage(currentPage - 1)"><span>&lsaquo;</span></a>
                        {{-- Links --}}
                        <template x-for="(link, index) in links" :key="index">
                            <a x-on:click="fetchPage(link.label)" x-bind:class="{ 'active': currentPage == link.label }" x-show="! isNaN(link.label)"><span x-text="link.label"></span></a>
                        </template>
                        {{-- Next --}}
                        <a x-on:click="fetchPage(currentPage + 1)"><span>&rsaquo;</span></a>
                    </x-block>
                </template>

                {{-- Tools --}}
                <x-row mt="normal" gap="small">
                    <x-button variation="outline small" x-on:click="clearFilters()">Clear Filters
                        <x-icon name="ui/stop" size="1.0em" />
                    </x-button>
                    <x-button variation="outline small" x-on:click="fetchData(true)">Refresh Data
                        <x-icon name="ui/file" size="1.0em" />
                    </x-button>
                </x-row>
            </div>
            <x-space size="x-large" />

            {{-- Remote Form --}}
            <x-title size="3" mb="none">Add News</x-title>
            <x-paragraph emphasis>You can add a News Post here:</x-paragraph>

            <x-modal-popup open="Add News" :clickoutEnabled="true" :escapeEnabled="true" py="normal" px="large">
                {{-- createFormState() defined in @river-agency/support/alpine/states/form --}}
                <x-form.form
                    remote
                    action="/api/demo/submit"
                    x-data="{...createFormState(), onSuccess: function(response, options) { console.log('Form Success Callback', options, response) } }"
                    x-on:submit.prevent="handleSubmit"
                    x-bind:class="{ 'is-loading style-loading': isLoading }">

                    {{-- Heading --}}
                    <x-title size="3" mb="none">Create News</x-title>
                    <x-paragraph emphasis>Fill in the details below to create a News Post:</x-paragraph>

                    {{-- Message --}}
                    <x-form.message remote mb="small" />

                    {{-- Input --}}
                    <x-form.control name="name" id="form-name" label="Name" type="text" />
                    <x-form.control name="summary" id="form-summary" label="Summary" type="text" />
                    <x-form.control name="content" id="form-content" label="Content" type="textarea" />

                    {{-- Submit --}}
                    <x-form.submit text="Add News" />
                </x-form.form>
            </x-modal-popup>

            {{-- Footer - Back to Demo --}}
            <x-flex direction="row">
                <x-button href="{{ url('demo') }}" my="normal" ml="auto">Back to Demo</x-button>
            </x-flex>
        </x-section>
    </x-container>
</x-page>

<script>
    // Alpine definition for a Remote Data instance with filtering
    function remoteData() {
        return {
            // State
            isLoading: true,
            success: false,
            response: {},

            // Data
            payload: {},
            models: {},
            paginated: false,
            currentPage: null,
            links: {},

            // Filtering
            filterKeyword: '',
            filterStatus: '',
            filterPage: 1,
            currentUrl: '',

            fetchData(force = false) {
                // Define API call and validate
                let url = 'api/demo/fetch' + `?page=${this.filterPage}&keyword=${this.filterKeyword.trim()}&status=${this.filterStatus}`
                if (!force && url == this.currentUrl) return

                // Reset variables
                this.isLoading = true
                this.success = false
                this.response = {}
                // this.models = {} // Keep models visible for improved UX (with 'style-loading' applied)

                // Fetch remote data
                console.log('remoteData: Fetching data', url)
                this.currentUrl = url
                fetch(url)
                    .then(response => response.json())
                    .then(response => {
                        console.log('remoteData: Fetched data', response)

                        // Note the payload
                        this.success = true
                        this.response = response
                        this.payload = response.payload

                        // Note the models
                        this.models = this.payload.data

                        // Manage pagination
                        if (this.payload.current_page) {
                            this.paginated = true
                            this.currentPage = this.payload.current_page
                            this.links = this.payload.links
                        } else {
                            this.paginated = false
                            this.links = {}
                        }

                        // Note loading as complete
                        this.isLoading = false;
                    });
            },

            fetchPage(page = 1) {
                page = Math.max(page, 1)
                if (this.payload.last_page && page > this.payload.last_page) {
                    page = this.payload.last_page
                }

                console.log('remoteData: Fetching page', page)
                this.filterPage = page
                this.fetchData()
            },

            clearFilters() {
                console.log('remoteData: Clearing filters')
                this.filterKeyword = '';
                this.filterStatus = '';
                this.filterPage = 1;
                this.fetchData(true);
            },

        }
    }
</script>
