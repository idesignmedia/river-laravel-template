<x-page title="Demo">

    {{-- Hero --}}
    <x-hero variation="small" title="Demo / Forms & Inputs" subtitle="An overview of form inputs" />

    {{-- Body --}}
    <x-container>
        <x-section small>

            {{-- Form --}}
            <x-form.form remote class="py--small px--normal border--normal">

                {{-- Heading --}}
                <x-title size="3" mb="none">Forms & Inputs</x-title>
                <x-paragraph emphasis>A preview of assorted input elements...</x-paragraph>

                {{-- Notification --}}
                <x-notification variation="success" mb="small">
                    <x-title size="6" mb="smallest">Success
                        <x-icon name="ui/info" size="normal" />
                    </x-title>
                    <x-text mb="none">Example of a success notification.</x-text>
                </x-notification>
                {{-- Note, we also have a more opinionated <x-form.message> component --}}

                {{-- Input --}}
                <x-grid columns="2" gapY="none">

                    {{-- Input --}}
                    <x-form.control name="input" id="form-input" label="Input" type="text" />

                    {{-- Input with Description --}}
                    <x-form.control name="description" id="form-description" label="Input with Description" type="text" description="Input description..." />

                    {{-- Input with Icon --}}
                    <x-form.control icon="ui/file" name="icon" id="form-icon" label="Input with Icon" type="text" success="This is a success message" />

                    {{-- Input with Button --}}
                    <x-form.control button="ui/search" name="button" id="form-button" label="Input with Button" type="text" error="This is an error message" />

                    {{-- Checkbox --}}
                    <x-form.control name="checkbox" id="form-checkbox" label="Checkbox" type="checkbox" :checked="true" description="Checkbox description..." />

                    {{-- Select --}}
                    <x-form.control name="select" id="form-select" label="Select" type="select" :options="[1 => 'Option 1', 2 => 'Option 2', 3 => 'Option 3']" />

                    {{-- Multiple Checkboxes --}}
                    <x-form.control name="checkbox-multiple" id="form-checkbox-multiple" label="Multiple Checkbox" type="checkboxes" :options="['c1' => 'Checkbox 1', 'c2' => 'Checkbox 2', 'c3' => 'Checkbox 3']" />

                    {{-- Radio Buttons --}}
                    <x-form.control name="radio" id="form-radio" label="Radio Buttons" type="radio" value="r1" :options="['r1' => 'Radio 1', 'r2' => 'Radio 2', 'r3' => 'Radio 3']" />

                    {{-- Textarea --}}
                    <x-form.control colSpan="2" name="textarea" id="form-textarea" label="Textarea" type="textarea" />

                </x-grid>

                {{-- Submit --}}
                <x-form.submit text="Submit Form" />

            </x-form.form>
            <x-space size="large">
            </x-space>

            {{-- Notifications --}}

            <div class="py--small px--normal border--normal">

                {{-- Heading --}}
                <x-title size="3" mb="none">Notifications</x-title>
                <x-paragraph emphasis>Our .style-notification classes...</x-paragraph>

                <x-grid columns="md:2">
                    <x-grid-cell variation="full">
                        <x-notification>Text notification.</x-notification>
                    </x-grid-cell>
                    <x-grid-cell>
                        <x-notification success>Success notification.</x-notification>
                    </x-grid-cell>
                    <x-grid-cell>
                        <x-notification info>Info notification.</x-notification>
                    </x-grid-cell>
                    <x-grid-cell>
                        <x-notification warning>Warning notification.</x-notification>
                    </x-grid-cell>
                    <x-grid-cell>
                        <x-notification error>Error notification.</x-notification>
                    </x-grid-cell>
                </x-grid>
            </div>

            {{-- Footer - Back to Demo --}}
            <x-flex direction="row">
                <x-button href="{{ url('demo') }}" my="normal" ml="auto">Back to Demo</x-button>
            </x-flex>

        </x-section>
    </x-container>

</x-page>
