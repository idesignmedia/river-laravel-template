<x-page title="Demo">

    {{-- Hero --}}
    <x-hero variation="small" title="Demo / Components" subtitle="A showcase of our blade components" />

    {{-- Body --}}
    <x-container>
        <x-section small>

            {{-- Links & Buttons --}}
            <x-title size="3">Links & Buttons</x-title>
            <x-box p="normal" my="normal" border="normal gray.border">
                <x-title size="5">Links</x-title>
                <x-row>
                    <x-link variation="default">Default</x-link>
                    <x-link variation="current">Current</x-link>
                    <x-link variation="default subtle">Subtle</x-link>
                    <x-link variation="default no-border">No Border</x-link>
                    <x-link variation="plain">Plain</x-link>
                    <x-link variation="default" disabled>Disabled</x-link>
                    <x-link variation="default" readonly>Readonly</x-link>
                </x-row>
                <x-space size="normal">
                </x-space>
                <x-row>
                    <x-link variation="default subtle">With Icon
                        <x-icon name="ui/info" size="1.0em" />
                    </x-link>
                    <x-link variation="default subtle icon-external">External
                        <x-icon name="ui/external" />
                    </x-link>
                    <x-link variation="default subtle icon-right">Right
                        <x-icon name="ui/right" />
                    </x-link>
                    <x-link variation="default subtle icon-down">Down
                        <x-icon name="ui/down" />
                    </x-link>
                    <x-link variation="default subtle">
                        <x-icon name="ui/add" size="large" />
                    </x-link>
                    <x-link variation="default subtle">
                        <x-icon name="ui/home" size="large" />
                    </x-link>
                    <x-link variation="default subtle">
                        <x-icon name="ui/file" size="large" />
                    </x-link>
                    <x-link variation="default subtle">
                        <x-icon name="ui/controlpanel" size="large" />
                    </x-link>
                </x-row>
                <x-space size="normal">
                </x-space>
                <x-title size="5">Buttons</x-title>
                <x-row>
                    <x-button as="div" variation="default">Default</x-button>
                    <x-button as="div" variation="outline">Outline</x-button>
                    <x-button as="div" variation="current">Current</x-button>
                    <x-button as="div" variation="subtle">Subtle</x-button>
                    <x-button as="div" variation="outline reverse">Reverse</x-button>
                    <x-button as="div" variation="default" disabled>Disabled</x-button>
                    <x-button as="div" variation="default" readonly>Readonly</x-button>
                    <x-button as="div" variation="default" text="Text prop"></x-button>
                </x-row>
                <x-space size="normal">
                </x-space>
                <x-row>
                    <x-button as="div" variation="default">With Icon
                        <x-icon name="ui/info" size="normal" />
                    </x-button>
                    <x-button as="div" variation="default icon-external">External
                        <x-icon name="ui/external" />
                    </x-button>
                    <x-button as="div" variation="outline icon-right">Right
                        <x-icon name="ui/right" />
                    </x-button>
                    <x-button as="div" variation="subtle icon-down">Down
                        <x-icon name="ui/down" />
                    </x-button>
                </x-row>
            </x-box>

            {{-- Demo: Links & Buttons (Reverse) --}}
            <x-box p="normal" my="normal" border="normal reverse" scope="reverse" bg="reverse">
                <x-title size="5">Links</x-title>
                <x-row>
                    <x-link variation="default">Default</x-link>
                    <x-link variation="current">Current</x-link>
                    <x-link variation="default subtle">Subtle</x-link>
                    <x-link variation="default no-border">No Border</x-link>
                    <x-link variation="plain">Plain</x-link>
                    <x-link variation="default" disabled>Disabled</x-link>
                    <x-link variation="default" readonly>Readonly</x-link>
                </x-row>
                <x-space size="normal">
                </x-space>
                <x-row>
                    <x-link variation="default subtle">With Icon
                        <x-icon name="ui/info" size="1.0em" />
                    </x-link>
                    <x-link variation="default subtle icon-external">External
                        <x-icon name="ui/external" />
                    </x-link>
                    <x-link variation="default subtle icon-right">Right
                        <x-icon name="ui/right" />
                    </x-link>
                    <x-link variation="default subtle icon-down">Down
                        <x-icon name="ui/down" />
                    </x-link>
                    <x-link variation="default subtle">
                        <x-icon name="ui/add" size="large" />
                    </x-link>
                    <x-link variation="default subtle">
                        <x-icon name="ui/home" size="large" />
                    </x-link>
                    <x-link variation="default subtle">
                        <x-icon name="ui/file" size="large" />
                    </x-link>
                    <x-link variation="default subtle">
                        <x-icon name="ui/controlpanel" size="large" />
                    </x-link>
                </x-row>
                <x-space size="normal">
                </x-space>
                <x-title size="5">Buttons</x-title>
                <x-row>
                    <x-button as="div" variation="default">Default</x-button>
                    <x-button as="div" variation="outline">Outline</x-button>
                    <x-button as="div" variation="current">Current</x-button>
                    <x-button as="div" variation="subtle">Subtle</x-button>
                    <x-button as="div" variation="outline reverse">Reverse</x-button>
                    <x-button as="div" variation="default" disabled>Disabled</x-button>
                    <x-button as="div" variation="default" readonly>Readonly</x-button>
                </x-row>
                <x-space size="normal">
                </x-space>
                <x-row>
                    <x-button as="div" variation="default">With Icon
                        <x-icon name="ui/info" size="normal" />
                    </x-button>
                    <x-button as="div" variation="default icon-external">External
                        <x-icon name="ui/external" />
                    </x-button>
                    <x-button as="div" variation="outline icon-right">Right
                        <x-icon name="ui/right" />
                    </x-button>
                    <x-button as="div" variation="subtle icon-down">Down
                        <x-icon name="ui/down" />
                    </x-button>
                </x-row>
            </x-box>
            <x-space size="large">
            </x-space>

            {{-- Grids & Columns --}}
            <x-title size="3">Grids & Columns</x-title>
            <x-title size="5" gap="small md:normal">Grid</x-title>
            <x-grid columns="2 md:4">
                <x-grid-cell bg="gray.bg" padding="x-small">Grid Item</x-grid-cell>
                <x-grid-cell bg="gray.bg" padding="x-small">Grid Item</x-grid-cell>
                <x-grid-cell bg="gray.bg" padding="x-small">Grid Item</x-grid-cell>
                <x-grid-cell bg="gray.bg" padding="x-small">Grid Item</x-grid-cell>
                <x-grid-cell variation="full" bg="gray.bg" padding="x-small">Grid Item (full width)</x-grid-cell>
            </x-grid>
            <x-space size="normal">
            </x-space>
            <x-title size="5">Columns</x-title>
            <x-columns variation="small">
                <x-column width="12/12 md:6/12" mb="small md:normal">
                    <x-box bg="gray.bg" padding="x-small">Column</x-box>
                </x-column>
                <x-column width="12/12 md:6/12" mb="small md:normal">
                    <x-box bg="gray.bg" padding="x-small">Column</x-box>
                </x-column>
                <x-column width="100% md:50%" mb="small md:normal">
                    <x-box bg="gray.bg" padding="x-small">Column</x-box>
                </x-column>
                <x-column width="100% md:50%" mb="small md:normal">
                    <x-box bg="gray.bg" padding="x-small">Column</x-box>
                </x-column>
                <x-column width="full" mb="small md:normal">
                    <x-box bg="gray.bg" padding="x-small">Column (full width)</x-box>
                </x-column>
            </x-columns>
            <x-space size="large">
            </x-space>

            {{-- Grids & Columns --}}
            <x-title size="3">Stacks & Rows</x-title>
            <x-title size="5">Stack</x-title>
            <x-stack gap="small md:normal">
                <x-box bg="gray.bg" padding="x-small">Stack Item</x-box>
                <x-box bg="gray.bg" padding="x-small">Stack Item</x-box>
                <x-box bg="gray.bg" padding="x-small">Stack Item</x-box>
            </x-stack>
            <x-space size="normal">
            </x-space>
            <x-title size="5">Row</x-title>
            <x-row gap="small md:normal" direction="col sm:row" inline>
                <x-box bg="gray.bg" padding="x-small">Row Item</x-box>
                <x-box bg="gray.bg" padding="x-small">Row Item</x-box>
                <x-box bg="gray.bg" padding="x-small">Row Item</x-box>
            </x-row>
            <x-space size="large">
            </x-space>

            {{-- Footer - Back to Demo --}}
            <x-flex direction="row">
                <x-button href="{{ url('demo') }}" my="normal" ml="auto">Back to Demo</x-button>
            </x-flex>

        </x-section>
    </x-container>

</x-page>
