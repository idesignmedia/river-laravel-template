@php
    // Demo Values
    $colorRangePrimary = [
        50 => '@ bg',
        100 => '@ bg-dark',
        200 => '@ border',
        300 => '@ light',
        400 => '@ placeholder',
        500 => '@ normal',
        600 => null,
        700 => '@ dark',
        800 => null,
        900 => '@ darkest',
    ];

    $colorRangeGray = $colorRangePrimary;

    $colorRangeBase = [
        'Base' => '@ base',
        'Reverse' => '@ reverse',
        'Alpha' => '@ alpha',
        'Shadow' => '@ shadow',
        'Gradient' => '@ gradient',
    ];

    $fontSizeRange = ['2xl', 'xl', 'lg', 'normal', 'sm', 'xs', '2xs'];

    $titleRange = [1, 2, 3, 4, 5, 6];

    $selectOptions = [
        'value_1' => 'Value 1',
        'value_2' => 'Value 2',
        'value_3' => 'Value 3',
    ];

    $radioOptions = [
        'value_1' => 'Option 1',
        'value_2' => 'Option 2',
        'value_3' => 'Option 3',
    ];

    $currentHref = Request::url();

@endphp
<x-page title="Demo - Design">

    {{-- Hero --}}
    <x-hero variation="small" title="Demo / Design" subtitle="A showcase of the project's design system" />

    {{-- Body --}}
    <x-container>
        <x-section small>

            {{-- Color --}}
            <x-title size="4">Color</x-title>
            <x-adjunct mb="sm">map-color.scss</x-adjunct>

            <x-subtitle mb="xs">Primary:</x-subtitle>
            <x-row gap="xs" wrap="wrap">
                @foreach($colorRangePrimary as $key => $label)
                    <x-box class="demo-color demo-color--primary-{{ $key }}">
                        <div class="demo-color__bg"></div>
                        <x-text class="demo-color__range" size="sm">{{ $key }}</x-text>
                        <x-text size="xs" color="gray.dark" class="demo-color__value" />
                        <x-text size="xs" class="demo-color__tags" color="gray.dark">{{ $label }}</x-text>
                    </x-box>
                @endforeach
            </x-row>

            <x-space size="sm" />
            <x-subtitle mb="xs">Gray:</x-subtitle>
            <x-row gap="xs" wrap="wrap">
                @foreach($colorRangeGray as $key => $label)
                    <x-box class="demo-color demo-color--gray-{{ $key }}">
                        <div class="demo-color__bg"></div>
                        <x-text class="demo-color__range" size="sm">{{ $key }}</x-text>
                        <x-text size="xs" color="gray.dark" class="demo-color__value" />
                        <x-text size="xs" color="gray.dark" class="demo-color__tags">{{ $label }}</x-text>
                    </x-box>
                @endforeach
            </x-row>

            <x-space size="sm" />
            <x-subtitle mb="xs">Base:</x-subtitle>
            <x-row gap="xs" wrap="wrap">
                @foreach($colorRangeBase as $key => $label)
                    <x-box class="demo-color demo-color--base-{{ $key }}">
                        <div class="demo-color__bg"></div>
                        <x-text class="demo-color__range" size="sm">{{ $key }}</x-text>
                        <x-text size="xs" color="gray.dark" class="demo-color__value" />
                        <x-text size="xs" color="gray.dark" class="demo-color__tags">{{ $label }}</x-text>
                    </x-box>
                @endforeach
            </x-row>

{{-- 
            <x-space size="sm" />
            <x-subtitle mb="xs">Base:</x-subtitle>
            <x-row gap="xs">
                <x-box class="demo-color demo-color--base">
                    <div class="demo-color__bg"></div>
                    <x-text class="demo-color__range" size="sm">Base</x-text>
                    <x-text size="xs" color="gray.dark" class="demo-color__value" />
                </x-box>
                <x-box class="demo-color demo-color--reverse">
                    <div class="demo-color__bg"></div>
                    <x-text class="demo-color__range" size="sm">Reverse</x-text>
                    <x-text size="xs" color="gray.dark" class="demo-color__value" />
                </x-box>
            </x-row>
 --}}

            {{-- Type --}}
            <x-divide my="sm"/>
            <x-title size="4">Type</x-title>
            <x-adjunct mb="sm">map-type.scss, styles-typography.scss</x-adjunct>

            <x-grid columns="1 md:2" gap="sm">
                <x-grid-cell>
                    <x-subtitle mb="xs">Font Size:</x-subtitle>
                    <x-stack gap="xs">
                        @foreach($fontSizeRange as $value)
                            <x-text class="demo-font-size demo-font-size--{{ $value }}" size="{{ $value }}">Font Size {{ '@' . $value }}<x-adjunct /></x-text>
                        @endforeach
                    </x-stack>
                    <x-space size="sm" />
                    <x-subtitle mb="xs">Common:</x-subtitle>
                    <x-stack gap="xs">
                        <x-subtitle>Subtitle</x-subtitle>
                        <x-label>Label</x-label>
                        <x-adjunct>Adjunct</x-adjunct>
                        <x-caption>Caption</x-caption>
                    </x-stack>
                </x-grid-cell>
                <x-grid-cell>
                    <x-subtitle mb="xs">Titles:</x-subtitle>
                    <x-stack gap="xs">
                        @foreach($titleRange as $value)
                            <x-title class="demo-title demo-title--{{ $value }}" size="{{ $value }}">Title {{ $value }}<x-adjunct /></x-title>
                        @endforeach
                    </x-stack>
                </x-grid-cell>
            </x-grid>

            {{-- Buttons --}}
            <x-divide my="sm"/>
            <x-title size="4">Buttons</x-title>
            <x-adjunct mb="sm">style-button.scss, map-sizes.scss, map-common.scss</x-adjunct>

            <x-grid columns="1 md:3" gap="sm">
                <x-grid-cell>
                    <x-subtitle mb="xs">Default:</x-subtitle>
                    <x-stack gap="xs">
                        <div><x-button>Button</x-button></div>
                        {{-- <div><el-button>El Button</el-button></div> --}}
                        <div><x-button icon="ui/plus">Icon Left</x-button></div>
                        <div><x-button icon="ui/right" :iconRight="true">Icon Right</x-button></div>
                        <div><x-button variation="outline">Outline</x-button></div>
                        {{-- <div><el-button variation="outline">El Outline</el-button></div> --}}
                        <div><x-button variation="text">Text</x-button></div>
                        <div><x-button variation="small">Small</x-button></div>
                        <div><x-button variation="outline small">Outline + Small</x-button></div>
                        {{-- <div><el-button variation="outline small">El Outline + Small</el-button></div> --}}
                    </x-stack>
                </x-grid-cell>
                <x-grid-cell>
                    <x-subtitle mb="xs">Primary:</x-subtitle>
                    <x-stack gap="xs">
                        <div><x-button variation="primary">Button</x-button></div>
                        {{-- <div><el-button type="primary">El Button</el-button></div> --}}
                        <div><x-button variation="primary" icon="ui/plus" iconLabel="+">Icon Left</x-button></div>
                        <div><x-button variation="primary" icon="ui/right" :iconRight="true" iconLabel=">">Icon Right</x-button></div>
                        <div><x-button variation="primary outline">Outline</x-button></div>
                        {{-- <div><el-button type="primary" variation="outline">El Outline</el-button></div> --}}
                        <div><x-button variation="primary text">Text</x-button></div>
                        <div><x-button variation="primary small">Small</x-button></div>
                        <div><x-button variation="primary outline small">Outline + Small</x-button></div>
                        {{-- <div><el-button type="primary" variation="outline small">El Outline + Small</el-button></div> --}}
                    </x-stack>
                </x-grid-cell>
                <x-grid-cell>
                    <x-subtitle mb="xs">Secondary:</x-subtitle>
                    <x-stack gap="xs">
                        <div><x-button variation="secondary">Button</x-button></div>
                        {{-- <div><el-button type="secondary">El Button</el-button></div> --}}
                        <div><x-button variation="secondary" icon="ui/plus" iconLabel="+">Icon Left</x-button></div>
                        <div><x-button variation="secondary" icon="ui/right" :iconRight="true" iconLabel=">">Icon Right</x-button></div>
                        <div><x-button variation="secondary outline">Outline</x-button></div>
                        {{-- <div><el-button type="secondary" variation="outline">El Outline</el-button></div> --}}
                        <div><x-button variation="secondary text">Text</x-button></div>
                        <div><x-button variation="secondary small">Small</x-button></div>
                        <div><x-button variation="secondary outline small">Outline + Small</x-button></div>
                        {{-- <div><el-button type="secondary" variation="outline small">El Outline + Small</el-button></div> --}}
                    </x-stack>
                </x-grid-cell>
            </x-grid>

            {{-- Links --}}
            <x-divide my="sm"/>
            <x-title size="4">Links</x-title>
            <x-adjunct mb="sm">style-link.scss</x-adjunct>

            <x-grid columns="1 md:2" gap="sm">
                <x-grid-cell>
                    <x-subtitle mb="xs">Default:</x-subtitle>
                    <x-stack gap="xs">
                        <div><x-link href="{{ $currentHref }}">Link</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="plain">Plain</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="subtle">Subtle</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="no-border">No Border</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="hover-border">Hover Border</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="current">Current</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="subtle hover">Subtle + Hover</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="block">Block</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="inline">Inline</x-link></div>
                    </x-stack>
                </x-grid-cell>
                <x-grid-cell>
                    <x-subtitle mb="xs">Button:</x-subtitle>
                    <x-stack gap="xs">
                        <div><x-link href="{{ $currentHref }}" variation="button">Link</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="button plain">Plain</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="button subtle">Subtle</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="button no-border">No Border</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="button hover-border">Hover Border</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="button current">Current</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="button plain hover">Plain + Hover</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="button block">Block</x-link></div>
                        <div><x-link href="{{ $currentHref }}" variation="button inline">Inline</x-link></div>
                    </x-stack>
                </x-grid-cell>
            </x-grid>

            {{-- Inputs --}}
            {{-- PENDING - for noew view /demo/inputs --}}

            {{-- Footer - Back to Demo --}}
            <x-flex direction="row">
                <x-button href="{{ url('demo') }}" my="normal" ml="auto">Back to Demo</x-button>
            </x-flex>

        </x-section>
    </x-container>

</x-page>
