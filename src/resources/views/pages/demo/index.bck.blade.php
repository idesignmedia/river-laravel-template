<x-page title="Demo">

    {{-- Hero --}}
    <x-hero title="Demo" subtitle="Assorted demo pages for review" img="hero/demo.jpg" />

    {{-- Body --}}
    <x-container>
        <x-section small>
            <x-paragraph>Click on a demo link to view more:</x-paragraph>
            <x-stack gap="small">
                <div>
                    <x-link href="{{ url('demo/components') }}">Components</x-link>
                </div>
                <div>
                    <x-link href="{{ url('demo/styles') }}">Styles & Formats</x-link>
                </div>
                <div>
                    <x-link href="{{ url('demo/inputs') }}">Forms & Inputs</x-link>
                </div>
                <div>
                    <x-link href="{{ url('demo/remote') }}">Remote Data</x-link> <small class="ml--smallest">via Alpine JS</small>
                </div>
                <div>
                    <x-link href="{{ url('demo/modal') }}">Modal Popup</x-link> <small class="ml--smallest">via Alpine JS</small>
                </div>
            </x-stack>

        </x-section>
    </x-container>
</x-page>
