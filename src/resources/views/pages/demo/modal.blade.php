<x-page title="Demo">

    {{-- Hero --}}
    <x-hero
        variation="small"
        title="Demo / Modal Popup"
        subtitle="Example modal popup using Alpine JS"
        />

    {{-- Body --}}
    <x-container>
        <x-section small>

            {{-- Modal Popup --}}
            <x-title size="3" mb="none">Modal Popup</x-title>
            <x-paragraph>Click here to open a simple popup modal:</x-paragraph>

            <x-modal-popup
                open="Open Example Modal"
                :clickoutEnabled="true"
                :escapeEnabled="true"
                py="normal"
                px="large"
                >

                {{-- Content --}}
                <x-title size="3" mb="none">Popup Modal</x-title>
                <x-paragraph emphasis>This is an example popup modal...</x-paragraph>
                <x-paragraph>...</x-paragraph>
                <x-paragraph>...</x-paragraph>
                <x-paragraph>...</x-paragraph>
                <x-paragraph>...</x-paragraph>
                <x-paragraph>...</x-paragraph>
                <x-paragraph>...</x-paragraph>
                <x-paragraph>...</x-paragraph>
                <x-paragraph>...</x-paragraph>
                <x-paragraph>...</x-paragraph>
                <x-paragraph>...</x-paragraph>
                <x-paragraph>...</x-paragraph>

                {{-- Buttons --}}
                <x-row>
                    {{-- Child Modal --}}
                    <x-modal-popup inline
                        :clickoutEnabled="false"
                        :escapeEnabled="false"
                        >
                        {{-- Open --}}
                        <x-slot name="open">
                            <x-button variation="default small" x-on:click="isOpen = $store.modalPopups.openModal()">Open Child Modal</x-button>
                        </x-slot>
                        {{-- Content --}}
                        <x-title size="4" mb="none">Child Modal</x-title>
                        <x-paragraph emphasis size="small">This is an inline child modal...</x-paragraph>
                        <x-paragraph emphasis size="small">Close by clicking outside disabled.</x-paragraph>
                    </x-modal-popup>

                    {{-- Close Parent Modal (optional) --}}
                    <x-button variation="outline small" ml="auto" x-on:click="isOpen = $store.modalPopups.closeModal()">Close Parent Modal</x-button>
                </x-row>

            </x-modal-popup>
            <x-space size="x-large"/>

            {{-- Modal Form --}}
            <x-title size="3" mb="none">Modal Form</x-title>
            <x-paragraph>Click here to open the Add News Post modal form:</x-paragraph>

            <x-modal-popup
                open="Add News"
                :clickoutEnabled="true"
                :escapeEnabled="true"
                py="normal"
                px="large"
                >
                    {{-- createFormState() defined in @river-agency/support/alpine/states/form --}}
                    <x-form.form
                        remote
                        action="/api/demo/submit"
                        x-data="createFormState()"
                        x-on:submit.prevent="handleSubmit"
                        x-bind:class="{ 'is-loading style-loading': isLoading }"
                        >

                        {{-- Heading --}}
                        <x-title size="3" mb="none">Create News</x-title>
                        <x-paragraph emphasis>Fill in the details below to create a News Post:</x-paragraph>

                        {{-- Message --}}
                        <x-form.message remote mb="small"/>

                        {{-- Input --}}
                        <x-form.control
                            name="name"
                            id="form-name"
                            label="Name"
                            type="text" />
                        <x-form.control
                            name="summary"
                            id="form-summary"
                            label="Summary"
                            type="text" />
                        <x-form.control
                            name="content"
                            id="form-content"
                            label="Content"
                            type="textarea" />

                        {{-- Submit --}}
                        <x-form.submit text="Add News"/>
                    </x-form.form>
            </x-modal-popup>





            {{-- Footer - Back to Demo --}}
            <x-flex direction="row">
                <x-button href="{{ url('demo') }}" my="normal" ml="auto">Back to Demo</x-button>
            </x-flex>

        </x-section>
    </x-container>

</x-page>

<script>
    // Alpine definition for a Remote Data instance with filtering
    function remoteData() {
        return {
            loading: true,
            success: false,
            payload: {},
            models: {},
            paginated: false,

            filterKeyword: '',
            filterStatus: '',
            filterPage: 1,
            currentUrl: '',

            fetchData(force = false) {
                // Define API call and validate
                let url = 'api/demo/fetch' + `?page=${this.filterPage}&keyword=${this.filterKeyword.trim()}&status=${this.filterStatus}`
                if (! force && url == this.currentUrl) return

                // Reset variables
                this.loading = true
                this.success = false
                this.paginated = false
                this.payload = {}
                // this.models = {} // Keep models visible for improved UX (with 'style-loading' applied)

                // Fetch remote data
                console.log('remoteData: Fetching data', url)
                this.currentUrl = url
                fetch(url)
                    .then(res => res.json())
                    .then(payload => {
                        console.log('remoteData: Fetched data', payload)

                        // Note the payload
                        this.success = true
                        this.payload = payload

                        // Note the models based on whether the data is paginated
                        if (payload.results.current_page !== undefined) {
                            this.models = payload.results.data
                            this.paginated = true
                        } else {
                            this.models = payload.results
                            this.paginated = false
                        }

                        // Note loading as complete
                        this.loading = false;
                    });
            },

            fetchPage(page = 1) {
                page = Math.max(page, 1)
                if (this.payload.results?.last_page && page > this.payload.results.last_page) {
                    page = this.payload.results.last_page
                }

                console.log('remoteData: Fetching page', page)
                this.filterPage = page
                this.fetchData()
            },

            clearFilters() {
                console.log('remoteData: Clearing filters')
                this.filterKeyword = '';
                this.filterStatus = '';
                this.filterPage = 1;
                this.fetchData(true);
            },

        }
    }

    // Alpine definition for a Remote Form instance with filtering
    function remoteForm() {
        return {
            loading: false,
            success: false,
            payload: {},
            message: '',

            inputName: '',
            inputSummary: '',
            inputContent: '',

            submitData() {
                // Define API call and validate
                let url = 'api/demo/submit' + `?name=${this.inputName.trim()}&summary=${this.inputSummary.trim()}&content=${this.inputContent.trim()}`

                // Reset variables
                this.loading = true
                this.success = false
                this.payload = {}

                // Submit remote data
                console.log('remoteForm: Submitting data', url)
                fetch(url)
                    // TODO, implement validation errors
                    .then(res => res.json())
                    .then(payload => {
                        console.log('remoteForm: Submitted data', payload)

                        // Note the payload
                        this.payload = payload

                        // Manage an error
                        if (! payload.success) {
                            this.handleError(payload.message)
                            return
                        }

                        // Note as successful
                        this.success = true
                        this.message = payload.message ?? 'Data successfully submitted...'

                        // Note loading as complete
                        this.loading = false;
                        this.$dispatch('remoteForm-submit')
                    }).catch(error => {
                        console.log('remoteForm: An error occured submitting the data', error)
                        this.handleError();
                    });
            },

            handleError(error = null) {
                error = error ?? 'An error occured submitting the form...'
                this.message = error
                this.success = false
                this.loading = false
            }
        }
    }

</script>
