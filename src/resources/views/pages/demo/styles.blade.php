<x-page title="Demo">

    {{-- Hero --}}
    <x-hero variation="small" title="Demo / Styles & Formats" subtitle="An overview of our .style classes and helpers" />

    {{-- Body --}}
    <x-container>
        <x-section small>

            <x-title size="3" mb="none">Scope Styles</x-title>
            <x-text emphasis>Formats applied directly on HTML via .scope-styles class (useful for CMS output)</x-text>
            <x-box p="normal" my="normal" border="normal gray.border" scope="styles">
                <hr class="my--none" />
                <p>Paragrph</p>
                <x-space size="x-small" />
                <a>Link</a>
                <x-space size="x-small" />
                <button>Button</button>
                <x-space size="x-small" />
                <input type="text" placeholder="Input..." />
                <x-space size="x-small" />
                <small>Small</small>
                <x-space size="x-small" />
                <big>Big</big>
                <x-space size="x-small" />
                <strong>Strong</strong> / <b>Bold</b>
                <x-space size="x-small" />
                <em>Emphasis</em>
                <x-space size="x-small" />
                <x-space size="x-small" />
                <blockquote>Blockquote</blockquote>
                <x-space size="x-small" />
                <q>Inline Quote</q>
                <x-space size="x-small" />
                <code>Code</code>
                <x-space size="x-small" />
                <pre>Codeblock</pre>
                <x-space size="x-small" />
                <x-space size="x-small" />
                <h1>Title 1</h1>
                <h2>Title 2</h2>
                <h3>Title 3</h3>
                <h4>Title 4</h4>
                <h5>Title 5</h5>
                <h6>Title 6</h6>
                <x-space size="x-small" />

                <ul>
                    <li>List Item</li>
                    <li>List Item</li>
                    <li>List Item</li>
                </ul>

                <ol>
                    <li>List Item</li>
                    <li>List Item</li>
                    <li>List Item</li>
                </ol>

                <table>
                    <thead>
                        <tr>
                            <th>Table Head</th>
                            <th>Table Head</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Table Data</td>
                            <td>Table Data</td>
                        </tr>
                        <tr>
                            <td>Table Data</td>
                            <td>Table Data</td>
                        </tr>
                    </tbody>
                </table>
            </x-box>
            <x-space size="large"></x-space>

            <x-title size="3" mb="none">Styles</x-title>
            <x-text emphasis>Formats applied via .style classes</x-text>
            <x-box p="normal" my="normal" border="normal gray.border">
                <hr class="style-divider my--none" />
                <div class="style-paragraph">Paragrph</div>
                <x-space size="x-small" />
                <a class="style-link">Link</a>
                <x-space size="x-small" />
                <div class="style-button">Button</div>
                <x-space size="x-small" />
                <input type="text" placeholder="Input..." class="style-input" />
                <x-space size="x-small" />
                <div class="style-small">Small</div>
                <x-space size="x-small" />
                <div class="style-big">Big</div>
                <x-space size="x-small" />
                <div class="style-strong">Strong</div>
                <x-space size="x-small" />
                <div class="style-emphasis">Emphasis</div>
                <x-space size="x-small" />
                <div class="style-caption">Caption</div>
                <x-space size="x-small" />
                <div class="style-subtitle">Subtitle</div>
                <x-space size="x-small" />
                <div class="style-blockquote">Blockquote</div>
                <x-space size="x-small" />
                <div class="style-inlinequote">Inline Quote</div>
                <x-space size="x-small" />
                <div class="style-quote">Quote</div>
                <x-space size="x-small" />
                <div class="style-code">Code</div>
                <x-space size="x-small" />
                <div class="style-codeblock">Codeblock</div>
                <x-space size="x-small" />
                <x-space size="x-small" />
                <h1 class="style-title-1">Title 1</h1>
                <h2 class="style-title-2">Title 2</h2>
                <h3 class="style-title-3">Title 3</h3>
                <h4 class="style-title-4">Title 4</h4>
                <h5 class="style-title-5">Title 5</h5>
                <h6 class="style-title-6">Title 6</h6>
                <x-space size="x-small" />
            </x-box>
            <x-space size="large"></x-space>

            <x-title size="3" mb="none">Reverse</x-title>
            <x-text emphasis>Formats applied via .style classes, within .scope-reverse</x-text>
            <x-box p="normal" my="normal" border="normal reverse" scope="reverse" bg="reverse">
                <hr class="style-divider my--none" />
                <div class="style-paragraph">Paragrph</div>
                <x-space size="x-small" />
                <a class="style-link">Link</a>
                <x-space size="x-small" />
                <div class="style-button">Button</div>
                <x-space size="x-small" />
                <input type="text" placeholder="Input..." class="style-input" />
                <x-space size="x-small" />
                <div class="style-small">Small</div>
                <x-space size="x-small" />
                <div class="style-big">Big</div>
                <x-space size="x-small" />
                <div class="style-strong">Strong</div>
                <x-space size="x-small" />
                <div class="style-emphasis">Emphasis</div>
                <x-space size="x-small" />
                <div class="style-caption">Caption</div>
                <x-space size="x-small" />
                <div class="style-subtitle">Subtitle</div>
                <x-space size="x-small" />
                <div class="style-blockquote">Blockquote</div>
                <x-space size="x-small" />
                <div class="style-inlinequote">Inline Quote</div>
                <x-space size="x-small" />
                <div class="style-quote">Quote</div>
                <x-space size="x-small" />
                <div class="style-code">Code</div>
                <x-space size="x-small" />
                <div class="style-codeblock">Codeblock</div>
                <x-space size="x-small" />
                <x-space size="x-small" />
                <h1 class="style-title-1">Title 1</h1>
                <h2 class="style-title-2">Title 2</h2>
                <h3 class="style-title-3">Title 3</h3>
                <h4 class="style-title-4">Title 4</h4>
                <h5 class="style-title-5">Title 5</h5>
                <h6 class="style-title-6">Title 6</h6>
                <x-space size="x-small" />
            </x-box>
            <x-space size="large"></x-space>

            {{-- Footer - Back to Demo --}}
            <x-flex direction="row">
                <x-button href="{{ url('demo') }}" my="normal" ml="auto">Back to Demo</x-button>
            </x-flex>

        </x-section>
    </x-container>

</x-page>
