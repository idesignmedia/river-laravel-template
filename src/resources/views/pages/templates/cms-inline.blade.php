{{-- RIVER Template (Website) --}}
{{-- Page Template: CMS Inline Hero --}}
@php
    // Incorporate an inline hero image, similar to the default UCM Show view
@endphp
<x-page :page="$page">

    <x-divide color="{{ $page->getFallbackColor() }}" my="none" />

    {{-- Body --}}
    <x-container>

        {{-- Hero Text; inspired by x-universalcontent.layout.post-hero-category --}}
        <x-box py="normal">
            <x-title size="2" mb="tiny">{{ $page->getName() }}</x-title>
            <x-subtitle>{{ $page->getSummary() }}</x-subtitle>
        </x-box>

        {{-- Hero Image, Content & Related; inspired by x-universalcontent.layout.grid-content-and-related-column --}}
        <x-box class="ucm-grid {{ $page->hasHero() ? 'has-image' : '' }}">

            {{-- Hero Image / Video Embed --}}
            @if($page->hasHero() || $page->hasVideoEmbedUrl())
                <div class="ucm-grid__image">
                    <x-universalcontent.layout.heroimage-and-video-embed :entity="$page" />
                </div>
            @endif

            {{-- Content --}}
            <x-box class="ucm-grid__content">

                {{-- Introduction (not Summary) --}}
                <x-universalcontent.content.introduction :entity="$page" />

                {{-- Content --}}
                <x-universalcontent.content.content :entity="$page" />

            </x-box>

            {{-- Aside --}}
            <x-box class="ucm-grid__aside">

                {{-- Related Column --}}
                <x-universalcontent.layout.page-related-column :entity="$page" />

            </x-box>
        </x-box>
        {{-- End: Hero Image, Content & Related --}}

        {{-- Content Blocks --}}
        <x-universalcontent.content.contentblocks :entity="$page" />

        {{-- Additional Images --}}
        <x-universalcontent.content.additional-images :entity="$page" />

        {{-- Footer --}}
        {{-- <x-universalcontent.layout.page-footer-return :entity="$page" /> --}}

    </x-container>

    <x-space size="xl" />

</x-page>