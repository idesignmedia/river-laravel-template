<x-page title="500 - Server Error">
    <x-hero variation="small" title="500" subtitle="Server Error" />
    <x-container>
        <x-section small>
            <x-paragraph>
                An unknown server error has occurred.
            </x-paragraph>
            <x-paragraph>
                You can navigate to another page, or <x-link href="{{ url('/') }}">return home</x-link>.
            </x-paragraph>
        </x-section>
    </x-container>
</x-page>
