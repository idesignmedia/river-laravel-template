<x-page title="404 - Page Not Found" {{-- view="layouts.minimal"  --}}>

    {{-- Hero --}}
    <x-hero variation="small" title="404" subtitle="Page Not Found" />

    {{-- Body --}}
    <x-container>

        {{-- Content --}}
        <x-section small>
            <x-paragraph>
                The page you are looking for cannot be found:<br>
                <x-inline class="style-emphasis">/{{ request()->path() }}</x-inline>
            </x-paragraph>
            <x-paragraph>
                You can navigate to another page, or <x-link href="{{ url('/') }}">return home</x-link>.
            </x-paragraph>
        </x-section>

    </x-container>

</x-page>
