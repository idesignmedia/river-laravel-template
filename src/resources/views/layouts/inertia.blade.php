<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <base href="{{ url('/') }}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <title inertia>{{ config('app.name') }}</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="robots" content="noindex, follow">

    <link rel="stylesheet" href="{{ mix('build/web/css/vendor.css') }}">
    <link rel="stylesheet" href="{{ mix('build/web/css/main.css') }}">
    <link rel="stylesheet" href="{{ mix('build/web/css/vendor-post.css') }}">

    <link rel="stylesheet" href="{{ mix('build/app/vue.css') }}">

    @routes
    <script defer>
        window.__RIVER__ = @js(\App\Support\InertiaGlobals::asArray());
    </script>
    <script defer src="{{ mix('build/app/main.js') }}"></script>
</head>

<body>
    @inertia
</body>

</html>
