{{-- Default Page Layout, loaded from Page Component app\View\Components\Page.php --}}
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"> {{-- class="preload no-js" --}}
@php
    // Pull all settings
    $pageSettings = \Outl1ne\NovaSettings\Models\Settings::all()->keyBy('key')->pluck('value', 'key')->toArray();
@endphp
<head>
    {{-- Page Setup --}}
    <base href="{{ url('/') }}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <title>{{ $title ?? config('app.name') }}</title>
    <meta name="description" content="{{ $description ?? '' }}">
    <meta name="keywords" content="{{ $keywords ?? '' }}">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if(config('project.features.noindex') || ( $page?->data_json['no_index'] ?? false) )
    <meta name="robots" content="noindex, follow">
    @endif

    {{-- Styles --}}
    <link rel="stylesheet" href="{{ mix('build/web/css/vendor.css') }}">
    <link rel="stylesheet" href="{{ mix('build/web/css/main.css') }}">
    <link rel="stylesheet" href="{{ mix('build/web/css/vendor-post.css') }}">
    @stack('styles')

    {{-- Scripts --}}
    <script defer src="{{ mix('build/web/js/main.js') }}"></script>
    @stack('scripts')

    {{-- Scripts (Production) --}}
    @include('layouts.page-scripts')

    {{-- Layout Head --}}
    {{ isset($head) ? $head : '' }}
    @stack('head')

    {{-- Settings / HEAD Code --}}
    {!! $pageSettings['code_head'] ?? null !!}

    {{-- Code Content Blocks / HEAD Code --}}
    @php
        if (! empty($page)) {
            $codeBlocks = $page->getCodeContentBlocks();
            foreach($codeBlocks as $codeBlock) {
                echo $codeBlock->getHeadCode() ?? null;
            }
        }
    @endphp

</head>

{{-- We register mobileMenu and darkMode Alpine states in main.js --}}
<x-box as="body" {{ $attributes }} class="layout-body js-preload-transitions page-{{ request()->path() }}" scope="{{ $scope ?? null }}" x-data="{}" x-bind:class="{
            'has-mobile-menu': $store.mobileMenu.open,
            'has-modal-popup': $store.modalPopups.count,
            'scope-dark' : $store.darkMode.on
        }">

    {{-- Settings / BODY Code --}}
    {!! $pageSettings['code_body'] ?? null !!}

    {{-- Layout Header --}}
    {!! $header ?? '' !!}
    @stack('header')

    {{-- Environment Banner --}}
    <x-layout.environment-banner />

    {{-- Admin Banner --}}
    <x-layout.admin-banner :entity="$entity ?? $page ?? null" />

    {{-- CMS Banner --}}
    @if($pageSettings['banner_enabled'] ?? false)
        <x-banner theme="{{ $pageSettings['banner_theme'] ?? 'text' }}" :container="true" :closable="false" key="cms-banner">
            <div class="scope-styles">
                {!! $pageSettings['banner_content'] ?? null !!}
            </div>
        </x-banner>
    @endif

    {{-- Body Content --}}
    {{-- We wrap the body contents in a wrapper for convenience, in case we need to make use of it as part of our app --}}
    {{-- Ensure you move the id="root" onto the <body> element if you remove the wrapper (used for assorted JS) --}}
    <div class="layout-wrapper" id="root">
        <x-layout.header :floating="$floatingHeader" />
        <main class="layout-content layout-body__content" id="main">
            {{ $slot }}
        </main>
        <x-layout.footer />
        <x-layout.mobile-menu />
    </div>

    {{-- Layout Footer --}}
    {!! $footer ?? '' !!}
    @stack('footer')

    {{-- Settings / FOOTER Code --}}
    {!! $pageSettings['code_footer'] ?? null !!}
</x-box>

</html>
