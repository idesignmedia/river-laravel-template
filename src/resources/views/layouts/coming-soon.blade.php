<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    {{-- Page Setup --}}
    <base href="{{ url('/') }}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <title>{{ $title ?? config('app.name') }}</title>
    <meta name="description" content="{{ $description ?? '' }}">
    <meta name="keywords" content="{{ $keywords ?? '' }}">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if(config('project.features.noindex'))
    <meta name="robots" content="noindex, follow">
    @endif

    {{-- Styles --}}
    <link rel="stylesheet" href="{{ mix('build/web/css/vendor.css') }}">
    <link rel="stylesheet" href="{{ mix('build/web/css/main.css') }}">
    <link rel="stylesheet" href="{{ mix('build/web/css/vendor-post.css') }}">

    {{-- Scripts --}}
    <script defer src="{{ mix('build/web/js/main.js') }}"></script>
    @stack('scripts')

    {{-- Google Analytics --}}
    @if(config('services.google.analytics_id'))

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ config('services.google.analytics_id') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', "{{ config('services.google.analytics_id') }}");
    </script>
    @endif

</head>

<body class="body">
    <div id="root">
        <div class="page-wrapper" style="min-height: 100vh; display: flex; justify-content: center; align-items: center; padding-top:  50px; margin-top: -50px; flex-direction: column; gap: 50px;">
            <img
                src="{{ asset('assets/images/company/logo.svg') }}"
                alt="{{ config('app.name') }}"
                style="display: block; width: 20%; min-width: 200px; height: auto;"
            >
            <div>
                <strong>Coming Soon</strong>
            </div>
        </div>
    </div>
</body>

</html>
