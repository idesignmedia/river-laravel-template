{{-- Minimal Page Layout, loaded from Page Component app\View\Components\Page.php --}}
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"> {{-- class="preload no-js" --}}

<head>
    {{-- Page Setup --}}
    <base href="{{ url('/') }}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <title>{{ $title ?? config('app.name') }}</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if(config('project.features.noindex'))
    <meta name="robots" content="noindex, follow">
    @endif

    {{-- Google Analytics --}}
    @if(config('services.google.analytics_id'))
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ config('services.google.analytics_id') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', "{{ config('services.google.analytics_id') }}");
    </script>
    @endif

    {{-- Style --}}
    <style>
        html {
            font-size: 62.5%;
            margin: 0;
            padding: 0;
        }

        body {
            font-family: Verdana, sans-serif;
            font-size: 1.6rem;
            line-height: normal;
            color: slategray;
            background-color: white;
            margin: 0;
            padding: 0;
            padding-top: 10%;
            padding-bottom: 2%;
        }

        .layout-wrapper {
            width: 80%;
            min-width: 400px;
            max-width: 1000px;
            margin: auto;
        }
    </style>
</head>

<body {{-- We register mobileMenu and darkMode Alpine states in main.js --}} class="layout-body page-{{ request()->path() }}">

    {{-- Body Contents --}}
    <div class="layout-wrapper" id="root">
        {{ $slot }}
    </div>

</body>

</html>
