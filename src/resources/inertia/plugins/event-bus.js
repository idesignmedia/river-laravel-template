// https://stackoverflow.com/questions/63471824/vue-js-3-event-bus
import { App, nextTick } from 'vue';
import mitt from 'mitt';

const events = mitt();

export const plugin = {
    install (app) {
        // Global events object
        app.config.globalProperties.events = events;

        // Global hydrate function for the layout
        app.config.globalProperties.hydrateLayout = function(layoutData = {}, layoutRoute = null) {
            nextTick(function() {
                events.emit('hydrateLayout', {
                    route: layoutRoute ?? route().current(),
                    data: layoutData
                });
            });
        }
    }
}

export default plugin;

/* // Events List

Event: hydrateLayout(Object layoutData)
- this is used by Inertia pages to hydrate the layout with custom page data
- this can then be listened to by layout components such as the navigation, breadcrumbs, and actionbar
- for convenience we have codified a global function "hydrateLayout" on the app

- Example usage, hydrate the layout with some custom page data:
    mounted: function() {
        this.hydrateLayout({
            route: route().current(),
            client: this.client,
        });
    },

- Example usage, take note when new data has been hydrated on the layout:
    mounted: function() {
        this.events.on('hydrateLayout', function(layoutData) {
            this.client = layoutData.data.client ?? null;
            this.checkpoint = layoutData.data.checkpoint ?? null;
        });
    }

Event: toggleSidebar(String state)
- this is used to toggle the sidebar
- state can be 'toggle' (default), 'open', or 'closed'

Event: reloadData
- this is used to tell any dynamic components that a change in state has happened, and they should reload their data
- for example, if you update details on a user page, you may want to reload related notes, etc

 */
