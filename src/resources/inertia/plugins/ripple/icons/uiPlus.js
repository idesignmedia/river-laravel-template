export const uiPlus = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12" height="12" viewBox="0 0 12 12">
  <g id="plus" transform="translate(-5.875 -5.875)">
    <path id="Path_38380" data-name="Path 38380" d="M18,7.875v8" transform="translate(-6.125)" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.25"/>
    <path id="Path_38381" data-name="Path 38381" d="M15.875,18h-8" transform="translate(0 -6.125)" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.25"/>
  </g>
</svg>`