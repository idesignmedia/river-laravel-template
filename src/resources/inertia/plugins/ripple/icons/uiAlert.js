export const uiAlert = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12" height="12" viewBox="0 0 12 12">
    <g id="warning-standard-line" transform="translate(0.266 1.315)">
      <path id="Path_38367" data-name="Path 38367" d="M17.447,25.119a.389.389,0,1,1-.389-.389A.389.389,0,0,1,17.447,25.119Z" transform="translate(-11.283 -17.789)" fill="currentColor"/>
      <path id="Path_38368" data-name="Path 38368" d="M17.292,13.116A.292.292,0,0,1,17,12.824V9.9a.292.292,0,1,1,.584,0v2.922A.292.292,0,0,1,17.292,13.116Z" transform="translate(-11.516 -6.794)" fill="currentColor"/>
      <path id="Path_38369" data-name="Path 38369" d="M4.918.5a.941.941,0,0,1,1.65,0l4.059,7.468A.941.941,0,0,1,9.8,9.358H1.692A.941.941,0,0,1,.845,7.967L4.918.5ZM1.358,8.247a.356.356,0,0,0,.327.527H9.8a.356.356,0,0,0,.313-.527L6.055.78a.356.356,0,0,0-.624,0L1.358,8.247Z" transform="translate(0)" fill="currentColor"/>
    </g>
</svg>`