export const navigationDashboard = `<svg id="Icon_-_Home" data-name="Icon - Home" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
  <path id="Path_38823" data-name="Path 38823" d="M0,0H20V20H0Z" fill="currentColor" opacity="0"/>
  <path id="home" d="M11.5,6.492,6.056,10.847v6.1H9.944V13.833h3.111v3.111h3.889V11.222a.778.778,0,0,0-.292-.607Zm0-1.992,6.124,4.9a2.333,2.333,0,0,1,.876,1.821v5.723A1.556,1.556,0,0,1,16.944,18.5H6.056A1.556,1.556,0,0,1,4.5,16.944v-6.1a1.556,1.556,0,0,1,.583-1.214Z" transform="translate(-1.5 -1.5)" fill="#d1d5db"/>
</svg>
`