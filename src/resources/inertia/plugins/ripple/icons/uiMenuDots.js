export const uiMenuDots = `<svg style="cursor: pointer" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 21 14">
    <text
        id="_..."
        data-name="..."
        transform="translate(4) rotate(90)"
        fill="currentColor"
        font-size="16"
        font-family="SegoeUI-Bold, Segoe UI"
        font-weight="700"
    >
        <tspan x="0" y="0">...</tspan>
    </text>
</svg>`