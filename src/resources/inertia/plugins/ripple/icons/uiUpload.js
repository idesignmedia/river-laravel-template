export const uiUpload = `<svg id="Icon_-_Upload" data-name="Icon - Upload" xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
<g id="upload" transform="translate(2.452 3)">
  <path id="Path_38867" data-name="Path 38867" d="M5.4,16.726a.875.875,0,0,1,.875-.875h10.5a.875.875,0,1,1,0,1.75H6.275a.875.875,0,0,1-.875-.875ZM8.281,7.72a.875.875,0,0,1,0-1.237l2.625-2.625a.875.875,0,0,1,1.237,0l2.625,2.625A.875.875,0,1,1,13.531,7.72L12.4,6.588v6.638a.875.875,0,1,1-1.75,0V6.588L9.519,7.72a.875.875,0,0,1-1.237,0Z" transform="translate(-5.4 -3.601)" fill="currentColor" fill-rule="evenodd"/>
</g>
</svg>`
