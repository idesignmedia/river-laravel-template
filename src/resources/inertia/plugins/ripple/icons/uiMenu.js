export const uiMenu = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12" height="12" viewBox="0 0 12 12">
  <path id="Path_38358" data-name="Path 38358" d="M6,9h8.581M6,12.218h8.581M6,15.436H9.754" transform="translate(-4.5 -6.5)" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
</svg>`