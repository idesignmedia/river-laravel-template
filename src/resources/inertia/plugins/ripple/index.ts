import { App } from "vue";
import { ripple, RippleConfig } from "@river-agency/ripple-vue";
import * as icons from "./icons";

// Layout
import Page from './components/Page.vue';

// Overrides
import Button from './components/override/Button.vue';

// Form Components
import FormContainer from './components/forms/form-container.vue';
import FormGrid from './components/forms/form-grid.vue';
import FormControl from './components/forms/form-control.vue';
import FormInput from './components/forms/form-input.vue';

// Custom
import Logo from './components/custom/logo.vue';
import LogoRiver from './components/custom/logo-river.vue';
import DisplayCard from './components/custom/display-card.vue';
import InnerCard from './components/custom/inner-card.vue';
import TitleDivide from './components/custom/title-divide.vue';
import LabelValue from './components/custom/label-value.vue';

// Note, you can add custom styling via resources\styles\ripple\project.scss

const config: Partial<RippleConfig> = {
    /**
     * Declare any SVG icons to be used by the <x-icon> component
     */
    icons,

    /**
     * Register any additional ripple plugins
     */
    addons: [],

    /**
     * Register any additional ripple components
     */
    components: {
        // Layout
        Page,
        // Overrides
        Button,
        // Form Components
        FormContainer,
        FormGrid,
        FormControl,
        FormInput,
        // Custom
        Logo,
        LogoRiver,
        DisplayCard,
        InnerCard,
        TitleDivide,
        LabelValue,
    },

    /**
     * Extend the default default box props config
     */
    extend: {
        props: [
            // Project props
            "flex-grow",
            "flex-shrink",
        ],
    },
};

export const plugin = {
    install(app: App) {
        app.use(ripple, config);
    },
};

export default plugin;
