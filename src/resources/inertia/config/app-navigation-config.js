// Define each section of the App navigation for users, indexed by route
// We refer to the index as the routeKey, and each entry as a section (or sectionConfig) in our navigation

// Fields:
// name = the name of the section to display (eg, tooltip)
// icon = the icon name to use in the <x-icon> for the section
// submenuComponent = the component to used to render the submenu on the right of the navigation (needs to be imported in navigation.vue)
// submenuData = optional data to pass through to the submenuComponent when rendered (for defaultSubmenu we pass an array of routes, matched in routes-config.js)
// ?visible = whether to show the page in the navigation menu (true by default); boolean or callable function (vm passed in)
const config = {
    'overview': {
        name: 'Overview',
        icon: 'navigationDashboard',
        submenuComponent: 'defaultSubmenu',
        submenuData: {
            routes: ['app.dashboard'],
        }
    },
    'profile.index': {
        name: 'Management',
        icon: 'navigationDashboard',
        submenuComponent: 'defaultSubmenu',
        submenuData: {
            routes: ['app.profile.index'],
        }
    },

};

export default config;
