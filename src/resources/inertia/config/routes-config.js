// Define each route in the application, indexed by route
// We refer to the index as the routeKey, and each entry as a route (or routeConfig) in our application

// Fields:
// name = the display label used for the route (buttons, labels, etc)
// icon = the display icon to use for the route where applicable (eg, in navigation)
// sectionRoute = route key for the parent section, used to match a section in navigation-config.js
// submenuRoute = optional route key to match the parent submenu, if applicable (to be used in navigation rendering)
// breadcrumbsComponent = the component to used to render the breadcrumbs in the header for this route (needs to be imported in header.vue)
// actionbarComponent = the component to used to render the actionbar in the header for this route (needs to be imported in header.vue)
// ?visible = whether to show the page in the navigation menu (true by default); boolean or callable function (vm passed in)
const config = {

    // APP

    // App Overview
    'app.dashboard': {
        name: 'Dashboard',
        icon: 'navigationDashboard',
        sectionRoute: 'app.dashboard',
        breadcrumbsComponent: 'defaultBreadcrumbs',
        actionbarComponent: 'defaultActionbar',
    },

    // App Profile
    'app.profile.index': {
        name: 'Profile',
        icon: 'navigationProfile',
        sectionRoute: 'app.profile.index',
        breadcrumbsComponent: 'defaultBreadcrumbs',
        actionbarComponent: 'defaultActionbar',
    },

    // STAFF

    // Staff Overview
    'staff.dashboard': {
        name: 'Dashboard',
        icon: 'navigationDashboard',
        sectionRoute: 'staff.dashboard',
        breadcrumbsComponent: 'defaultBreadcrumbs',
        actionbarComponent: 'defaultActionbar',
    },
    'staff.reporting.index': {
        name: 'Reporting',
        icon: 'navigationReporting',
        sectionRoute: 'staff.reporting.index',
        breadcrumbsComponent: 'defaultBreadcrumbs',
        actionbarComponent: 'defaultActionbar',
    },

    // Staff Contacts
    'staff.contacts.index': {
        name: 'Contacts',
        icon: null,
        sectionRoute: 'staff.contacts.index',
        breadcrumbsComponent: 'defaultBreadcrumbs',
        actionbarComponent: 'defaultActionbar',
    },
    'staff.contacts.create': {
        name: 'Add Contact',
        icon: null,
        sectionRoute: 'staff.contacts.index',
        breadcrumbsComponent: 'defaultBreadcrumbs',
        actionbarComponent: 'defaultActionbar',
    },
    'staff.organisations.index': {
        name: 'Organisations',
        icon: 'navigationOrganisations',
        sectionRoute: 'staff.contacts.index',
        breadcrumbsComponent: 'defaultBreadcrumbs',
        actionbarComponent: 'defaultActionbar',
    },
    'staff.organisations.show': {
        name: 'View Organisation',
        icon: 'navigationOrganisations',
        sectionRoute: 'staff.contacts.index',
        breadcrumbsComponent: 'defaultBreadcrumbs',
        actionbarComponent: 'defaultActionbar',
    },
    'staff.users.index': {
        name: 'Users',
        icon: 'navigationUsers',
        sectionRoute: 'staff.contacts.index',
        breadcrumbsComponent: 'defaultBreadcrumbs',
        actionbarComponent: 'defaultActionbar',
    },

    // Staff Management
    'staff.staff.index': {
        name: 'Staff',
        icon: 'navigationStaff',
        sectionRoute: 'staff.staff.index',
        breadcrumbsComponent: 'defaultBreadcrumbs',
        actionbarComponent: 'defaultActionbar',
        visible: function(vm) {
            // Only Admin with management permission can view this page
            // let admin = vm.$page.props.auth.user ?? null;
            // return admin?.permissions.manage;
            return true;
        },
    },
    'staff.profile.index': {
        name: 'Profile',
        icon: 'navigationProfile',
        sectionRoute: 'staff.profile.index',
        breadcrumbsComponent: 'defaultBreadcrumbs',
        actionbarComponent: 'defaultActionbar',
    },

    // Staff Demo
    'staff.demo.index': {
        name: 'Demo',
        icon: 'navigationDemo',
        sectionRoute: 'staff.demo.index',
        breadcrumbsComponent: 'defaultBreadcrumbs',
        actionbarComponent: null,
    },

}

export default config;
