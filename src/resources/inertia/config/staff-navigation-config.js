// Define each section of the navigation, indexed by route
// We refer to the index as the routeKey, and each entry as a section (or sectionConfig) in our navigation

// @debt refactor to staff-navigation-config, and update navigation components to use relevant config

// Fields:
// name = the name of the section to display (eg, tooltip)
// icon = the icon name to use in the <x-icon> for the section
// submenuComponent = the component to used to render the submenu on the right of the navigation (needs to be imported in navigation.vue)
// submenuData = optional data to pass through to the submenuComponent when rendered (for defaultSubmenu we pass an array of routes, matched in routes-config.js)
// ?visible = whether to show the page in the navigation menu (true by default); boolean or callable function (vm passed in)
const config = {
    'overview': {
        name: 'Overview',
        icon: 'navigationDashboard',
        submenuComponent: 'defaultSubmenu',
        submenuData: {
            routes: ['staff.dashboard', 'staff.reporting.index'],
        }
    },
    'staff.contacts.index': {
        name: 'Contacts',
        icon: 'navigationDashboard',
        submenuComponent: 'defaultSubmenu',
        submenuData: {
            routes: ['staff.organisations.index', 'staff.users.index'],
        }
    },
    'staff.staff.index': {
        name: 'Management',
        icon: 'navigationDashboard',
        submenuComponent: 'defaultSubmenu',
        submenuData: {
            routes: ['staff.staff.index', 'staff.profile.index'],
        }
    },

    // Demo
    'staff.demo.index': {
        name: 'Demo',
        icon: 'navigationDemo',
        submenuComponent: 'defaultSubmenu',
        submenuData: {
            routes: ['staff.demo.index'],
        },
        visible: function(vm) {
            // let admin = vm.$page.props.auth.user ?? null;
            // return admin?.permissions.developer;
            return true; // Always return false as this can be distracting during presentations...
        },
    },
};

export default config;
