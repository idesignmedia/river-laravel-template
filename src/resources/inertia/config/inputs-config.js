export default {
    
    // Activity
    activityTypes() {
        // Should match ActivityType.php enum
        return {
            // log: 'Log',
            note: 'Note',
            organisation: 'Organisation',
            user: 'User',
            // admin: 'Admin',
            // other: 'Other',
        }
    },
    activityOptions() {
        // @debt type this from the backend
        return {
            pinned: 'Pinned',
            important: 'Important',
            attachment: 'Has Attchment',
        }
    },

    // Contacts
    contactTypeOptions() {
        // Should match CotactType.php enum
        return {
            organisation: 'Organisation',
            user: 'User',
        }
    },

    // Organisations
    organisationTypeOptions() {
        // Should match OrganisationType.php enum
        return {
            company: 'Company',
            partnership: 'Partnership',
            sole_trader: 'Sole Trader',
            charitable_trust: 'Charitable Trust',
            other: 'Other',
        }
    },

    // Users
    userAccessOptions() {
        return {
            auth: 'Login Access',
            none: 'No Access',
        }
    },

    // General
    countryOptions() {
        return {
            nz : 'New Zealand'
        }
    },

}
