export const sizeUnits = ["rem", "em", "ex", "px", "%", "vw", "vh", "lh", "pc", "pt", "mm", "cm", "in", "Q", "vmin", "vmax"];

export function isSizeUnit(value) {
    if (!Number.isNaN(Number(value))) {
        return true;
    }

    return Boolean(sizeUnits.find((unit) => value.endsWith(unit)));
}
