// A simple typewriter effect for writing messages
// Writes plain text, though also accounts for newline characters
import moment from 'moment';

// Write a message out character by character, and append it to the given element
function writeMessagePlainText({
    message, // The message to write (plain text)
    title = null, // Optional title to show above the message
    parentElement, // The Parent DOM element to attach this message (at the bottom)
    className = 'chat-message mb--sm', // An optional class name to give to the message div
    callback = null, // An optionally callback function to call once the typewritter has finished writing
    error = false, // Whether an error class should be added to this message
    theme = null, // Optional display theme; null, 'success', 'info', 'warning', 'error', 'text' (set to 'error' if error is enabled)
    speed = 20, // The speed of the typing in ms (this is increased for punctuation)
    instant = false, // Whether to skip the typewriting effect this time
}) {
    console.log('writeMessage:', [message]);

    // Validate
    if (! message) {
        console.error("writeMessage invalid argument: 'message' is required.");
        return;
    }
    if (! parentElement instanceof Element) {
        console.error("writeMessage invalid argument: 'parentElement' must be a DOM element.");
        return;
    }

    // Define
    const outputDiv = document.createElement('div');
    outputDiv.className = className;
    let currentDate = moment();
    let formattedDate = currentDate.format('DD MMM YY @ HH:mm:ss');

    // Title
    if (title) {
        const titleDiv = document.createElement('div');
        titleDiv.className = 'style-title style-title--6 mb--none';
        titleDiv.textContent = `${title}, ${formattedDate}`;
        outputDiv.appendChild(titleDiv);
    }

    // Type Message
    const messageDiv = document.createElement('div');
    theme = error ? 'error' : theme;
    messageDiv.className = 'style-text text--sm' + ( theme ? ' style-notification style-notification--' + theme : '' );
    outputDiv.appendChild(messageDiv);

    // Type Message
    // const messageDiv = document.createElement('div');
    // messageDiv.className = 'style-text text--sm' + ( error ? ' style-notification style-notification--error' : '' );
    // message = message.replace(/\n/g, "<br>"); // converts newline characters to <br> tags
    // messageDiv.innerHTML = message; // use innerHTML instead of textContent
    // outputDiv.appendChild(messageDiv);

    let typeWriter = null;
    if (! instant) {
        // Type
        let i = 0;
        typeWriter = () => {
            if (i < message.length) {
                let lastChar = message.charAt(i - 1);
                let newChar = message.charAt(i);
        
                messageDiv.textContent += message.charAt(i);
                
                // Increase delay after comma and fullstop
                let delay = speed;
                if (newChar == ' ') {
                    if (lastChar === ',' || lastChar === ';') {
                        delay = speed*5;
                    }
                    if (lastChar === '.' || lastChar === '!') {
                        delay = speed*10;
                    }
                }
        
                i++;
                setTimeout(typeWriter, delay);
            } else if (callback && typeof callback === 'function') {
                callback();
            }
        }
    } else {
        // Instant
        typeWriter = () => {
            messageDiv.textContent = message;
            if (callback && typeof callback === 'function') {
                callback();
            }
        }
    }

    // Append
    parentElement.appendChild(outputDiv);
    typeWriter();
}

function writeMessage({
    message, 
    title = null,
    parentElement,
    className = 'chat-message mb--sm',
    callback = null,
    error = false,
    theme = null,
    speed = 20,
    instant = false,
}) {
    if (! message) {
        console.error("writeMessage invalid argument: 'message' is required.");
        return;
    }
    if (! parentElement instanceof Element) {
        console.error("writeMessage invalid argument: 'parentElement' must be a DOM element.");
        return;
    }

    const outputDiv = document.createElement('div');
    outputDiv.className = className;
    let currentDate = moment();
    let formattedDate = currentDate.format('DD MMM YY @ HH:mm:ss');

    if (title) {
        const titleDiv = document.createElement('div');
        titleDiv.className = 'style-title style-title--6 mb--none';
        titleDiv.textContent = `${title}, ${formattedDate}`;
        outputDiv.appendChild(titleDiv);
    }

    const messageDiv = document.createElement('div');
    theme = error ? 'error' : theme;
    messageDiv.className = 'style-text text--sm' + ( theme ? ' style-notification style-notification--' + theme : '' );
    outputDiv.appendChild(messageDiv);

    let typeWriter = null;
    if (! instant) {
        // Type
        let i = 0;
        typeWriter = () => {
            if (i < message.length) {
                let lastChar = message.charAt(i - 1);
                let newChar = message.charAt(i);
    
                // If we encounter a newline character, create a new <br> element
                if (newChar === '\n') {
                    const br = document.createElement('br');
                    messageDiv.appendChild(br);
                } else {
                    messageDiv.appendChild(document.createTextNode(newChar));
                }
    
                // Increase delay after comma and fullstop
                let delay = speed;
                let minorPunctuation = /[,:;\-/]/;
                let majorPunctuation = /[.!?]/;
                if (newChar === ' ') {
                    if (minorPunctuation.test(lastChar)) {
                        delay = speed * 5;
                    } else if (majorPunctuation.test(lastChar)) {
                        delay = speed * 10;
                    }
                }
    
                i++;
                setTimeout(typeWriter, delay);
            } else if (callback && typeof callback === 'function') {
                callback();
            }
        }
    } else {
        // Instant
        typeWriter = () => {
            message = message.split('\n').join('<br>'); // Replace newline characters with <br> tags
            messageDiv.innerHTML = message; // Use innerHTML to interpret the <br> tags
            if (callback && typeof callback === 'function') {
                callback();
            }
        }
    }
    
    // Append
    parentElement.appendChild(outputDiv);
    typeWriter();    
}

export { writeMessage }
