// Helper functions for Inertia
// https://codingblast.com/organising-javascript-helpers/
// https://stackoverflow.com/questions/38402025/how-to-create-helper-file-full-of-functions-in-react-native
// https://www.codingem.com/export-multiple-functions-in-javascript/
import axios from "axios";
import { ElNotification } from 'element-plus'

/**
 * Send an inertia form via form.post.
 * This matches our WithInertiaResponses PHP trait.
 * vm expects:
 * - formLoading
 * - formError
 * - dialogVisible
 * - form (Inertia form)
 * - ?clearForm()
 *
 * Params:
 * - vm: a Vue reference
 * - routeOrOptions: the route to send make the request to, OR an object containing the params for the call
 * - params: optional params to send through with the route (as GET query parameters)
 * - options: options to pass through to the inertia post mothod
 * - transform: an optional callback to transform the data before it is sent
 * - visit: whether to visit the result.url that is returned from inertia
 * - afterSuccess: an optional callback to run at the end of the success method
 * - afterError: an optional callback to run at the end of the error method
 * - reloadData: whether to emit the "reloadData" event, to tell other components to reload their data (see event-bus.js)
 */

function sendInertiaForm(vm, routeOrOptions = null, params = {}, options = {}, transform = null, visit = true, afterSuccess = null, afterError = null, reloadData = false) {
    // Send based on routeOrOptions
    if (typeof routeOrOptions === 'object' && routeOrOptions !== null) {
        _sendInertiaForm(
            vm,
            routeOrOptions.route,
            routeOrOptions.params ?? params,
            routeOrOptions.options ?? options,
            routeOrOptions.transform ?? transform,
            routeOrOptions.visit ?? visit,
            routeOrOptions.afterSuccess ?? afterSuccess,
            routeOrOptions.afterError ?? afterError,
            routeOrOptions.reloadData ?? reloadData,
        );
    } else {
        _sendInertiaForm(vm, routeOrOptions, params, options, transform, visit, afterSuccess, afterError, reloadData);
    }
}
function _sendInertiaForm(vm, route = null, params = {}, options = {}, transform = null, visit = true, afterSuccess = null, afterError = null, reloadData = false) {
    // Validate
    if (! vm) {
        console.error("A vm was not provided to send the form");
        return;
    }

    // Log
    // console.log('_sendInertiaForm', route, params, options, transform, visit, afterSuccess, afterError, reloadData);

    // Route
    route = route ?? vm.$route().current();

    // Post
    vm.formLoading = true;
    vm.form
        .transform((data) => {
            return transform
                ? transform(data)
                : data;
        })
        .post(
            vm.route(route, params),
            {
                // Default options / behaviour
                preserveScroll: true,
                onSuccess: (res) => {
                    if (vm.clearForm) {
                        vm.clearForm();
                    }
                    vm.formLoading = false;
                    vm.dialogVisible = false;
                    ElNotification({
                        title: 'Success',
                        message: vm.$page.props.flash.success ?? null,
                        type: 'success',
                    });
                    if (visit && res.url) {
                        vm.$inertia.visit(res.url, { headers: { 'via-inertia-redirect': true } });
                    }
                    if (afterSuccess && afterSuccess instanceof Function) {
                        afterSuccess(res);
                    }
                    if (reloadData) {
                        vm.events.emit('reloadData');
                    }
                },
                onError: (res) => {
                    // vm.form.errors = res;
                    vm.formError = vm.$page.props.flash.error ?? null; //res._error ?? null;
                    vm.formLoading = false;
                    if (afterError && afterError instanceof Function) {
                        afterError(res);
                    }
                },
                // Override with custom options
                ...options,
            }
        );
}

// Push errors back into an Inertia Form (to display on the front-end)
// This is useful if you have made an axios call (as opposed to an inertia call)
function handleFormValidationErrors(
    response,
    form,
) {
    if (response && response.status === 422) {
        // Convert each error message array to a single string.
        form.errors = Object.fromEntries(
            Object.entries(response.data.errors).map(([field, messages]) => [field, messages[0]])
        );
    }
}

// Notify the user with a success message
function notifySuccessMessage(
    message = null,
    title = 'Success',
) {
    // Show the error message using ElNotification
    if (message) {
        ElNotification({
            title: title,
            message: message,
            type: 'success'
        });
    }
}

// Check if an error has a response message, and show an ElNotification if so
function notifyErrorResponse(
    error = null,
    title = 'Error',
) {
    // Show the error message using ElNotification
    if (error && error.response && error.response.data && error.response.data.message) {
        ElNotification({
            title: title,
            message: error.response.data.message,
            type: 'error'
        });
    }
}

// Notify the user with an error message
function notifyErrorMessage(
    message = null,
    title = 'Error',
) {
    // Show the error message using ElNotification
    if (message) {
        ElNotification({
            title: title,
            message: message,
            type: 'error'
        });
    }
}

// Return an error message based on the error and status code provided
function getErrorMessageFromResponse(
    error = null,
    fallback = 'A general error occurred, please try again later or contact support.',
)
{
    let status = error?.response?.status;
    let message = null;
    switch(status) {
        case 400:
            // 'Request Error'
            message = 'Invalid request. Please check your input and try again.';
            break;
        case 401:
            // Authorization Error
            message = 'Access error. Please login and try again.';
            break;
        case 419:
            // Session Error
            message = 'Your session has expired. Please login or refresh and try again.'
            break;
        default:
            if (error && ! error.response) {
                // Network Error
                message = 'An error occurred connecting to the server. Please check your connection and try again later.'
            } else {
                // Unknown Error
                message = fallback;
            }
    }
    return message;
}

export { sendInertiaForm, handleFormValidationErrors, notifySuccessMessage, notifyErrorResponse, notifyErrorMessage, getErrorMessageFromResponse }
