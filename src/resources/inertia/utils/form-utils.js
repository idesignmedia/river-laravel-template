// Helper functions for Forms

function submitOnCtrlEnter(e, vm) {
    if (this.dialogVisible && e.keyCode == 13 && e.ctrlKey) {
        this.handleSubmit();
    }
}

export { submitOnCtrlEnter }
