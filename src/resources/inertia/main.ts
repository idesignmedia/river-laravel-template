import { createApp, h } from "vue";
import { InertiaProgress } from "@inertiajs/progress";
import { createInertiaApp, Head } from "@inertiajs/inertia-vue3";

// Inertia Layout
import DefaultLayout from "./layouts/Project.vue";

// Plugins
import ripple from './plugins/ripple';
import events from './plugins/event-bus';

// Element Plus
import element from "./plugins/element-plus";
// import ElementPlusIcons from './plugins/element-plus-icon';

// Global components
// import TestComponent from "./components/test-component.vue";
import Navigate from "./components/navigate.vue";

// App Setup
const rawTitle = document.getElementsByTagName("title")[0];
const appName = rawTitle ? rawTitle.innerText : "";

createInertiaApp({
    /**
     * The ID of the element vue will mount to
     */
    id: "app",

    /**
     * Resolve the pages title
     */
    title: (title) => (appName ? `${title} - ${appName}` : title),

    /**
     * Resolve the page's vue component. This is the path you defined when calling
     * Inertia::render(...) from the controller.
     */
    resolve: (name) => {
        const page = require(`./pages/${name}`).default; // eslint-disable-line


        // Define the default layout for page components (optional)
        // https://inertiajs.com/pages#creating-layouts
        page.layout = page.layout || DefaultLayout;

        return page;
    },

    /**
     * Setup the Vue application.
     */
    setup(inertia: any) {
        // Declare the root Vue instance
        const app = createApp({
            render: () => h(inertia.App, inertia.props),
        });

        // Declare global variables and services
        // You should declare types for globalProperties in shims.d.ts
        // When types are declared you will receive autocompletion in your IDE
        app.config.globalProperties.console = window.console;
        app.config.globalProperties.route = (window as any).route;

        // Register plugins
        app.use(inertia.plugin);
        app.use(ripple);
        app.use(events);
        app.use(element);
        // app.use(ElementPlusIcons);

        // Register global components
        // app.component('TestComponent', TestComponent);
        app.component('inertia-head', Head);
        app.component('navigate', Navigate);

        // Mount the application
        app.mount(inertia.el);
    },
});

/**
 * Configure progress bar
 */
InertiaProgress.init({
    // Don't include a delay in development
    delay: process.env.NODE_ENV === "production" ? 250 : 0,

    // Set the color of the progress bar
    // color: "#000",
});


/**
 * Enable hot-module-replacement (HMR)
 *
 * - $ npm run hot
 * - $ yarn hot
 */
const mdl = module as any
if (mdl.hot) {
    const firstVisit = window.location.pathname;
    mdl.hot.accept();
    mdl.hot.dispose(() => {
        if (firstVisit !== window.location.pathname) {
            window.location.reload();
        }
    });
}
