declare namespace App.Data {
export type AddressData = {
line_1: string | null;
line_2: string | null;
suburb: string | null;
city: string | null;
country: string | null;
state: string | null;
postal_code: string | null;
};
export type AuthData = {
id: number;
name: string;
email: string;
phone: string | null;
company: string | null;
label: string;
role: string;
addedLabel: string;
addedAdjunct: string;
class: string;
type: string;
permissions: Array<any>;
};
export type NotificationData = {
id: any;
state: string;
message: string;
};
}
