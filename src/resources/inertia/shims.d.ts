
// Declare the types for Ziggy
// import type * as Ziggy from 'ziggy-js';

declare var route: any;

// declare function route(
//   name?: undefined,
//   params?: Ziggy.RouteParamsWithQueryOverload,
//   absolute?: boolean,
//   config?: Ziggy.Config,
// ): Ziggy.Router;

// declare function route(
//   name: string,
//   params?: Ziggy.RouteParamsWithQueryOverload,
//   absolute?: boolean,
//   config?: Ziggy.Config,
// ): string;

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
      // Declare global component property types for autocompletion
      route: typeof route,
      console: typeof console
  }
}
