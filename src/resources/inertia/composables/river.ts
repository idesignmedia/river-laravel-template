import cloneDeep from 'lodash/cloneDeep'

const config = cloneDeep((window as any).__RIVER__ ?? {});

const csrfMetaTag = document.querySelector('meta[name="csrf-token"]') as any;

export function useConfig<T extends Record<string, any>>(): T {
  return {
    csrf: csrfMetaTag?.content || "",
    ...config
  };
}
