// RIVER DMS
// Default JS used in the RIVER DMS, published to the project


// Swiper JS
// We use Swiper JS for Carousel sliders
// This is used in combination with the .gallery-slider class used in block-imagegallery.blade.php
// We import styles via styles/plugins/swiper.scss
import Swiper, { Navigation, Pagination } from 'swiper';

// First, give each Swiper a Unique ID to reference their controls
// We do this separate to any swiper initialisation, in case we initalise different types of sliders elsewhere
const swiperContainers = document.querySelectorAll('.swiper-container');
swiperContainers.forEach((swiperContainer) => {
    // Generate a Unique ID
    const uniqueId = Math.floor(Math.random() * Date.now());

    // Add the ID to the parent container
    swiperContainer.setAttribute('unique-id', uniqueId); // Add vai data attribute
    // swiperContainer.classList.add('unique-id-' + uniqueId); // Add via class

    // Add the ID to each child
    swiperContainer.querySelector('.swiper').setAttribute('unique-id', uniqueId);
    swiperContainer.querySelector('.swiper-pagination')?.setAttribute('unique-id', uniqueId);
    swiperContainer.querySelector('.swiper-button-prev')?.setAttribute('unique-id', uniqueId);
    swiperContainer.querySelector('.swiper-button-next')?.setAttribute('unique-id', uniqueId);
});

// Create each Gallery Slider
const gallerySliders = document.querySelectorAll('.swiper.gallery-slider');
gallerySliders.forEach((swiperElement) => {
    // Grab the unique ID
    const uniqueId = swiperElement.getAttribute('unique-id');
    const uniqueIdSelector = '[unique-id="' + uniqueId + '"]';

    const swiper = new Swiper(swiperElement, {
        modules: [Navigation, Pagination],
        loop: true,
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination' + uniqueIdSelector,
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next' + uniqueIdSelector,
            prevEl: '.swiper-button-prev' + uniqueIdSelector,
        },
    });

});

// FS Lightbox //
// We use FS Lightbox for a fullscreen lightbox for images
// This is used for Hoer Images, Image Blocks, and the Image Gallery Content Blocks
import FsLightbox from 'fslightbox';
