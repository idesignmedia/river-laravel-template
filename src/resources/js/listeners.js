window.addEventListener("DOMContentLoaded", () => {
    // Remove 'js-preload-transitions' class from body once loaded in (to block transitions)
    // https://css-tricks.com/transitions-only-after-page-load/
    document.body.classList.remove("js-preload-transitions");
});
