import Alpine from "alpinejs";
import * as states from "@river-agency/support/alpine/states";
import * as alpineStates from "@river-agency/support/alpine";

/**
 * Register Alpine states
 *
 * This allows us to define advanced states directly within x-data
 * eg, <menu x-data="toggleState()">...</menu>
 *
 * https://alpinejs.dev/globals/alpine-data
 * https://alpinejs.dev/globals/alpine-store
 * https://alpinejs.dev/globals/alpine-bind
 * */

alpineStates.bootstrap(Alpine, {
    // counter: (initialCount = 0) => ({
    //   count: initialCount,
    //   increase() {
    //     this.count++;
    //   },
    //   decrease() {
    //     this.count--;
    //   },
    // }),
});

/**
 * Register Alpine stores
 *
 * You can access these from any Alpine instance using $store
 * eg, <menu class="mobile-menu" x-bind:class="{ 'is-open': $store.mobileMenu.open }">...</div>
 *
 */
Alpine.store("mobileMenu", states.disclosure("open", false));
Alpine.store("darkMode", states.disclosure("on", false));
Alpine.store("modalPopups", {
    count: 0,
    openModal() {
        this.count += 1;
        return true;
    },
    closeModal() {
        this.count -= 1;
        return false;
    },
});
