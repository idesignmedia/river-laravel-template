// Imports
import Alpine from "alpinejs";
import lozad from "lozad";
import logger, { Logger } from "@river-agency/support/logger";

import * as cookies from "./cookies";

import "@river-agency/support/core";
import "@river-agency/support/scripts";

import "./bootstrap";
import "./listeners";

// Vendor
import "./vendor/river-dms";

// Expose services globally
window.logger = logger;
window.Logger = Logger;
window.lozad = lozad;
window.Alpine = Alpine;
window.getCookie = cookies.get;
window.setCookie = cookies.set;
window.unsetCookie = cookies.clear;

// Configure services
Alpine.start();
lozad().observe();
