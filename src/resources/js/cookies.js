// Manage Cookies; thanks to https://plainjs.com/javascript/utilities/set-cookie-get-cookie-and-delete-cookie-5/

export function get(name) {
    const v = document.cookie.match(`(^|;) ?${name}=([^;]*)(;|$)`);
    return v ? v[2] : null;
}

export function set(name, value, days = 1) {
    const d = new Date();
    d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * days);

    document.cookie = `${name}=${value};path=/;expires=${d.toGMTString()}`;
}

export function clear(name) {
    set(name, "", -1);
}
