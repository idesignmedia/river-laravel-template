<?php

namespace Database\Seeders;

use App\Models\NavigationGroup;
use App\Models\Page;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

// Sync programmatic page content
// Note, we skip the majority of seeding during unit tests due to issues with sqlite
class PageSeeder extends Seeder
{
    public function run(Faker $faker): void
    {
        // All pages (master versions)
        $models = Page::query()
            ->masterVersion()
            ->get();

        // PROJECT DATA
        if (config('database.seed_content') && ! \App::runningUnitTests()) {

            // Hero Images to config pages
            // Identified by images in the "seeders/images/pages/" folder with a filename.jpg matching a page slug
            if (config('database.seed_images')) {
                $images = glob(database_path('seeders/images/pages/*.jpg'));
                if (! empty($images)) {
                    foreach($images as $image) {
                        $filename = basename($image); // eg, 'about.jpg'
                        $fileslug = pathinfo($filename, PATHINFO_FILENAME); // 'about'
                        $model = $models->where('slug', $fileslug)->first();

                        // If we match a page imaeg by slug, add it
                        if ($model && ! $model->hasMedia('hero')) {
                            // Add an image
                            $model->addMedia($image)
                                ->preservingOriginal()
                                ->toMediaCollection('hero');
                            // Publish the page with image
                            $model->createPublishedVersionFromMasterVersion();
                        }
                    }
                }
            }

        }

        // DUMMY DATA
        if (config('database.seed_dummy_content') && ! \App::runningUnitTests()) {

            // Test Page
            $testPage = Page::create([
                // Setup
                'slug'      => 'test',
                'name'      => 'Test Page',
                'status'    => \River\DMS\Enums\ContentStatus::PUBLIC,
                // Content
                'summary'   => '[Summary] This is a test page for adding content.',
                'data_json' => [
                    'content_tereo' => 'Whakamatau',
                    'content_intro' => '[Introductio] Nullam dictum felis eu pede mollis pretium. Nulla facilisi.',
                ],
                'content'   => '[Content] Etiam ultricies nisi vel augue. Duis vel nibh at velit scelerisque suscipit. Phasellus consectetuer vestibulum elit.',
                // Related
                'content_related' => '[Related Content] Etiam ultricies nisi vel augue.',
            ]);

            // Test Page - Content Blocks
            $this->attachContentBlock(
                model: $testPage,
                modelAttribute: 'content_blocks',
                blockName: \App\ContentBlocks\CtaBlock::getBlockIdentifier(),
                blockContent: [
                    'title'     => 'Test Block',
                    'content'   => 'An example CTA...',
                ],
                childBlocksAttribute: 'child_links',
                childBlocks: [
                    [
                        'name' => \River\DMS\ContentBlocks\Child\LinkChildBlock::getBlockIdentifier(),
                        'content' => [
                            'text' => 'About',
                            'url' => '/about',
                        ],
                    ],
                    [
                        'name' => \River\DMS\ContentBlocks\Child\LinkChildBlock::getBlockIdentifier(),
                        'content' => [
                            'text' => 'Google',
                            'url' => 'https://google.com',
                        ],
                    ],
                ]
            );

            // Test Page - Save and publish
            $testPage->save();
            $testPage->createPublishedVersionFromMasterVersion();

            // Test Page - add to More navigation
            $this->attachNavigationModel(
                navigationSlug: 'more',
                model: $testPage::class,
                id: $testPage->id,
                name: 'Test',
            );

            // Test Child Page
            $testChildPage = Page::create([
                // Setup
                'slug'      => 'test-child',
                'name'      => 'Test Child Page',
                'status'    => \River\DMS\Enums\ContentStatus::PUBLIC,
                'parent_id' => $testPage->id,
                // Content
                'summary'   => '[Summary] This is a test child page.',
                'data_json' => [
                    'content_tereo' => 'Whakamatau',
                    'content_intro' => '[Introductio] Nullam dictum felis eu pede mollis pretium. Nulla facilisi.',
                ],
                'content'   => '[Content] Etiam ultricies nisi vel augue. Duis vel nibh at velit scelerisque suscipit. Phasellus consectetuer vestibulum elit.',
            ]);

            // Test Child Page - Content Blocks
            $this->attachContentBlock(
                model: $testChildPage,
                modelAttribute: 'content_blocks',
                blockName: \App\ContentBlocks\CtaBlock::getBlockIdentifier(),
                blockContent: [
                    'title'     => 'Test Block',
                    'content'   => 'An example CTA...',
                ],
                childBlocksAttribute: 'child_links',
                childBlocks: [
                    [
                        'name' => \River\DMS\ContentBlocks\Child\LinkChildBlock::getBlockIdentifier(),
                        'content' => [
                            'text' => 'About',
                            'url' => '/about',
                        ],
                    ],
                    [
                        'name' => \River\DMS\ContentBlocks\Child\LinkChildBlock::getBlockIdentifier(),
                        'content' => [
                            'text' => 'Google',
                            'url' => 'https://google.com',
                        ],
                    ],
                ]
            );

            // Test Child Page - Save and publish
            $testChildPage->save();
            $testChildPage->createPublishedVersionFromMasterVersion();

        }

    }

    // FUNCTIONS

    /**
     * Attach a UCM mdoel to a Navigation group.
     *
     * @param string $navigationSlug The slug of the Navigation group; eg, 'more'
     * @param string $models         The model to add; eg, \App\Models\Page
     * @param int $id                The model id to add
     * @param ?string $name          An optional name for the menu item
     */
    private function attachNavigationModel(
        string $navigationSlug, // The slug of the Navigation group; eg, 'more'
        string $model, // The model to add; eg, \App\Models\Page
        int $id, // The model id to add
        ?string $name = null, // An optional name for the menu item
    ): void {
        // Define
        $navigationBlock = config('river.pages.navigation_block');
        $navigationGroup = NavigationGroup::where('slug', $navigationSlug)->firstOrFail();

        // Add Model
        $navigationGroupBlocks = json_decode($navigationGroup->getRawOriginal('navigation_blocks'), true);
        $navigationGroupBlocks[] = $navigationBlock::getDefinition($model, $id, $name);
        $navigationGroup->navigation_blocks = json_encode($navigationGroupBlocks);

        // Save
        $navigationGroup->save();
    }

    /**
     * Create and attach a new Content Block to a given model (optionally with Child Blocks).
     *
     * @param UniversalContentModel $model  The UCM model to attach the Block to
     * @param string $modelAttribute        The attribute on the parent model to attach to
     * @param string $blockName             The name (identification key) for the parent block; usually Block::getBlockIdentifier()
     * @param array $blockContent           [attribute => 'value', ... ] Array of content attributes for the parent block (should match your Block's fields)
     * @param ?string $media                Optional file for adding as media (on the 'image' media collection)
     * @param ?string $childBlocksAttribute The attribute to attach child blocks to, if children are provided
     * @param $childBlocks                  [ [name => 'key', content => [...], ?media = 'file'], ... ] Optional array child blocks to attach, each with a (string)name and (array)content
     */
    private function attachContentBlock(
        \River\DMS\Models\UniversalContentModel $model, // The model to attach to
        string $modelAttribute, // The attribute on the parent model to attach to

        // Parent Block
        string $blockName, // The name (identification key) for the parent block; usually Block::getBlockIdentifier()
        array $blockContent = [], // [attribute => 'value', ... ] Array of content attributes for the parent block (should match your Block's fields)
        ?string $media = null, // Optional file for adding as media (on the 'image' media collection)
        ?int $order = 1, // The order for the Block, if multiple have been added

        // Child Blocks
        ?string $childBlocksAttribute = null, // The attribute to attach child blocks to, if children are provided
        ?array $childBlocks = null, // [ [name => 'key', content => [...], ?media = 'file'], ... ] Optional array child blocks to attach, each with a (string)name and (array)content
    ): void {
        // Parent Block
        $parentBlock = \River\DMS\Models\ContentBlock::create([
            'blockable_type'    => $model::class,
            'blockable_id'      => $model->id,
            'key'               => \Str::random(16),
            'name'              => $blockName,
            'attribute'         => $modelAttribute,
            'content'           => $blockContent,
            'order'             => $order,
        ]);
        $parentBlock->save();

        // Optional media
        if (config('database.seed_images') && $media) {
            // Add the media (file) to the Parent Block, keeping the original file
            $mediaCollection = 'image'; // Name of media collection; this is the default media collection for Content Block images
            $parentBlock->addMedia($media)
                ->preservingOriginal()
                ->toMediaCollection($mediaCollection);
        }

        if (! $childBlocksAttribute || ! $childBlocks) {
            return;
        }

        // Child Blocks
        // [ [name => 'key', content => [...], ?media = 'file'], ... ]
        $childBlocksAsArray = [];
        $order=1;
        foreach ($childBlocks as $childBlockData) {

            $childBlock = \River\DMS\Models\ContentBlock::create([
                'blockable_type'    => \River\DMS\Models\ContentBlock::class,
                'blockable_id'      => $parentBlock->id,
                'key'               => \Str::random(16),
                'name'              => $childBlockData['name'],
                'attribute'         => $childBlocksAttribute,
                'content'           => $childBlockData['content'],
                'order'             => $order++,
            ]);
            $childBlock->save();

            // Optional media
            if (config('database.seed_images') && ! empty($childBlockData['media'])) {
                // Add the media (file) to the Child Block, keeping the original file
                $mediaCollection = 'image'; // Name of media collection; this is the default media collection for Content Block images
                $childBlock->addMedia($childBlockData['media'])
                    ->preservingOriginal()
                    ->toMediaCollection($mediaCollection);
            }

            // Make note of the child, to add to the parent array of children
            $childBlockAsArray = $childBlock->toArray();
            $childBlockAsArray['layout'] =  $childBlock->name;
            $childBlockAsArray['attributes'] = $childBlock->content;
            unset($childBlockAsArray['blockable_type'], $childBlockAsArray['blockable_id'], $childBlockAsArray['name'], $childBlockAsArray['content']);
            $childBlocksAsArray[] = $childBlockAsArray;
        }

        // Insert children into the parent block content
        $contentWithChildren = array_merge($parentBlock->content, [
            $childBlocksAttribute => $childBlocksAsArray,
        ]);
        $parentBlock->content = $contentWithChildren;
        $parentBlock->save();
    }

}
