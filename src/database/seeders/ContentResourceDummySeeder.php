<?php

namespace Database\Seeders;

use App\Models\ContentResource;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class ContentResourceDummySeeder extends Seeder
{
    protected $count = 24;

    public function run(Faker $faker): void
    {
        if (! config('database.seed_dummy_content')) {
            return;
        }

        // Default Seed
        $models = ContentResource::seedFactoryModels(
            count: $this->count,
            truncateTable: true,
            callback: function ($model) use ($faker) {
                // Add random Resource Categories
                $resourceCategories = array_keys(ContentResource::resourceCategoriesFlat());
                shuffle($resourceCategories);
                $max = min(6, count($resourceCategories));
                $count = mt_rand(0, $max);
                $model->categories_json = array_slice($resourceCategories, 0, $count);

                // Return
                return $model;
            }
        );

        // Factory Seed
        // $models = ContentResource::factory(10)->make();
        // $models->each(fn ($item, $key) => $item->save());
    }
}
