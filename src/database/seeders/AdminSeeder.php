<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

// use Silvanite\Brandenburg\Permission;
// use Silvanite\Brandenburg\Role;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Clean up any existing admins - this seeder assumes we want to reset
        // all accounts.
        Admin::truncate();
        User::truncate();

        // DEVELOPERS
        foreach (config('river.admin.developers.accounts', []) as $email => $name) {
            $this->createDeveloper($email, $name, config('river.admin.developers.company', null));
        }

        // STAFF
        foreach (config('river.admin.staff.accounts', []) as $email => $name) {
            $this->createStaff($email, $name, config('river.admin.staff.company', null));
        }

        // USERS
        foreach (config('river.admin.users.accounts', []) as $email => $name) {
            $this->createUser($email, $name, config('river.admin.users.company', null));
        }

        // Permission::truncate();
        // Role::where('is_nova', true)->delete();
        // Admin::truncate();

        // Silvanite Roles & Permissions decomission as part of Nova 4 upgrade Oct 22
        // Create roles & permissions
        // foreach (config('river.admin.roles') as $slug => $roleConfig) {
        //     $role = new Role;
        //     $role->name = $roleConfig['name'];
        //     $role->slug = $slug;
        //     $role->is_nova = true;
        //     if (isset($roleConfig['super']) && $roleConfig['super'] == true) {
        //         $role->is_super = true;
        //     }
        //     $role->save();

        //     if (
        //         isset($roleConfig['permissions']) &&
        //         is_array($roleConfig['permissions']) &&
        //         count($roleConfig['permissions'])
        //     ) {
        //         $role->getPermissions()->createMany(
        //             collect($roleConfig['permissions'])->map(function ($slug) {
        //                 return ['permission_slug' => $slug];
        //             })
        //         );
        //     }
        // }

        // Create users and attach roles
        // foreach (config('river.admin.users') as $group) {
        //     foreach ($group['accounts'] as $email => $name) {
        //         $this->createAdmin([
        //             'name' => $name,
        //             'email' => $email,
        //             'password' => $group['password'],
        //         ]);
        //     }
        // }
    }

    private function createDeveloper(string $email, string $name, ?string $company = null): Admin
    {
        $password = config('river.admin.developers.password', null);
        $role = config('river.admin.developers.role', null);
        $admin = Admin::create([
            'name' => $name,
            'first_name' => Str::before($name, ' '),
            'last_name' => Str::after($name, ' ') ?? '',
            'email' => $email,
            'password' => Hash::make($password),
            'role' => $role,
            'company' => $company,
        ]);
        return $admin;
    }

    private function createStaff(string $email, string $name, ?string $company = null): Admin
    {
        $password = config('river.admin.staff.password', null);
        $role = config('river.admin.staff.role', null);
        $admin = Admin::create([
            'name' => $name,
            'first_name' => Str::before($name, ' '),
            'last_name' => Str::after($name, ' ') ?? '',
            'email' => $email,
            'password' => Hash::make($password),
            'role' => $role,
            'company' => $company,
        ]);
        return $admin;
    }

    private function createUser(string $email, string $name, ?string $company = null): User
    {
        $password = config('river.admin.users.password', null);
        $role = config('river.admin.users.role', null);
        $user = User::create([
            'name' => $name,
            'first_name' => Str::before($name, ' '),
            'last_name' => Str::after($name, ' ') ?? '',
            'email' => $email,
            'password' => Hash::make($password),
            'role' => $role,
            'company' => $company,
        ]);
        return $user;
    }
    
    // private function getRole(?string $roleName): Role|null
    // {
    //     $role = null;

    //     if ($roleName) {
    //         $role = Role::where('name', $roleName)->first();
    //     }

    //     return $role;
    // }

    // private function createAdmin(array $data): Admin
    // {
    //     $user = Admin::create([
    //         'name' => $data['name'],
    //         'first_name' => Str::before($data['name'], ' '),
    //         'last_name' => Str::after($data['name'], ' ') ?? '',
    //         'email' => $data['email'],
    //         'password' => Hash::make($data['password']),
    //     ]);

    //     // if ($role) {
    //     //     $user->assignRole($role);
    //     //     $user->save();
    //     // }

    //     return $user;
    // }
}
