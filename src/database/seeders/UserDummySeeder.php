<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class UserDummySeeder extends Seeder
{
    protected $count = 10;

    protected $password = 'testing';

    public function run(Faker $faker): void
    {
        if (! config('database.seed_dummy_content')) {
            return;
        }

        // Setup
        // User::truncate();

        // Loop and seed
        for ($i = 0; $i < $this->count; $i++) {
            // Create user
            $user = new User;

            // Details
            $user->name = $faker->name();
            $user->email = $faker->unique()->safeEmail();
            $user->email_verified_at = now();
            $user->password = \Illuminate\Support\Facades\Hash::make($this->password);
            $user->remember_token = \Str::random(10);

            // Save
            $user->save();
        }


        // Factory Seed
        // $users = User::factory(10)->create();
        // $users->each(fn ($item, $key) => $item->save());
    }
}
