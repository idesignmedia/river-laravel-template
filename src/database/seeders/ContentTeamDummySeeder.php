<?php

namespace Database\Seeders;

use App\Models\ContentTeam;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ContentTeamDummySeeder extends Seeder
{
    protected $count = 10;

    public function run(Faker $faker): void
    {
        if (! config('database.seed_dummy_content')) {
            return;
        }

        // Default Seed
        $models = ContentTeam::seedFactoryModels(
            count: $this->count,
            truncateTable: true,
            callback: function ($model) use ($faker) {
                // Human name
                $model->name = $faker->name();
                $model->slug = Str::slug($model->name);
                $model->position = Str::after($faker->jobTitle(), 'and ');
                return $model;
            }
        );

        // Factory Seed
        // $models = ContentTeam::factory(10)->make();
        // $models->each(fn ($item, $key) => $item->save());
    }
}
