<?php

namespace Database\Seeders;

use App\Models\ContentUpdate;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class ContentUpdateDummySeeder extends Seeder
{
    protected $count = 20;

    public function run(Faker $faker): void
    {
        if (! config('database.seed_dummy_content')) {
            return;
        }

        // Default Seed
        $models = ContentUpdate::seedFactoryModels(
            count: $this->count,
            truncateTable: true,
            callback: function ($model) use ($faker) {
                // 25% chance for a null category
                if (rand(1, 4) === 1) {
                    $model->category = null;
                }
                return $model;
            }
        );

        // Factory Seed
        // $models = ContentUpdate::factory(10)->make();
        // $models->each(fn ($item, $key) => $item->save());
    }
}
