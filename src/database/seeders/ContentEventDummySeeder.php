<?php

namespace Database\Seeders;

use App\Enums\EventRegions;
use App\Models\ContentEvent;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class ContentEventDummySeeder extends Seeder
{
    protected $count = 12;

    public function run(Faker $faker): void
    {
        if (! config('database.seed_dummy_content')) {
            return;
        }

        // Default Seed
        $models = ContentEvent::seedFactoryModels(
            count: $this->count,
            truncateTable: true,
            callback: function ($model) use ($faker) {
                // 20% chance for a null category
                if (rand(1, 5) === 1) {
                    $model->category = null;
                }

                // Event Details
                $model->region = EventRegions::getRandomValue();
                $model->date_commence = Carbon::today()->addDays(rand(0, 31))->addHours(rand(1, 12));

                // Return
                return $model;
            }
        );

        // Factory Seed
        // $models = ContentEvent::factory(10)->make();
        // $models->each(fn ($item, $key) => $item->save());
    }
}
