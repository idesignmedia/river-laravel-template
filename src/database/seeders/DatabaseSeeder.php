<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // Disable the UCM "UniversalContentSaved" job
        $universalContentSavedJob = config('river.universalcontent.jobs.UniversalContentSaved');
        config(['river.universalcontent.jobs.UniversalContentSaved' => null]);

        // Run default framework seeds
        $this->frameworkSeeds();

        // Run dummy seeds
        if (config('database.seed_dummy_content')) {
            $this->dummySeeds();
        }

        // Run project seeds
        if (config('database.seed_content')) {
            $this->projectSeeds();
        }

        // Run post framework seeds
        $this->prepareDatabase();

        // Restore the UCM "UniversalContentSaved" job
        config(['river.universalcontent.jobs.UniversalContentSaved' => $universalContentSavedJob]);

        // Run a global UCM cache
        $cacheService = config('river.universalcontent.services.UniversalContentCaching');
        if ($cacheService::cachingEnabled(disableDuringSeed: false)) {
            $cacheService::cacheAfterUniversalContentSaved(model: null, force: true);
        }
    }

    public function projectSeeds()
    {
        //
    }

    public function dummySeeds()
    {
        // Dummy Users
        // $this->call(UserDummySeeder::class);

        // Dummy Posts (ContentUpdate)
        $this->call(ContentUpdateDummySeeder::class);

        // Dummy Resources (ContentResource)
        $this->call(ContentResourceDummySeeder::class);

        // Dummy Events (ContentEvent)
        $this->call(ContentEventDummySeeder::class);

        // Dummy Members (ContentTeam)
        $this->call(ContentTeamDummySeeder::class);

        // Dummy Contacts (Organisations, Web App Users)
        $this->call(ContactsDummySeeder::class);
    }

    public function frameworkSeeds()
    {
        // Admin roles and users, based on 'config/river/admin' config
        $this->call(AdminSeeder::class);
    }

    public function prepareDatabase()
    {
        // Seed pages via pages:sync command
        \Illuminate\Support\Facades\Artisan::call('pages:sync');
        $this->call(PageSeeder::class);
    }

}
