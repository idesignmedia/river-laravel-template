<?php
// RIVER Template (Web App)

namespace Database\Seeders;

use App\Actions\Activity\StoreNote;
use App\Actions\Contacts\StoreOrganisation;
use App\Data\Activity\StoreUpdateNoteData;
use App\Data\Contacts\StoreUpdateOrganisationData;
use App\Enums\ContactStatus;
use App\Models\Admin;
use App\Models\Organisation;
use App\Services\FakerService;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class ContactsDummySeeder extends Seeder
{
    protected $count = 30;

    protected $password = 'testing';

    public function run(Faker $faker): void
    {
        if (! config('database.seed_dummy_content')) {
            return;
        }

        // Setup
        Organisation::truncate();

        // Admin
        $author = null;//Admin::find(1);

        // Loop and seed
        for ($i = 0; $i < $this->count; $i++) {

            // Name & Domain
            $name = $faker->company();
            $first = \Str::before(\Str::before(\Str::before($name, '-'), ','), ' ');
            $reference = strtoupper(\Str::before($first, ' '));

            $replacer = $faker->randomElement(['', '-']);
            $domain = str_replace([' ', ',', "'"], $replacer, $name);
            $domain = str_replace("{$replacer}{$replacer}", $replacer, $domain);
            $domain = strtolower($domain) . $faker->randomElement(['.com', '.co.nz', '.nz']);

            $date = Carbon::instance($faker->dateTimeBetween('-36 months', '-1 month'));

            // Organisation DTO
            $organisationData = new StoreUpdateOrganisationData(
                // Setup
                status: ContactStatus::ACTIVE,
                admin_id: $faker->randomElement([null, 1]),
                date_added: $date,

                // Contact Details
                name: $name,
                reference: $reference,
                phone: mt_rand(0, 1) ? FakerService::getPhone($faker) : null,
                email: $faker->randomElement(['info', 'admin']) . '@' . $domain,
                website: $faker->randomElement(['https://', 'www.', 'https://www.']) . $domain,
                address: null,
                country: 'nz',
                type: 'other',
                type_other: null,
                comments: mt_rand(0, 1) ? $faker->paragraph() : null,

                // Organisation Details
                industry: null,
            );

            // Store Organisation
            $organisation = StoreOrganisation::run(
                organisationData: $organisationData,
                author: $author,
                createdAt: $date,
            );

            // @debt we get an occasional error during seeding:
            // Invalid datetime format: 1292 Incorrect datetime value: '2022-09-25 02:06:59' for column 'created_at'
        }

        // Update Organisations
        $organisations = Organisation::all();
        foreach($organisations as $organisation) {

            // Date
            $date = $organisation->created_at;

            // Add Notes
            for ($i = 0; $i < mt_rand(0, 6); $i++) {
                $noteData = new StoreUpdateNoteData(
                    // Setup
                    author_type: $author ? $author::class : null,
                    author_id: $author ? $author->id : null,
                    noteable_type: $organisation::class,
                    noteable_id: $organisation->id,

                    // Content
                    name: FakerService::getNoteName($faker),
                    content: FakerService::getNoteContent($faker),

                    // Options
                    option_pinned: mt_rand(0, 3) ? true : false,
                    option_important: mt_rand(0, 3) ? true : false,

                    // Media
                    attachments: null,

                    // Activity relations
                    subject_type: $organisation::class,
                    subject_id: $organisation->id,
                    parent_type: $organisation::class,
                    parent_id: $organisation->id,
                );
                StoreNote::run(
                    noteData: $noteData,
                    author: $author,
                    createdAt: Carbon::instance($faker->dateTimeBetween($date, '-1 week', null)),
                );
            }

        }

    }
}
