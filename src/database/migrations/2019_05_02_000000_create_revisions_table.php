<?php
// RIVER Template

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('revisions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->nullableMorphs('revisionable', 'revisionable');
            $table->nullableMorphs('causer', 'causer');
            $table->string('causer_email')->nullable()->default(null);
            $table->json('properties')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('revisions');
    }
};
