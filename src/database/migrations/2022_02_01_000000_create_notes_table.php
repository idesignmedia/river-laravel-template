<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('notes', function (Blueprint $table) {
            // Setup
            $table->id();
            $table->string('author_type')->nullable();
            $table->bigInteger('author_id')->nullable();

            // Polymorphic Relationship
            $table->string('noteable_type')->nullable();
            $table->bigInteger('noteable_id')->nullable();

            // Content
            $table->string('name')->nullable();
            $table->text('content')->nullable();

            // Options
            $table->boolean('option_pinned')->default(false);
            $table->boolean('option_important')->default(false);

            // Data
            $table->json('data')->nullable();

            // Timestamps / Soft Deletes
            $table->timestamp('date_actioned')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('notes');
    }
};
