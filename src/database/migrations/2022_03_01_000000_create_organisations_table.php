<?php
// RIVER Template (Web App)

use App\Enums\ContactStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('organisations', function (Blueprint $table) {

            // Setup
            $table->id();
            $table->bigInteger('admin_id')->nullable(); // Admin manager (defaults to author)
            $table->enum('status', ContactStatus::getValues());
            $table->date('date_added')->nullable();

            // Contact Details (align with User Migration)
            $table->string('name');
            $table->string('reference')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->string('type')->nullable();
            $table->string('type_other')->nullable();
            $table->text('comments')->nullable();

            // Organisation Details
            $table->string('industry')->nullable();

            // Data
            $table->json('data')->nullable();

            // Timestamps / Soft Deletes
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('organisations');
    }
};
