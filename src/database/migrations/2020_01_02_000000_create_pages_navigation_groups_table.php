<?php
// RIVER Template (Website)

use Illuminate\Database\Schema\Blueprint;
use River\DMS\Migrations\NavigationGroupsMigration;

/**
 * Run our default migration for Navigation Groups.
 * Create custom fields for the project as needed.
 */
class CreatePagesNavigationGroupsTable extends NavigationGroupsMigration
{
    protected function makeCustomFields(Blueprint $table): void
    {
        //
    }
}
