<?php
// RIVER Template (Website)

use Illuminate\Database\Schema\Blueprint;
use River\DMS\Migrations\UniversalContentMigration;

class CreateContentResourcesTable extends UniversalContentMigration
{
    public static $table = 'content_resources';

    protected function makeCustomFields(Blueprint $table): void
    {
        // Advanced Categories
        $table->json('categories_json')->nullable();
    }
}
