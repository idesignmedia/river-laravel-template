<?php
// RIVER Template (Website)

use Illuminate\Database\Schema\Blueprint;
use River\DMS\Migrations\PagesMigration;

/**
 * Run our default migration for Pages.
 * Create custom fields for the project as needed.
 */
class CreatePagesTable extends PagesMigration
{
    protected function makeCustomFields(Blueprint $table): void
    {
        //
    }
}
