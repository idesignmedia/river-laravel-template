<?php
// RIVER Template (Website)

use Illuminate\Database\Schema\Blueprint;
use River\DMS\Migrations\UniversalContentMigration;

class CreateContentUpdatesTable extends UniversalContentMigration
{
    public static $table = 'content_updates';

    protected function makeCustomFields(Blueprint $table): void
    {
        //
    }
}
