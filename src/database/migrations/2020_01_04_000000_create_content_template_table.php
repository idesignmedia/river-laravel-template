<?php
// RIVER Template (Website)

use Illuminate\Database\Schema\Blueprint;
use River\DMS\Migrations\UniversalContentMigration;

class CreateContentTemplateTable extends UniversalContentMigration
{
    public static $table = 'content_templates';

    protected function makeCustomFields(Blueprint $table): void
    {
        //
    }
}
