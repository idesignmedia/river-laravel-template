<?php

use Illuminate\Database\Schema\Blueprint;
use River\DMS\Migrations\CommonUserMigration;

class CreateAdminsTable extends CommonUserMigration
{
    public static $table = 'admins';

    // Default makeUserFields
    // protected function makeUserFields(Blueprint $table): void
    // {
    //     $table->string('name');
    //     $table->string('email')->unique();
    //     $table->timestamp('email_verified_at')->nullable();
    //     $table->string('first_name')->nullable();
    //     $table->string('last_name')->nullable();
    //     $table->string('password');
    //     $table->string('role');
    //     $table->rememberToken();
    // }

    protected function makeCustomFields(Blueprint $table): void
    {
        // Avatar field
        $table->string('avatar')->nullable()->default(null);

        // Admin details
        $table->string('phone')->nullable();
        $table->string('company')->nullable();
        $table->text('comments')->nullable();

    }
}
