<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->boolean('is_nova')->default(false);
            $table->boolean('is_super')->default(false);
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create('role_permission', function (Blueprint $table) {
            $table->integer('role_id')->unsigned();
            $table->string('permission_slug');
            $table->timestamps();

            // $table->foreign('role_id')
            //       ->references('id')
            //       ->on('roles')
            //       ->onDelete('cascade');

            $table->primary(['role_id', 'permission_slug']);
        });

        Schema::create('role_user', function (Blueprint $table) {
            $table->integer('role_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->timestamps();

            // $table->foreign('role_id')
            //       ->references('id')
            //       ->on('roles')
            //       ->onDelete('cascade');

            // $table->foreign('user_id')
            //       ->references('id')
            //       ->on('users')
            //       ->onDelete('cascade');

            $table->primary(['role_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('role_permission');
        Schema::dropIfExists('role_user');
        Schema::dropIfExists('roles');
    }
};
