<?php

use River\DMS\Migrations\PasswordResetsMigration;

class CreateAdminPasswordResetsTable extends PasswordResetsMigration
{
    public static $table = 'admin_password_resets';
}
