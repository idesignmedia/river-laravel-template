<?php
// RIVER Template (Website)

use Illuminate\Database\Schema\Blueprint;
use River\DMS\Migrations\UniversalContentMigration;

class CreateContentEventsTable extends UniversalContentMigration
{
    public static $table = 'content_events';

    protected function makeCustomFields(Blueprint $table): void
    {
        // Event Details
        $table->string('region')->nullable();
        $table->string('registration_link')->nullable();
        $table->string('registration_label')->nullable();
        $table->boolean('show_address')->default(true);

        // Event Date
        $table->string('date_label')->nullable(); // Display Date
        $table->dateTime('date_commence')->nullable();
        $table->dateTime('date_end')->nullable();

        // Event Address
        $table->string('address_label')->nullable(); // Display Address
        $table->string('address_line_1')->nullable();
        $table->string('address_line_2')->nullable();
        $table->string('city')->nullable();
        $table->string('state')->nullable();
        $table->string('postal_code')->nullable();
        $table->string('suburb')->nullable();
        $table->string('country')->nullable();
        $table->string('latitude')->nullable();
        $table->string('longitude')->nullable();
    }

}
