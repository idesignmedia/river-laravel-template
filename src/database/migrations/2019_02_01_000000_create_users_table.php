<?php

use App\Enums\ContactStatus;
use Illuminate\Database\Schema\Blueprint;
use River\DMS\Migrations\CommonUserMigration;

class CreateUsersTable extends CommonUserMigration
{
    // Default makeUserFields
    // protected function makeUserFields(Blueprint $table): void
    // {
    //     $table->string('name');
    //     $table->string('email')->unique();
    //     $table->timestamp('email_verified_at')->nullable();
    //     $table->string('first_name')->nullable();
    //     $table->string('last_name')->nullable();
    //     $table->string('password');
    //     $table->string('role');
    //     $table->rememberToken();
    // }

    protected function makeCustomFields(Blueprint $table): void
    {
        // Setup
        // $table->id();
        $table->bigInteger('admin_id')->nullable(); // Admin manager (defaults to author)
        $table->enum('status', ContactStatus::getValues())->nullable();
        $table->date('date_added')->nullable();
        $table->string('avatar')->nullable()->default(null);

        // Admin details
        // $table->string('phone')->nullable();
        $table->string('company')->nullable();
        // $table->text('comments')->nullable();

        // Contact Details
        // $table->string('name');
        $table->string('reference')->nullable();
        $table->string('phone')->nullable();
        // $table->string('email')->nullable();
        $table->string('website')->nullable();
        $table->string('address')->nullable();
        $table->string('country')->nullable();
        $table->string('type')->nullable();
        $table->string('type_other')->nullable();
        $table->text('comments')->nullable();

        // User Details
        $table->string('occupation')->nullable();
        $table->timestamp('auth_at')->nullable(); // When (and if) auth access was granted
        $table->timestamp('login_at')->nullable(); // Last login time
    }
}
