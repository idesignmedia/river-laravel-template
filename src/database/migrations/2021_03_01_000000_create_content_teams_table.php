<?php
// RIVER Template (Website)

use Illuminate\Database\Schema\Blueprint;
use River\DMS\Migrations\UniversalContentMigration;

class CreateContentTeamsTable extends UniversalContentMigration
{
    public static $table = 'content_teams';

    protected function makeCustomFields(Blueprint $table): void
    {
        $table->string('position')->nullable();
    }
}
