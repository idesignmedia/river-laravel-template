<?php
// RIVER Template

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public static $table = 'contact_messages';
    
    // Run the migrations.
    public function up(): void
    {
        Schema::create(static::$table, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->index();
            $table->string('subject')->nullable()->default(null);
            $table->text('message');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    // Reverse the migrations.
    public function down(): void
    {
        Schema::dropIfExists(static::$table);
    }
};
