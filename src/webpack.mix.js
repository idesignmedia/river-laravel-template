const mix = require("laravel-mix");
const fs = require("fs");

if (fs.existsSync(".airdrop_skip")) {
    console.log("Assets already exist. Skipping compilation.");
    process.exit(0);
}

// Styles
mix.sass("resources/styles/main.scss", "public/build/web/css");
mix.sass("resources/styles/vendor.scss", "public/build/web/css");
mix.sass("resources/styles/vendor-post.scss", "public/build/web/css");

// Scripts
mix.js("resources/js/main.js", "public/build/web/js");

// Inertia
mix.ts("resources/inertia/main.ts", "public/build/app", {
    transpileOnly: true,
});
mix.vue({
    extractStyles: "public/build/app/vue.css",
});

// Compile Options
mix.version();
mix.webpackConfig(require("./webpack.config"));
mix.options({
    hmrOptions: {
        /**
         *  Change the default dev-server port to not conflict
         *  with Docker configurations
         * */
        port: 3006,
        host: "127.0.0.1",
    },
});
