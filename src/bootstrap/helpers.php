<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use River\Core\Contracts\CommonUser;

if (!function_exists('flash')) {
    /**
     * Set a session notification
     *
     * @param  string $message
     * @param  string $status
     *
     * @return null
     */
    function flash(string $state, string $message)
    {
        if (!in_array($state, [
            'info',
            'success',
            'error',
            'warning',
        ])) {
            throw new Exception('Invalid notification state.');
        }

        Session::flash('notification', compact('message', 'state'));
    }
}

// https://laravel.com/docs/9.x/helpers#method-array-to-css-classes
if (!function_exists('classNames')) {
    function classNames()
    {
        $args = func_get_args();

        return Str::toCssClasses($args);
    }
}

if (!function_exists('user')) {
    function user(string $guard = null): ?CommonUser
    {
        $guard = $guard ?? config('auth.defaults.guard');

        return auth($guard)->user();
    }
}

if (!function_exists('admin')) {
    function admin(string $guard = null): ?CommonUser
    {
        $guard = $guard ?? config('nova.guard');

        return auth($guard)->user();
    }
}
