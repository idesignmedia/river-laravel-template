<?php

return [

    // DEVELOPERS
    // "Admin" model with developer privileges and Nova access
    // Must persist in this list to be a valid developer (whitelist only)
    'developers' => [
        'password' => env('RIVER_DEVELOPER_PASSWORD', 'admin#'),
        'role' => env('RIVER_DEVELOPER_ROLE', \App\Enums\AuthRole::DEVELOPER),
        'company' => 'RIVER',
        'accounts' => [
            'info@weareriver.nz'            => 'RIVER Agency',
            'isaac.r@weareriver.nz'         => 'Isaac Rolfe',
            'john.l@weareriver.nz'          => 'John Li',
            'rainui.t@weareriver.nz'        => 'Rainui Teihotua',
        ],
    ],

    // ADMIN + STAFF
    // "Admin" model with administrator privileges and Nova access
    'staff' => [
        'password' => env('RIVER_STAFF_PASSWORD', 'admin#'),
        'role' => env('RIVER_STAFF_PASSWORD', \App\Enums\AuthRole::ADMIN),
        'company' => 'RIVER Saff',
        'accounts' => [
            'staff@weareriver.nz'          => 'RIVER Staff',
        ],
    ],

    // USERS
    // "User" model with no privileges
    'users' => [
        'password' => env('RIVER_USER_PASSWORD', 'password#'),
        'role' => env('RIVER_USER_ROLE', \App\Enums\AuthRole::USER),
        'company' => null,
        'accounts' => [
            'isaac.rolfe@gmail.com'         => 'Isaac Rolfe',
        ],
    ],

    // 'users' => [
    //     'admins' => [
    //         'password' => env('RIVER_ADMIN_PASSWORD', 'password#'),
    //         'role' => env('RIVER_ADMIN_ROLE', 'editor'),
    //         'accounts' => [
    //             //..
    //         ],
    //     ],

    //     'developers' => [
    //         'password' => env('RIVER_DEVELOPER_PASSWORD', 'admin#'),
    //         'role' => env('RIVER_DEVELOPER_ROLE', 'administrator'),
    //         'accounts' => [
    //             'info@weareriver.nz'   => 'RIVER Agency',
    //         ],
    //     ],
    // ],

    // 'roles' => [
    //     'administrator' => [
    //         'name' => 'Administrator',
    //         'super' => true,
    //     ],
    //     'editor' => [
    //         'name' => 'Editor',
    //         'permissions' => [
    //             'viewNova',
    //         ],
    //     ],
    // ],
];
