<?php

// [river-laravel-dms] Universal Content config
return [

    // Optional folder to load component views from for this project
    'view_location' => base_path('resources/views/components/universalcontent'),

    // Available UCM Models
    // Used for selecting models via UniversalContentSelectorBlock Content Block
    'models' => [
        \App\Models\Page::class,
        \App\Models\ContentUpdate::class,
        \App\Models\ContentResource::class,
        \App\Models\ContentEvent::class,
    ],

    // Default Styles
    'defaults' => [
        'cards-grid-size' => '1 sm:2 md:3 lg:3',
        'cards-grid-gap' => 'normal',
        'card-title-size' => 6,
    ],

    // Content Defaults
    'content' => [
        // The default placeholder image for thumbnails
        'thumbnail_placeholder' => null, //'http://placehold.it/800x600',

        // The default placeholder image for heros
        'hero_placeholder' => null, //'http://placehold.it/1200x800'
    ],

    // Default DTOs
    'data_objects' => [
        'ModelData'     => \River\DMS\Data\ModelData::class,
        'PageData'      => \River\DMS\Data\PageData::class,
        'CardData'      => \River\DMS\Data\CardData::class,
    ],

    // Default Jobs
    'jobs' => [
        'UniversalContentSaved'     => \River\DMS\Jobs\UniversalContentSaved::class,
        'UniversalContentDeleted'   => \River\DMS\Jobs\UniversalContentDeleted::class,
    ],

    // Default Services and Support
    'services' => [
        'UniversalContentOptions'   => \River\DMS\Support\UniversalContentOptions::class,
        'UniversalContentFilters'   => \River\DMS\Support\UniversalContentFilters::class,
        'UniversalContentService'   => \River\DMS\Support\UniversalContentService::class,
        'UniversalContentEvents'    => \River\DMS\Support\UniversalContentEvents::class,
        'UniversalContentRepository'=> \River\DMS\Support\UniversalContentRepository::class,
        'UniversalContentCaching'   => \River\DMS\Support\UniversalContentCaching::class,
    ],

    // Caching
    'caching_enabled' => env('DMS_CACHING', false),
    'json_caching_enabled' => env('DMS_JSON_CACHING', false),

    // Default UCM Content Options for DefaultUniversalContentOptions
    // This is merged into DefaultUniversalContentOptions::$defaults via array_merge if provided
    'ucm_content_options' => [
        'contentBlocks' => true,
    ],

    // Default UCM Resource Options for DefaultUniversalResourceOptions
    // This is merged into DefaultUniversalResourceOptions::$defaults via array_replace_recursive if provided
    'ucm_resource_options' => null,

];
