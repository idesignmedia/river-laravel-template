<?php

// This config defines how to expand attributes into design-system classes
// Should reflect your utilities defined in 'design-system/utilities',
// and '@river-agency/design-system/scss/defaults/utilities'.
return [

    // Seperator to use for scoped attribute classes; generally '-'
    'scope-seperator' => '-',

    // Seperator to use for helper variations; generally '--'
    'helper-separator' => '--',

    // Seperator to use for utility name/value pairs; generally '--'
    'utility-separator' => '--',

    // Seperator to use for component name/value pairs; generally '--'
    // This is based within the context of your project
    'component-separator' => '--',

    // Seperator to use for a variation prefix; generally ':'
    'variation-seperator' => ':',

    /*
     * Scopes are classes that get expanded using the attribute name
     *
     * eg, scope="dark compact" => "scope-dark scope-compact"
     */
    'scopes' => [
        'scope',
        'theme',
    ],

    'addons' => [
        // \River\Ripple\Addons\Media::class,
    ],

    // Helpers are classes that get expanded using the helper class, alongside any defined variations.
    // eg, container => "layout-container" (same as :container="true")
    // eg, container="fluid" => "layout-container layout-container--fluid"
    // eg, flexCol="auto md:fixed" => "flex-col flex-col--auto md:flex-col--fixed"
    // DEPRECATED
    'helpers' => [
        // attribute => helper class

        // Layout Container
        'container'         => 'layout-container',

        // Layout Section
        'section'           => 'layout-section',

        // Styles
        // ...

        // Grid Cols
        'gridCols'          => 'grid-cols', // parent
        'gridCol'           => 'grid-col', // child

        // Flex Cols
        'flexCols'          => 'flex-columns', // parent
        'flexCol'           => 'flex-col', // child
    ],

    // Utilities are classes that get expanded using the utility class only
    // eg, text="center md:left" => "text--center md:text--left"
    // eg, color="primary.normal hover:primary.dark" => "color--primary.normal hover:color--primary.dark"
    // eg, cols="12/12 md:6/12 lg:20%" => 'width--12/12 md:width--6/12 lg:width--20%'
    'utilities' => [
        // attribute => utility class (null if the attribute is the utility class)

        // Ripple <> Tailwind
        'font'              => 'font',
        'fontFamily'        => 'font',
        'text'              => 'text',
        'fontSize'          => 'text',
        'weight'            => 'weight',
        'fontWeight'        => 'weight',
        'leading'           => 'leading',
        'lineHeight'        => 'leading',
        'tracking'          => 'tracking',
        'letterSpacing'     => 'tracking',
        'paragraph'         => 'paragraph',
        'paragraphSpacing'  => 'paragraph',

        // Width Helper
        'width'             => 'width',
        'offset'            => 'offset',

        // Grid Helper
        'colSpan'           => 'grid-span',
        'colStart'          => 'grid-start',
        'colEnd'            => 'grid-end',

        // Display
        'display'           => null,

        // Position
        'position'          => null,

        // Text
        'textAlign'         => 'text-align',

        // Flex
        'direction'         => 'direction',
        'wrap'              => 'flex-wrap',
        'shrink'            => 'flex-shrink',
        'grow'              => 'flex-grow',
        'justify'           => 'justify',
        'items'             => 'align',
        'flexDirection'     => 'direction',
        'flexWrap'          => 'flex-wrap',
        'flexShrink'        => 'flex-shrink',
        'flexGrow'          => 'flex-grow',
        'justifyContent'    => 'justify',
        'alignContent'      => 'content',
        'alignItems'        => 'align',
        'placeItems'        => 'place',
        'place'             => 'place',
        'order'             => 'order',

        // Design - Color @colors
        'color'             => 'color',
        'textColor'         => 'color',
        'bg'                => 'bg',
        'background'        => 'bg',
        'border'            => 'border',
        'borderColor'       => 'border',

        // Design - Type @type
        // 'font'              => 'font-family',
        // 'fontFamily'        => 'font-family',
        // 'fontSize'          => 'font-size',
        // 'fontWeight'        => 'font-weight',
        // 'weight'            => 'font-weight',
        // 'lineHeight'        => 'line-height',
        // 'letterSpacing'     => 'letter-spacing',
        // 'spacing'           => 'spacing',

        // Design - Dimension @sizes
        // Note, 'width' is reserved for the width helper
        'h'                 => 'h',
        'w'                 => 'w',

        // Design - Margin @sizes
        'm'                 => 'm',
        'margin'            => 'm',
        'mx'                => 'mx',
        'marginX'           => 'mx',
        'my'                => 'my',
        'marginY'           => 'my',
        'mt'                => 'mt',
        'marginTop'         => 'mt',
        'mb'                => 'mb',
        'marginBottom'      => 'mb',
        'ml'                => 'ml',
        'marginLeft'        => 'ml',
        'mr'                => 'mr',
        'marginRight'       => 'mr',

        // Design - Padding @sizes
        'p'                 => 'p',
        'padding'           => 'p',
        'px'                => 'px',
        'paddingX'          => 'px',
        'py'                => 'py',
        'paddingY'          => 'py',
        'pt'                => 'pt',
        'paddingTop'        => 'pt',
        'pb'                => 'pb',
        'paddingBottom'     => 'pb',
        'pl'                => 'pl',
        'paddingLeft'       => 'pl',
        'pr'                => 'pr',
        'paddingRight'      => 'pr',

        // Design - Gap @sizes
        'g'                 => 'gap',
        'gap'               => 'gap',
        'gy'                => 'gap-y',
        'gapY'              => 'gap-y',
        'gx'                => 'gap-x',
        'gapX'              => 'gap-x',
    ],

    // Transforms are attributes that get transformed into another class
    // eg, as-row => "flex-direction--row" (same as :as-row="true")
    // DEPRECATED
    'transforms' => [
        // Text
        'text-left'         => 'text-align--left',
        'text-center'       => 'text-align--center',
        'text-right'        => 'text-align--right',
        'text-justify'      => 'text-align--justify',
        // Flex
        'as-row'            => 'flex-direction--row',
        'as-col'            => 'flex-direction--col',
        'justify-center'    => 'justify--center',
        'align-center'      => 'align--center',
        'flex-center'       => 'justify--center align--center',
        'flex-start'        => 'justify--start content--start',
        'flex-end'          => 'justify--end content--end',
        'flex-between'      => 'justify--between content--between',
        'flex-around'       => 'justify--around content--around',
        'flex-evenly'       => 'justify--evenly content--evenly',
    ],

    'components' => [
        'button' => [
            'className' => 'style-button',
            'variations' => [],
            'defaultProps' => [
                'as' => 'button',
                'variation' => 'default', // Enforce default styling
                'size' => null,
            ],
        ],
        'clear' => [
            'className' => 'layout-clear',
            'variations' => [],
            'defaultProps' => [
                'as' => 'div',
                'variation' => null,
            ],
        ],
        'column' => [
            'className' => 'flex-col',
            'variations' => ['auto', 'full', 'fixed'],
            'defaultProps' => [
                'as' => 'div',
                'variation' => null,
            ],
        ],
        'columns' => [
            'className' => 'flex-cols',
            'variations' => ['inline', 'center', 'justify', 'equal'],
            'defaultProps' => [
                'as' => 'div',
                'variation' => null,
            ],
        ],
        'container' => [
            'className' => 'layout-container',
            'variations' => ['small', 'normal', 'large', 'fluid'],
            'defaultProps' => [
                'as' => 'div',
                'variation' => 'normal',
            ],
        ],
        'divide' => [
            'className' => 'style-divide',
            'variations' => ['vertical'],
            'defaultProps' => [
                'as' => 'div',
                'variation' => null,
            ],
        ],
        'flex' => [
            'defaultProps' => [
                'as' => 'div',
                'display' => 'flex',
                'direction' => 'row',
                'wrap' => 'wrap',
                'gap' => null,
                'center' => false,
            ],
        ],
        'grid-cell' => [
            'className' => 'grid-col',
            'variations' => ['auto', 'full'],
            'defaultProps' => [
                'as' => 'div',
                'variation' => null,
            ],
        ],
        'grid' => [
            'className' => 'grid-cols',
            'variations' => ['inline'],
            'defaultProps' => [
                'as' => 'div',
                'variation' => null,
                'columns' => 12,
                'gap' => 'normal',
            ],
        ],
        'heading' => [
            'className' => 'style-heading',
            'defaultProps' => [
                'variation' => null,
                'size' => 2,
                'asSize' => true,
            ],
        ],
        'image' => [
            'className' => 'style-image',
            'variations' => ['fullwidth', 'cover', 'contain'],
            'defaultProps' => [
                'as' => 'div',
                'variation' => null,
                'size' => null,
                'importSvg' => false,
            ],
        ],
        'link' => [
            'className' => 'style-link',
            'variations' => ['overlay'],
            'defaultProps' => [
                'variation' => 'default', // Enforce default styling
            ],
        ],
        'list-item' => [
            'className' => 'style-li',
            'variations' => [],
            'defaultProps' => [
                'as' => 'li',
                'variation' => null,
            ],
        ],
        'list' => [
            'className' => 'style-list',
            'variations' => ['ol', 'ul'],
            'defaultProps' => [
                'as' => 'ul',
                'variation' => null,
                'ol' => false,
                'ul' => false,
            ],
        ],
        'notification' => [
            'className' => 'style-notification',
            'variations' => ['text', 'success', 'info', 'warning', 'error'],
            'defaultProps' => [
                'as' => 'div',
                'variation' => null,
            ],
        ],
        'overlay' => [
            'className' => 'style-overlay',
            'variations' => [],
            'defaultProps' => [
                'variation' => null,
            ],
        ],
        'paragraph' => [
            'className' => 'style-paragraph',
            'variations' => [],
            'defaultProps' => [
                'as' => 'p',
                'variation' => null,
                'size' => null,
                'weight' => null,
                'italic' => false,
                'strong' => false,
                'emphasis' => false,
            ],
        ],
        'row' => [
            'defaultProps' => [
                'as' => 'div',
                'display' => 'flex',
                'direction' => 'row',
                'wrap' => null,
                'inline' => false,
                'gap' => 'normal',
                'center' => false,
            ],
        ],
        'section' => [
            'className' => 'layout-section',
            'variations' => ['small', 'large', 'nopadding'],
            'defaultProps' => [
                'as' => 'section',
                'variation' => null,
                'size' => null,
            ],
        ],
        'space' => [
            'className' => 'layout-space',
            'variations' => ['vertical'],
            'defaultProps' => [
                'as' => 'div',
                'variation' => null,
                'size' => 'small',
                'vertical' => false,
            ],
        ],
        'stack' => [
            'defaultProps' => [
                'as' => 'div',
                'display' => 'flex',
                'direction' => 'col',
                'wrap' => null,
                'gap' => 'small',
                'center' => false,
            ],
        ],
        'text' => [
            'className' => 'style-text',
            'variations' => [],
            'defaultProps' => [
                'as' => 'div',
                'variation' => null,
                'size' => null,
                'weight' => null,
                'italic' => false,
                'strong' => false,
                'emphasis' => false,
            ],
        ],

        'title' => [
            'className' => 'style-title',
            'variations' => [],
            'defaultProps' => [
                'variation' => null,
                'size' => 2,
                'asSize' => false,
            ],
        ],

        'video' => [
            'className' => 'style-video',
            'variations' => [],
            'defaultProps' => [
                'variation' => null,
            ],
        ],

        'icon' => [
            'className' => 'style-icon',
            'variations' => [],
            'defaultProps' => [
                'variation' => null,
                'size' => null,
                'set' => 'default',
            ],
            'iconSets' => [
                'default' => base_path('public/assets/icons'),
            ],
        ],

        'animate' => [
            'className' => 'animate',
            'variations' => [],
            'defaultProps' => [
                'as' => 'div',
                'block' => false,
                'inline' => false,

                'trigger' => 'scroll',
                'delay' => 0,
                'reverse' => false,
                'root' => null,
                'rootMargin' => null,
                'threshold' => null,
            ],
        ],
    ],
];
