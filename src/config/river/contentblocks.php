<?php

// [river-laravel-dms] Content Blocks config
return [

    // Optional folder to load component views from for this project
    'view_location' => base_path('resources/views/components/contentblocks'),

    // Default Styles
    'defaults' => [
        // Partials
        'partial-title-size' => 3,
        'partial-title-mb' => 'small',
        'partial-links-mt' => 'small',
        'partial-image-mb' => 'small',
        'partial-video-mb' => 'small',
    ],

    // Custom Blocks for this project, loaded above the Default Blocks
    'project_blocks' => [
        \App\ContentBlocks\CtaBlock::class,
    ],

    // Default Blocks
    'blocks' => [
        \River\DMS\ContentBlocks\Blocks\HeadingBlock::class,
        \River\DMS\ContentBlocks\Blocks\RichContentBlock::class,
        \River\DMS\ContentBlocks\Blocks\ContentImageBlock::class,
        \River\DMS\ContentBlocks\Blocks\ContentVideoBlock::class,
        \River\DMS\ContentBlocks\Blocks\ImageBlock::class,
        \River\DMS\ContentBlocks\Blocks\VideoBlock::class,
        \River\DMS\ContentBlocks\Blocks\ImageGalleryBlock::class,
        \River\DMS\ContentBlocks\Blocks\FilesAttachmentsBlock::class,
        \River\DMS\ContentBlocks\Blocks\ExpandableContentBlock::class,
        \River\DMS\ContentBlocks\Blocks\UniversalCardsBlock::class,
        \River\DMS\ContentBlocks\Blocks\DividerBlock::class,
        \River\DMS\ContentBlocks\Blocks\CodeBlock::class,

        // Content Template
        \App\ContentBlocks\ContentTemplateBlock::class,
    ],

    // Child Blocks
    'child_blocks' => [
        \River\DMS\ContentBlocks\Child\ContentChildBlock::class,
        \River\DMS\ContentBlocks\Child\LinkFormattedChildBlock::class,
        \River\DMS\ContentBlocks\Child\LinkChildBlock::class,
        \River\DMS\ContentBlocks\Child\UniversalContentSelectorBlock::class,
        \River\DMS\ContentBlocks\Child\PageSelectorBlock::class,
    ],

    'child_aliases' => [
        'child-content' => \River\DMS\ContentBlocks\Child\ContentChildBlock::class,
        'child-link-formatted' => \River\DMS\ContentBlocks\Child\LinkFormattedChildBlock::class,
        'child-link' => \River\DMS\ContentBlocks\Child\LinkChildBlock::class,
        'ucm-selector' => \River\DMS\ContentBlocks\Child\UniversalContentSelectorBlock::class,
        'page-selector' => \River\DMS\ContentBlocks\Child\PageSelectorBlock::class,
    ],

    // Block Services
    'resolver' => \River\DMS\ContentBlocks\Resolver::class,
    'renderer' => \River\DMS\ContentBlocks\Renderer::class,

];
