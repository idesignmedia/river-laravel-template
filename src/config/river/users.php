<?php

return [
    // The default auth guards for users.
    'guards' => [
        'default'   => 'web',
        'admin'     => 'admin', // 'nova'
    ],
];
