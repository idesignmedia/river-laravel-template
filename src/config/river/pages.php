<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Page Models
    |--------------------------------------------------------------------------
    |
    */
    'model' => \App\Models\Page::class,

    /*
     * The Nova Resource used for managing Pages, used in Nova Relation fields
     */
    'resource' => \App\Nova\Resources\PageResource::class,

    /*
     * What model to use for the NavigationGroup, linked to the pages:sync command.
     * Note you will need to define your own NavigationGroupResource locally for Nova.
     */
    'navigation_model' => \App\Models\NavigationGroup::class,

    /*
     * The Flexible Block to use within the NavigationGroup Model and Resource,
     * to determine what can be selected and attached to a NavigationGroup.
     * This will generally be the UniversalContentSelectorBlock, or the PageSelectorBlock.
     *
     * For the UniversalContentSelectorBlock, this will allow models defined in config.river.universalcontent.models.
     * Note, you will need to re-sync navigation after changing the Block
     * (as the "key" identifier changes, such as 'ucm-selector' vs 'page-selector').
     */
    'navigation_block' => \River\DMS\ContentBlocks\Child\UniversalContentSelectorBlock::class,
    // 'navigation_block' => \River\DMS\ContentBlocks\Child\PageSelectorBlock::class,

    /*
     * What helper class to use for the PageHelper, which supports with gathering pages and the like.
     */
    'helper_class' => \River\DMS\Support\PageHelper::class,


    /*
    |--------------------------------------------------------------------------
    | Page Templates
    |--------------------------------------------------------------------------
    |
    | Templates can be selected from the Nova admin and allow you to
    | define custom fields.
    |
    */

    // 'default_template' => \River\DMS\View\Templates\SyncedPageTemplate::class,

    'templates' => [
        'cms'       => \App\View\PageTemplates\CmsPageTemplate::class,
        'home'      => \App\View\PageTemplates\HomePageTemplate::class,
        'synced'    => \App\View\PageTemplates\SyncedPageTemplate::class,
        'ucm'       => \App\View\PageTemplates\UcmPageTemplate::class,
    ],

    /*
     * The default template to use (when creating a Page in Nova).
     */
    'default_template' => 'cms',


    /*
    |--------------------------------------------------------------------------
    | Fallback Handlers
    |--------------------------------------------------------------------------
    |
    | Fallback handlers control how content models are rendered. By default we
    | have handlers for Redirects, CMS Pages, and Static Pages.
    |
    */
    'fallback_handlers' => [
        \River\DMS\Http\Handlers\RedirectHandler::class => [
            \App\Models\Redirect::class,
        ],

        \River\DMS\Http\Handlers\CmsPageHandler::class => [
            \App\Models\Page::class,
        ],

        \River\DMS\Http\Handlers\StaticPageHandler::class => [
            'pages.static',
        ],
    ],


    /*
    |--------------------------------------------------------------------------
    | Navigation Groups
    |--------------------------------------------------------------------------
    |
    | Navigation Groups managed via the pages.sync command, to provide editable Navigation through Nova.
    | Pages can have the following formats:
    | 'pages' => ['page slug', ..., ['page slug', 'menu name'], ...]
    |
    */
    'navigation' => [
        [
            'name'          => 'Main Menu',
            'slug'          => 'main',
            'description'   => 'Pages to include in the main menu. This needs to be managed carefully, as it may impact responsive support on mobile.',
            'is_locked'     => true,
            'pages'         => ['about', 'updates'],
        ],
        [
            'name'          => 'More Menu',
            'slug'          => 'more',
            'description'   => 'Additional pages to include in the "More" main menu dropdown. Skipped if no pages are added.',
            'is_locked'     => false,
            'pages'         => ['resources', 'events', ['contact', 'Contact']],
        ],
        [
            'name'          => 'Footer Menu',
            'slug'          => 'footer',
            'description'   => 'Pages to include in the Footer menu. These should support a continuous user journey.',
            'is_locked'     => true,
            'pages'         => ['about', 'updates', 'events', 'resources', ['contact', 'Contact']],
        ],
    ],

    /*
     * Whether to do a full delete and resync of navigation when running pages.sync
     */
    'resync_navigation' => false,


    /*
    |--------------------------------------------------------------------------
    | Default Attributes
    |--------------------------------------------------------------------------
    |
    | Optionally define any attributes that you want applied to all pages during
    | the sync process. These can be overwritten on individual pages.
    | You may want to override any UCM defaults, or Page defaults.
    |
    */
    'default_page_attributes' => [
        // Make synced pages public by default
        'status'        => \River\DMS\Enums\ContentStatus::PUBLIC,

        // Use the synced Page Template by default
        'template'      => 'synced',

        // Prevent synced pages from being deleted from the admin
        'is_locked'     => true,

        // Note the optional content_intro (project specific)
        'data_json' => [
            'content_intro' => null,
        ],
    ],


    /*
    |--------------------------------------------------------------------------
    | Synced Pages
    |--------------------------------------------------------------------------
    |
    | Define the list of pages to sync.
    | Each set of page attributes must have at least a 'slug', and/or a 'name', at a minimum (all other attributes are optional).
    | You can define any valid attributes on the page model, which get pushed directly into the database.
    | You can also define a 'children' array within a set of page attributes, following the same attributes format for child pages.
    | You can also define a custom 'data' attribute which does not get pushed into the database.
    |
    */
    'pages' => [
        [
            'slug'          => '/',
            'name'          => 'Home',
            'template'      => 'home',
            'data'          => [],
            'summary'       => 'Unlimited power, unlimited flexibility...',
            'data_json'     => [
                'content_intro' => 'On another level...',
            ],
        ],
        [
            'slug'          => 'about',
            'name'          => 'About Us',
            'template'      => 'synced',
            'data'          => ['menu' => true, 'footer' => true],
            // Placeholder Content
            'summary'       => 'Our philosophy, story, and what we\'re all about...',
            'data_json'     => [
                'content_intro' => 'Our story, our journey...',
            ],
            'content'       => 'Content. Nemo tempore voluptatem rerum nam earum nulla molestiae. Consequuntur cum iusto quo quia quo architecto tenetur laboriosam ut. Quam enim illo ea qui voluptates. Ut doloremque necessitatibus exercitationem.',
            'content_related' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam era.',
            // Children
            'children' => [
                [
                    // We use a Synced page instead of UCM Index for team, as we manually bring in Team Members underneath CMS content
                    'name'          => 'Our Team',
                    'slug'          => 'team',
                    'template'      => 'synced',
                    'summary'       => 'Meet the team behind it all...',
                    'content'       => 'Content. Nemo tempore voluptatem rerum nam earum nulla molestiae. Consequuntur cum iusto quo quia quo architecto tenetur laboriosam ut. Quam enim illo ea qui voluptates. Ut doloremque necessitatibus exercitationem.',
                ],
            ],
        ],
        [
            'slug'          => 'updates',
            'name'          => 'Updates',
            'template'      => 'ucm',
            'data'          => ['menu' => true, 'footer' => true],
            'summary'       => 'Keep up with the latest...',
        ],
        [
            'name'          => 'Resources',
            'slug'          => 'resources',
            'template'      => 'ucm',
            'data'          => ['more' => true, 'footer' => true],
            'summary'       => 'Learn and improve...',
        ],
        [
            'name'          => 'Events',
            'slug'          => 'events',
            'template'      => 'ucm',
            'data'          => ['more' => true, 'footer' => true],
            'summary'       => 'Find out what\'s happening...',
        ],
        [
            'slug'          => 'contact',
            'name'          => 'Contact Us',
            'template'      => 'synced',
            'data'          => ['more' => true, 'footer' => true],
            'summary'       => 'Get in touch with us today...',
        ],

        // Test Page
        // Synced via PageSeeder.php if Dummy Content is enabled
        // [
        //     'slug'          => 'test',
        //     'name'          => 'Test Page',
        //     'template'      => 'cms',
        //     'is_locked'     => false,
        //     // Placeholder Content
        //     'summary'       => 'This is a test CMS page...',
        //     'content'       => 'Content. Lorem nemo tempore voluptatem rerum nam earum nulla molestiae. Consequuntur cum iusto quo quia quo architecto tenetur laboriosam ut.',
        //     'data_json'     => [
        //         'content_intro' => 'Testing...',
        //     ],
        // ],
    ],
];
