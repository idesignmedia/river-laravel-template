<?php

return [

    /*
     * Nova User resource tool class.
     */
    'userResource' => \App\Nova\Resources\AdminResource::class,

    /*
     * The group associated with the resource
     */
    'roleResourceGroup' => 'Management / Admin',
];
