<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Application Tag
    |--------------------------------------------------------------------------
    |
    | Similar to application env, the app tag indicates the named environment of
    | the application. Since env can only be set to "production", "local", or
    | "test", The app tag is used to identify deployment & development environments
    | such as "UAT".
    |
    | The app tag is also be used to partition assets between environments.
    */
    'tag' => env('APP_TAG', env('APP_ENV', 'production')),


    /*
    |--------------------------------------------------------------------------
    | Force App URL
    |--------------------------------------------------------------------------
    |
    | For laravel to redirect to the configured app url. Set this to true when
    | you do not want the application to be served on multiple domains.
    |
    */
    'force_app_url' => env('APP_FORCE_URL', false),


    /*
    |--------------------------------------------------------------------------
    | No Index
    |--------------------------------------------------------------------------
    |
    | 'noindex' is used to determine whether the application should be indexed.
    | If true, a noindex tag is added to the <head> of the application html
    | (this is managed in the 'views/layouts/page.blade.php' blade layout).
    | The application should only be indexed if it is in production,
    | and we haven't told it otherwise.
    |
    */
    'noindex' => env('APP_NOINDEX', env('APP_ENV', 'production') !== 'production'),


    /*
    |--------------------------------------------------------------------------
    | Coming Soon
    |--------------------------------------------------------------------------
    |
    | Used to determine whether to put the application behind a simple
    | "Coming Soon" page.
    | If true, only the '/' route will be enabled, and it will point directly to
    | the 'views/layouts/coming-soon.blade.php' blade layout.
    |
    */
    'coming_soon' => env('APP_COMING_SOON', false),

    /*
    |--------------------------------------------------------------------------
    | Demo Pages
    |--------------------------------------------------------------------------
    |
    | Identifies whether the /demo pages should be
    | loaded for viewing (loaded via routes/web.php).
    |
    */
    'demo_pages' => env('APP_DEMO_PAGES', env('APP_ENV') === 'local'),


    /*
    |--------------------------------------------------------------------------
    | Auth
    |--------------------------------------------------------------------------
    |
    |
    */
    'auth' => [
        /*
        * Turn on user accounts?
        */
        'enabled' => env('AUTH_ENABLED', false),

        /*
        * Turn on the user registration form?
        */
        'registration' => env('AUTH_REGISTRATION', true),

        /*
        * Users may reset their passwords?
        */
        'password_resets' => env('AUTH_PASSWORD_RESETS', true),

        /*
        * Users must verify their email address?
        */
        'email_verification' => env('AUTH_EMAIL_VERIFICATION', false),
    ],

    /*
    |--------------------------------------------------------------------------
    | Emails
    |--------------------------------------------------------------------------
    | Route all mail messages to the testing email defined when not
    | in production environments
    |
    */
    'system_email' => [
        'enabled' => env('SYSTEM_EMAIL_ENABLED', false),
        'account' => env('SYSTEM_EMAIL_ACCOUNT', null),
    ],
];
