<?php

return [
    // General Info
    'name'                  => env('APP_NAME', 'RIVER'),
    'contact_phone'         => null,
    'contact_email'         => env('MAIL_FROM_ADDRESS', 'info@riverproject.co.nz'),

    // General emails
    'email_sender'          => env('MAIL_FROM_ADDRESS', 'mailer@riverproject.co.nz'),
    'email_sender_name'     => env('APP_NAME', 'RIVER'),
    'email_recipient'       => env('MAIL_TO_ADDRESS', 'info@riverproject.co.nz'),

    // Admin emails
    // 'admin_email_sender'          => env('MAIL_FROM_ADDRESS', 'mailer@riverproject.co.nz'),
    // 'admin_email_sender_name'     => 'Website',
    // 'admin_email_recipient'       => env('MAIL_TO_ADDRESS', 'info@riverproject.co.nz'),

    // Social Links
    'social' => [
        'facebook'          => 'https://www.facebook.com/',
        'instagram'         => 'https://www.instagram.com/',
        'tiktok'            => 'https://www.tiktok.com/',
    ],

    // System details
    'system_name'               => 'SYSTEM',
];
