<?php

return [
    /*
     * User model class name.
     */
    'userModel' => \App\Models\Admin::class,

    /*
     * Configure Brandenburg to not register its migrations.
     */
    'ignoreMigrations' => true,
];
