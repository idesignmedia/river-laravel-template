<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Revision Model
    |--------------------------------------------------------------------------
    |
    | Must implement \River\Revisionable\Contracts\Revision
    */
    'model' => \River\Revisionable\Models\Revision::class,

    /*
    |--------------------------------------------------------------------------
    | Revision Database
    |--------------------------------------------------------------------------
    |
    | You may specify a custom database connection and table name to
    | to store revisions in. The Revision model will use the default
    | connection if `null` is provided.
    |
    */
    'database' => [
        'connection' => null,
        'table' => 'revisions',
    ],

    /*
    |--------------------------------------------------------------------------
    | Deleting Revisions
    |--------------------------------------------------------------------------
    |
    | You can configure revisions to be deletable. When `false` and exception
    | will be thrown when attempting to call $revision->delete() or
    | Revision::destroy(...)
    |
    | !IMPORTANT: The package will not prevent Revision::truncate() calls
    |
    */
    'revisions_can_be_deleted' => false,
];
