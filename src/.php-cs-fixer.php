#!/usr/bin/php

<?php

$finder = Symfony\Component\Finder\Finder::create()
    ->exclude('vendor')
    ->exclude('bootstrap')
    ->exclude('storage')
    ->exclude('public')
    ->exclude('node_modules')
    ->in(__DIR__)
    ->name('*.php')
    ->notName('*.blade.php')
    ->ignoreDotFiles(true)
    ->ignoreVCS(true);

return (new \PhpCsFixer\Config())
    ->setRules([
        '@PSR2'                                     => true,
        'align_multiline_comment'                   => true,
        'array_indentation'                         => true,
        'array_syntax'                              => ['syntax' => 'short'],
        'blank_line_after_namespace'                => true,
        'linebreak_after_opening_tag'               => true,
        'cast_spaces'                               => ['space' => 'single'],
        'declare_strict_types'                      => false,
        'constant_case'                             => ['case' => 'lower'],
        'method_argument_space'                     => true,
        'ordered_imports'                           => ['sort_algorithm' => 'alpha'],
        'multiline_whitespace_before_semicolons'    => true,
        'echo_tag_syntax'                           => ['format' => 'long'],
        'no_unused_imports'                         => true,
        'no_useless_else'                           => true,
        'not_operator_with_successor_space'         => true,
        'phpdoc_add_missing_param_annotation'       => true,
        'phpdoc_indent'                             => true,
        'phpdoc_no_package'                         => true,
        'phpdoc_order'                              => true,
        'phpdoc_separation'                         => true,
        'phpdoc_single_line_var_spacing'            => true,
        'phpdoc_trim'                               => true,
        'phpdoc_var_without_name'                   => true,
        'phpdoc_to_comment'                         => true,
        'return_type_declaration'                   => ['space_before' => 'none'],
        'single_quote'                              => true,
        'ternary_operator_spaces'                   => true,
        'trailing_comma_in_multiline'               => ['elements' => ['arrays']],
        'trim_array_spaces'                         => true,
        'unary_operator_spaces'                     => true,
        'visibility_required'                       => true,
        'whitespace_after_comma_in_array'           => true,
    ])
    ->setFinder($finder);
