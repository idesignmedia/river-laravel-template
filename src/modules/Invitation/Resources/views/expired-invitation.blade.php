<x-page title="Invitation Expired" class="auth-page" bg="primary.bg" view="layouts.page-headless">
    <x-section>
        {{-- Expired Invitation --}}
        <x-box class="auth-form">
            <x-title size="4" mb="small">{{ __('Invitation Expired') }}</x-title>
            <x-text>
                Sorry, your invitation has expired. <br>
                Please ask your administrator to send a new link.
            </x-text>
            <x-space mb="normal"></x-space>
            <div class="form-group">
                <!-- TODO: need to replace this with frontend login or use login route -->
                {{-- {{ route('login') }} --}}
                <a class="link" href="/#login">{{ __('Back to Login') }}</a>
            </div>
        </x-box>

    </x-section>
</x-page>
