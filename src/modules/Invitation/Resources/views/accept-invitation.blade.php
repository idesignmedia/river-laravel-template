<x-page title="Accept Invitation" class="auth-page" bg="primary.bg" view="layouts.page-headless">
    <x-section>
        <x-container>

            <x-box class="auth-form">
                {{-- <x-image class="auth-form__logo" src="company/logo.svg" /> --}}

                <x-title size="4">Welcome</x-title>
                <x-text>Create a password for your account to continue.</x-text>

                @if($notification = Session::get('notification'))
                    <x-notification :variation="$notification['state']" my="xs">
                        {{ $notification['message'] }}
                    </x-notification>
                @endif

                <x-form.provider mt="sm" action="{{ route('invitation.accept', ['code' => $invitation->code]) }}">
                    <x-form.control
                        label="{{ __('Password') }}"
                        placeholder="Password *"
                        name="password"
                        type="password"
                        value="{{ old('password') }}"
                        error="{{ $errors->first('password') }}"
                        autofocus
                    />

                    <x-form.control
                        label="{{ __('Confirm Password') }}"
                        placeholder="Confirm Password *"
                        name="password_confirmation"
                        type="password"
                        error="{{ $errors->first('password_confirmation') }}"
                    />

                    <x-button type="submit" variation="default" width="full">
                        {{ __('Create Password') }}
                    </x-button>
                </x-form.provider>
            </x-box>

        </x-container>
    </x-section>
</x-page>
