<?php
declare(strict_types=1);

namespace Modules\Invitation;

use \Exception;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Modules\Invitation\Contracts\Invitable;
use Modules\Invitation\Enums\InvitationStatus;
use Modules\Invitation\Notifications\AccountInvitation;

/**
 * Invitation Service
 */
class InvitationService
{
    /**
     * Invitation expiry time in minutes
     *
     * @var integer
     */
    private $expiry;

    /**
     * Constructor
     */
    public function __construct(int $expiry)
    {
        $this->expiry = $expiry;
    }

    /**
     * Create and send a new invitation
     */
    public function invite(Invitable $model): void
    {
        $invitation = $this->createInvitation($model);
        $model->notify(new AccountInvitation($invitation));
    }

    /**
     * Create an invitation
     */
    public function createInvitation(Invitable $model): Invitation
    {
        $expiry = Carbon::now()->addMinutes($this->expiry);

        // Can refactor later to be unique / salted
        $code = Str::random(64);

        $invitation                 = new Invitation;
        $invitation->code           = $code;
        $invitation->email          = $model->email;
        $invitation->model_id       = $model->id;
        $invitation->status         = InvitationStatus::PENDING;
        $invitation->is_valid_until = $expiry;
        $invitation->save();

        return $invitation;
    }


    public function get($code)
    {
        return $this->interface->setCode($code)->get();
    }
    public function status($code)
    {
        return $this->interface->setCode($code)->status();
    }
    public function isValid($code)
    {
        return $this->interface->setCode($code)->isValid();
    }
    public function isExpired($code)
    {
        return $this->interface->setCode($code)->isExpired();
    }
    public function isPending($code)
    {
        return $this->interface->setCode($code)->isPending();
    }
    public function isAllowed($code, $email)
    {
        return $this->interface->setCode($code)->isAllowed($email);
    }
    public function consume($code)
    {
        return $this->interface->setCode($code)->consume();
    }
    public function cancel($code)
    {
        return $this->interface->setCode($code)->cancel();
    }
    public function reminder($code)
    {
        return $this->interface->setCode($code)->reminder();
    }
    public function validateEmail($email)
    {
        if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new Exception('Invalid Email Address', 1);
        }
        return $this;
    }
}
