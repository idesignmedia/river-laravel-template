<?php

declare(strict_types=1);
namespace Modules\Invitation\Providers;

use App\Support\ModuleServiceProvider;
use Illuminate\Support\Facades\Gate;
use Modules\Invitation\InvitationService;

// use Silvanite\Brandenburg\Traits\ValidatesPermissions;

class InvitationServiceProvider extends ModuleServiceProvider
{
    // use ValidatesPermissions;

    /**
     * Module name
     *
     * @var string
     */
    protected $name = 'Invitation';

    /**
     * Module version
     *
     * @var string
     */
    protected $version = '1.0';

    /**
     * Nova Resources
     *
     * @var array
     */
    protected $resources = [
        //'App\Blog\Nova\BlogPost',
    ];

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Blog\BlogPost' => 'App\Blog\Policies\BlogPostPolicy',
    ];

    /**
     * Register module dependencies
     */
    public function register(): void
    {
        parent::register();

        // load views from resources folder
        $this->loadViewsFrom(__DIR__ . '/../Resources/views/', 'Invitation'); // Invitation Module

        // migrations
        $this->loadMigrationsFrom([
            __DIR__ . '/../database/migrations',
        ]);

        // Set up Invitation Service
        $this->app->singleton('Modules\Invitation\InvitationService', function ($app) {
            $expiry  = 4320; // 72 hours
            $service = new InvitationService($expiry);
            return $service;
        });
    }

    /**
     * Register gates for this module
     */
    public function registerGates(): void
    {
        // collect([
        //     'View Blog',
        //     'Manage Blog',
        // ])->each(function ($permission) {
        //     Gate::define($permission, function ($user) use ($permission) {
        //         if ($this->nobodyHasAccess($permission)) {
        //             return true;
        //         }
        //         return $user->hasRoleWithPermission($permission);
        //     });
        // });
    }
}
