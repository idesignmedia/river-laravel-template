<?php

declare(strict_types=1);

namespace Modules\Invitation\Enums;

use BenSampo\Enum\Enum;

final class InvitationStatus extends Enum
{
    public const PENDING    = 'pending';
    public const SUCCESSFUL = 'successful';
    public const CANCELLED  = 'cancelled';
    public const EXPIRED    = 'expired';

    /**
     * A list of human-readable labels for each value.
     */
    public static $labels = [
        'pending'    => 'Pending',
        'successful' => 'Successful',
        'cancelled'  => 'Cancelled',
        'expired'    => 'Expired',
    ];
}
