<?php
declare(strict_types=1);

namespace Modules\Invitation;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Invitation\Enums\InvitationStatus;

class Invitation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invitations';

    protected $casts = [
        'is_valid_until' => 'date',
    ];

    /**
     * Referral User
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'model_id');
    }

    /**
     * Can this invitation be accepted
     */
    public function canAccept(): bool
    {
        return $this->status === InvitationStatus::PENDING
            && $this->is_valid_until->gte(Carbon::now());
    }

    /**
     * Can this invitation be accepted
     */
    public function isExpired(): bool
    {
        return $this->is_valid_until->gte(Carbon::now());
    }
}
