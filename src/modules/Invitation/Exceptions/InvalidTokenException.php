<?php
declare(strict_types=1);

namespace Modules\Invitation\Exceptions;

use Exception;

class InvalidTokenException extends Exception
{
    //
}
