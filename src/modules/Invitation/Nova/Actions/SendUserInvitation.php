<?php
declare(strict_types=1);

namespace Modules\Invitation\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Http\Requests\NovaRequest;

class SendUserInvitation extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;



    /**
     * Perform the action on the given models.
     *
     *
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $invitationService = resolve(\Modules\Invitation\InvitationService::class);

        foreach ($models as $model) {
            $invitationService->invite($model);
        }
    }

    /**
     * Get the fields available on the action.
     */
    public function fields(NovaRequest $request): array
    {
        return [];
    }
}
