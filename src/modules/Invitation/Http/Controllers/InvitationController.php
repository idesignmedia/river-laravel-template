<?php
declare(strict_types=1);

namespace Modules\Invitation\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;
use Modules\Invitation\Enums\InvitationStatus;
use Modules\Invitation\Http\Requests\AcceptInvitationRequest;
use Modules\Invitation\Invitation;
use Modules\Invitation\InvitationService;

class InvitationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the form to accept an invitation
     */
    public function accept(string $code, Request $request, InvitationService $invitationService): View
    {
        $invitation = Invitation::select()
            ->where('code', $code)
            ->first();

        if (! $invitation) {
            abort(404);
        }

        if (! $invitation->canAccept()) {
            return view('Invitation::expired-invitation');
        }

        return view('Invitation::accept-invitation', [
            'invitation' => $invitation,
        ]);
    }

    /**
     * Handle accept invitation request
     */
    public function submit(string $code, AcceptInvitationRequest $request): Response
    {
        $invitation = Invitation::select()
            ->where('code', $code)
            ->first();

        if (! $invitation) {
            abort(404);
        }

        if (! $invitation->canAccept()) {
            return view('Invitation::expired-invitation');
        }

        // Retrieve Password
        $password = $request->password;

        // Retrieve User
        $user = $invitation->user;

        // Update User Password
        $user->password = Hash::make($password);
        $user->save();

        Auth::guard()->login($user);

        $invitation->status = InvitationStatus::SUCCESSFUL;
        $invitation->save();

        return redirect($this->redirectTo());
    }

    /**
     * Get the post register / login redirect path.
     */
    protected function redirectTo(): string
    {
        return '/';
    }
}
