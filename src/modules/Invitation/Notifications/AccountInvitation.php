<?php
declare(strict_types=1);

namespace Modules\Invitation\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Modules\Invitation\Invitation;

class AccountInvitation extends Notification
{
    /**
     * Invitation model
     *
     * @var Invitation
     */
    public $invitation;

    /**
     * Create a notification instance.
     *
     *
     * @return void
     */
    public function __construct(Invitation $invitation)
    {
        $this->invitation = $invitation;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array|string
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     */
    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('Account Invitation')
            ->line('Welcome to Knode.')
            ->line('You have been invited to join an organisation.')
            ->action('Accept Invitation', route('invitation.accept', ['code' => $this->invitation->code]))
            ->line('This invitation will expire in 7 days.');
    }
}
