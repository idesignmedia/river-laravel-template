<?php
declare(strict_types=1);

namespace Modules\Invitation;

use Illuminate\Database\Eloquent\Relations\HasMany;

trait HasInvitations
{
    /**
     * Return all model invitations
     */
    public function invitations(): HasMany
    {
        return $this->hasMany(\Modules\Invitation\Invitation::class);
    }
    /**
     * Return all successful invitations
     */
    public function invitationSuccess(): HasMany
    {
        return $this->invitations()->where('status', 'successful');
    }
    /**
     * Return all pending invitations
     */
    public function invitationPending(): HasMany
    {
        return $this->invitations()->where('status', 'pending');
    }
}
