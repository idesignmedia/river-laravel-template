<?php

namespace Modules\Example;

use Illuminate\Console\Scheduling\Schedule;
use River\Core\Support\Module;

class ExampleModule extends Module
{
    /**
     * List of Artisan commands to register
     *
     * @var array
     */
    protected $commands = [
        // Commands\Command::class,
    ];

    /**
     * List of Event Listeners to register
     *
     * @var array
     */
    protected $listen = [
        // Event::class => [
        //     Listener::class,
        // ],
    ];

    /**
     * List of Blade components to register
     *
     * @var array
     */
    protected $components = [
        // Components\Component::class,
    ];


    /**
     * Hook onto the register lifecycle method in the module provider
     */
    protected function onRegister()
    {
        //
    }

    /**
     * Hook onto the boot lifecycle method in the module provider
     */
    protected function onBoot()
    {
        //
    }

    /**
     * Schedule commands that should be run periodically
     *
     * @var array
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
    }
}
