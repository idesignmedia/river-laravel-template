<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Artisan;

trait CreatesApplication
{
    /**
     * Has the database been set up?
     *
     * @var boolean
     */
    protected static $databaseSetUp = false;

    /**
     * Creates the application in a state ready to test.
     */
    public function createApplication(): Application
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        // We'll ensure the database is set up prior to running tests.
        // This means we're starting from the following point for the tests:
        // - Database migrations run
        // - Database seeds run with seed dummy content enabled
        // - Pages:sync run
        //
        // You can change the configuration below as needed for the project.
        //
        // When writing tests, use the DatabaseTransactions trait to wrap
        // each test in a transaction then reset to the point set up below.
        //
        // Note: if you use RefreshDatabase this can be dangerous as it will
        // reset the database by running migrations but *not* seeds as below.
        if (! static::$databaseSetUp) {
            config(['database.seed_content' => false]);
            config(['database.seed_images' => false]);
            config(['database.seed_dummy_content' => true]);
            config(['river.universalcontent.caching_enabled' => false]);
            config(['river.universalcontent.json_caching_enabled' => false]);

            Artisan::call('migrate --seed');
            //Artisan::call('pages:sync');
            static::$databaseSetUp = true;
        }

        return $app;
    }
}
