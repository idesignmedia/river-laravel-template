<?php

namespace Tests\Browser;

use PHPUnit\Framework\Attributes\Test;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

final class HomePageTest extends DuskTestCase
{
    // use RefreshDatabase;

    #[Test]
    public function visit(): void
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit('/')
                ->assertSourceHas(
                    '<div class="mb--smallest font-size--title-3 sm:font-size--title-2 md:font-size--title-1 style-title style-title--1">RIVER Laravel Template</div>'
                );
        });
    }
}
