<?php

namespace Tests\Browser;

use PHPUnit\Framework\Attributes\Test;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

final class RegisterTest extends DuskTestCase
{
    use RefreshDatabase;

    #[Test]
    public function validationRules(): void
    {
        $user = User::factory()->create();

        $this->browse(function (Browser $browser) use ($user) {
            $browser
                ->visit('/register')
                ->press('Continue')
                ->assertPathIs('/register')
                ->assertSee('The name field is required.')
                ->assertSee('The email field is required.')
                ->assertSee('The password field is required.');

            $browser
                ->visit('/register')
                ->type('password', 'pass')
                ->press('Continue')
                ->assertPathIs('/register')
                ->assertSee('The password must be at least 8 characters.');

            // $browser
            //     ->visit('/register')
            //     ->type("email", $user->email)
            //     ->press('Continue')
            //     ->assertPathIs('/register')
            //     ->assertSee('The email has already been taken.');
        });
    }

    #[Test]
    public function registrationWorks(): void
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit('/register')
                ->type('name', 'Foo Bar')
                ->type('email', 'foo@bar.com')
                ->type('password', 'password123456789')
                ->press('Continue')
                ->assertPathIs(RouteServiceProvider::HOME);
        });
    }
}
