<?php

namespace Tests\Browser;

use PHPUnit\Framework\Attributes\Test;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

final class LoginTest extends DuskTestCase
{
    use RefreshDatabase;

    /**
     * Temporal solution for cleaning up session
     */
    protected function setUp(): void
    {
        parent::setUp();

        foreach (static::$browsers as $browser) {
            $browser->driver->manage()->deleteAllCookies();
        }
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->browse(function (Browser $browser) {
            $browser->driver->manage()->deleteAllCookies();
        });
    }

    /** @test */
    // public function loginWorks()
    // {
    //     $admin = User::factory()->admin()->create();

    //     $this->browse(function (Browser $browser) use ($admin) {

    //         $browser
    //             ->logout()
    //             ->visit('/login')
    //             ->type('email', $admin->email)
    //             ->type('password', 'password')
    //             ->press('Login')
    //             ->assertPathIs(RouteServiceProvider::HOME);
    //     });
    // }

    #[Test]
    public function validationRules(): void
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit('/login')
                ->type('email', 'foo@bar.com')
                ->type('password', 'password')
                ->press('Login')
                ->assertPathIs('/login')
                ->assertSee('These credentials do not match our records.');

            $browser
                ->visit('/login')
                ->press('Login')
                ->assertPathIs('/login')
                ->assertSee('The email field is required.')
                ->assertSee('The password field is required.');
        });
    }
}
