<?php

namespace Tests\Unit\Database;

use PHPUnit\Framework\Attributes\Test;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * A simple unit test class to ensure that the Database can be seeded
 */
final class DatabaseSeedTest extends TestCase
{
    use DatabaseTransactions;

    // TESTS

    #[Test]
    public function database_seeds_can_run(): void
    {
        // @debt this is an error with DB seeding for Unit Tests; @todo resolve

        // Manage Seeding & Caching
        config(['database.seed_content' => true]);
        config(['database.seed_images' => false]);
        config(['database.seed_dummy_content' => true]);
        config(['river.universalcontent.caching_enabled' => false]);
        config(['river.universalcontent.json_caching_enabled' => false]);

        // Set Database to seed with dummy data
        // config(['database.seed_dummy_content' => true]);
        $seedDummyContent = config('database.seed_dummy_content');
        $this->assertEquals(true, $seedDummyContent);

        // Seed the Database with Dummy Data (and ensure no exceptions occur)
        $this->artisan('db:seed');

        // Confirm no exceptions
        $this->assertEquals(true, true);
    }

}
