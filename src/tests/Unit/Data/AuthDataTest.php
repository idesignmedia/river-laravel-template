<?php

namespace Tests\Unit\Data;

use PHPUnit\Framework\Attributes\Test;
use App\Data\AuthData;
use App\Models\User;
use Tests\TestCase;

/**
 * A simple unit test class to ensure that tests are running as expected
 */
final class AuthDataTest extends TestCase
{
    /**
     *
     * A basic feature test example.
     * This test confirm user data can be created with the expected format.
     */
    #[Test]
    public function can_create_user_data(): void
    {
        $data = [
            'id'           => 2,
            'name'         => 'John Smith',
            'email'        => 'john@smith.com',
            'label'        => 'Admin',
            'role'         => 'Admin',
            'addedLabel'   => '12 Jan 2022',
            'addedAdjunct' => 'One month ago',
            'class'        => User::class,
            'type'         => 'user',
            'permissions'  => [],
        ];

        $dto = AuthData::from($data);

        $this->assertEquals(
            $data['id'],
            $dto->id
        );

        $this->assertEquals(
            $data['name'],
            $dto->name
        );

        $this->assertEquals(
            $data['email'],
            $dto->email
        );

    }
}
