<?php

namespace Tests\Feature;

use PHPUnit\Framework\Attributes\Test;
use App\Enums\AuthRole;
use App\Models\Admin;
use App\Models\ContentUpdate;
use River\DMS\Enums\ContentStatus;
use Tests\TestCase;

final class RouteTest extends TestCase
{
    // Get an Admin Developer for Testing (from the freshly seeded DB, as per CreatesApplication.php)
    public function getAdminDeveloper(): Admin
    {
        return Admin::where('role', AuthRole::DEVELOPER)->first();
    }

    // WEBSITE + DMS

    #[Test]
    public function home_pages_load(): void
    {
        // Home
        $response = $this->get('/');
        $response->assertOk();
    }

    #[Test]
    public function content_pages_load(): void
    {
        // About
        $response = $this->get('/about');
        $response->assertOk();
    }

    #[Test]
    public function ucm_updates_pages_load(): void
    {
        // Index
        $response = $this->get('/' . ContentUpdate::routePrefix());
        $response->assertOk();

        // Show (from Seed)
        $provider = ContentUpdate::where('status', ContentStatus::PUBLIC)->first();
        if ($provider) {
            $url = $provider->getPublishedUrl();
            $response = $this->get($url);
            $response->assertOk();
        }
    }

    // WEB APP

    #[Test]
    public function user_app_redirects_to_login(): void
    {
        if(! config('features.auth.enabled')) {
            $this->assertTrue(true);
            return;
        }

        // Ensure login redirect works
        $response = $this->get(\App\Providers\RouteServiceProvider::USER_PREFIX);
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));

        // Ensure login works
        $response = $this->get(route('login'));
        $response->assertOk();
    }

    #[Test]
    public function dashboard_route_requires_auth(): void
    {
        if(! config('features.auth.enabled')) {
            $this->assertTrue(true);
            return;
        }

        // Ensure dashboard requires an Admin
        $response = $this->get(route('app.dashboard'));
        $response->assertRedirect(route('login'));
    }

    #[Test]
    public function dashboard_route_load_with_auth(): void
    {
        if(! config('features.auth.enabled')) {
            $this->assertTrue(true);
            return;
        }

        // Ensure dashboard loads for an Admin
        $response = $this->actingAs($this->getAdminDeveloper())
            ->get(route('app.dashboard'));
        $response->assertOk();
    }

}
