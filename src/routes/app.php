<?php
// RIVER Template (Web App)

namespace src\routes;

use App\Actions\Auth\UpdateAuthDetails;
use App\Actions\Auth\UpdateAuthPassword;
use App\Actions\Media\HandleTemporaryUpload;
use App\Http\Controllers\API\OrganisationAPIController;
use App\Http\Controllers\App\AppDashboardController;
use App\Http\Controllers\App\AppManagementController;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;

// Routes for the Inertia powered User app.
// Users are represented by the "User" model and 'web' auth gaurd.
// To enable auth routes, you will need to turn on auth.enabled within "config/project/features.php".
// By default Users do not have access to Nova.
// Note, we check for a user in Inertia via this.$page.props.auth.user
// Note, to allow staff access to app routes, change 'auth:web' to 'auth:web,admin'
$middleware = array_filter([
    'web',
    config('project.features.auth.email_verification') ? 'verified' : null,
    'auth:web', // Check the default web guard for "users"
]);

// App Routes for Users
Route::prefix(RouteServiceProvider::USER_PREFIX)->middleware($middleware)->group(function () {

    // Auth
    // login: managed via Auth::routes in routes/auth.php
    // logout: visit 'auth/logout' to logout

    // Overview
    Route::get('/', [AppDashboardController::class, 'index'])->name('app.dashboard');

    //  Management
    Route::get('/profile', [AppManagementController::class, 'profile'])->name('app.profile.index');

});

// API Routes for Users
Route::middleware($middleware)->prefix(RouteServiceProvider::USER_PREFIX . '/api')->group(function () {

    // Contacts
    Route::get('organisations/list', [OrganisationAPIController::class, 'list'])->name('api.app.organisations.list');

});

// Shared API Routes for Users and Staff
$sharedMiddleware = array_filter([
    'web',
    'auth:web,admin', // Check the default web guard for "users", or the admin guard for staff
]);
Route::middleware($sharedMiddleware)->prefix('api')->group(function () {

    // Auth
    Route::post('auth/update-details', UpdateAuthDetails::class)->name('api.auth.update-details');
    Route::post('auth/update-password', UpdateAuthPassword::class)->name('api.auth.update-password');

    // Temporary Uploads
    Route::post('temporary-upload', HandleTemporaryUpload::class)->name('api.temporary-upload');

});
