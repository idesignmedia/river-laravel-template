<?php
// RIVER Template (Web App)

namespace src\routes;

use App\Actions\Activity\DestroyNote;
use App\Actions\Activity\DownloadNoteAttachment;
use App\Actions\Activity\StoreNote;
use App\Actions\Activity\UpdateNote;
use App\Actions\Contacts\ManageUserAuth;
use App\Actions\Contacts\StoreOrganisation;
use App\Actions\Contacts\StoreUser;
use App\Actions\Contacts\UpdateOrganisation;
use App\Actions\Contacts\UpdateUser;
use App\Http\Controllers\API\ActivityAPIController;
use App\Http\Controllers\API\OrganisationAPIController;
use App\Http\Controllers\API\UserAPIController;
use App\Http\Controllers\Staff\ContactController;
use App\Http\Controllers\Staff\DashboardController;
use App\Http\Controllers\Staff\DemoController;
use App\Http\Controllers\Staff\ManagementController;
use App\Http\Controllers\Staff\OrganisationController;
use App\Http\Controllers\Staff\ReportingController;
use App\Http\Controllers\Staff\UserController;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;

// Routes for the Inertia powered Staff app.
// Staff are represented by the "Admin" model and 'admin' auth gaurd.
// By default Staff also have access to Nova.
// Note, we check for an admin in Inertia via this.$page.props.auth.admin

// @debt refactor all route names to "staff.{route}" to match what we are doing for app Users
$middleware = array_filter([
    'web',
    config('project.features.auth.email_verification') ? 'verified' : null,
    'auth:admin', // Check the admin guard for "staff"
]);

// App Routes for Staff
Route::prefix(RouteServiceProvider::STAFF_PREFIX)->middleware($middleware)->group(function () {

    // Demo
    Route::get('/demo', [DemoController::class, 'index'])->name('staff.demo.index');

    // Auth
    // login: managed via Auth::routes in routes/auth.php
    // logout: visit 'staff/auth/logout' to logout

    // Overview
    Route::get('/', [DashboardController::class, 'index'])->name('staff.dashboard');
    Route::resource('reporting', ReportingController::class)->only('index')->names([
        'index' => 'staff.reporting.index',
    ]);

    // Contacts
    // Note, we group several Organisation + User concepts under "Contacts" to reduce the Template surface area
    Route::resource('contacts', ContactController::class)->only('index', 'create')->names([
        'index' => 'staff.contacts.index',
        'create' => 'staff.contacts.create',
    ]);
    Route::resource('organisations', OrganisationController::class)->only('index', 'show')->names([
        'index' => 'staff.organisations.index',
        'create' => 'staff.organisations.create',
    ]);
    ;
    Route::resource('users', UserController::class)->only('index', 'show')->names([
        'index' => 'staff.users.index',
        'create' => 'staff.users.create',
    ]);
    ;

    //  Management
    Route::get('staff', [ManagementController::class, 'staff'])->name('staff.staff.index');
    Route::get('profile', [ManagementController::class, 'profile'])->name('staff.profile.index');

});

// API Routes for Staff
Route::middleware($middleware)->prefix(RouteServiceProvider::STAFF_PREFIX . '/api')->group(function () {

    // Activities & Notes
    Route::get('activities/all', [ActivityAPIController::class, 'all'])->name('api.activities.all');
    Route::get('activities/list', [ActivityAPIController::class, 'list'])->name('api.activities.list');
    Route::post('notes/store', StoreNote::class)->name('api.notes.store');
    Route::post('notes/update', UpdateNote::class)->name('api.notes.update');
    Route::post('notes/destroy ', DestroyNote::class)->name('api.notes.destroy');
    Route::get('notes/{id}/attachment', DownloadNoteAttachment::class)->name('api.notes.attachment');

    // Contacts
    // Note, we group several Organisation + User concepts under "Contacts" to reduce the Template surface area
    Route::post('organisations/store', StoreOrganisation::class)->name('api.organisations.store');
    Route::post('organisations/update', UpdateOrganisation::class)->name('api.organisations.update');
    Route::get('organisations/get', [OrganisationAPIController::class, 'get'])->name('api.organisations.get');
    Route::get('organisations/all', [OrganisationAPIController::class, 'all'])->name('api.organisations.all');
    Route::get('organisations/list', [OrganisationAPIController::class, 'list'])->name('api.organisations.list');
    Route::post('users/store', StoreUser::class)->name('api.users.store');
    Route::post('users/update', UpdateUser::class)->name('api.users.update');
    Route::post('users/manage-auth', ManageUserAuth::class)->name('api.users.manage-auth');
    Route::get('users/get', [UserAPIController::class, 'get'])->name('api.users.get');
    Route::get('users/all', [UserAPIController::class, 'all'])->name('api.users.all');
    Route::get('users/list', [UserAPIController::class, 'list'])->name('api.users.list');

    // Auth (defined in routes/app.php under Shared API Routes)

    // Temporary Uploads (defined in routes/app.php under Shared API Routes)

});
