<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Auth Routes
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Demo Pages
if (config('project.features.demo_pages')) {
    Route::get('/demo/fetch', [\App\Http\Controllers\API\DemoController::class, 'fetch']);
    Route::get('/demo/submit', [\App\Http\Controllers\API\DemoController::class, 'submit']); // Used for testing
    Route::post('/demo/submit', [\App\Http\Controllers\API\DemoController::class, 'submit']); // Actual submit
}
