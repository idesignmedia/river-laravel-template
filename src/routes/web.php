<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContentTeamController;
use App\Http\Controllers\ContentEventController;
use App\Http\Controllers\ContentUpdateController;
use River\DMS\Http\Controllers\FallbackController;
use App\Http\Controllers\ContentResourceController;
use App\Http\Controllers\ContentTemplateController;

// Forms
Route::post('contact', [App\Http\Controllers\ContactFormController::class, 'handle']);

// Universal Content
Route::get('updates', [ContentUpdateController::class, 'index'])->name('updates.index');
Route::get('updates/{slug}', [ContentUpdateController::class, 'show'])->name('updates.show');

Route::get('resources', [ContentResourceController::class, 'index'])->name('resources.index');
Route::get('resources/{slug}', [ContentResourceController::class, 'show'])->name('resources.show');

Route::get('about/team', [ContentTeamController::class, 'index'])->name('team.index'); // We use a Synced page instead of UCM Index for team
Route::get('about/team/{slug}', [ContentTeamController::class, 'show'])->name('team.show');

Route::get('events', [ContentEventController::class, 'index'])->name('events.index');
Route::get('events/{slug}', [ContentEventController::class, 'show'])->name('events.show');

Route::get('content-templates/{slug}', [ContentTemplateController::class, 'show'])->name('content-templates.show');

// Demo Pages
if (config('project.features.demo_pages')) {
    Route::prefix('demo')->group(function () {
        Route::view('/', 'pages.demo.index');
        Route::view('/design', 'pages.demo.design');
        Route::view('/components', 'pages.demo.components');
        Route::view('/styles', 'pages.demo.styles');
        Route::view('/inputs', 'pages.demo.inputs');
        Route::view('/remote', 'pages.demo.remote');
        Route::view('/modal', 'pages.demo.modal');
        Route::view('/popup-form', 'pages.demo.popup-form');
    });
}

// Invitations routes (nova action)
Route::get('/invitation/{code}', [\Modules\Invitation\Http\Controllers\InvitationController::class, 'accept'])->name('invitation.accept');
Route::post('/invitation/{code}', [\Modules\Invitation\Http\Controllers\InvitationController::class, 'submit']);

// DMS fallback routes (Pages, Redirects)
Route::fallback(FallbackController::class);
