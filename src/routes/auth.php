<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\StaffLoginController;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

// Auth Routes for Users, via the "Users" model and web gaurd
Auth::routes([
    'register' => config('project.features.auth.registration'),
    'reset' => config('password_resets'),
    'verify' => config('project.features.auth.email_verification'),
]);
Route::get('logout', [LoginController::class, 'logout']);//->name('logout');

// Auth Route for Staff, via the "Admin" model and admin gaurd
// We only worry about login, and rediret forgot password etc to Nova
// https://medium.com/@sagarmaheshwary31/laravel-multiple-guards-authentication-setup-and-login-2761564da986
Route::prefix(RouteServiceProvider::STAFF_PREFIX)->group(function () {
    Route::get('login', [StaffLoginController::class, 'showLoginForm'])->name('staff.login');
    Route::post('login', [StaffLoginController::class, 'login']);
    Route::get('logout', [StaffLoginController::class, 'logout'])->name('staff.logout');
});
