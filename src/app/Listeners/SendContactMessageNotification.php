<?php

namespace App\Listeners;

use App\Mail\ContactEnquiry;
use Illuminate\Support\Facades\Mail;

class SendContactMessageNotification
{
    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        $recipient = config('project.owner.email_recipient');
        $sender = [
            config('project.owner.email_sender'),
            config('project.owner.email_sender_name'),
        ];

        Mail::to($recipient)->queue(
            (new ContactEnquiry($event->message))->from(...$sender)
        );
    }
}
