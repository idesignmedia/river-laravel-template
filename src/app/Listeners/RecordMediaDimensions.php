<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use River\Core\Helpers\File;
use River\Core\Helpers\Rect;
use Spatie\MediaLibrary\Conversions\Events\ConversionWillStart;

class RecordMediaDimensions
{
    /**
     * Handle the event.
     */
    public function handle(ConversionWillStart $event): void
    {
        $media = $event->media;

        if ($media->hasCustomProperty('width')) {
            return;
        }

        if (! File::pathIsImage($media->getPath())) {
            return;
        }

        $diskDriver = config('media-library.disk_name');
        $disk = Storage::disk($diskDriver);

        // The file is not yet available
        if (! $disk->exists($media->getPath())) {
            Log::error('Failed to generate blur has because the file was not available. [driver: ' . $diskDriver . ']');
            return;
        }

        // Load the file data from S3
        $file = $disk->get($media->getPath());
        $image = \Image::make($file);

        $rect = new Rect(
            width: $image->width(),
            height: $image->height()
        );

        $blurRect = $rect->resize(32);
        $blurImage = (string) $image
            ->resize($blurRect->width, $blurRect->height)
            ->blur(5)
            ->encode('data-url');


        // Record dimensions to media
        $media->setCustomProperty('width', $rect->width);
        $media->setCustomProperty('height', $rect->height);
        $media->setCustomProperty('aspectRatio', $rect->aspectRatio());
        $media->setCustomProperty('aspectRatioNormalized', $rect->aspectRatioNormalized());
        $media->setCustomProperty('orientation', $rect->orientation());
        $media->setCustomProperty('blur', $blurImage);
        $media->save();
    }
}
