<?php

namespace App\Listeners;

use App\Mail\CustomerRegisteredNotification;
use Illuminate\Support\Facades\Mail;

class SendCustomerRegisteredNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        $recipient = config('project.owner.email_recipient');
        $sender = [
            config('project.owner.email_sender'),
            config('project.owner.email_sender_name'),
        ];

        Mail::to($recipient)->queue(
            (new CustomerRegisteredNotification($event->user))->from(...$sender)
        );
    }
}
