<?php

namespace App\Listeners;

use App\Mail\ContactAutoResponse;
use Illuminate\Support\Facades\Mail;

class SendContactMessageAutoResponse
{
    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        $recipient = $event->message->email;
        $sender = [
            config('project.owner.email_sender'),
            config('project.owner.email_sender_name'),
        ];

        Mail::to($recipient)->queue(
            (new ContactAutoResponse($event->message))->from(...$sender)
        );
    }
}
