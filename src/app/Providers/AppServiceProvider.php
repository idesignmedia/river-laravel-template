<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        // Laravel's url() and route() methods do not reliably ensure generated urls are secured
        // when the app url starts with `https`, so we'll force laravel to use https.
        if (Str::startsWith(config('app.url'), 'https')) {
            URL::forceScheme('https');
        }

        // Route all mail messages to the configured system email address when
        // not in production environments
        if (
            ! app()->environment('production') &&
            config('project.features.system_email.enabled')
        ) {
            Mail::alwaysTo(config('project.features.system_email.address'));
        }

        // Laravel paginator uses tailwind by default
        // switch it to bootstrap css
        Paginator::useBootstrap();
    }
}
