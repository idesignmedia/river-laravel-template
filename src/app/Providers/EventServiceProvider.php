<?php

namespace App\Providers;

use App\Events\ContactMessageReceived;
use App\Listeners\RecordMediaDimensions;
use App\Listeners\SendContactMessageAutoResponse;
use App\Listeners\SendContactMessageNotification;
use App\Listeners\SendCustomerRegisteredNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Spatie\MediaLibrary\Conversions\Events\ConversionWillStart;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
            SendCustomerRegisteredNotification::class,
        ],
        ContactMessageReceived::class => [
            SendContactMessageNotification::class,
            SendContactMessageAutoResponse::class,
        ],
        ConversionWillStart::class => [
            RecordMediaDimensions::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
