<?php

namespace App\Providers;

use App\Models\Admin;
use App\Models\User;
use Barryvdh\Debugbar\Facades\Debugbar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Code;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Menu\Menu;
use Laravel\Nova\Menu\MenuItem;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;
use Outl1ne\NovaSettings\NovaSettings;
use River\DMS\Behaviour\HasCommonResourceFields;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    use HasCommonResourceFields;

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        parent::boot();

        // Debug Bar
        Nova::serving(function (ServingNova $event) {
            // Don't use debugbar in nova if disabled
            if (! env('DEBUGBAR_NOVA_ENABLED', false)) {
                Debugbar::disable();
            }
        });

        // Update Footer
        $this->updateFooter();

        // Update User Menu
        $this->updateUserMenu();

        // Nova Settings
        $this->registerSettings();
    }

    /**
     * Register the Nova routes.
     */
    protected function routes(): void
    {
        Nova::routes()
            ->withAuthenticationRoutes()
            ->withPasswordResetRoutes()
            ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     */
    protected function gate(): void
    {
        // Allow all users to view nova as we use a dedicated auth guard
        // for nova users and website users.
        Gate::define('viewNova', function ($user) {
            return true;
        });
    }

    /**
     * Register the application's Nova resources.
     */
    protected function resources(): void
    {
        Nova::resourcesIn(app_path('Nova/Resources'));
    }

    /**
     * Get the extra dashboards that should be displayed on the Nova dashboard.
     */
    protected function dashboards(): array
    {
        return [
            new \App\Nova\Dashboards\Main,
            // (new \River\Nova\Metrics\NewUsers)->width('1/2'),
            // (new \River\Nova\Metrics\NewUsersPerDay)->width('1/2'),
        ];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     */
    public function tools(): array
    {
        return [
            new NovaSettings(),
        ];
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Update the Nova footer.
     */
    private function updateFooter(): void
    {
        // Custom footer
        Nova::footer(function ($request) {
            return Blade::render('
                <footer class="text-center text-xs">
                    <a href="' . url('/') . '" target="_blank" class="text-primary-500"
                        >' . config('app.name') . '</a>
                    <span class="px-1">|</span>
                    <a href="https://weareriver.nz/" rel="noopener nofollow" target="_blank" class="text-primary-500"
                        >Powered by RIVER</a>
                    (Nova ' . Nova::version() . ')
                </footer>
            ');
        });
    }

    /**
     * Update the User Menu for Nova.
     */
    private function updateUserMenu(): void
    {
        // User Menu Links
        Nova::userMenu(function (Request $request, Menu $menu) {
            // Admin Profile
            if ($request->user() instanceof Admin) {
                $menu->prepend(
                    MenuItem::make(
                        'Profile',
                        "/resources/admin-resources/{$request->user()->getKey()}"
                    )
                );
            }
            // User Profile
            if ($request->user() instanceof User) {
                $menu->prepend(
                    MenuItem::make(
                        'Profile',
                        "/resources/user-resources/{$request->user()->getKey()}"
                    )
                );
            }
            // Frontend Link
            $menu->prepend(
                MenuItem::externalLink(
                    'View Frontend',
                    '/'
                )->openInNewTab()
            );
            return $menu;
        });
    }

    /**
     * Register any Nova Settings.
     */
    private function registerSettings(): void
    {
        // Code & Scripts
        NovaSettings::addSettingsFields([
            Heading::make('Code and Scripts'),
            Code::make('Head Code', 'code_head')->language('html'),
            Code::make('Body Code', 'code_body')->language('html'),
            Code::make('Footer Code', 'code_footer')->language('html'),
        ], [], 'Code Scripts');

        // Layout Banner
        NovaSettings::addSettingsFields([
            Heading::make('Website Banner'),
            Boolean::make('Enable Banner', 'banner_enabled')
                ->hideFromIndex()
                ->help('
                    Whether the website banner should be shown at the top of the page.<br>
                    If active, this is shown on every page of the website.
                '),
            // Froala::make('Banner Content', 'banner_content')
            //     ->hideFromIndex()
            //     ->help('
            //         The content to display in the website banner.
            //     '),
            $this->getEditorField('Banner Content', 'banner_content', buttons: 'some'),
            Select::make('Banner Theme', 'banner_theme')
                ->hideFromIndex()
                ->options([
                    'success' => 'Announcement (green)',
                    'info' => 'Information (blue)',
                    'warning' => 'Warning (orange)',
                    'error' => 'Alert (red)',
                ])
                ->displayUsingLabels()
                ->help(
                    'The theme and colour of the banner if in use.'
                ),
        ], [], 'Banner');
    }

}
