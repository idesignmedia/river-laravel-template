<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/';

    /**
     * RIVER Template (Web App)
     * The prefix for the Inertia powered user application, via the "User" model.
     * You can set this to "/" if your project does not have any fron-tend pages outside of the Web App.
     */
    public const USER_PREFIX = 'app';

    /**
     * RIVER Template (Web App)
     * The prefix for the Inertia powered staff application, via the "Admin" model.
     */
    public const STAFF_PREFIX = 'staff';

    /**
     * Define your route model bindings, pattern filters, etc.
     */
    public function boot(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });

        $this->routes(function () {

            // API Routes
            Route::middleware('api')
                ->prefix('api')
                ->group(base_path('routes/api.php'));

            // Coming Soon
            if (config('project.features.coming_soon')) {
                Route::view('/', 'layouts.coming-soon');
                return;
            }

            // Auth Routes
            if (config('project.features.auth.enabled')) {
                Route::middleware('web')
                    ->group(base_path('routes/auth.php'));
            }

            // Web App - User Routes for Inertia (via User model)
            Route::middleware('web')
                ->group(base_path('routes/app.php'));

            // Web App - Staff Routes for Inertia (via Admin model)
            Route::middleware('web')
                ->group(base_path('routes/staff.php'));

            // Website Routes (Blade)
            Route::middleware('web')
                ->group(base_path('routes/web.php'));

        });
    }
}
