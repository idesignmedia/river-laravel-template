<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    protected function redirectTo(Request $request): ?string
    {
        // RIVER Template (Web App)
        if (! \Route::has('login')) {
            throw new \DomainException('Unable to authenticate, login has not been defined. Check config "project.features.auth" to enable.');
        }

        // RIVER Template (Web App)
        if (! $request->expectsJson()) {
            if($request->is(RouteServiceProvider::STAFF_PREFIX) || $request->is(RouteServiceProvider::STAFF_PREFIX . '/*')) {
                return route('staff.login');
            }
        }

        // JSON Response (such as an Inertia request)
        if ($request->expectsJson()) {
            throw new AuthenticationException('Unauthenticated.', [], route('login'));
        }

        // Default behaviour
        return route('login');
    }

}
