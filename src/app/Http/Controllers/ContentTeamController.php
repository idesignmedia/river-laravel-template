<?php

namespace App\Http\Controllers;

use App\Models\ContentTeam;
use River\DMS\Support\DisplaysUniversalContentTrait;

class ContentTeamController extends Controller
{
    use DisplaysUniversalContentTrait;

    public static $model = ContentTeam::class;

    // Note, we use a Synced page instead of UCM Index for team
    public static $viewIndex = 'pages.universalcontent.team.index';

    public static $viewShow = 'pages.universalcontent.team.show';

    public static $pagination = false;

}
