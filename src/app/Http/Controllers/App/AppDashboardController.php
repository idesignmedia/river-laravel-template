<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;
use Inertia\Response;

class AppDashboardController extends Controller
{
    public function index(): Response
    {
        // Return
        return Inertia::render('app/dashboard', [
            //
        ]);
    }

    public function redirect(): RedirectResponse
    {
        // Redirect to the app dashboard
        return redirect()->route('app.dashboard');
    }

}
