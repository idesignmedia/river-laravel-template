<?php
// RIVER Template (Web App)

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Inertia\Inertia;
use Inertia\Response;

class AppManagementController extends Controller
{
    public function profile(): Response
    {
        // User passed in via HandleInertiaRequests Middleware
        return Inertia::render('app/management/profile', [
            //
        ]);
    }

}
