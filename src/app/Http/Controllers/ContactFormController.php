<?php

namespace App\Http\Controllers;

use App\Events\ContactMessageReceived;
use App\Models\ContactMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use River\Core\Http\Support\ApiResponse;

class ContactFormController extends Controller
{
    public function handle(Request $request)
    {
        // Validate and grab our data for the message; returns a 422 on error
        $messageData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            // 'subject' => 'sometimes|string|max:255|nullable',
            'message' => 'required|string',
        ]);

        // Dispatch our contact message
        $message = ContactMessage::create($messageData);
        ContactMessageReceived::dispatch($message);

        // Define response message
        $firstName = Str::before($messageData['name'], ' ');
        $message = "Thanks {$firstName}, your message has been successfully sent.";

        // Return an API response
        if ($request->wantsJson()) {
            return new ApiResponse(
                success: true,
                message: $message,
            );
        }

        // Return a standard on-page response
        return back()->with('success', $message);
    }
}
