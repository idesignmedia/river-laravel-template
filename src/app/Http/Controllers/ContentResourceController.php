<?php

namespace App\Http\Controllers;

use App\Models\ContentResource;
use Illuminate\Http\Request;
use Illuminate\View\View;
use River\DMS\Support\DisplaysUniversalContentTrait;

class ContentResourceController extends Controller
{
    use DisplaysUniversalContentTrait;

    public static $model = ContentResource::class;

    public static $viewIndex = 'pages.universalcontent.resources.index';

    public static $viewShow = 'pages.universalcontent.resources.show';

    public static $pagination = 6; // Turned off if any categories are passed in

    // Custom index method to pull resources by custom Resource Categories
    public function index(Request $request): View
    {
        // Get the index page view
        $view = $this->getIndexView();
        if (! is_string($view)) {
            abort(404);
        }

        // Note the Query Filters
        $filters = $this->getIndexFilters($request);
        $keyword = $filters->keyword();

        // Note the active Resource Categories (anywhere a category key has been added to the request, regardless of value)
        $activeCategories = [];
        $hasCategories = false;
        $categoriesAny = true; // Whether a resource can have any of the Resource Categories (orWhere), or must have all of the Resource Categories
        foreach(ContentResource::resourceCategoriesFlat() as $key => $label) {
            if ($request->has($key)) {
                $activeCategories[] = $key;
                $hasCategories = true;
            }
        }

        // Entities query, accounting for Resource Categories
        $query = $this->getModel()::repository()->getEntitiesQuery();

        // Keyword
        if ($keyword) {
            $query->keyword($keyword);
        }

        // To only show Resources that contain ALL of the selected Resource Categories:
        if ($hasCategories && ! $categoriesAny) {
            $query->whereJsonContains('categories_json', $activeCategories);
        }

        // To show Resources that contain ANY of the selected Resource Categories:
        if ($hasCategories && $categoriesAny) {
            $query->where(function ($q) use ($filters, $activeCategories) {
                foreach ($activeCategories as $category) {
                    $q->orWhereJsonContains('categories_json', $category);
                }
            });
        }

        // Note pagination; turn off if we have any categories to resolve paging issues
        $pagination = $keyword || $hasCategories
            ? false
            : $filters->pagination;

        // Get the matching Resources
        $entities = $pagination
            ? $query->paginate($pagination)
            : $query->get();

        // Get the index attributes
        $attributes = array_merge($this->getIndexAttributes($entities), [
            'hasCategories' => $hasCategories,
        ]);

        return view($view, $attributes);
    }

    // // Check for a Page object that matches our UCM Model by route prefix <> slug
    // protected function getIndexAttributes($entities, bool $hasCategories = false): array
    // {
    //     $slug = $this->getModel()->getRoutePrefix();

    //     $shouldDisplayPreview = request()->has('preview') && admin()?->isAdmin();

    //     $query = \App\Models\Page::query()->whereSlug($slug);

    //     $page = $shouldDisplayPreview
    //         ? $query->masterVersion()->first()
    //         : $query->published()->first();

    //     // Return index attributes with page
    //     return [
    //         'model' => $this->getModel(),
    //         'entities' => $entities,
    //         'page' => $page,
    //         'hasCategories' => $hasCategories,
    //     ];
    // }
}
