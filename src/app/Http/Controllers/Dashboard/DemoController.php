<?php

namespace App\Http\Controllers\Dashboard;

use App\Data\AuthData;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Inertia\Response;

class DemoController extends Controller
{
    public function index(): Response
    {
        return Inertia::render('demo', [
            'user' => AuthData::from(Auth::user()),
        ]);
    }

    public function test(): Response
    {
        return Inertia::render('test', [
            'user' => AuthData::from(Auth::user()),
            'message' => 'Test Message',
        ]);
    }
}
