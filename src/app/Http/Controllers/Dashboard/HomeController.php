<?php

namespace App\Http\Controllers\Dashboard;

use App\Data\AuthData;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Inertia\Response;

class HomeController extends Controller
{
    public function index(): Response
    {
        return Inertia::render('home', [
            'user' => AuthData::from(Auth::user()),
        ]);
    }
}
