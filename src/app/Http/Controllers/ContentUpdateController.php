<?php

namespace App\Http\Controllers;

use App\Models\ContentUpdate;
use River\DMS\Support\DisplaysUniversalContentTrait;

class ContentUpdateController extends Controller
{
    use DisplaysUniversalContentTrait;

    public static $model = ContentUpdate::class;

    public static $viewIndex = 'pages.universalcontent.updates.index';

    public static $viewShow = 'pages.universalcontent.updates.show';

    public static $pagination = 12;

}
