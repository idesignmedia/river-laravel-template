<?php

namespace App\Http\Controllers\Staff;

use App\Data\AuthData;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Inertia\Response;

class DemoController extends Controller
{
    public function index(): Response
    {
        return Inertia::render('staff/demo', [
            'user' => AuthData::from(Auth::user()),
        ]);
    }
}
