<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Inertia\Inertia;
use Inertia\Response;

class DashboardController extends Controller
{
    public function index(): Response
    {
        // Admin (context aware)
        $authenticatedAdmin = auth()->user(); // Authenticated admin to check data against (passed in via middleware)
        // dd($authenticatedAdmin->getType());
        //

        // Return
        return Inertia::render('staff/dashboard', [
            //
        ]);
    }
}
