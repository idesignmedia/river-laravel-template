<?php
// RIVER Template (Web App)

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\ContactService;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class UserController extends Controller
{
    public function index(): Response
    {
        // Admin (context aware)
        $authenticatedAdmin = auth()->user(); // Authenticated user

        // users
        $usersLimit = 12;
        $usersResult = ContactService::listUsers(limit: $usersLimit);

        return Inertia::render('staff/users/index', [
            'usersLimit' => $usersLimit,
            'usersResult' => ['users' => $usersResult],
        ]);
    }

    public function create(Request $request)
    {
        //
    }

    public function show(user $user): Response
    {
        return Inertia::render('staff/users/show', [
            'user' => $user->asData(),
        ]);
    }

}
