<?php
// RIVER Template (Web App)

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Inertia\Inertia;
use Inertia\Response;

class ManagementController extends Controller
{
    public function profile(): Response
    {
        // User passed in via HandleInertiaRequests Middleware
        return Inertia::render('staff/management/profile', [
            //
        ]);
    }

    public function staff(): Response
    {
        // Admin user 'auth.user' passed in via HandleInertiaRequests Middleware
        return Inertia::render('staff/management/staff', [
            //
        ]);
    }

}
