<?php
// RIVER Template (Web App)

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use Inertia\Inertia;
use Inertia\Response;

class ReportingController extends Controller
{
    public function index(): Response
    {
        return Inertia::render('staff/reporting/index', [
            //
        ]);
    }
}
