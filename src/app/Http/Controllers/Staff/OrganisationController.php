<?php
// RIVER Template (Web App)

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Models\Organisation;
use App\Services\ContactService;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class OrganisationController extends Controller
{
    public function index(): Response
    {
        // Admin (context aware)
        $authenticatedAdmin = auth()->user(); // Authenticated user

        // Organisations
        $organisationsLimit = 12;
        $organisationsResult = ContactService::listOrganisations(limit: $organisationsLimit);

        return Inertia::render('staff/organisations/index', [
            'organisationsLimit' => $organisationsLimit,
            'organisationsResult' => ['organisations' => $organisationsResult],
        ]);
    }

    public function create(Request $request)
    {
        //
    }

    public function show(Organisation $organisation): Response
    {
        return Inertia::render('staff/organisations/show', [
            'organisation' => $organisation->asData(),
        ]);
    }

}
