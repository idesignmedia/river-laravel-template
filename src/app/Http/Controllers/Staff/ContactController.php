<?php
// RIVER Template (Web App)

namespace App\Http\Controllers\Staff;

use App\Enums\ContactType;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Inertia\Response;

class ContactController extends Controller
{
    public function index(): RedirectResponse
    {
        // Redirect to "Organisations" by default
        return Redirect::route('staff.organisations.index');
    }

    public function create(Request $request): Response
    {
        // Check for a contact type
        $type = null;
        foreach(ContactType::getValues() as $contactType) {
            if ($request->has($contactType)) {
                $type = $contactType;
                break;
            }
        }

        return Inertia::render('staff/contacts/create', [
            'type' => $type,
        ]);
    }

}
