<?php
// RIVER Template (Web App)

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\ContactService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class UserAPIController extends Controller
{

    /**
     * Returns a single User
     * Example: http://river-laravel-template.test/api/users/all
     */
    public function get(Request $request): array
    {
        // DISABLE
        // abort(403, 'This route is disabled for security purposes...');

        // Authenticaton
        $admin = admin(); // Nothing to do here...

        // All Users
        $user = ContactService::getUser(
            user: $request->get('user', null),
        );

        // Return
        return [
            'user' => $user,
        ];
    }

    /**
     * Returns a flat list of all Users (optionally for a given user), keyed by 'users'.
     * Example: http://river-laravel-template.test/api/users/all
     */
    public function all(Request $request): array
    {
        // DISABLE
        // abort(403, 'This route is disabled for security purposes...');

        // Authenticaton
        $admin = admin(); // Nothing to do here...

        // All Users
        $users = ContactService::allUsers(
            user: $request->get('user', null),
        );

        // Return
        return [
            'users' => $users,
        ];
    }

    /**
     * Returns a paginated list of Users, filtered by optional params, keyed by 'users'.
     * Example: http://river-laravel-template.test/api/users/list?status=active&keyword=lorem
     */
    public function list(Request $request): array
    {
        // Authenticaton
        $admin = admin(); // Nothing to do here...

        // Year
        $year = $request->get('year', null);
        if ($year) {
            // This will default to the current year if an invalid format is passed through
            $year = Carbon::create($year)->format('Y');
        }

        // List Users
        $users = ContactService::listUsers(
            user: $request->get('user', null),
            status: $request->get('status', null),
            keyword: $request->get('keyword', null),
            type: $request->get('type', null),
            year: $year,
            limit: $request->get('limit', null),
        );

        // Return
        return [
            'users' => $users,
        ];
    }

}
