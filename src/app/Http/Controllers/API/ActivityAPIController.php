<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\organisation;
use App\Services\ActivityService;
use Illuminate\Http\Request;

// Activity API Controller to return activity and note data.
// Uses the DisplayActivityData DTO to return data to the front-end.
class ActivityAPIController extends Controller
{

    /**
     * Returns a flat list of all activity (optionally for a given organisation), keyed by 'activity'.
     * Example: http://river-laravel-template.test/api/activities/all
     */
    public function all(Request $request): array
    {
        // Permissions
        $authenticatedAdmin = admin(); // Authenticated admin to check permissions against
        // if (! $authenticatedAdmin->canManage()) {
        //     abort(403, 'You do not have permission to view all data');
        // }

        // All Activity
        $activities = ActivityService::allActivity();

        // Return
        return [
            'activities' => $activities,
        ];
    }

    /**
     * Returns a paginated list of activity, filtered by optional params, keyed by 'activity'.
     * Example: http://river-laravel-template.test/api/activities/list?organisation=1
     */
    public function list(Request $request): array
    {
        // Permissions
        $authenticatedAdmin = admin();
        // if (! $authenticatedAdmin->canManage()) {
        //     abort(403, 'You do not have permission to view all data');
        // }

        // Relations
        $author = $request->author_type ? $request->author_type::find($request->author_id) : null;
        $subject = $request->subject_type ? $request->subject_type::find($request->subject_id) : null;
        $with = $request->with_type ? $request->with_type::find($request->with_id) : null;
        $parent = $request->parent_type ? $request->parent_type::find($request->parent_id) : null;

        // List Activity
        $activities = ActivityService::listActivity(
            // Relations
            author: $author,
            subject: $subject,
            with: $with,
            parent: $parent,

            // Filters
            keyword: $request->get('keyword', null),
            type: $request->get('type', null),
            pinned: filter_var($request->get('pinned'), FILTER_VALIDATE_BOOLEAN) ?? null,
            important: filter_var($request->get('important'), FILTER_VALIDATE_BOOLEAN) ?? null,
            attachment: filter_var($request->get('attachment'), FILTER_VALIDATE_BOOLEAN) ?? null,

            // Options
            limit: $request->get('limit', null),
        );

        // Return
        return [
            'activities' => $activities,
        ];
    }

}
