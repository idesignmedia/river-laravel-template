<?php
// RIVER Template (Web App)

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\ContactService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class OrganisationAPIController extends Controller
{

    /**
     * Returns a single Organisation
     * Example: http://river-laravel-template.test/api/organisations/all
     */
    public function get(Request $request): array
    {
        // DISABLE
        // abort(403, 'This route is disabled for security purposes...');

        // Authenticaton
        //$admin = admin(); // Nothing to do here...

        // All Organisations
        $organisation = ContactService::getOrganisation(
            organisation: $request->get('organisation', null),
        );

        // Return
        return [
            'organisation' => $organisation,
        ];
    }

    /**
     * Returns a flat list of all Organisations (optionally for a given organisation), keyed by 'organisations'.
     * Example: http://river-laravel-template.test/api/organisations/all
     */
    public function all(Request $request): array
    {
        // DISABLE
        // abort(403, 'This route is disabled for security purposes...');

        // Authenticaton
        $admin = admin(); // Nothing to do here...

        // All Organisations
        $organisations = ContactService::allOrganisations(
            organisation: $request->get('organisation', null),
        );

        // Return
        return [
            'organisations' => $organisations,
        ];
    }

    /**
     * Returns a paginated list of Organisations, filtered by optional params, keyed by 'organisations'.
     * Example: http://river-laravel-template.test/api/organisations/list?status=active&keyword=lorem
     */
    public function list(Request $request): array
    {
        // Authenticaton
        $admin = admin(); // Nothing to do here...

        // Year
        $year = $request->get('year', null);
        if ($year) {
            // This will default to the current year if an invalid format is passed through
            $year = Carbon::create($year)->format('Y');
        }

        // List Organisations
        $organisations = ContactService::listOrganisations(
            organisation: $request->get('organisation', null),
            status: $request->get('status', null),
            keyword: $request->get('keyword', null),
            type: $request->get('type', null),
            year: $year,
            limit: $request->get('limit', null),
        );

        // Return
        return [
            'organisations' => $organisations,
        ];
    }

}
