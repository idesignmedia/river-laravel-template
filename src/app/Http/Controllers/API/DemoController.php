<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ContentUpdate;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use River\Core\Http\Support\ApiResponse;

// Demo Controller to return (fetch) and store (submit) an Update Post
class DemoController extends Controller
{
    // Example fetch: http://river-laravel-template.test/api/demo/fetch?keyword=lorem
    public function fetch(Request $request)
    {
        // ALL UCM
        // Define all UCM Classes
        // This does not work as not all classes get returned? (eg, \App\Models\ContentUpdate...)
        // $classes = array_filter(get_declared_classes(), function ($class) {
        //     return in_array(UniversalContent::class, class_implements($class));
        // });

        // CONTENT UPDATES

        // Query parameters
        $status = $request->get('status');
        $keyword = $request->get('keyword');
        $paginate = $request->get('paginate', true);

        $paginateDefault = 10;
        if ($paginate === true || $paginate === 'true' || is_numeric($paginate)) {
            $paginate = is_numeric($paginate) ? (int) $paginate : $paginateDefault;
        } else {
            $paginate = false;
        }

        // Query
        $query = ContentUpdate::query()->publishedVersion();

        if ($status) {
            $query->status($status);
        }
        if ($keyword) {
            $query->keyword($keyword);
        }

        // Results
        $results = $paginate
            ? $query->paginate($paginate)
            : $query->get();

        // Response
        $response = new ApiResponse(
            success: true,
            message: "Successfully retrieved {$results->count()} " . ContentUpdate::pluralLabel(),
            meta: ['class' => ContentUpdate::class],
        );

        return $response->withDataset($results);
    }

    // Example submit: http://river-laravel-template.test/api/demo/submit?name=Testing&summary=...&slug=12345
    public function submit(Request $request)
    {
        // Test exceptions
        // throw new \Exception('Invalid request...');

        // Define a default slug if not provided
        $slug = $request->get('slug') ?? Str::slug($request->get('name'));

        // Merge in slug for validation
        $request->merge(['slug' => $slug]);

        // Validate
        // Note, this immediately throws a 422 response on failure
        // If you are testing the route via GET, you will be redirected home...
        $validated = $request->validate([
            'name' => 'required|min:4',
            'summary' => 'required',
            'slug' => 'unique:' . app(ContentUpdate::class)->getTable() . ',slug',
            'content' => 'sometimes|nullable',
        ]);

        // Create new post
        $post = new ContentUpdate;
        $post->name = $validated['name'];
        $post->slug = $validated['slug'];
        $post->summary = $validated['summary'];
        $post->content = $validated['content'];
        $post->save();


        // Format message
        $message = sprintf(
            'Successfully created a new %s %s /%s (ID %s)',
            ContentUpdate::singularLabel(),
            $post->getName(),
            $post->getSlug(),
            $post->getId(),
        );

        // Response
        $response = new ApiResponse(
            success: true,
            message: $message,
            meta: ['class' => ContentUpdate::class],
        );

        return $response->withDataset($post);
    }
}
