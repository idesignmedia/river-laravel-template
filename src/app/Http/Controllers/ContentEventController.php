<?php

namespace App\Http\Controllers;

use App\Models\ContentEvent;
use River\DMS\Support\DisplaysUniversalContentTrait;

class ContentEventController extends Controller
{
    use DisplaysUniversalContentTrait;

    public static $model = ContentEvent::class;

    public static $viewIndex = 'pages.universalcontent.events.index';

    public static $viewShow = 'pages.universalcontent.events.show';

    public static $pagination = 3;

    protected function getIndexAttributes($entities): array
    {
        // Check for a Page object that matches our UCM Model by route prefix <> slug
        $slug = $this->getModel()->getRoutePrefix();

        $shouldDisplayPreview = request()->has('preview') && admin()?->isAdmin();

        $query = \App\Models\Page::query()->whereSlug($slug);

        $page = $shouldDisplayPreview
            ? $query->masterVersion()->first()
            : $query->published()->first();

        // Return index attributes with page
        return [
            'model' => $this->getModel(),
            'entities' => $entities,
            'page' => $page,
        ];
    }
}
