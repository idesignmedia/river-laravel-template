<?php

namespace App\Http\Controllers;

use App\Models\ContentTemplate;
use River\DMS\Support\DisplaysUniversalContentTrait;
use Illuminate\Http\Request;

class ContentTemplateController extends Controller
{
    use DisplaysUniversalContentTrait;

    public static $model = ContentTemplate::class;

    public static $viewIndex = 'pages.universalcontent.content-templates.index';

    public static $viewShow = 'pages.universalcontent.content-templates.show';

    public static $pagination = 12;

    public function index(Request $request)
    {
        // No index page for Content Templates
        abort(404);
    }

    public function show(Request $request, string $slug)
    {
        // Disabled unless you are logged in
        if (! admin()?->isAdmin()) {
            abort(404);
        }

        // DisplaysUniversalContentTrait @ show 
        $view = $this->getShowView();
        if (! is_string($view)) {
            abort(404);
        }

        $filters = $this->getShowFilters($request, $slug);
        $entity = $this->getModel()::repository()->getEntity(filters: $filters);

        return view($view, $this->getShowAttributes($entity));
    }

}
