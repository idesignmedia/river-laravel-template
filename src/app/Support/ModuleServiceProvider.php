<?php

declare(strict_types=1);
namespace App\Support;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Nova;

/**
 * ModuleServiceProvider Abstract Class: abstract parent class for modules
 */
abstract class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Module name
     *
     * @var string
     */
    protected $name = null;

    /**
     * Optional module description
     *
     * @var string
     */
    protected $description = null;

    /**
     * Optional module author
     *
     * @var string
     */
    protected $author = null;

    /**
     * Module version
     *
     * @var string
     */
    protected $version = '1.0'; // Base version

    /**
     * Nova Resources
     *
     * @var array
     */
    protected $resources = [];

    /**
     * The policy mappings for the module.
     *
     * @var array
     */
    protected $policies = [];

    /**
     * The commands for the module.
     *
     * @var array
     */
    protected $commands = [];

    /**
     * Register module dependencies
     *
     * @return void
     */
    public function register(): void
    {
        $class = get_called_class();
        $this->app->singleton($class, function () {
            return $this;
        });
        $this->app->alias($class, $this->name);
    }

    /**
     * Initialise the module.
     */
    public function boot(): void
    {
        // Determine the module details
        $name = $this->getName();

        // Set up global class alias.
        $facade = $name . '\\Facade';
        if (class_exists($facade)) {
            if (! class_exists($name)) {
                class_alias($facade, $name);
            }
        }

        // Set up migration directories
        $this->registerMigrationPaths();

        // Set up view directories
        $paths = [
            base_path() . '/modules/' . $this->name . '/Resources/views',
            base_path() . '/river/' . $this->name . '/Resources/views',
        ];
        foreach ($paths as $path) {
            if (is_dir($path)) {
                view()->addNamespace($this->name, $path);
            }
        }

        // Load states, if required
        if (property_exists($this, 'states')) {
            \State::addStates($this->states);
        }

        // Bind event listeners, if required
        if (property_exists($this, 'listen')) {
            $events = $this->app->make('Illuminate\Contracts\Events\Dispatcher');
            foreach ($this->listen as $event => $listeners) {
                foreach ($listeners as $listener) {
                    $events->listen($event, $listener);
                }
            }
        }

        if (count($this->commands) && $this->app->runningInConsole()) {
            $this->commands($this->commands);
        }

        // Register Nova Resources
        // @see https://nova.laravel.com/docs/1.0/resources/#registering-resources
        if (count($this->resources)) {
            Nova::resources($this->resources);
        }

        $this->registerGates();

        $this->registerPolicies();
    }

    /**
     * Get the name of the module
     *
     * @return string
     */
    protected function getName(): string
    {
        if (! $this->name) {
            $segments = explode('\\', get_called_class());
            $this->name = $segments['2'];
        }
        return $this->name;
    }

    /**
     * Define routes for this module
     *
     * @return void
     */
    protected function defineRoutes()
    {
        // extendable
    }

    /**
     * Register gates for this module
     *
     * @return void
     */
    public function registerGates()
    {
        // extendable
    }

    /**
     * Register the application's policies.
     *
     * @return void
     */
    public function registerPolicies()
    {
        foreach ($this->policies as $key => $value) {
            Gate::policy($key, $value);
        }
    }

    /**
     * Register Migration Paths
     *
     * @return void
     */
    protected function registerMigrationPaths()
    {
        // Set up migration directories
        $this->loadMigrationsFrom([
            base_path() . '/modules/' . $this->name . '/database/migrations',
            base_path() . '/river/' . $this->name . '/database/migrations',
        ]);
    }
}
