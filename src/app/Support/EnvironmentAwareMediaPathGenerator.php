<?php

namespace App\Support;

use Illuminate\Support\Str;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Support\PathGenerator\DefaultPathGenerator;

class EnvironmentAwareMediaPathGenerator extends DefaultPathGenerator
{
    /*
     * Get a unique base path for the given media.
     */
    protected function getBasePath(Media $media): string
    {
        $diskName = config('media-library.disk_name');
        $diskConfig = config('filesystems.disks.' . $diskName);

        $tag = config('project.features.tag');

        // If disk driver's folder path has been tagged,
        // we do not need to change the default media path
        if ($diskConfig['driver'] == 'local') {
            if (
                Str::endsWith($diskConfig['root'], "/$tag/") ||
                Str::endsWith($diskConfig['root'], "/$tag")
            ) {
                $tag = null;
            }
        }

        return join(DIRECTORY_SEPARATOR, array_filter([
            $tag,
            $media->getKey(),
        ]));
    }
}
