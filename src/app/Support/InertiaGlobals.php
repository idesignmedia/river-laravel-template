<?php

namespace App\Support;

class InertiaGlobals
{
    public static function asArray()
    {
        return [
            'name' => config('app.name'),
            'baseUrl' => config('app.url'),
            'environment' => config('app.env'),
        ];
    }
}
