<?php
// RIVER Template (Web App)

namespace App\Support\Behaviours;

// Centralise common Inertia responses
trait WithInertiaResponses
{

    /**
     * The default success message to provide.
     *
     * @var string
     */
    public static string $inertiaSuccessDefaultMessage = 'Success!';

    /**
     * The default error message to provide when we don't want to pass through the underlying exception message.
     * By default, only \DomainExceptions have their message passed to the frontend.
     *
     * @var string
     */
    public static string $inertiaErrorDefaultMessage = 'An error occured submitting your request...';

    /**
     * Return back to inertia with a success message, optionally to the given route.
     * The message will be passed to inertia via $page.props.flash.success = $message (via HandleInertiaRequests).
     *
     * @param  string|null $message The success message to pass through
     * @param  string|null $route   The named route to follow if used
     * @param  array|null  $params  Paramaters for the named route if used
     */
    public static function inertiaSuccess(?string $message = null, ?string $route = null, ?array $params = null)
    {
        $message = $message ?? self::$inertiaSuccessDefaultMessage;
        return $route
            ? redirect()
                ->route($route, $params ?? [])
                ->with('success', $message)
            : redirect()
                ->back()
                ->with('success', $message);

    }

    /**
     * Return back to inertia with a success message, to the specified route (alias to @inertiaSuccess, with the route name first).
     *
     * @param  string|null $route   The named route to follow if used
     * @param  array|null  $params  Paramaters for the named route if used
     * @param  string|null $message The success message to pass through
     */
    public static function inertiaSuccessTo(?string $route = null, ?array $params = null, ?string $message = null)
    {
        return self::inertiaSuccess($message, $route, $params);
    }

    /**
     * Return back to inertia with an error message.
     * The message will be passed to inertia via $page.props.flash.error = $message (via HandleInertiaRequests),
     * and within the response data via response._error = $message.
     *
     * @param  string|null $message The error message to pass to the frontend
     */
    public static function inertiaError(?string $message = null, ?array $errors = null)
    {
        $message = $message ?? self::$inertiaErrorDefaultMessage;
        $errors = $errors ?? ['_error' => $message];
        return redirect()
            ->back()
            ->with('error', $message)
            ->withErrors($errors);
    }

    /**
     * Handle an exception and then pass it through to inertia as an error message.
     * By default was pass the message thruogh for \DomainExceptions, and hide it for anything else.
     *
     * @param  \Exception $e The exception that was caught.
     */
    public static function handleInertiaError(\Exception $e)
    {
        try {
            throw $e;
        } catch (\DomainException $e) {
            return self::inertiaError($e->getMessage());
        } catch (\Exception $e) {
            // @debt consider logging the whole stacktrace, or pushing to a service
            $error = get_called_class() . ' error: ' . $e->getMessage();
            \Log::error($error);
            return self::inertiaError(null);
        }
    }

}
