<?php
// RIVER Template

namespace App\Support\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use River\Revisionable\Behaviour\HasRevisions;
use River\Revisionable\Contracts\Revisionable;

class Model extends BaseModel implements Revisionable
{
    use SoftDeletes;
    use HasFactory;
    use HasRevisions;

    protected $guarded = ['id'];
}
