<?php

namespace App\Support\Models;

use App\Enums\AuthRole;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use River\Core\Contracts\CommonUser;

class Authenticatable extends User implements CommonUser
{
    use HasFactory;
    use Notifiable;

    // SETUP

    protected $guarded = ['id'];

    // Attributes that should be hidden for arrays
    protected $hidden = [
        'password',
        'remember_token',
    ];

    // The attributes that should be cast to native types
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Safe public array
    public function toPublicArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'role' => $this->role,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }


    // COMMON USER INTERFACE

    public function getName(): string
    {
        return $this->name;
    }

    public function setNameAttribute(string $value): void
    {
        $value = trim($value);
        $this->first_name = Str::before($value, ' ');
        $this->last_name = Str::contains($value, ' ') ? Str::after($value, ' ') : null;
        $this->attributes['name'] = $value;
    }

    public function getShortName(): string
    {
        return $this->first_name ?? $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getLabel(): string
    {
        return "{$this->getName()} ({$this->getEmail()})";
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function getRoleLabel(): string
    {
        return AuthRole::getLabel($this->role);
    }

    public function getType(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    // USER TYPE

    // Whether the user is a CMS Admin (ie, can access Nova)
    public function isAdmin(): bool
    {
        return false;
    }

    // Whether the user is a front-end user
    public function isUser(): bool
    {
        return false;
    }

    // Whether this user is a developer account
    public function isDeveloper(): bool
    {
        return false;
    }

}
