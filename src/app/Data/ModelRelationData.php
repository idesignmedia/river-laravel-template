<?php
// RIVER Template

namespace App\Data;

use Spatie\LaravelData\Data;

/**
 * Data that contains a class and id, used to represent a Model relationship.
 */
class ModelRelationData extends Data
{
    public function __construct(

        // The Model's full Class
        public ?string $class = null,

        // The Model's ID
        public ?int $id = null,

        // The short label of the Model for reference
        public ?string $label = null,

        // An identifier used to represent the relationship, "{class}:{id}"
        public ?string $identifier  = null,

        // Whether the relationship is considered 'valid' (a Class and ID were both passed in)
        public bool $valid = false,
    ) {
        // This relationship is considered 'valid' if we have passed in a Class and ID
        // Note, we do not validate the class exists, we just confirm that values were passed in
        if ($class && $id) {
            $this->valid = true;
            $this->label = (new \ReflectionClass($class))->getShortName();
            $this->identifier = "{$class}:{$id}";
        }
    }

    public static function fromModel(?Object $model = null): ModelRelationData
    {
        return new self(
            class:          $model ? $model::class : null,
            id:             $model ? $model->id : null,
        );
    }

    public function getModel(): ?Object
    {
        return $this->class && $this->id
            ? $this->class::find($this->id)
            : null;
    }
}
