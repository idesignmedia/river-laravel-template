<?php

namespace App\Data;

use App\Models\Admin;
use App\Models\User;
use App\Services\FormatService;
use Spatie\LaravelData\Data;

/** @typescript */
class AuthData extends Data
{
    public function __construct(
        // Database
        public int $id,
        public string $name,
        public string $email,
        public ?string $phone,
        public ?string $company,

        // Display
        public string $label,
        public string $role,
        public string $addedLabel,
        public string $addedAdjunct,

        // Type & Permissions
        public string $class,
        public string $type,
        public array $permissions = [],
    ) {
        //
    }

    public static function fromModel(User|Admin $auth): self
    {

        // Permissions
        // @debt ERROR - AdminPolicy does not appear to load here
        $permissions = [
            'isAdmin' => $auth->isAdmin(),
            'isUser' => $auth->isUser(),
            'isDeveloper' => $auth->isDeveloper(),
        ];

        $data = new self(
            // Database
            id:                     $auth->id,
            name:                   $auth->getName(),
            email:                  $auth->getEmail(),
            phone:                  $auth->phone ?? null,
            company:                $auth->company ?? null,

            // Display
            label:                  $auth->getLabel(),
            role:                   $auth->getType(), // @debt TBC
            addedLabel:             FormatService::formatShortDate($auth->created_at),
            addedAdjunct:           FormatService::dateDifferenceLabel($auth->created_at),

            // Type & Permissions
            class:                  $auth::class,
            type:                   $auth->getType(),
            permissions:            $permissions,
        );

        return $data;
    }

}
