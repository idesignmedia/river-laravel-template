<?php

namespace App\Data\Admin;

use App\Enums\AdminRole;
use App\Models\Admin;
use App\Services\FormatService;
use Spatie\LaravelData\Data;

/**
 * DTO to generically display an Admin in the frontend.
 * This is automatically shared with the frontend for the currently authenticated admin,
 * via src\app\Http\Middleware\HandleInertiaRequests.php
 */
class DisplayAdminData extends Data
{
    public function __construct(
        // Data
        public int $id,
        public string $firstName,
        public string $lastName,
        public string $name,
        public string $label,
        public string $email,
        public string $added,
        public ?string $avatar,
        public ?string $role,
        public ?string $company,

        // Display
        public string $addedLabel,
        public string $addedAdjunct,
        public ?string $roleLabel = null,

        // Permissions
        public array $permissions = [],
    ) {
    }

    public static function fromAdmin(?Admin $admin): ?DisplayAdminData
    {
        // Validate
        if (! $admin) {
            return null; // eg, HandleInertiaRequests.php during logout
        }

        // Permissions
        // @debt ERROR - AdminPolicy does not appear to load here
        $permissions = [
            'admin_id' => $admin->id,
            'role' => $admin->role,
            'developer' => $admin->isDeveloper(),
            'manage' => $admin->canManage(), // $admin->can('manageStaff', $admin),
        ];

        // Data
        return new self(
            // Data
            id:                     $admin->id,
            firstName:              $admin->first_name,
            lastName:               $admin->last_name,
            name:                   $admin->getName(),
            label:                  $admin->getLabel(),
            email:                  $admin->email,
            added:                  $admin->created_at,
            avatar:                 null,
            role:                   $admin->role,
            company:                $admin->company,

            // Display
            addedLabel:             FormatService::formatShortDate($admin->created_at),
            addedAdjunct:           FormatService::dateDifferenceLabel($admin->created_at),
            roleLabel:              $admin->role ? AdminRole::getLabel($admin->role) : null,

            // Permissions
            permissions:            $permissions,
        );
    }

}
