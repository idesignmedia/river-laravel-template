<?php

namespace App\Data;

use Spatie\LaravelData\Data;

/** @typescript */
class AddressData extends Data
{
    public function __construct(
        public ?string $line_1,
        public ?string $line_2,
        public ?string $suburb,
        public ?string $city,
        public ?string $country,
        public ?string $state,
        public ?string $postal_code,
    ) {
    }

    public function asSentence(): string
    {
        return join(', ', array_filter([
            $this->line_1,
            $this->line_2,
            $this->suburb,
            join(' ', array_filter([$this->city, $this->postal_code])),
        ]));
    }
}
