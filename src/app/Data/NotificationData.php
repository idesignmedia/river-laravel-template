<?php

namespace App\Data;

use Illuminate\Support\Str;
use Spatie\LaravelData\Data;

/** @typescript */
class NotificationData extends Data
{
    public $id;

    public function __construct(
        public string $state,
        public string $message,
    ) {
        $this->id = Str::replace('-', '', Str::uuid());
    }
}
