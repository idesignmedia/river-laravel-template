<?php

namespace App\Data\Activity;

use App\Models\Admin;
use App\Models\Note;
use Illuminate\Support\Carbon;
use Spatie\LaravelData\Data;

/**
 * DTO to represent a History entry for a Note, created when a Note is updated.
 * Call toArray() on this DTO to return the data in an array format.
 */
class NoteHistoryData extends Data
{
    public function __construct(
        // Note History
        public string $name,
        public string $content,
        public bool $option_pinned,
        public bool $option_important,

        // Change Details
        ?Admin $admin = null,
        public ?int $updated_admin_id = null,
        public ?string $updated_at = null,
    ) {
        $this->updated_admin_id = $admin ? $admin->id : $updated_admin_id;
        $this->updated_at = $updated_at ?? Carbon::now()->toISOString();
    }

    public static function fromNote(?Note $note, ?Admin $admin = null, ?Carbon $updated_at = null): ?NoteHistoryData
    {
        if (! $note) {
            return null;
        }

        $data = NoteHistoryData::from($note);
        $data->updated_admin_id = $admin ? $admin->id : null;
        $data->updated_at = $updated_at ?? Carbon::now()->toISOString();

        return $data;
    }

}
