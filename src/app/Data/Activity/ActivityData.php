<?php

namespace App\Data\Activity;

use App\Data\ModelRelationData;
use App\Data\PageLinkData;
use App\Enums\ActivityType;
use App\Models\Note;
use App\Services\ActivityService;
use App\Services\FormatService;
use Spatie\Activitylog\Models\Activity;
use Spatie\LaravelData\Data;

/**
 * A generic Display DTO for all Activity (Activity, Notes, etc), for display on the frontend.
 * Activity come from the Actiivty Log in the first instance (powered by Spatie Activity Log),
 * and then get customised depending on the various relations when we create this DTO.
 */
class ActivityData extends Data
{
    public function __construct(
        // Database Details
        public int $id,

        // Activity Details
        public ?string $type, // eg, "note"
        public ?string $typeLabel, // eg, "Note"
        public ?string $action, // eg, "create"
        public ?string $title, // eg, "Test Note"
        public string $message, // eg, "Testing..."
        public ?string $messageHtml,
        public string $date, // eg, "2022-06-12 06:45:55"
        public string $dateLabel, // eg, "12 Jun 22"
        public string $dateDiff, // eg, "7 days ago"

        // Author Details
        public ?string $authorType = null,
        public ?string $authorName = null,
        public ?string $authorEmail = null,

        // Relations
        public ?ModelRelationData $author = null,
        public ?ModelRelationData $subject = null,
        public ?ModelRelationData $with = null,
        public ?ModelRelationData $parent = null,

        // Options
        public ?bool $isPinned = false,
        public ?bool $isImportant = false,
        public ?bool $hasAttachments = false,
        public ?bool $edited = null,
        public ?int $thread = null,
        public ?string $theme = null,

        // Related Action
        public ?PageLinkData $pageLink = null,

        // Attachment Details
        public ?string $attachmentName = null,
        public ?string $attachmentLocation = null,
    ) {
    }

    public static function fromModel(Activity $activity): self
    {
        // Underlying ActivityData DTO
        $dto = new self(
            // Database Details
            id:                 $activity->id,

            // Activity Details
            type:               $activity->properties['type'] ?? null,
            typeLabel:          null,
            action:             $activity->properties['action'] ?? null,
            title:              $activity->properties['title'] ?? 'Activity',
            message:            $activity->description,
            messageHtml:        null,
            date:               $activity->created_at,
            dateLabel:          FormatService::formatShortDate($activity->created_at),
            dateDiff:           FormatService::dateDifferenceLabel($activity->created_at),

            // Author Details
            authorType:         null,
            authorName:         null,
            authorEmail:        null,

            // Relations
            author:             null,
            subject:            null,
            with:               null,
            parent:             null,

            // Options
            isPinned:           $activity->properties['pinned'] ?? null,
            isImportant:        $activity->properties['important'] ?? false,
            hasAttachments:     false,
            edited:             $activity->properties['edited'] ?? null,
            thread:             $activity->properties['thread'] ?? null,
            theme:              $activity->properties['theme'] ?? null,

            // Related Action
            pageLink:           null,
        );

        // Activity Details
        $dto->typeLabel = ActivityType::getLabel($dto->type);

        // Author Details
        $author = $activity->causer_type && $activity->causer_id
            ? $activity->causer_type::find($activity->causer_id)
            : null;
        if ($author) {
            $dto->authorType = method_exists($author, 'getType') ? $author->getType() : null;
            $dto->authorEmail = method_exists($author, 'getEmail') ? $author->getEmail() : null;
        }
        $dto->authorName = FormatService::authorName($author);

        // Relations
        $dto->author = $activity->causer_type
            ? new ModelRelationData(class: $activity->causer_type, id: $activity->causer_id)
            : null;
        $dto->subject = $activity->subject_type
            ? new ModelRelationData(class: $activity->subject_type, id: $activity->subject_id)
            : null;
        $dto->with = $activity->properties['with_type']
            ? new ModelRelationData(class: $activity->properties['with_type'], id: $activity->properties['with_id'])
            : null;
        $dto->parent = $activity->properties['parent_type']
            ? new ModelRelationData(class: $activity->properties['parent_type'], id: $activity->properties['parent_id'])
            : null;

        // Page Link
        $dto->pageLink = ActivityService::getPageLink($activity);

        // Manage WITH
        // Push in custom display data accordingly
        switch($activity->properties['with_type'] ?? null) {
            case Note::class:
                $note = Note::find($activity->properties['with_id']);
                if ($note) {
                    $attachment = $note->getFirstMedia('attachments');

                    $dto->title = $note->name;
                    $dto->message = $note->content;
                    $dto->messageHtml = $note->content ? nl2br($note->content) : null;

                    if ($attachment) {
                        $dto->hasAttachments = true;
                        $dto->attachmentName = $attachment->file_name;
                        $dto->attachmentLocation = $note->getAttachmentDownloadUrl();

                    }
                }
                break;
        }

        // Return DTO
        return $dto;
    }

}
