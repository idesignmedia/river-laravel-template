<?php

namespace App\Data\Activity;

use Spatie\LaravelData\Data;

/**
 * DTO required to store a new Note, or update an existing Note.
 * Used by the Action Activity/StoreNote
 * (this DTO also outlines the shape of required data).
 *
 * @debt rename to CreateUpdateNoteData
 */
class StoreUpdateNoteData extends Data
{
    public function __construct(
        // Setup
        public ?string $author_type,
        public ?int $author_id,
        public ?string $noteable_type,
        public ?int $noteable_id,

        // Content
        public ?string $name,
        public string $content,

        // Options
        public ?bool $option_pinned = null,
        public ?bool $option_important = null,

        // Media
        public ?string $attachments = null,

        // Activity relations
        // The Subject is required during store to log an associated Activity
        public ?string $subject_type = null,
        public ?int $subject_id = null,
        public ?string $parent_type = null,
        public ?int $parent_id = null,
    ) {
    }

}
