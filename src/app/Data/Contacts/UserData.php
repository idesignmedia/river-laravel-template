<?php
// RIVER Template (Web App)

namespace App\Data\Contacts;

use App\Enums\ContactStatus;
use App\Models\User;
use App\Services\FormatService;
use Carbon\Carbon;
use Spatie\LaravelData\Attributes\WithCast;
use Spatie\LaravelData\Casts\DateTimeInterfaceCast;
use Spatie\LaravelData\Data;

/**
 * DTO to represent a User for the frontend.
 */
class UserData extends Data
{
    public function __construct(
        // Setup
        public int $id,
        public ?int $admin_id,
        public ?string $status,
        #[WithCast(DateTimeInterfaceCast::class, format: 'Y-m-d+')] // Handle Element Plus: "1980-05-17T12:00:00.000Z"
        public ?Carbon $date_added,

        // Contact Details
        public string $name,
        public ?string $reference,
        public ?string $phone,
        public ?string $email,
        public ?string $website,
        public ?string $address,
        public ?string $country,
        public ?string $type,
        public ?string $type_other,
        public ?string $comments,

        // User Details
        public ?string $occupation,
        public ?Carbon $auth_at,
        public ?Carbon $login_at,

        // Model
        public string $modelType,
        public int $modelId,

        // Display
        public ?string $adminName,
        public string $statusLabel,
        public string $dateAddedLabel,
        public string $dateAddedDiff,
        public ?string $typeLabel,
        public ?string $commentsHtml,
        public ?string $addressLabel,

        // Auth
        public bool $authEnabled,
        public string $authAtLabel,
        public string $authAtDiff,
        public string $loginAtLabel,
        public string $loginAtDiff,
    ) {
        //
    }

    public static function fromModel(User $user): self
    {
        // Relations
        $admin = $user->admin ?? null;

        // Merge data with added fields
        $data = array_merge($user->toArray(), [
            // Model
            'modelType'             => user::class,
            'modelId'               => $user->id,
            // Display
            'adminName'             => FormatService::adminName($admin),
            'statusLabel'           => $user->status ? ContactStatus::getLabel($user->status) : '-',
            'dateAddedLabel'        => FormatService::formatShortDate(date: $user->date_added),
            'dateAddedDiff'         => FormatService::dateDifferenceLabel(date: $user->date_added, tense: true),
            'typeLabel'             => $user->type ? ucfirst($user->type) : null, // @debt
            'commentsHtml'          => $user->comments ? nl2br($user->comments) : null,
            'addressLabel'          => $user->getAddressLabel(),
            // Auth
            'authEnabled'           => $user->authEnabled(),
            'authAtLabel'           => FormatService::formatShortDate(date: $user->auth_at),
            'authAtDiff'            => FormatService::dateDifferenceLabel(date: $user->auth_at, tense: true),
            'loginAtLabel'          => FormatService::formatShortDate(date: $user->login_at),
            'loginAtDiff'           => FormatService::dateDifferenceLabel(date: $user->login_at, tense: true),
        ]);

        // Return DTO
        return self::from($data);
    }

}
