<?php
// RIVER Template (Web App)

namespace App\Data\Contacts;

use Illuminate\Support\Carbon;
use Spatie\LaravelData\Attributes\WithCast;
use Spatie\LaravelData\Casts\DateTimeInterfaceCast;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Support\TransformationType;

/**
 * DTO required to store or update a User.
 * Used by the Action Contacts/StoreUser
 * (this DTO also outlines the shape of required data).
 */
class StoreUpdateUserData extends Data
{
    public function __construct(

        // Setup
        public ?string $status,
        public ?int $admin_id,
        #[WithCast(DateTimeInterfaceCast::class, format: 'Y-m-d+')] // Handle Element Plus: "1980-05-17T12:00:00.000Z"
        public ?Carbon $date_added,

        // Contact Details
        public string $name, // Required
        public ?string $reference,
        public ?string $phone,
        public string $email, // Required
        public ?string $website,
        public ?string $address,
        public ?string $country,
        public ?string $type,
        public ?string $type_other,
        public ?string $comments,

        // User Details
        public ?string $occupation,
    ) {
        //
    }

    public function toArray(): array
    {
        // Keep date_added in Carbon format when transforming to an array, used via StoreOrganisation@handle.
        // This is optional, though showcases how to implement this if required.
        $data = $this->transform(TransformationType::full());
        $data['date_added'] = $this->date_added;
        return $data;
    }

}
