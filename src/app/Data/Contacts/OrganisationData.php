<?php
// RIVER Template (Web App)

namespace App\Data\Contacts;

use App\Enums\ContactStatus;
use App\Models\Organisation;
use App\Services\FormatService;
use Carbon\Carbon;
use Spatie\LaravelData\Attributes\WithCast;
use Spatie\LaravelData\Casts\DateTimeInterfaceCast;
use Spatie\LaravelData\Data;

/**
 * DTO to represent an Organisation for the frontend.
 */
class OrganisationData extends Data
{
    public function __construct(
        // Setup
        public int $id,
        public ?int $admin_id,
        public string $status,
        #[WithCast(DateTimeInterfaceCast::class, format: 'Y-m-d+')] // Handle Element Plus: "1980-05-17T12:00:00.000Z"
        public Carbon $date_added,

        // Contact Details
        public string $name,
        public ?string $reference,
        public ?string $phone,
        public ?string $email,
        public ?string $website,
        public ?string $address,
        public ?string $country,
        public ?string $type,
        public ?string $type_other,
        public ?string $comments,

        // Organisation Details
        public ?string $industry,

        // Model
        public string $modelType,
        public int $modelId,

        // Display
        public ?string $adminName,
        public string $statusLabel,
        public string $dateAddedLabel,
        public string $dateAddedDiff,
        public ?string $typeLabel,
        public ?string $commentsHtml,
        public ?string $addressLabel,
    ) {
        //
    }

    public static function fromModel(Organisation $organisation): self
    {
        // Relations
        $admin = $organisation->admin ?? null;

        // Merge data with added fields
        $data = array_merge($organisation->toArray(), [
            // Model
            'modelType'             => Organisation::class,
            'modelId'               => $organisation->id,
            // Display
            'adminName'             => FormatService::adminName($admin),
            'statusLabel'           => ContactStatus::getLabel($organisation->status),
            'dateAddedLabel'        => FormatService::formatShortDate(date: $organisation->date_added),
            'dateAddedDiff'         => FormatService::dateDifferenceLabel(date: $organisation->date_added, tense: true),
            'typeLabel'             => $organisation->type ? ucfirst($organisation->type) : null, // @debt
            'commentsHtml'          => $organisation->comments ? nl2br($organisation->comments) : null,
            'addressLabel'          => $organisation->getAddressLabel(),
        ]);

        // Return DTO
        return self::from($data);
    }

}
