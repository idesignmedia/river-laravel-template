<?php
// RIVER Template (Web App)

namespace App\Data\Contacts;

use Spatie\LaravelData\Data;

/**
 * DTO required to manage Auth for a User.
 * Used by the Action Contacts/ManageUserAuth
 * (this DTO also outlines the shape of required data).
 */
class ManageAuthUserData extends Data
{
    public function __construct(
        public bool $enabling_auth,
        public ?string $message,
    ) {
        //
    }
}
