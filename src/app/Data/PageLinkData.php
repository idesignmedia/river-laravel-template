<?php

namespace App\Data;

use Spatie\LaravelData\Data;

/**
 * DTO to represent a page link, generally used for link and buttons in the frontend.
 */
class PageLinkData extends Data
{
    public function __construct(
        // The name of the page link.
        public ?string $name = null,

        // The URL of the page link, if it has one.
        // If not, the page link will not be noted as "valid".
        public ?string $url = null,

        // Whether this page link is external, and should open in a new tab.
        // Use null to let the page link determine this for itself.
        public bool $external = false,

        // Whether this page link is considered active within the usage context.
        public bool $active = false,

        // Whether this page link is considered valid within the usage context.
        public bool $valid = false,
    ) {
        // If not defined, set this link as "valid" if we have a name and url
        $this->valid = $name && $url;
    }
}
