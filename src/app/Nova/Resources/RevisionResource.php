<?php

// RIVER Template
namespace App\Nova\Resources;

use River\DMS\Nova\Resources\RevisionResource as BaseRevisionResource;

class RevisionResource extends BaseRevisionResource
{
    public static $model = \River\Revisionable\Models\Revision::class;

    public static $group = 'Content';

    public static $displayInNavigation = false;
}
