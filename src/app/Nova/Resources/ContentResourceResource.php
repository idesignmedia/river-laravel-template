<?php

// RIVER Template
namespace App\Nova\Resources;

use App\Models\ContentResource;
use Laravel\Nova\Fields\Heading;
// use Laravel\Nova\Fields\MultiSelect;
use Outl1ne\MultiselectField\Multiselect;
use River\DMS\Nova\Resources\UniversalContentResource;

class ContentResourceResource extends UniversalContentResource
{
    // public static $displayInNavigation = false;

    public static $model = ContentResource::class;

    public function getCustomFields(): array
    {
        // Note, we use OptimistDigital\MultiselectField\Multiselect as opposed to the native
        // Laravel\Nova\Fields\MultiSelect Nova field for a better UX
        return [
            // Heading::make("{$this->resource->getSingularLabel()} Details"),
            MultiSelect::make('Resource Categories', 'categories_json')
                ->options(ContentResource::resourceCategoriesMultiSelectOptions())
                ->reorderable()
                ->saveAsJSON()
                ->placeholder('Select Categories:')
                ->help('
                    The Categories for this Resource (multi-select).
                '),
        ];
    }

}
