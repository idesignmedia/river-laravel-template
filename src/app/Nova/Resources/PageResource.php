<?php

// RIVER Template
namespace App\Nova\Resources;

use App\Models\Page;
use Laravel\Nova\Fields\Text;
use River\DMS\Nova\Resources\PageResource as BasePageResource;
// use River\DMS\Contracts\UniversalResourceOptions;
use River\DMS\Support\UniversalResourceOptions;

class PageResource extends BasePageResource
{
    // NOVA SETUP

    public static $model = Page::class;

    public static $group = 'Pages';

    // RESOURCE OPTIONS

    public function getOptions(?array $customOptions = null): UniversalResourceOptions
    {
        // Change options based on the type of page
        // Options are based on DefaultUniversalResourceOptions
        // As options are not deep merged, full options need to be provided
        $options = parent::getOptions();

        // Index Pages
        if ($this->isHomepageOrIndexpage()) {
            // Remove additional fields
            $options->extendValues([
                'media' => [
                    'heading'               => 'Media',
                    'enabled'               => true,
                    'hero'                  => true,
                    'thumbnail'             => true,
                    'additionalImages'      => false,
                    'video'                 => false,
                    'fallbackColor'         => true,
                ],
                'content' => [
                    'heading'               => 'Content',
                    'enabled'               => true,
                    'summary'               => true,
                    'summaryRequired'       => true,
                    'content'               => false,
                ],
                'related' => [
                    'heading'               => 'Related Content',
                    'enabled'               => false,
                    'content'               => true,
                    'files'                 => true,
                    'posts'                 => true,
                ],
            ]);
        }

        // Return updated options
        return $options;
    }

    // NOVA FIELDS

    // Remove the "Children" relation panel for the Homepage / UCM Index pages
    public function getCustomFields(): array
    {
        $fields = parent::getCustomFields();

        // Homepage, UCM index pages - remove Child Pages
        if ($this->isHomepageOrIndexpage()) {
            $fields = $this->removeFields($fields, ['children']);
        }

        return $fields;
    }

    // Add the Introduction field
    public function getContentFields(): array
    {
        // Define
        $fields = parent::getContentFields();

        // Add the "Introduction" text field after the Summary field
        $summaryPosition = 0;
        foreach($fields as $index => $field) {
            if ($field->attribute == 'summary') {
                $summaryPosition = $index + 1;
                break;
            }
        }
        $fields = array_merge(array_slice($fields, 0, $summaryPosition), [
            Text::make('Introduction', 'data_json->content_intro')
                ->hideFromIndex()
                ->help('Large introduction text for the beginning of the page (optional).'),
        ], array_slice($fields, $summaryPosition));

        // Updated fields
        return $fields;
    }

    // Disable the Content Builder for the Homepage / UCM Index pages
    public function getContentBuilderFields(): array
    {
        // Homepage, UCM index pages - skip the Content Builder
        if ($this->isHomepageOrIndexpage()) {
            return [];
        }

        return parent::getContentBuilderFields();
    }

    // HELPERS

    /**
     * Whether a page represents the homepage, or a Universal Content index pages.
     * These pages will generally have less editable fields in the CMS.
     */
    public function isHomepageOrIndexpage(): bool
    {
        // Account for our local UcmPageTemplate
        return $this->resource->isHomepage() || $this->usesPageTemplate(\App\View\PageTemplates\UcmPageTemplate::class);
    }

}
