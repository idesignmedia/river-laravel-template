<?php

// RIVER Template
namespace App\Nova\Resources;

use App\Enums\AuthRole;
use KABBOUCHI\NovaImpersonate\Impersonate;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use River\DMS\Nova\Resource;
use River\DMS\Nova\Resources\RevisionResource;

class UserResource extends Resource
{
    // NOVA SETUP //

    // The model that this Resource corresponds to (must be defined).
    public static $model = \App\Models\User::class;

    // The logical group associated with the resource.
    public static $group = 'Management';

    // The columns that should be searched.
    public static $search = [
        'id', 'name', 'email',
    ];

    // Get the displayable label of the resource.
    public static function label()
    {
        return 'Users';
    }

    // Get the value that should be displayed to represent the resource.
    public function title()
    {
        return "{$this->name} ({$this->email})";
    }

    // NOVA FIELDS //

    /**
     * Get the fields displayed by the resource.
     */
    public function fields(NovaRequest $request): array
    {
        return [
            // Setup
            Heading::make('User Details', 'user_details'),
            Avatar::make('Avatar')
                ->disk('public')
                ->path('avatars')
                ->prunable()
                ->deletable(),
            Text::make('Name')
                ->sortable()
                ->rules('required', 'max:255'),

            // Ensure we check emails are unique against the "users" table
            Text::make('Email Address', 'email')
                ->sortable()
                ->rules('required', 'email', 'max:255')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            // Password (create form)
            Password::make('Password')
                ->onlyOnForms()
                ->hideWhenUpdating()
                ->creationRules('required', 'string', 'min:5')
                ->updateRules('nullable', 'string', 'min:5'),

            // Admin details
            Select::make('Role', 'role')
                ->rules('required')
                ->options(AuthRole::getValueLabels())
                ->displayUsingLabels()
                ->readonly(fn ($request) => $this->isReadonly($request)),
            Text::make('Phone', 'phone')
                ->rules('nullable', 'max:255')
                ->readonly(fn ($request) => $this->isReadonly($request)),
            Text::make('Company', 'company')
                ->rules('nullable', 'max:255')
                ->readonly(fn ($request) => $this->isReadonly($request)),
            Textarea::make('Comments', 'comments')
                ->alwaysShow()
                ->hideFromIndex(),

            // Impersonate users
            // Impersonate::make($this), // @debt throws an error

            // Revisions
            HasMany::make('Revisions', 'revisions', RevisionResource::class),
        ];
    }

    /**
     * Get the actions for the resource.
     */
    public function actions(NovaRequest $request): array
    {
        return [
            new \River\DMS\Nova\Actions\UpdatePassword,
            new \Modules\Invitation\Nova\Actions\SendUserInvitation,
        ];
    }

    // Whether a user field is readonly
    public function isReadonly($request)
    {
        return false;
    }

}
