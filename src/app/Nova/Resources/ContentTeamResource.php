<?php

// RIVER Template
namespace App\Nova\Resources;

use App\Models\ContentTeam;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use River\DMS\Nova\Resources\SortableUniversalContentResource;

class ContentTeamResource extends SortableUniversalContentResource
{
    // public static $displayInNavigation = false;

    public static $model = ContentTeam::class;

    public function getCustomFields(): array
    {
        return [
            // Heading::make("{$this->resource->getSingularLabel()} Details"),
            Select::make("{$this->resource->getSingularLabel()} Category", 'category')
                ->options(ContentTeam::categoriesArray())
                ->displayUsingLabels()
                ->help("The Category for this {$this->resource->getSingularLabel()}."),
            Text::make('Position', 'position')
                ->hideFromIndex()
                ->help('The position of the team member, to show on the team page.'),
        ];
    }
}
