<?php

// RIVER Template
namespace App\Nova\Resources;

use App\Models\Redirect;
use River\DMS\Nova\Resources\RedirectResource as BaseRedirectResource;

class RedirectResource extends BaseRedirectResource
{
    public static $model = Redirect::class;

    public static $group = 'Pages';
}
