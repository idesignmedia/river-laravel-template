<?php

// RIVER Template
namespace App\Nova\Resources;

use App\Models\NavigationGroup;
use River\DMS\Nova\Resources\NavigationGroupResource as BaseNavigationGroupResource;

class NavigationGroupResource extends BaseNavigationGroupResource
{
    // NOVA SETUP
    
    public static $model = NavigationGroup::class;

    public static $group = 'Pages';
}
