<?php

// RIVER Template
namespace App\Nova\Resources;

use App\Enums\ContactStatus;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use River\DMS\Nova\Resource;
use River\DMS\Nova\Resources\RevisionResource;

class OrganisationResource extends Resource
{
    // NOVA SETUP //

    // The model that this Resource corresponds to (must be defined).
    public static $model = \App\Models\Organisation::class;

    // The logical group associated with the resource.
    public static $group = 'Management';

    // The columns that should be searched.
    public static $search = [
        'id', 'name', 'email',
    ];

    // Get the displayable label of the resource.
    public static function label()
    {
        return 'Organisations';
    }

    // Get the value that should be displayed to represent the resource.
    public function title()
    {
        return $this->name . ($this->reference ? " ({$this->reference}" : '');
    }

    // NOVA FIELDS //

    /**
     * Get the fields displayed by the resource.
     */
    public function fields(NovaRequest $request): array
    {
        return [
            // Setup
            Heading::make('Organisation Details', 'user_details'),
            Text::make('ID', 'id')
                ->sortable()
                ->exceptOnForms(),
            Text::make('Name', 'name')
                ->sortable()
                ->rules('required', 'max:255'),
            Select::make('Status', 'status')
                ->rules('required')
                ->options(ContactStatus::getValueLabels())
                ->displayUsingLabels(),

            // Contact Details
            Text::make('Reference Code', 'reference'),
            Text::make('Phone', 'phone')
                ->hideFromIndex(),
            Text::make('Email', 'email')
                ->hideFromIndex(),
            Text::make('Website', 'website')
                ->hideFromIndex(),
            // Text::make('Address', 'address'),
            // Text::make('Country', 'country'),
            // Text::make('Type', 'type'),
            // Text::make('Type_other', 'type_other'),
            Textarea::make('Comments', 'comments')
                ->hideFromIndex(),

            // Relations
            BelongsToMany::make('Users', 'users', UserResource::class)
                ->fields(function () {
                    return [
                        Text::make('Role'),
                    ];
                }),
            HasMany::make('Revisions', 'revisions', RevisionResource::class),
        ];
    }

}
