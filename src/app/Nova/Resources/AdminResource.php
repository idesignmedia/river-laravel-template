<?php

// RIVER Template
namespace App\Nova\Resources;

use App\Enums\AuthRole;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use River\DMS\Nova\Resource;
use River\DMS\Nova\Resources\RevisionResource;

// use Silvanite\NovaToolPermissions\Role;

class AdminResource extends Resource
{
    // NOVA SETUP //

    // The model that this Resource corresponds to (must be defined).
    public static $model = \App\Models\Admin::class;

    // The logical group associated with the resource.
    public static $group = 'Management';

    // The columns that should be searched.
    public static $search = [
        'id', 'name', 'email',
    ];

    // Get the displayable label of the resource.
    public static function label()
    {
        return 'Admins (Staff)';
    }


    // NOVA FIELDS //

    /**
     * Get the fields displayed by the resource.
     */
    public function fields(NovaRequest $request): array
    {
        return [
            // Setup
            Heading::make('User Details', 'user_details'),
            Avatar::make('Avatar')
                ->disk('public')
                ->path('avatars-nova')
                ->prunable()
                ->deletable(),
            Text::make('Name')
                ->sortable()
                ->rules('required', 'max:255'),

            // Ensure we check emails are unique against the "admins" table
            Text::make('Email Address', 'email')
                ->sortable()
                ->rules('required', 'email', 'max:255')
                ->readonly(fn ($request) => $this->isReadonly($request))
                ->creationRules('unique:admins,email')
                ->updateRules('unique:admins,email,{{resourceId}}'),

            // Password (create form)
            Password::make('Password')
                ->onlyOnForms()
                ->hideWhenUpdating()
                ->creationRules('required', 'string', 'min:5')
                ->updateRules('nullable', 'string', 'min:5'),

            // Admin details
            Select::make('Role', 'role')
                ->rules('required')
                ->options(AuthRole::getValueLabels())
                ->displayUsingLabels()
                ->readonly(fn ($request) => $this->isReadonly($request)),
            Text::make('Phone', 'phone')
                ->rules('nullable', 'max:255')
                ->readonly(fn ($request) => $this->isReadonly($request)),
            Text::make('Company', 'company')
                ->rules('nullable', 'max:255')
                ->readonly(fn ($request) => $this->isReadonly($request)),
            Textarea::make('Comments', 'comments')
                ->alwaysShow()
                ->hideFromIndex(),

            // Manage roles
            // Silvanite Roles & Permissions decomissioned as part of Nova 4 upgrade Oct 22
            // BelongsToMany::make('Roles', 'roles', Role::class),

            // Revisions
            HasMany::make('Revisions', 'revisions', RevisionResource::class),
        ];
    }

    /**
     * Get the actions for the resource.
     */
    public function actions(NovaRequest $request): array
    {
        return [
            new \River\DMS\Nova\Actions\UpdatePassword,
        ];
    }

    // Whether an admin field is readonly
    public function isReadonly($request)
    {
        // A Developer can only be edited by another developer (we use the company as a proxy, generally "RIVER")
        $developerCompany = config('river.admin.developers.company', null);
        if (
            $developerCompany
            && ($this->resource->company == $developerCompany)
            && ($request->user()->company !== $developerCompany)
        ) {
            return true;
        }
        return false;
    }

}
