<?php

// RIVER Template
namespace App\Nova\Resources;

use App\Models\ContentTemplate;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use River\DMS\Nova\Resources\UniversalContentResource;
use Whitecube\NovaFlexibleContent\Flexible;

class ContentTemplateResource extends UniversalContentResource
{
    // public static $displayInNavigation = false;

    // The model that this Resource corresponds to (must be defined).
    public static $model = ContentTemplate::class;

    public static $defaultSort = 'name';
    public static $defaultSortDirection = 'asc';

    // The logical group associated with the resource.
    public static $group = 'Content';

    // How many relations to display per page (use null for no limit).
    public static $perPageViaRelationship = 10;

    public function getCustomFields(): array
    {
        return [];
    }

    // RESOURCE OPTIONS

    // Only ContentBlocks are enabled
    public $options = [
        'setup' => [
            'heading'               => 'Template Setup',
            'nameLabel'             => 'Template Name',
            'editableStatus'        => false,
            'slugCreationRules'     => 'unique:#table#,slug',
            'slugUpdateRules'       => 'unique:#table#,slug,{{resourceId}},id,version_type,master',
            'publishedAt'           => false,
        ],
        'media' => [
            'heading'               => 'Media',
            'enabled'               => false,
        ],
        'content' => [
            'heading'               => 'Content',
            'enabled'               => false,
        ],
        'related' => [
            'heading'               => 'Related Content',
            'enabled'               => false,
        ],
        'meta' => [
            'heading'               => 'Metadata (SEO)',
            'enabled'               => false,
        ],
    ];

    // NOVA FIELDS

}
