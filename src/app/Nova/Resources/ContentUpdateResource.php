<?php

// RIVER Template
namespace App\Nova\Resources;

use App\Models\ContentUpdate;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use River\DMS\Nova\Resources\UniversalContentResource;

class ContentUpdateResource extends UniversalContentResource
{
    // public static $displayInNavigation = false;

    public static $model = ContentUpdate::class;

    public function getCustomFields(): array
    {
        return [
            // Heading::make("{$this->resource->getSingularLabel()} Details"),
            Select::make("{$this->resource->getSingularLabel()} Category", 'category')
                //->rules('required')
                ->options(ContentUpdate::categoriesArray())
                ->displayUsingLabels()
                ->help("The Category for this {$this->resource->getSingularLabel()}."),

            // TEST
            // Text::make('Test', 'data_json->test'),
        ];
    }
}
