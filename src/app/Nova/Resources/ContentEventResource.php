<?php

// RIVER Template
namespace App\Nova\Resources;

use App\Enums\EventRegions;
use App\Models\ContentEvent;
use Illuminate\Support\Carbon;
use Laravel\Nova\Fields\Country;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\Place;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use River\DMS\Nova\Resources\UniversalContentResource;

class ContentEventResource extends UniversalContentResource
{
    // public static $displayInNavigation = false;

    public static $model = ContentEvent::class;

    public function getCustomFields(): array
    {
        return [
            // Heading::make("{$this->resource->getSingularLabel()} Details"),
            Select::make("{$this->resource->getSingularLabel()} Category", 'category')
                //->rules("required")
                ->options(ContentEvent::categoriesArray())
                ->displayUsingLabels()
                ->help("The Category for this {$this->resource->getSingularLabel()}."),

            // Event Details
            Heading::make('Event Details'),
            Select::make('Region', 'region')
                ->rules("required")
                ->options(EventRegions::getValueLabels())
                ->displayUsingLabels(),
            Text::make('Registration Link', 'registration_link')
                ->hideFromIndex()
                ->help('The URL for the register button, if applicable (optional).'),
            Text::make('Registration Label', 'registration_label')
                ->hideFromIndex()
                ->help('The text to use for the Registration Link if used; defaults to "Register" (optional).'),

            // Event Date
            Heading::make('Event Date'),
            DateTime::make('Date / Time Commence', 'date_commence')
                ->rules("required")
                ->fillUsing(function ($request, $model, $attribute, $requestAttribute) {
                    // WIP: only create works
                    // Handle UTC datetime strings passed in during create; eg, "2023-06-06T08:17:55.000Z"; and edit, eg, "2023-06-05T08:06:44+"
                    $dateString = str_replace('+', '.000Z', $request->input($requestAttribute));
                    if($dateString) {
                        $model->{$attribute} = Carbon::parse($dateString)->format('Y-m-d H:i:s');
                    }
                })
                ->help('The official start date and time of this Event.'),
            DateTime::make('Date / Time End', 'date_end')
                ->hideFromIndex()
                ->fillUsing(function ($request, $model, $attribute, $requestAttribute) {
                    // WIP: only create works
                    // Handle UTC datetime strings passed in during create; eg, "2023-06-06T08:17:55.000Z"; and edit, eg, "2023-06-05T08:06:44+"
                    $dateString = str_replace('+', '.000Z', $request->input($requestAttribute));
                    if($dateString) {
                        $model->{$attribute} = Carbon::parse($dateString)->format('Y-m-d H:i:s');
                    }

                })
                ->help('The date and time the Event ends (optional).'),
            Text::make('Date Label', 'date_label')
                ->hideFromIndex()
                ->help('
                    An optional label to use for the date, instead of the actual date.<br>
                    Useful to convey additional information.
                '),

            // Event Address
            Heading::make('Event Address'),
            Text::make('Address Label', 'address_label')
                ->hideFromIndex()
                ->help('
                    An optional label to use for the address, instead of the actual address.<br>
                    Useful to convey additional information.
                '),
            ...$this->addressFields(),
        ];
    }

    protected function addressFields(): array
    {
        return [
            Place::make('Address', 'address_line_1')
                ->hideFromIndex()
                ->countries(['NZ'])
                ->help('
                    The full street address of the Event (optional).<br>
                    Start typing to select a New Zealand address.
                '),
            Text::make('Address Line 2', 'address_line_2')->hideFromIndex(),
            Text::make('Suburb', 'suburb')->hideFromIndex(),
            Text::make('City', 'city')->hideFromIndex(),
            // Text::make('State', 'state')->hideFromIndex(),
            Text::make('Postal Code', 'postal_code')->hideFromIndex(),
            Country::make('Country', 'country')
                ->hideFromIndex()
                ->withMeta([
                    'value' => 'NZ',
                ]),
            Text::make('Latitude', 'latitude')->hideFromIndex(),
            Text::make('Longitude', 'longitude')->hideFromIndex(),
        ];
    }
}
