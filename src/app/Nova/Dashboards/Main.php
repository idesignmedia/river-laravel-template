<?php

namespace App\Nova\Dashboards;

use Laravel\Nova\Cards\Help;
use Laravel\Nova\Dashboards\Main as Dashboard;

class Main extends Dashboard
{
    /**
     * Get the cards for the dashboard.
     */
    public function cards(): array
    {
        return [
            new Help,

            // new \Tightenco\NovaGoogleAnalytics\PageViewsMetric,
            // new \Tightenco\NovaGoogleAnalytics\VisitorsMetric,
            // new \Tightenco\NovaGoogleAnalytics\MostVisitedPagesCard,
            // new \Tightenco\NovaGoogleAnalytics\ReferrersList,
            // new \Tightenco\NovaGoogleAnalytics\OneDayActiveUsersMetric,
            // new \Tightenco\NovaGoogleAnalytics\SevenDayActiveUsersMetric,
            // new \Tightenco\NovaGoogleAnalytics\FourteenDayActiveUsersMetric,
            // new \Tightenco\NovaGoogleAnalytics\TwentyEightDayActiveUsersMetric,
            // new \Tightenco\NovaGoogleAnalytics\SessionsMetric,
            // new \Tightenco\NovaGoogleAnalytics\SessionDurationMetric,
            // new \Tightenco\NovaGoogleAnalytics\SessionsByDeviceMetric,
            // new \Tightenco\NovaGoogleAnalytics\SessionsByCountryMetric,
            
        ];
    }
}
