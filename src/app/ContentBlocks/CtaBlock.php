<?php

// RIVER Template: Website
namespace App\ContentBlocks;

use Laravel\Nova\Fields\Heading;
use River\DMS\ContentBlocks\BaseContentBlock;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Whitecube\NovaFlexibleContent\Flexible;
use Whitecube\NovaFlexibleContent\Layouts\Layout;

// A CTA for this Project
// This is an example Content Block
class CtaBlock extends BaseContentBlock
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'block-cta';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'CTA Block';

    /**
     * Get the fields displayed by the layout.
     */
    public function fields(): array
    {
        $linkBlock = \River\DMS\ContentBlocks\Child\LinkChildBlock::class;

        return [
            $this->getTitleField('Title', 'title')
                ->help('An optional title for the CTA.')
                ->required(),
            $this->getContentField(buttons: 'some')
                ->help('An optional summary for the CTA.'),
            Flexible::make('Links', 'child_links')
                ->addLayout($linkBlock)
                ->limit(2)
                ->button('Add Link'),
            $this->getImageField(),
            // ->help('An optional image to show to the left of the CTA.'),
            Heading::make('Optional image should be a JPG image file, square format (approx. 1200px x 1200px), less than 2MB.'),
        ];
    }

    public function getLinks(bool $filterValid = true)
    {
        $links = $this->getContentBlocks('child_links')->filter(function ($link) use ($filterValid) {
            return $filterValid
                ? $link->isValid()
                : true;
        });
        return $links;
    }

    public function hasLinks()
    {
        $links = $this->getContentBlocks('child_links')->filter(function ($link) {
            return $link->isValid();
        });
        return $links->count() ? true : false;
    }

    public function getLink(int $index = 1)
    {
        $link = $this->getContentBlocks('child_links')->get($index - 1) ?? null;
        return $link && $link->isValid() ? $link : null;
    }

    public function getImageMedia(): ?Media
    {
        return $this->getFirstMedia('image');
    }

    public function getImageUrl(): ?string
    {
        return $this->getImageMedia()?->getUrl('medium');
    }

    public function isValid(): bool
    {
        return ($this->attributes['title'] || $this->attributes['content'] || $this->hasLinks());
    }

}
