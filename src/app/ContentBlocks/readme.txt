[river-laravel-dms] Content Blocks
--

Project folder for Content Blocks.

To add custom Content Blocks for this project:
- Define Content Block classes here (see the DMS for examples)
- Add a matching view component inside src/resources/views/components/contentblocks/{$name}.blade.php
- Load the ContentBlock class inside src/config/river/contentblocks.php (either 'project_blocks' or 'blocks')
