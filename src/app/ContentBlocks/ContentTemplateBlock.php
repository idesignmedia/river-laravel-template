<?php

// RIVER Template: Website
namespace App\ContentBlocks;

use App\Models\ContentTemplate;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\Select;
use River\DMS\ContentBlocks\BaseContentBlock;
use Whitecube\NovaFlexibleContent\Flexible;
use Whitecube\NovaFlexibleContent\Layouts\Layout;

// Select a ContentTemplate to display for the Block
// Inspired by UniversalContentSelectorBlock
class ContentTemplateBlock extends BaseContentBlock
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'block-content-template';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'Content Template';

    /**
     * Get the fields displayed by the layout.
     *
     * @return array
     */
    public function fields()
    {
        // Template Options
        $options = $this->getModelOptions();

        return [
            Select::make('Content Template', 'model')
                ->options($options)
                ->required()
                ->searchable()
                ->displayUsingLabels()
                ->help('The Content Template to display. Note, only Public Templates are shown.'),
        ];
    }

    /**
     * Whether this Block is valid (all required fields are filled in).
     * Note, this does not check if the model is public - this should be done in the corresponding view.
     */
    public function isValid(): bool
    {
        return (! empty($this->attributes['model']));
    }

    /**
     * Return the Master Version of the associated Universal Content Model (or Page) for this Block.
     * The published relation is loaded, so you can call ->getPublishedVersion() to render content.
     * If the model cannot be found (for example, it was deleted) then null is returned.
     */
    public function getModel(): ?ContentTemplate
    {
        return static::getModelFromSignature($this->attributes['model'] ?? null);
    }

    /**
     * Get the Content Template options to display to the user.
     * Each key should refer to a model signature (eg, "{class}::{$id}").
     */
    public function getModelOptions(): array
    {
        // Gather Page instances
        $resourceClass = ContentTemplate::class;
        $resources = $resourceClass::query()
            ->masterVersion()
            ->with('publishedVersionRelation')
            ->orderBy('name', 'asc')
            ->get();
        $options = [];

        $resources->map(function($resource) use(&$options, $resourceClass) {
            // Define
            $key = static::getModelSignature($resourceClass, $resource->id);
            $slug = '/' . trim($resource->getPath(), '/');
            $name = $resource->getPublishedVersion()->getName();
            $label = "{$resourceClass::indexLabel()}: {$name} ({$slug})";

            // Push into options
            $options[$key] = $label;
        });

        return $options;
    }

    /**
     * Return a model signature ready for storage from the database.
     * The signature format is "{$modelClass}::{$modelId}".
     */
    public static function getModelSignature(string $modelClass, int $modelId): string
    {
        return "{$modelClass}::{$modelId}";
    }
    
    /**
     * Return the Master Version of the associated Universal Content Model (or Page),
     * from a valid model signature in the format "{$modelClass}::{$modelId}".
     * If the model cannot be found (for example, it was deleted) then null is returned.
     */
    public static function getModelFromSignature(?string $modelSignature): ?ContentTemplate
    {
        if (! empty($modelSignature)) {
            $modelClass = \Str::before($modelSignature, '::');
            $modelId = \Str::after($modelSignature, '::');
            $model = $modelClass::query()
                ->masterVersion()
                ->with('publishedVersionRelation')
                ->where('id', $modelId)
                ->first();
            return $model;
        }

        return null;
    }

}
