<?php
// RIVER Template (Web App)

namespace App\Enums;

use River\Core\Enums\LabelEnum;

final class ActivityType extends LabelEnum
{
    public const LOG            = 'log';
    public const NOTE           = 'note';
    public const ORGANISATION   = 'organisation';
    public const USER           = 'user';
    public const ADMIN          = 'admin';
    public const OTHER          = 'other';

    /**
     * A list of human-readable labels for each value.
     */
    public static $labels = [
        'log'               => 'Log',
        'note'              => 'Note',
        'organisation'      => 'Organisation',
        'user'              => 'User',
        'admin'             => 'Admin',
        'other'             => 'Other',
    ];

}
