<?php
// RIVER Template (Web App)

namespace App\Enums;

use River\Core\Enums\LabelEnum;

// Shared status options for Orgainsations and Users
final class ContactStatus extends LabelEnum
{
    // Status options
    public const DRAFT = 'draft';
    public const ACTIVE = 'active';
    public const ARCHIVED = 'archived';

    /**
     * A list of human-readable labels for each value.
     */
    public static $labels = [
        'draft'         => 'Draft',
        'active'        => 'Active',
        'archived'      => 'Archived',
    ];

}
