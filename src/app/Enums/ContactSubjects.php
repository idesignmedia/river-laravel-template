<?php
// RIVER Template (Website)

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * Fallback Content Colours for Universal Content
 * Defaults to a pastel set of rainbow colours
 */
final class ContactSubjects extends Enum
{
    public const GENERAL_ENQUIRY = 'General Enquiry';
    public const FEEDBACK = 'Feedback';
    public const MEDIA_ENQUIRY = 'Media Enquiry';
    public const VOLUNTEER = 'Volunteer or Support Anxiety NZ';
    public const OTHER = 'Other';

    public static function getKeyValues()
    {
        return self::getConstants();
    }

    public static function getValueAsValueArray()
    {
        $values = [];
        foreach (self::getValues() as $value) {
            $values[$value] = $value;
        }
        return $values;
    }
}
