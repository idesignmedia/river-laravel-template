<?php
declare(strict_types=1);

namespace App\Enums;

use River\Core\Enums\LabelEnum;

// Event Regions
final class EventRegions extends LabelEnum
{
    // New Zealand Regions
    public const Northland         = 'northland';
    public const Auckland          = 'auckland';
    public const Waikato           = 'waikato';
    public const BayOfPlenty       = 'bayofplenty';
    public const Gisborne          = 'gisborne';
    public const HawkesBay         = 'hawkesbay';
    public const Taranaki          = 'taranaki';
    public const ManawatuWanganui  = 'manawatuwanganui';
    public const Wellington        = 'wellington';
    public const Nelson            = 'nelson';
    public const Marlborough       = 'marlborough';
    public const WestCoast         = 'westcoast';
    public const Canterbury        = 'canterbury';
    public const Otago             = 'otago';
    public const Southland         = 'southland';
    public const Nationwide        = 'nationwide';

    // Other
    public const Online            = 'online';
    public const International     = 'international';
    public const Other             = 'other';

    /**
     * A list of human-readable labels for each value.
     */
    public static $labels = [
        // New Zealand Regions
        'northland'         => 'Northland',
        'auckland'          => 'Auckland',
        'waikato'           => 'Waikato',
        'bayofplenty'       => 'Bay of Plenty',
        'gisborne'          => 'Gisborne',
        'hawkesbay'         => 'Hawke\'s Bay',
        'taranaki'          => 'Taranaki',
        'manawatuwanganui'  => 'Manawatu-Wanganui',
        'wellington'        => 'Wellington',
        'nelson'            => 'Nelson',
        'marlborough'       => 'Marlborough',
        'westcoast'         => 'West Coast',
        'canterbury'        => 'Canterbury',
        'otago'             => 'Otago',
        'southland'         => 'Southland',
        'nationwide'        => 'Nationwide',

        // Other
        'online'            => 'Online',
        'international'     => 'International',
        'other'             => 'Other',
    ];

}
