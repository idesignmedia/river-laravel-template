<?php
// RIVER Template

namespace App\Enums;

use River\Core\Enums\LabelEnum;

// Default Auth Roles for Admin and Users
final class AuthRole extends LabelEnum
{
    // Auth Roles
    public const DEVELOPER = 'developer';
    public const ADMIN = 'admin';
    public const STAFF = 'staff';
    public const USER = 'user';

    /**
     * A list of human-readable labels for each value.
     */
    public static $labels = [
        'developer'     => 'Developer',
        'admin'         => 'Admin',
        'staff'         => 'Staff',
        'user'          => 'User',
    ];

}
