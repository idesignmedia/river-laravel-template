<?php
// RIVER Template (Web App)

namespace App\Enums;

use River\Core\Enums\LabelEnum;

// Official Contact types - Orgainsations and Users
final class ContactType extends LabelEnum
{
    // Status options
    public const ORGANISATION = 'organisation';
    public const USER = 'user';

    /**
     * A list of human-readable labels for each value.
     */
    public static $labels = [
        'organisation'  => 'Organisation',
        'user'          => 'User',
    ];

}
