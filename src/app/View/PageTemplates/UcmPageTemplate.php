<?php

// RIVER Template
namespace App\View\PageTemplates;

use Illuminate\Http\Request;
use Laravel\Nova\Resource;
use River\DMS\View\PageTemplates\Template;

// A Universal Content Index page represents the landing page for UCM Posts.
class UcmPageTemplate extends Template
{

    public string $name = 'Index Page';

    /**
     * The location to search for page templates.
     */
    protected string $directory = 'pages.universalcontent';

    /**
     * Content fields to enable in Nova.
     * Here we add custom fields relevant for Synced pages.
     * We remove fields via our local PageResource resource.
     */
    public function novaFields(Request $request, Resource $resource): array
    {
        return [];
    }

    /**
     * Locate the blade view to render.
     * For synced pages we look for a pages by name or index, in the' synced' folder.
     */
    public function view(): string
    {
        $viewName = $this->getViewName();

        // Check if there is a folder matching the viewName with an "index" view.
        if (view()->exists("{$this->directory}.{$viewName}.index")) {
            return "{$this->directory}.{$viewName}.index";
        }

        return "{$this->directory}.default.index";
    }

    /**
     * Determine the view name from the pages path.
     * A UCM index page should be defined via routes/web
     */
    private function getViewName(): string
    {
        return $this->page->slug;
    }

}
