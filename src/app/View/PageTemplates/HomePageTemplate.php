<?php

// RIVER Template
namespace App\View\PageTemplates;

use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Resource;
use River\DMS\View\PageTemplates\Template;
use Whitecube\NovaFlexibleContent\Flexible;

// The Homepage represents the "/" index page of the website.
class HomePageTemplate extends Template
{

    public string $name = 'Homepage';

    /**
     * Content fields to enable in Nova.
     * Here we add custom fields for the Homepage based on the project.
     * We remove fields via our local PageResource resource.
     */
    public function novaFields(Request $request, Resource $resource): array
    {
        $navigationBlock = \River\DMS\ContentBlocks\Child\UniversalContentSelectorBlock::class;

        return [

            // Featured Posts
            Heading::make('Featured Posts', 'featured-heading'),
            Flexible::make('Featured Posts', 'related_blocks')
                ->button('Add Featured Post')
                ->fullWidth()
                ->hideFromIndex()
                ->limit(2)
                ->addLayout($navigationBlock),

            // Hero Promo (for the homepage)
            Heading::make('Hero Promo', 'promo-heading'),
            Boolean::make('Promo Enabled', 'data_json->promo_enabled')
                ->hideFromIndex()
                ->help('Showcase the custom Hero Promo.'),
            Images::make('Promo Image', 'promo')
                ->conversionOnIndexView('small')
                ->enableExistingMedia()
                ->customPropertiesFields([
                    Text::make('Name', 'name'),
                    Text::make('Summary', 'summary'),
                    Text::make('Alt Text', 'alt'),
                ])
                ->help('Promo image  to use (optional).')
                ->hideFromIndex(),

            Text::make('Promo Text 1', 'data_json->promo_text')
                ->hideFromIndex()
                ->help('The main hero text to use (optional).'),
            Text::make('Promo Text 2', 'data_json->promo_text2')
                ->hideFromIndex()
                ->help('Secondary smaller text to use (optional).'),

            Text::make('Button 1 Name', 'data_json->promo_button_name')
                ->hideFromIndex()
                ->help('The name to use for the first button  (optional).'),
            Text::make('Button 1 URL', 'data_json->promo_button_url')
                ->onlyOnForms()
                ->help('The URL to use for the first button, if applicable.<br>Internal links should start with "/", external links with "http://".'),
            Text::make('Button 1 URL', function () {
                $href = $this->page->data_json['promo_button_url'] ?? null;
                return $href
                    ? "<a href='{$href}' class='link-default' target='_blank'>{$href}</a>"
                    : '—';
            })->asHtml()->onlyOnDetail(),

            Text::make('Button 2 Name', 'data_json->promo_button2_name')
                ->hideFromIndex()
                ->help('The name to use for the first button (optional).'),
            Text::make('Button 2 URL', 'data_json->promo_button2_url')
                ->onlyOnForms()
                ->help('The URL to use for the first button, if applicable.<br>Internal links should start with "/", external links with "http://".'),
            Text::make('Button 2 URL', function () {
                $href = $this->page->data_json['promo_button2_url'] ?? null;
                return $href
                    ? "<a href='{$href}' class='link-default' target='_blank'>{$href}</a>"
                    : '—';
            })->asHtml()->onlyOnDetail(),
        ];
    }
    
    /**
     * Identify the blade view to render.
     */
    public function view(): string
    {
        return 'pages.synced.home';
    }

}
