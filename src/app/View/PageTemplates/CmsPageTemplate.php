<?php

// RIVER Template
namespace App\View\PageTemplates;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Resource;
use River\DMS\View\PageTemplates\Template;

// A CMS Page is used for pages that are created from the CMS.
class CmsPageTemplate extends Template
{

    public string $name = 'CMS Page';

    /**
     * Available page template layouts for nova admin to choose from.
     * If multiple templates are available, these show up on the Page Resource in Nova accordingly.
     * These represent blade template layouts located in "/resources/views/pages/templates/..."
     */
    public array $templates = [
        'cms-default' => 'Default',
        'cms-inline' => 'Inline Hero',
    ];

    /**
     * Get the list of available page templates.
     * Defaults to $this->templates
     */
    public function pageTemplates(): array
    {
        return $this->templates;
    }

    /**
     * Get the default page template to use.
     * Defaults to the first key of $this->templates, with a fallback value of 'cms-default'
     */
    public function defaultPageTemplate(): string
    {
        $keys = array_keys($this->templates);
        return $keys[0] ?? 'cms-default';
    }

    /**
     * Content fields to enable in Nova.
     */
    public function novaFields(Request $request, Resource $resource): array
    {
        // Page Template Select Field
        $pageTemplateField = null;

        if (count($this->pageTemplates()) > 1) {
            // Only show the Page Templates field if multiple options are available
            $pageTemplateField = Select::make('Template Layout', 'template_layout')
                    ->required()
                    ->hideFromIndex()
                    ->options($this->pageTemplates())
                    ->displayUsingLabels()
                    ->help('The Template Layout to use for the design of this Page');
            if (! $this->page->id) {
                $pageTemplateField->withMeta([
                    'value' => $this->defaultPageTemplate(),
                ]);
            }
        }

        $pageTemplateFields = $pageTemplateField
            ? [$pageTemplateField]
            : [];

        // CMS Page Fields
        return [
            ...$pageTemplateFields,
        ];
    }

    /**
     * Locate the blade view to render.
     */
    public function view(): string
    {
        $template = $this->page->template_layout ?? $this->defaultPageTemplate();
        return "pages.templates.{$template}"; // eg, pages.templates.cms-default'
    }

}
