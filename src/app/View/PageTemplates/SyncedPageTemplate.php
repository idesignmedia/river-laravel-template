<?php

// RIVER Template
namespace App\View\PageTemplates;

use Illuminate\Http\Request;
use Laravel\Nova\Resource;
use River\DMS\View\PageTemplates\Template;

// A Synced page represents a custom design file that is manually added via config.
class SyncedPageTemplate extends Template
{

    public string $name = 'Default Page';

    /**
     * The location to search for page templates.
     */
    protected string $directory = 'pages.synced';

    /**
     * Content fields to enable in Nova.
     * Here we add custom fields relevant for Synced pages.
     * We remove fields via our local PageResource resource.
     */
    public function novaFields(Request $request, Resource $resource): array
    {
        return [];
    }

    /**
     * Locate the blade view to render.
     * For synced pages we look for a pages by name or index, in the' synced' folder.
     */
    public function view(): string
    {
        $viewName = $this->getViewName();

        // Check if there is a folder matching the viewName with an "index" view.
        if (view()->exists("{$this->directory}.{$viewName}.index")) {
            return "{$this->directory}.{$viewName}.index";
        }

        return "{$this->directory}.$viewName";
    }

    /**
     * Determine the view name from the pages path.
     */
    private function getViewName(): string
    {
        // Grab the path; ensure home paths are formatted to "home"
        $path = $this->page->path == '/' ? 'home' : $this->page->path;

        // Format in the laravel view path syntax
        return str_replace('/', '.', $path);
    }

}
