<?php

// RIVER Template
namespace App\View\Components;

use Illuminate\View\Component;
use River\DMS\Contracts\DisplayablePage;
use River\DMS\Contracts\WithDisplayablePage;

/**
 * Render a Page view with associated attributes and meta data.
 * This Page components knows about Universal Content, and Pages.
 */
class Page extends Component
{
    public function __construct(

        // An object that implements River\DMS\Contracts\DisplayablePage,
        // or River\DMS\Contracts\WithDisplayableCard.
        // This will be used to determine the page attributes if none are provided.
        ?Object $page = null,

        // The page title.
        public ?string $title = null,

        // Whether to suffix the title with the app name
        bool $appendTitle = true,

        // The page meta description.
        public ?string $description = null,

        // The page meta keywords.
        public ?string $keywords = null,

        // The default body scope for this project
        // This is applied on the page via views/layouts/page.blade.php, onto <x-box as="body" class="...">
        public null|bool|string $scope = null,

        // Whether the header should be "floating"
        // This informs the 'header' variable of the <x-layout.header> component
        // You can use request()->is('/') to default to true for the homepage
        public ?bool $floatingHeader = false,

        // The layout view to render.
        public string $view = 'layouts.page',
    ) {
        // Define our displayable page.
        $displayPage = null;

        // Check if $page implements WithDisplayablePage
        if ($page instanceof WithDisplayablePage) {
            $displayPage = $page;//->getDisplayablePage();
        }

        // Check if $page is a DisplayablePage
        if ($page instanceof DisplayablePage) {
            $displayPage = $page;
        }

        // Determine page attributes
        $this->page = $page;
        $this->title = $title ?? $displayPage?->getPageTitle();
        $this->description = $description ?? $displayPage?->getPageDescription();
        $this->keywords = $keywords ?? $displayPage?->getPageKeywords();

        if ($appendTitle) {
            $this->title = ! empty($this->title)
                ? $this->title . ' | ' . config('app.name')
                : config('app.name');
        }
    }

    public function getView(): string
    {
        return $this->view;
    }

    public function getAttributes(): array
    {
        // Note, public attributes are automatically passed to the view
        return [
            'page' => $this->page,
            'title' => $this->title,
            'description' => $this->description,
            'keywords' => $this->keywords,
        ];
    }

    // Render the page layout
    public function render()
    {
        return view(
            $this->getView(),
            $this->getAttributes()
        );
    }
}
