<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Spatie\MediaLibrary\MediaCollections\Models\Media as BaseMedia;

/**
 * @debt we have an issue with Laravel Nova > 4.17.1, that stops custom attributes within Spatie Media Library + Advanced media Library for Nova from working:
 * In Nova > 4.18 certain casts must be AsArrayObject::class (as opposed to 'array') to avoid "indirect overload of properties" exception,
 * However, this clashes with Spatie Media Libary FileAdder::withCustomProperties which requires argument 1 to be of type 'array',
 * Which means we either cannot add the custom attributes via Nova, or we cannot publish our Universal Content, without an exception thrown.
 * We temporarily resolve this by locking Nova to 4.17.1...
 */
class SpatieMedia extends BaseMedia
{
    // Fix casts for Nova (from 'array' to AsArrayObject::class)
    // protected $casts = [
    //     'manipulations' =>              AsArrayObject::class,
    //     'custom_properties' =>          AsArrayObject::class,
    //     'generated_conversions' =>      AsArrayObject::class,
    //     'responsive_images' =>          AsArrayObject::class,
    // ];
}
