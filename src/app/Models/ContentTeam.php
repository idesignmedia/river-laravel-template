<?php
// RIVER Template (Website)

namespace App\Models;

use River\DMS\Models\SortableUniversalContentModel;
use River\Revisionable\Behaviour\HasRevisions;
use River\Revisionable\Contracts\Revisionable;

class ContentTeam extends SortableUniversalContentModel implements Revisionable
{
    use HasRevisions;

    public $routePrefix     = 'about/team';
    public $indexLabel      = 'Team';
    public $singularLabel   = 'Member';

    public $options = [
        'hasRevisions' => true,
        'dateFormat' => false,
        // 'categories' => null,
        'contentBlocks' => true,
        'contentVersions' => true,
        'shareableLinks' => true,
        // 'showPage' => true, // Disable showPage if the show pages are disabled for this project
        // 'relatedPostsBlock' => \River\DMS\ContentBlocks\Child\UniversalContentSelectorBlock::class,
        'categories' => [
            'board'         => 'Board',
            'executive'     => 'Executive',
            'production'    => 'Production',
        ],
    ];

    public $filters = [
        'orderBy' => ['sort_order' => 'asc'],
        'pagination' => null,
    ];

    public function getPosition(): string
    {
        return $this->position ?? '-';
    }

    public function getPostDetailAttributes(): ?array
    {
        return [
            'name' => 'Name',
            'position' => 'Position',
        ];
    }

}
