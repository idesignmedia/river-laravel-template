<?php
// RIVER Template (Web App)

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use River\Revisionable\Behaviour\HasRevisions;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

// Used to store notes on the Platform. Notes are a subset of Activity that include additional information such as attachments.
// When we transform Activity into ActivityData DTOs for the frontend, if an Activity is linked to a Note, we pass through additional information.
class Note extends Model implements HasMedia
{
    // SETUP
    use HasRevisions;
    use SoftDeletes;
    use InteractsWithMedia;
    // use HasFactory;

    protected $attributes = [
        'data'              => '[]',
    ];

    protected $casts = [
        'date_actioned'     => 'datetime', // datetime:Y-m-d
        'data'              => AsArrayObject::class,
    ];

    protected $fillable = [
        'author_type', 'author_id',
        'name', 'content',
        'option_pinned', 'option_important',
    ];

    // RELATIONS

    public function author()
    {
        return $this->morphTo(__FUNCTION__, 'author_type', 'author_id');
    }

    public function noteable()
    {
        return $this->morphTo(__FUNCTION__, 'noteable_type', 'noteable_id');
    }

    // FUNCTIONS

    /**
     * Get the attachment download URL for this note
     *
     * The route can always be generated but may return a 404 if the note does not have an attachment.
     */
    public function getAttachmentDownloadUrl(): string
    {
        return route('api.notes.attachment', ['id' => $this->id]);
    }
}
