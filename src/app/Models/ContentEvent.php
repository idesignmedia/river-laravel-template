<?php
// RIVER Template (Website)

namespace App\Models;

use App\Enums\EventRegions;
use Carbon\CarbonInterface;
use River\DMS\Models\UniversalContentModel;
use River\Revisionable\Behaviour\HasRevisions;
use River\Revisionable\Contracts\Revisionable;
use Whitecube\NovaFlexibleContent\Value\FlexibleCast;
use Illuminate\Database\Eloquent\Casts\AsArrayObject;

class ContentEvent extends UniversalContentModel implements Revisionable
{
    use HasRevisions;

    public $routePrefix = 'events';
    public $indexLabel = 'Events';
    public $singularLabel = 'Event';

    public $options = [
        'hasRevisions' => true,
        // 'dateFormat' => 'j M y',
        // 'categories' => null,
        'contentBlocks' => true,
        'contentVersions' => true,
        'shareableLinks' => true,
        // 'showPage' => true,
        // 'relatedPostsBlock' => \River\DMS\ContentBlocks\Child\UniversalContentSelectorBlock::class,
        'categories' => [
            'person'            => 'In-Person',
            'online'            => 'Online',
            'other'             => 'Other',
        ],
    ];

    public $filters = [
        'orderBy' => ['date_commence' => 'desc'],
        'pagination' => 12,
    ];

    // 'Y-m-d+' "1980-05-17T12:00:00.000Z"
    protected $casts = [
        // Events
        // :Y-m-d\TH:m:s+
        'date_commence' => 'datetime',
        'date_end'      => 'datetime',
        // Default UCM
        'published_at'      => 'datetime',
        'sort_order'        => 'integer',
        'data_json'         => 'array', // AsArrayObject::class,
        'cache_json'        => 'array', // AsArrayObject::class,
        'related_blocks'    => FlexibleCast::class,
    ];

    // FUNCTIONS

    // Event Date

    public function getTimezone(): string
    {
        // return 'Pacific/Auckland';
        return 'UTC';
    }

    public function getFormattedDate($datetime): ?string
    {
        return $datetime
            ? $datetime->setTimezone($this->getTimezone())->format('jS M, y')
            : null;
    }

    public function getFormattedTime($datetime): ?string
    {
        return $datetime
            ? $datetime->setTimezone($this->getTimezone())->format('H:i')
            : null;
    }

    public function getFormattedDateTime($datetime): ?string
    {
        return $datetime
            ? $datetime->setTimezone($this->getTimezone())->format('D jS M, Y \@ H:i')
            : null;
    }

    public function getDisplayDate(): string
    {
        if ($this->date_label) {
            return $this->date_label;
        }
        if ($this->date_commence) {
            $commence = $this->getFormattedDateTime($this->date_commence);
            $end = $this->getFormattedDateTime($this->date_end);
            return $commence  . ($end ? ' - ' . $end : '');
        }
        return '-';
    }

    // Event Region

    public function getDisplayRegion(): string
    {
        return $this->region
            ? EventRegions::getLabel($this->region)
            : '-';
    }

    // Event Registration

    public function hasRegistrationLink(): bool
    {
        return $this->registration_link
            ? true
            : false;
    }

    public function getRegistrationLink(): ?string
    {
        return $this->registration_link ?? null;
    }

    public function getRegistrationLabel(): ?string
    {
        return $this->registration_label ?? 'Register';
    }

    // Event Address

    public function showAddress(): bool
    {
        return $this->show_address;
    }

    public function hasStreetAddress(): bool
    {
        return $this->address_line_1
            ? true
            : false;
    }

    public function getDisplayAddress(): string
    {
        if ($this->address_label) {
            return $this->address_label;
        }
        if ($this->hasStreetAddress()) {
            return $this->getFormattedAddress();
        }
        return '-';
    }

    public function getFormattedAddress(): ?string
    {
        $address = [
            $this->address_line_1,
            $this->address_line_2,
            $this->suburb,
            $this->postal_code,
            $this->state,
            $this->country,
        ];

        return $this->hasStreetAddress()
            ? implode(', ', array_filter($address))
            : null;
    }

    public function getGoogleMapAddress(): ?string
    {
        $address = [
            $this->address_line_1,
            $this->address_line_2,
            $this->suburb,
            $this->postal_code,
            $this->state,
            $this->country,
        ];

        return $this->hasStreetAddress()
            ? implode(',+', array_filter($address))
            : null;
    }

    public function getGoogleMapAddressLink(): ?string
    {
        $address = $this->getGoogleMapAddress();

        return $address
            ? 'https://www.google.com/maps/place/' . $address
            : null;
    }

    // UNIVERSAL CONTENT

    public function getCardCaption(): ?string
    {
        $caption = $this->indexLabel() ?? '';

        if ($this->getCategory()->isValid()) {
            $caption .= ($caption ? ' / ' : '') . (string) $this->getCategory();
        }

        $dateLabel = $this->getDisplayDate();
        if ($dateLabel) {
            $caption .= ($caption ? ', ' : '') . $dateLabel;
        }

        return $caption;
    }

    public function getPostDetailAttributes(): ?array
    {
        return [
            'date_label' => 'Date',
            'date_commence' => 'Time',
            'address_label' => 'Address',
        ];
    }

    public function getPostDetails(): ?array
    {
        // Define
        $details = [];

        // Date & Time
        if ($this->date_label) {
            $details['Date'] = $this->date_label;
        } elseif ($this->date_commence && $this->date_end) {
            $details['Start'] = $this->getFormattedDateTime($this->date_commence);
            $details['End'] = $this->getFormattedDateTime($this->date_end);
            $details['Duration'] = $this->date_commence->diffForHumans($this->date_end, CarbonInterface::DIFF_ABSOLUTE);
        } elseif ($this->date_commence) {
            $details['Date'] = $this->getFormattedDateTime($this->date_commence);
        }

        // Region
        $details['Region'] = $this->getDisplayRegion();

        // Address
        if ($this->show_address) {
            $details['Address'] = $this->getDisplayAddress();
        }

        return $details;
    }

}
