<?php
// RIVER Template (Web App)

namespace App\Models;

use App\Data\Contacts\UserData;
use App\Enums\AuthRole;
use App\Support\Models\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Invitation\Contracts\Invitable;
use Modules\Invitation\HasInvitations;
use River\Revisionable\Behaviour\HasRevisions;
use River\Revisionable\Contracts\Revisionable;

class User extends Authenticatable implements Revisionable, Invitable
{
    // SETUP
    use HasRevisions;
    // use SoftDeletes;
    // use HasFactory;

    // Invitations
    use HasInvitations;

    protected $casts = [
        // Auth
        'email_verified_at' => 'datetime',
        // Contact
        'date_added'        => 'datetime', // datetime:Y-m-d
        'auth_at'           => 'datetime:Y-m-d',
        'login_at'          => 'datetime:Y-m-d',
    ];

    protected $attributes = [
        'role' => AuthRole::USER,
    ];

    // RELATIONSHIPS

    public function admin()
    {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }

    // COMMON USER

    // Whether the user is a front-end user
    public function isUser(): bool
    {
        return true;
    }

    // AUTH

    public function authEnabled(): bool
    {
        return $this->auth_at ? true : false;
    }

    // CONTACT

    public function getName(): string
    {
        return $this->name;
    }

    public function getNumber(): int
    {
        return $this->id;
    }

    public function getNumberLabel(): string
    {
        return 'User ' . $this->getNumber();
    }

    public function getCountryLabel(): ?string
    {
        // @debt tie into a country package (and type to front-end)
        if ($this->country) {
            return $this->country == 'nz'
                ? 'New Zealand'
                : strtoupper($this->country);
        }
        return null;
    }

    public function getAddressLabel(): ?string
    {
        $pieces = [];
        if ($this->address) {
            $pieces[] = $this->address;
        }
        if ($this->getCountryLabel()) {
            $pieces[] = $this->getCountryLabel();
        }
        return ! empty($pieces)
            ? implode(', ', $pieces)
            : null;
    }

    // QUERY

    /**
     * Scope query to match the given status.
     *
     * @param  string  $keyword
     */
    public function scopeStatus(Builder $query, ?string $status): Builder
    {
        return $status
        ? $query->where('status', strtolower($status))
        : $query;
    }

    /**
     * Scope query to match the given keyword.
     */
    public function scopeKeyword(Builder $query, ?string $keyword): Builder
    {
        return $keyword
        ? $query->where(function ($q) use ($keyword) {
            $keywordSearch = '%' . $keyword . '%';
            $q->where('id', 'LIKE', $keywordSearch)
                ->orWhere('name', 'LIKE', $keywordSearch)
                ->orWhere('reference', 'LIKE', $keywordSearch)
                ->orWhere('phone', 'LIKE', $keywordSearch)
                ->orWhere('email', 'LIKE', $keywordSearch)
                ->orWhere('website', 'LIKE', $keywordSearch)
                ->orWhere('address', 'LIKE', $keywordSearch);
        })
        : $query;
    }

    // OTHER

    public function asData(): UserData
    {
        return UserData::from($this);
    }

}
