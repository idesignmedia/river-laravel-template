<?php
// RIVER Template (Website)

namespace App\Models;

use Illuminate\Support\Collection;
use River\DMS\Models\UniversalContentModel;
use River\Revisionable\Behaviour\HasRevisions;
use River\Revisionable\Contracts\Revisionable;
use Whitecube\NovaFlexibleContent\Value\FlexibleCast;
use Illuminate\Database\Eloquent\Casts\AsArrayObject;

class ContentResource extends UniversalContentModel implements Revisionable
{
    use HasRevisions;

    public $routePrefix     = 'resources';
    public $indexLabel      = 'Resources';
    public $singularLabel   = 'Resource';

    public $options = [
        'hasRevisions' => true,
        'dateFormat' => false,
        // 'categories' => null,
        'contentBlocks' => true,
        'contentVersions' => true,
        'shareableLinks' => true,
        // 'showPage' => true,
        // 'relatedPostsBlock' => \River\DMS\ContentBlocks\Child\UniversalContentSelectorBlock::class,
    ];

    public $filters = [
        'orderBy' => ['name' => 'asc'],
        'pagination' => 12,
    ];

    protected $casts = [
        // Resource
        'categories_json'   => 'array', // AsArrayObject::class,
        // Default UCM
        'published_at'      => 'datetime',
        'sort_order'        => 'integer',
        'data_json'         => 'array', // AsArrayObject::class,
        'cache_json'        => 'array', // AsArrayObject::class,
        'related_blocks'    => FlexibleCast::class,
    ];

    // CATEGORY FILTERS

    /**
     * Get a collection of custom PageLink objects with pre-selected Resource filters.
     * Each object has: name, summary, url, external
     *
     * @return [type] [description]
     */
    public static function getCategoryFilterLinks(): Collection
    {
        $categoryFilters = [
            [
                'title'         => 'Select Resources',
                'description'   => 'This is list of pre-selected Resource...',
                'keyword'       => false,
                'categories'    => [
                    'important', 'myself', 'article',
                ],
            ],
        ];

        // Spin into displayable links
        $links = [];

        foreach($categoryFilters as $categoryFilter) {

            // Build URL
            $url = url('/resources') . '?';
            if ($categoryFilter['keyword'] ?? null) {
                $url .= 'keyword=' . urlencode($categoryFilter['keyword']) . '&';
            }
            foreach($categoryFilter['categories'] ?? [] as $category) {
                $url .= $category . '&';
            }
            $url = rtrim($url, '&');

            // Create Link
            // @debt consider using \River\DMS\ValueObjects\ ... of some sort
            $links[] = (object) [
                'name' => $categoryFilter['title'],
                'summary' => $categoryFilter['description'],
                'url' => $url,
                'external' => false,
            ];
        }

        // Return
        return collect($links);
    }

    // CATEGORIES (all Resources)

    /**
     * Resource categories are grouped for display purposes, though the categories are still unique.
     * As most Tags packages do not account for separate keys, we simply treat the tags as the keys.
     *
     * @return array ['Group Label' => ['key' => 'Category 1 Label', 'Category 2 Label', ...], ...]
     */
    public static function resourceCategories(): array
    {
        return [
            'Subject' => [
                'important'     => 'Important Resources',
                'general'       => 'General Resources',
            ],
            'Audience' => [
                'myself'        => 'For Myself',
                'family'        => 'For Family',
                'workplace'     => 'For Workplaces',
            ],
            'Type' => [
                'article'       => 'Article',
                'presentation'  => 'Presentation',
                'video'         => 'Video',
                'webinar'       => 'Webinar',
                'other'         => 'Other',
            ],
        ];
    }

    /**
     * Get a flat list of Resource Categories, without the Groups
     *
     * @return array ['key' => 'label', ...]
     */
    public static function resourceCategoriesFlat(): array
    {
        $categoriesByGroup = self::resourceCategories();
        $categories = [];
        foreach($categoriesByGroup as $groupLabel => $group) {
            foreach($group as $categoryKey => $categoryLabel) {
                $categories[$categoryKey] = $categoryLabel;
            }
        }
        return $categories;
    }

    /**
     * Get a flat list of Resource Categories, to use in Nova Multiselect.
     *
     * @return array ['key' => ['label ' => 'Label', 'group' => 'Group', ...], ...]
     */
    public static function resourceCategoriesMultiSelectOptions(): array
    {
        $categoriesByGroup = self::resourceCategories();
        $categories = [];
        foreach($categoriesByGroup as $groupLabel => $group) {
            foreach($group as $categoryKey => $categoryLabel) {
                // Format for Option Groups: [ key => ['label ' => 'Label', 'group' => 'Group', ...], ...]
                $categories[$categoryKey] = ['label' => $categoryLabel,  'group' => $groupLabel];
            }
        }
        return $categories;
    }

    /**
     * Get the label of a Resource Category by key (null if the key is not found).
     *
     * @param  string $key the Resource Category key
     *
     * @return ?string the Resource Category label if found (null otherwise)
     */
    public static function resourceCategoryLabel(string $key): ?string
    {
        $categoriesByGroup = self::resourceCategories();
        $categories = [];
        foreach($categoriesByGroup as $groupLabel => $group) {
            foreach($group as $categoryKey => $categoryLabel) {
                if ($key == $categoryKey) {
                    return $categoryLabel;
                }
            }
        }
        return null;
    }

    // CATEGORIES (this resource)

    /**
     * Whether this Resource has any Categories attached.
     */
    public function hasResourceCategories(): bool
    {
        $categoryKeys = $this->categories_json ?? [];
        return count($categoryKeys)
            ? true
            : false;
    }

    /**
     * Get a flat list of Categories for this Resource.
     *
     * @return array ['key' => 'label', ...]
     */
    public function getResourceCategories(): array
    {
        $categoryKeys = $this->categories_json ?? [];
        $categories = [];
        foreach($categoryKeys as $key) {
            $categories[$key] = self::resourceCategoryLabel($key) ?? $key;
        }
        return $categories;
    }

    /**
     * Get a front-end label that represents Categories for this Resource.
     */
    public function getResourceCategoriesLabel(): string
    {
        return $this->hasResourceCategories()
            ? implode(', ', $this->getResourceCategories())
            : '-';
    }

}
