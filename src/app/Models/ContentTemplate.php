<?php
// RIVER Template (Website)

namespace App\Models;

use Illuminate\Support\Collection;
use River\DMS\Models\UniversalContentModel;
use River\Revisionable\Behaviour\HasRevisions;
use River\Revisionable\Contracts\Revisionable;

class ContentTemplate extends UniversalContentModel implements Revisionable
{
    use HasRevisions;

    public $routePrefix     = 'content-templates';
    public $indexLabel      = 'Content Templates';
    public $singularLabel   = 'Template';

    public $options = [
        'hasRevisions' => true,
        'dateFormat' => false,
        'categories' => null,
        'contentBlocks' => true,
        'contentVersions' => false,
        'shareableLinks' => false,
        // 'showPage' => true,
        // 'relatedPostsBlock' => \River\DMS\ContentBlocks\Child\UniversalContentSelectorBlock::class,
    ];

    public $filters = [
        'orderBy' => ['name' => 'asc'],
        'pagination' => 12,
    ];

}
