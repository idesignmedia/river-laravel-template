<?php

namespace App\Models;

use App\Support\Models\Pivot;

// use Silvanite\Brandenburg\Traits\HasRoles;

class UserOrganisation extends Pivot
{
    // SETUP

    protected $table = 'organisation_user';

    protected $fillable = [
        'admin_id', 'user_id', 'organisation_id', 'role',
    ];

}
