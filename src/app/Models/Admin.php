<?php

namespace App\Models;

use App\Enums\AuthRole;
use App\Support\Models\Authenticatable;
use River\Revisionable\Behaviour\HasRevisions;
use River\Revisionable\Contracts\Revisionable;
use Silvanite\Brandenburg\Traits\HasRoles;

// use Silvanite\Brandenburg\Traits\HasRoles;

class Admin extends Authenticatable implements Revisionable
{
    // Silvanite Roles & Permissions decomission as part of Nova 4 upgrade Oct 22
    // use HasRoles;
    use HasRevisions;

    // The auth guard to use
    protected $guard = 'nova';

    protected $attributes = [
        'role' => AuthRole::ADMIN,
    ];

    // Whether the user is a CMS Admin (ie, can access Nova)
    public function isAdmin(): bool
    {
        return true;
    }

    // Whether the user is a front-end user
    public function isUser(): bool
    {
        return false;
    }

    // Whether this user is a developer account
    public function isDeveloper(): bool
    {
        // return array_key_exists($this->email, config('river.admin.developers.accounts', []));
        return $this->getRole() == AuthRole::DEVELOPER;
    }

}
