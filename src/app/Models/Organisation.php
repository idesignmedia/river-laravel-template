<?php
// RIVER Template (Web App)

namespace App\Models;

use App\Data\Contacts\OrganisationData;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use River\Revisionable\Behaviour\HasRevisions;
use River\Revisionable\Contracts\Revisionable;

class Organisation extends Model implements Revisionable
{
    // SETUP
    use HasRevisions;
    use SoftDeletes;
    // use HasFactory;

    protected $attributes = [
        'data'              => '[]',
    ];

    protected $casts = [
        'date_added'        => 'datetime', // datetime:Y-m-d
        'data'              => AsArrayObject::class,
    ];

    protected $guarded = ['id'];

    protected $fillable = [
        'admin_id', 'status', 'date_added',
        'name', 'code', 'reference', 'phone', 'email', 'website', 'address', 'country', 'type', 'type_other', 'comments',
        'industry',
    ];

    // RELATIONSHIPS

    public function admin()
    {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'organisation_user')->withPivot('admin_id', 'role')->withTimestamps();
    }

    // CONTACT

    public function getName(): string
    {
        return $this->name;
    }

    public function getNumber(): int
    {
        return $this->id;
    }

    public function getNumberLabel(): string
    {
        return 'Organisation ' . $this->getNumber();
    }

    public function getCountryLabel(): ?string
    {
        // @debt tie into a country package (and type to front-end)
        if ($this->country) {
            return $this->country == 'nz'
                ? 'New Zealand'
                : strtoupper($this->country);
        }
        return null;
    }

    public function getAddressLabel(): ?string
    {
        $pieces = [];
        if ($this->address) {
            $pieces[] = $this->address;
        }
        if ($this->getCountryLabel()) {
            $pieces[] = $this->getCountryLabel();
        }
        return ! empty($pieces)
            ? implode(', ', $pieces)
            : null;
    }

    // QUERY

    /**
     * Scope query to match the given status.
     *
     * @param  string  $keyword
     */
    public function scopeStatus(Builder $query, ?string $status): Builder
    {
        return $status
        ? $query->where('status', strtolower($status))
        : $query;
    }

    /**
     * Scope query to match the given keyword.
     */
    public function scopeKeyword(Builder $query, ?string $keyword): Builder
    {
        return $keyword
        ? $query->where(function ($q) use ($keyword) {
            $keywordSearch = '%' . $keyword . '%';
            $q->where('id', 'LIKE', $keywordSearch)
                ->orWhere('name', 'LIKE', $keywordSearch)
                ->orWhere('reference', 'LIKE', $keywordSearch)
                ->orWhere('phone', 'LIKE', $keywordSearch)
                ->orWhere('email', 'LIKE', $keywordSearch)
                ->orWhere('website', 'LIKE', $keywordSearch)
                ->orWhere('address', 'LIKE', $keywordSearch)
                ->orWhere('industry', 'LIKE', $keywordSearch);
        })
        : $query;
    }

    // OTHER

    public function asData(): OrganisationData
    {
        return OrganisationData::from($this);
    }

}
