<?php
// RIVER Template (Website)

namespace App\Models;

use River\DMS\Models\Page as BasePage;
use River\Revisionable\Behaviour\HasRevisions;
use River\Revisionable\Contracts\Revisionable;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Page extends BasePage implements Revisionable
{
    use HasRevisions;

    // protected $attributes = [
    //     // UCM Attributes
    //     'status'                => \River\DMS\Enums\ContentStatus::DRAFT,
    //     'enable_hero_thumbnail' => true,
    //     // 'data_json'            => '{ "example_value": true }',
    //     // Page Attributes
    //     'template' => 'cms', // The default PageTemplate class to use, defined in: config/river/pages.php
    //     'enable_navigation' => true,
    //     'enable_indexing'   => true,
    // ];

    protected $fillable = [
        'slug',
        'name',
        'status',
        'summary',
        'data_json',
        'content',
        'content_related',
    ];

    public $options = [
        'hasRevisions' => true,
        // 'dateFormat' => 'j M y',
        'categories' => null,
        'contentBlocks' => true,
        'contentVersions' => true,
        'shareableLinks' => true,
        // 'showPage' => true,
        // 'relatedPostsBlock' => \River\DMS\ContentBlocks\Child\UniversalContentSelectorBlock::class,
    ];

    // Add "promo" image for the homepage (HomePageTemplate.php),
    // and "image" for general use by custom Page Templates
    public function registerUniversalMediaCollections(): void
    {
        parent::registerUniversalMediaCollections();
        $this->addMediaCollection('promo')->singleFile();
        $this->addMediaCollection('image')->singleFile();
    }

    // Add "promo" and "image" media conversions
    public function registerMediaConversions(Media $media = null): void
    {
        parent::registerUniversalMediaConversions();
        $this->addMediaConversion('small')
            ->width(600)
            ->height(600)
            ->performOnCollections('promo', 'image');
        $this->addMediaConversion('medium')
            ->width(1200)
            ->height(1200)
            ->performOnCollections('promo');
        $this->addMediaConversion('large', 'image')
            ->width(1800)
            ->height(1800)
            ->performOnCollections('promo', 'image');
    }

    public function hasContentTitle(): bool
    {
        return true;
    }

    public function getContentTitle(): ?string
    {
        return $this->name;
    }

    public function hasContentIntroduction(): bool
    {
        return ! empty($this->data_json['content_intro']);
    }

    public function getContentIntroduction(): ?string
    {
        return $this->data_json['content_intro'] ?? null;
    }

}
