<?php
// RIVER Template (Website)

namespace App\Models;

use River\DMS\Models\UniversalContentModel;
use River\Revisionable\Behaviour\HasRevisions;
use River\Revisionable\Contracts\Revisionable;

class ContentUpdate extends UniversalContentModel implements Revisionable
{
    use HasRevisions;

    public $routePrefix     = 'updates';
    public $indexLabel      = 'Updates';
    public $singularLabel   = 'Post';

    public $options = [
        'hasRevisions' => true,
        // 'dateFormat' => 'j M y',
        // 'categories' => null,
        'contentBlocks' => true,
        'contentVersions' => true,
        'shareableLinks' => true,
        // 'showPage' => true,
        // 'relatedPostsBlock' => \River\DMS\ContentBlocks\Child\UniversalContentSelectorBlock::class,
        'categories' => [
            'news'          => 'News',
            'community'     => 'Community',
            'other'         => 'Other',
        ],
    ];

    public $filters = [
        'orderBy' => ['published_at' => 'desc'],
        'pagination' => 12,
    ];
}
