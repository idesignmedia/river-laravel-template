<?php



namespace App\Mail;

use App\Models\ContactMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactEnquiry extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        public ContactMessage $message
    ) {
    }

    /**
     * Build the message.
     */
    public function build(): static
    {
        $data = [
            'name'    => $this->message->name,
            'email'   => $this->message->email,
            'message' => $this->message->message,
        ];

        $subject = sprintf(
            'New Contact Form Enquiry (%s)',
            $this->message->email,
        );

        return $this
            ->subject($subject)
            ->replyTo($this->message->email, $this->message->name)
            ->markdown('emails.contact-enquiry', ['form' => $data]);
    }
}
