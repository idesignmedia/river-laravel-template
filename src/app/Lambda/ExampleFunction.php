<?php

use Hammerstone\Sidecar\LambdaFunction;

class ExampleFunction extends LambdaFunction
{
    public function handler()
    {
        return 'resources/aws/lambda/example.handle';
    }

    public function package()
    {
        return [
            'resources/aws/lambda/example.js',
        ];
    }
}
