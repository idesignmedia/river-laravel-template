<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class CopyStorageFilesFromLocalToS3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copy-from-local-to-s3 {--F|file-system-name=public}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copies all local files from storage/app to S3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $fileSystemName = $this->option('file-system-name');

        // zip the files in the local storage
        // $zip = new \ZipArchive();
        // $zip->open(storage_path('app/backup.zip'), \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        // $path = storage_path('app/public/local');
        // $files = new \RecursiveIteratorIterator(
        //     new \RecursiveDirectoryIterator(
        //         $path,
        //         \FilesystemIterator::FOLLOW_SYMLINKS
        //     ),
        //     \RecursiveIteratorIterator::SELF_FIRST
        // );
        // while ($files->valid()) {
        //     if (!$files->isDot()) {
        //         $filePath = $files->getPathName();
        //         $relativePath = substr($filePath, strlen($path) + 1);

        //         if (!$files->isDir()) {
        //             $zip->addFile($filePath, $relativePath);
        //         } else {
        //             if ($relativePath !== false) {
        //                 $zip->addEmptyDir($relativePath);
        //             }
        //         }
        //     }
        //     $files->next();
        // }

        $localFiles = Storage::disk($fileSystemName)->allFiles();
        $progress = $this->output->createProgressBar(count($localFiles));
        $progress->start();
        foreach ($localFiles as $file) {
            // check if file exist before copying from s3
            if (Storage::disk('s3')->exists($file)) {
                $progress->advance();
                continue;
            }
            Storage::disk('s3')->writeStream($file, Storage::disk($fileSystemName)->readStream($file));
            $progress->advance();
            // break;
        }
        $progress->finish();
    }
}
