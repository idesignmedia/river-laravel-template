<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ChangeMediaDisk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change-media-disk {--F|file-system-name=s3}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Changes the disk for all media files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $count = Media::count();

        $bar = $this->output->createProgressBar($count);
        $bar->start();

        Media::query()
            ->chunkById(200, function ($media) use ($bar) {
                try {
                    DB::beginTransaction();

                    foreach ($media as $item) {
                        $item->disk = $this->option('file-system-name');
                        $item->conversions_disk = $this->option('file-system-name');
                        $item->save();
                        $bar->advance();
                    }

                    DB::commit();
                } catch (\Exception $e) {
                    //handle your error (log ...)
                    DB::rollBack();
                    $bar->finish();
                }
            });

        $bar->finish();
    }
}
