<?php
// RIVER Template

namespace App\Services;

use App\Models\Admin;
use Illuminate\Support\Carbon;

/**
 * General Formatter Service.
 * Provides various formatting functions based on preferred formatting for the project.
 * We use this Service to centralise repeated code, and ensure consistent formatting is applied for the front-end.
 */
class FormatService
{

    /**
     * What to return when an empty date is provided to format
     *
     * @return null|string the empty value to display
     */
    public static function emptyDateValue(): ?string
    {
        return '-'; // null
    }

    /**
     * Get the standard date format for this project
     *
     * @return string date format
     */
    public static function dateFormat(): string
    {
        return 'jS F Y'; // eg, '3rd June 2022'
    }

    /**
     * Get the timezone used to format dates for this project
     *
     * @return [type] [description]
     */
    public static function getTimezone(): ?string
    {
        return 'Pacific/Auckland';
    }

    /**
     * Get the short date format for this project
     *
     * @return string date format
     */
    public static function shortDateFormat(): string
    {
        return 'j M y'; // eg, '3 Jun 22'
    }

    /**
     * Apply this projects timezone to the given date
     *
     * @param  Carbon $date [description]
     *
     * @return [type]       [description]
     */
    public static function applyTimezone(Carbon $date): Carbon
    {
        $timezone = self::getTimezone();
        return $timezone
            ? $date->setTimezone($timezone)
            : $date;
    }

    /**
     * Format a date to the standard date format for this project
     *
     * @param  Carbon $date the date to format
     *
     * @return string the formatted date
     */
    public static function formatDate(?Carbon $date): string
    {
        return $date ? self::applyTimezone($date)->format(self::dateFormat()) : self::emptyDateValue();
    }

    /**
     * Format a date to the short date format for this project
     *
     * @param  Carbon $date the date to format
     *
     * @return string the formatted date
     */
    public static function formatShortDate(?Carbon $date): string
    {
        return $date ? self::applyTimezone($date)->format(self::shortDateFormat()) : self::emptyDateValue();
    }

    /**
     * Get the difference in two dates as a short label, such as "1.5y" or "5d", etc
     *
     * @debt push this into a generic date service (possible the same service that holds our default date formats used)
     *
     * @param  Carbon       $date        The date to check from
     * @param  Carbon|null  $now         The date to check against; details to today
     * @param  bool|boolean $today       Whether to note anything within 24h as "Today"
     * @param  bool|boolean $short       Whether to use shorthand for labels (eg, day = d, month = m, etc)
     * @param  bool|boolean $tense       Whether to add " ago" if the date is in the past
     *
     * @return string                    The data label
     */
    public static function dateDifferenceLabel(?Carbon $date = null, ?Carbon $now = null, bool $today = false, bool $short = false, bool $tense = true): string
    {
        if (! $date) {
            return self::emptyDateValue();
        }

        $date = self::applyTimezone($date);

        $now = $now ?? Carbon::now();
        $diff = $date->diff($now);
        $label = $date->diffForHumans($now); // CarbonInterface::DIFF_ABSOLUTE

        // Remove CARBON modifiers
        $label = str_replace(['ago', 'after', 'before'], '', $label);

        // Manage 1 - 8 weeks (Carbon treats 4+ weeks as "one month")
        if ($diff->days >= 7 && $diff->days <= 56) {
            // $label = round($diff->days/7) . ' weeks'; // TBC - we still sometime get "1 month" coming through and some odd occurances
        }

        // Mange short times (seconds only)
        if (! $diff->days && ! $diff->h && ! $diff->i) {
            $label = 'Now';
            $tense = false;
            $short = false;
        }

        // Add our modifier
        if ($tense) {
            if ($date > $now) {
                $label = "in {$label}";
            }
            if ($date < $now) {
                $label = "{$label} ago";
            }
        }
        $label = str_replace('  ', ' ', $label);

        // Manage shorthand
        if ($short) {
            $label = str_replace([' minutes', ' minute'], 'mins', $label);
            $label = str_replace([' hours', ' hours'], 'h', $label);
            $label = str_replace([' days', ' day'], 'd', $label);
            $label = str_replace([' weeks', ' week'], 'w', $label);
            $label = str_replace([' months', ' month'], 'm', $label);
            $label = str_replace([' years', ' year'], 'y', $label);
        } else {
            $label = str_replace(['minutes', 'minute'], ['mins', 'minute'], $label);
        }

        // Manage whether to note anything less than 24h as "today"
        if ($today && ! $diff->days) {
            $label = 'Today';
        }

        // if ($label = '1m ago') {
        //     dd($date, $now, $diff, $label, $date->diffForHumans($now));
        // }

        return $label;
    }

    /**
     * Return the name to of a specific admin.
     * This returns the default system name if an admin is not provided.
     */
    public static function adminName(?Admin $admin): string
    {
        return $admin
            ? $admin->getName()
            : config('project.owner.system_name');
    }

    /**
     * Return the name to of a specific author.
     * This uses the "getName()" method if available, and returns the class shortname otherwise.
     * This returns the default system name if an author is not provided.
     * Note, we could change Object to River\Core\Contracts\CommonUser if we want to be more specific.
     */
    public static function authorName(?Object $author): string
    {
        if ($author) {
            return method_exists($author, 'getName')
                ? $author->getName()
                : (new \ReflectionClass($this))->getShortName();
        }
        return config('project.owner.system_name');
    }

    // Get a label to represent something as Not Available
    public static function naLabel(bool $short = false): string
    {
        return $short
            ? '-'
            : 'N/A';
    }

    public static function valueOrNa(mixed $value, bool $short = false): string
    {
        return $value === null
            ? self::naLabel($short)
            : (string) $value;
    }

}
