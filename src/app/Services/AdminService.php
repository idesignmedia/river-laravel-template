<?php
// RIVER Template

namespace App\Services;

use App\Models\Admin;

/**
 * Admin Service.
 * Functions related to Admin model
 */
class AdminService
{

    public static function listAdmins(
        ?string $role = null,
        bool $asData = true,
    ) {
        // Query
        $query = Admin::query();

        // Role
        if ($role) {
            $query->where('role', $role);
        }

        // Order
        $query->orderBy('first_name', 'asc');

        // Results
        $admins = $query->get();


        // Transform into DisplayAdminData DTO
        // if ($asData) {
        //     $admins->transform(function ($admin)) {
        //         return $admin->asData();
        //     });
        // }

        // Return
        return $admins;
    }

}
