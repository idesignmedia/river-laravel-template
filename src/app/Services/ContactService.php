<?php
// RIVER Template (Web App)

namespace App\Services;

use App\Data\Contacts\OrganisationData;
use App\Data\Contacts\UserData;
use App\Models\Admin;
use App\Models\Organisation;
use App\Models\User;
use Illuminate\Support\Carbon;

/**
 * Contact Service.
 * Functions related to Organisations and App Users.
 */
class ContactService
{

    // ORGANISATIONS

    /**
     * Return a single Organisation
     */
    public static function getOrganisation(null|int|Organisation $organisation = null, bool $asData = true): null|Organisation|OrganisationData
    {
        // Organisation
        $query = Organisation::where('id', self::getObjectId($organisation));

        // Get
        $result = $query->first();

        // Transform into DTO
        if ($asData && $result) {
            $result = $result->asData();
        }

        // Return
        return $result;
    }

    /**
     * Returns a flat list of all Organisations
     */
    public static function allOrganisations(null|int|Organisation $organisation = null, bool $asData = true): \Illuminate\Database\Eloquent\Collection
    {
        // All Organisations
        $query = Organisation::query();

        // Get
        $results = $query->get();

        // Transform into DTO
        if ($asData) {
            $results->transform(function ($result) {
                return $result->asData();
            });
        }

        // Return
        return $results;
    }

    /**
     * Returns a paginated list of Organisations, filtered by optional params.
     * Note, the paginator checks the current request for the page by default
     * ...
     */
    public static function listOrganisations(
        null|int|Organisation $organisation = null,
        null|int|Admin $admin = null,
        ?string $status = null,
        ?string $keyword = null,
        ?string $type = null,
        ?int $year = null,
        ?bool $orderLatestFirst = true,
        ?int $limit = null,
        string $pageName = 'page',
        bool $asData = true,
    ): \Illuminate\Pagination\LengthAwarePaginator {
        // Query
        $query = Organisation::query();

        // Organisation
        if ($organisation) {
            $query->where('id', self::getObjectId($organisation));
        }

        // Admin
        if ($admin) {
            $query->where('admin_id', self::getObjectId($admin));
        }

        // Status (context aware, ignored if a partcular Organisation is passed through)
        if ($status && ! $organisation) {
            $query->status($status);
        }

        // Keyword (inc. ID & Name)
        $query->keyword($keyword);

        // Type
        if ($type) {
            $query->where('type', $type);
        }

        // Year
        if ($year && (int) $year) {
            $yearStart = Carbon::createFromDate((int) $year, 1, 1)->startOfMonth()->startOfDay();
            $yearEnd = Carbon::createFromDate((int) $year, 12, 1)->endOfMonth()->endOfDay();
            $query->whereBetween('date_added', [$yearStart, $yearEnd]);
        }

        // Order
        $query->orderBy('date_added', $orderLatestFirst ? 'desc' : 'asc');

        // Paginated Results (all results if limit is not set)
        $limit = $limit ?? $query->count();
        $results = $query->paginate(
            perPage:$limit,
            columns:['*'],
            pageName:$pageName,
        );

        // Transform into DTO
        if ($asData) {
            $results->getCollection()->transform(function ($result) {
                return $result->asData();
            });
        }

        // Return
        return $results;
    }

    // USERS

    /**
     * Return a single User
     */
    public static function getUser(null|int|User $user = null, bool $asData = true): null|User|UserData
    {
        // User
        $query = User::where('id', self::getObjectId($user));

        // Get
        $result = $query->first();

        // Transform into DTO
        if ($asData && $result) {
            $result = $result->asData();
        }

        // Return
        return $result;
    }

    /**
     * Returns a flat list of all Users
     */
    public static function allUsers(null|int|user $user = null, bool $asData = true): \Illuminate\Database\Eloquent\Collection
    {
        // All users
        $query = user::query();

        // Optionally for a given user
        if ($user) {
            $query->where('id', self::getObjectId($user));
        }

        // Get
        $results = $query->get();

        // Transform into DTO
        if ($asData) {
            $results->transform(function ($result) {
                return $result->asData();
            });
        }

        // Return
        return $results;
    }

    /**
     * Returns a paginated list of users, filtered by optional params.
     * Note, the paginator checks the current request for the page by default
     * ...
     */
    public static function listUsers(
        null|int|user $user = null,
        null|int|Admin $admin = null,
        ?string $status = null,
        ?string $keyword = null,
        ?string $type = null,
        ?int $year = null,
        ?bool $orderLatestFirst = true,
        ?int $limit = null,
        string $pageName = 'page',
        bool $asData = true,
    ): \Illuminate\Pagination\LengthAwarePaginator {
        // Query
        $query = user::query();

        // user
        if ($user) {
            $query->where('id', self::getObjectId($user));
        }

        // Admin
        if ($admin) {
            $query->where('admin_id', self::getObjectId($admin));
        }

        // Status (context aware, ignored if a partcular user is passed through)
        if ($status && ! $user) {
            $query->status($status);
        }

        // Keyword (inc. ID & Name)
        $query->keyword($keyword);

        // Type
        if ($type) {
            $query->where('type', $type);
        }

        // Year
        if ($year && (int) $year) {
            $yearStart = Carbon::createFromDate((int) $year, 1, 1)->startOfMonth()->startOfDay();
            $yearEnd = Carbon::createFromDate((int) $year, 12, 1)->endOfMonth()->endOfDay();
            $query->whereBetween('date_added', [$yearStart, $yearEnd]);
        }

        // Order
        $query->orderBy('date_added', $orderLatestFirst ? 'desc' : 'asc');

        // Paginated Results (all results if limit is not set)
        $limit = $limit ?? $query->count();
        $results = $query->paginate(
            perPage:$limit,
            columns:['*'],
            pageName:$pageName,
        );

        // Transform into DTO
        if ($asData) {
            $results->getCollection()->transform(function ($result) {
                return $result->asData();
            });
        }

        // Return
        return $results;
    }

    // HELPERS

    /**
     * Get the id from the given int or Object accordingly.
     */
    public static function getObjectId(null|int|Object $object = null): ?int
    {
        return is_object($object)
            ? $object->id
            : $object;
    }

}
