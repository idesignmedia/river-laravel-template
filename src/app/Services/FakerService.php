<?php

namespace App\Services;

/**
 * Faker Service to support with tailored faker data.
 */
class FakerService
{
    public static $genders = ['m', 'f'];

    public static $firstNames = [
        'f' => ['Olivia', 'Emma', 'Charlotte', 'Amelia', 'Ava', 'Sophia', 'Isabella', 'Mia', 'Evelyn', 'Harper'],
        'm' => ['Liam', 'Noah', 'Oliver', 'Elijah', 'James', 'William', 'Benjamin', 'Lucas', 'Henry', 'Theodore'],
    ];

    public static $lastNames = [
        'Doe', 'Smith', 'Johnson', 'Williams', 'Davis', 'Wilson', 'Taylor', 'Thomas', 'White',
        'Moore', 'Harris', 'Clark', 'Lewis', 'Robinson', 'Walker', 'Hall', 'Jackson',
    ];

    public static function getGender($faker): string
    {
        return $faker->randomElement(self::$genders);
    }

    public static function getName($faker, string $gender): string
    {
        $firstName = $faker->randomElement(self::$firstNames[$gender]);
        $lastName = $faker->randomElement(self::$lastNames);
        $name = "{$firstName} {$lastName}";
        return $name;
    }

    public static function getFirstName($faker, string $gender): string
    {
        $firstName = $faker->randomElement(self::$firstNames[$gender]);
        return $firstName;
    }

    public static function getPhone($faker): string
    {
        $phone = '64 ';
        for ($i = 0; $i < 6; $i++) {
            $phone .= mt_rand(0, 9);
        }
        return $phone;
    }

    public static function getAge($faker, $min = 10, $max = 50): float
    {
        $age = mt_rand($min, $max);
        if (mt_rand(1, 4) > 1) {
            $age += 0.5;
        }
        return $age;
    }

    public static function getNoteName($faker): string
    {
        return ucfirst($faker->word()) . ' Test';
    }

    public static function getNoteContent($faker): string
    {
        $content = 'Example Note.';
        for ($i = 0; $i < mt_rand(0, 4); $i++) {
            $content .= "\n" . rtrim($faker->sentence(), '.') . '.';
        }
        // $content = rtrim($content, '.');
        return $content;
    }

}
