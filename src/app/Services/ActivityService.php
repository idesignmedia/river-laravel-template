<?php
// RIVER Template

namespace App\Services;

use App\Data\Activity\ActivityData;
use App\Data\ModelRelationData;
use App\Data\PageLinkData;
use App\Models\Admin;
use App\Models\Organisation;
use App\Models\User;
use Illuminate\Support\Carbon;
use River\Core\Contracts\CommonUser;
use Spatie\Activitylog\Models\Activity;

/**
 * General Service to interact with displayable Activity.
 * Note, when passing activity to the front-end, it's recommended they are keyed by 'activity' for consistency.
 */
class ActivityService
{

    /**
     * The name to use for the 'log_name' when storing displayable Activity.
     */
    public const LOGNAME = 'display';

    /**
     * The name to use for the 'log_name' when deleting displayable Activity.
     */
    public const DELETEDLOGNAME = 'display-deleted';

    /**
     * Create a "display" log which we use to show in the frontend.
     * These have a set of opinionated fields we use for search an display.
     */
    public static function log(
        // The title of the log entry to display
        string $title,

        // The message for the log entry to display
        string $message,

        // Subject - the model that the log applies to, such as a Contact (User or Organisation)
        // This is required for the Spatie activity logger
        Object $on,

        // Causer - the model that logs the message, such as an Authenticatable Admin or User
        ?CommonUser $by = null,

        // An optional model we are logging with, such as a Contact's Project, etc
        // This is useful for pulling related data (eg, when displaying the Activity on the frontend)
        ?Object $with = null,

        // An optional "parent" model we are logging against, such as a Note being added into a Project
        // This is useful when searching for Activity (eg, all Activity that belongs to a given Project, etc)
        ?Object $parent = null,

        // The "type" of log that this is (see ActivityType enum)
        ?string $type = null,

        // The "activity" that this log represents
        // Common actions: create, assign, update, activate, approve, complete, close, cancel, archive, delete
        ?string $action = null,

        // Optional content for the activity, such as the message from an associated Note (used for keyword searching)
        ?string $content = null,

        // Whether this log is considered "pinned"
        bool $pinned = false,

        // Whether this log is considered "importanted"
        bool $important = false,

        // Whether the associated activity is considered to have an "attachment"
        bool $attachment = false,

        // Whether the associated activity has been considered "edited"
        bool $edited = false,

        // The "theme" of this log if applicable: null, 'success', 'info', 'warning', 'error'
        ?string $theme = null,

        // An optional thread that this log is part of
        ?int $thread = null,

        // The date/time this log should be made against (defaults to now)
        ?Carbon $date = null,
    ): Activity {
        activity(self::LOGNAME)
            ->by($by)
            ->on($on)
            ->withProperties([
                'with_type' => $with ? $with::class : null,
                'with_id' => $with ? $with->id : null,
                'parent_type' => $parent ? $parent::class : null,
                'parent_id' => $parent ? $parent->id : null,
                'title' => $title,
                'type' => $type,
                'action' => $action,
                'content' => $content,
                'pinned' => $pinned,
                'important' => $important,
                'attachment' => $attachment,
                'edited' => $edited,
                'theme' => $theme,
                'thread' => $thread,
            ])
            ->createdAt($date ?? Carbon::now())
            ->log($message);

        $activity = Activity::where('log_name', self::LOGNAME)->orderBy('id', 'desc')->first();
        return $activity;
    }

    /**
     * Get the latest display activity logged
     *
     * @param  array  $params Optional params to filter by (indexed array)
     *
     * @return [type]         [description]
     */
    public static function lastActivity(array $params = []): ?Activity
    {
        $query = Activity::where('log_name', self::LOGNAME)
            ->orderBy('id', 'desc');

        foreach ($params as $key => $value) {
            $query->where($key, $value);
        }

        $activity = $query->first();
        return $activity;
    }

    /**
     * Returns a flat list of all activity display logs
     */
    public static function allActivity(bool $asData = true): \Illuminate\Database\Eloquent\Collection
    {
        // All Activity
        $query = Activity::query()
            ->where('log_name', self::LOGNAME)
            ->with('subject');

        // Get
        $activity = $query->get();

        // Transform into ActivityData DTO
        if ($asData) {
            $activity->transform(function ($activity) {
                return ActivityData::from($activity);
            });
        }

        // Return
        return $activity;
    }

    /**
     * Returns a paginated list of activity, filtered by optional params.
     * Note, the paginator checks the current request for the page by default
     */
    public static function listActivity(
        // Relations
        ?CommonUser $author = null,
        ?Object $subject = null,
        ?Object $with = null,
        ?Object $parent = null,

        // Filters
        ?string $keyword = null,
        ?string $type = null,
        ?bool $pinned = null,
        ?bool $important = null,
        ?bool $attachment = null,

        // Options
        ?bool $orderPinnedFirst = true,
        ?bool $orderLatestFirst = true,
        ?int $limit = null,
        string $pageName = 'page',
        bool $asData = true,
    ): \Illuminate\Pagination\LengthAwarePaginator {
        // Query
        // @debt we are using non-indexed columns (particularly the JSON checks), keep an eye on this query to see if it slows down
        $query = Activity::query()
            ->where('log_name', self::LOGNAME)
            ->with('subject');

        // Author
        if ($author) {
            $query->where('causer_type', $author::class)
                ->where('causer_type', self::getObjectId($author));
        }

        // Subject
        if ($subject) {
            $query->where('subject_type', $subject::class)
                ->where('subject_id', self::getObjectId($subject));
        }

        // With
        if ($with) {
            $query->where('properties->with_type', $with::class)
                ->where('properties->with_id', self::getObjectId($with));
        }

        // Parent
        if ($parent) {
            $query->where('properties->parent_type', $parent::class)
                ->where('properties->parent_id', self::getObjectId($parent));
        }

        // If we need to check both the parent and with properties on a parent search:
        // $activityQuery->where(function ($q) use ($parent) {
        //     $q->where(function ($q) use ($parent) {
        //             $q->where('properties->parent_type', $parent::class)
        //                 ->where('properties->parent_id', $parent->id);
        //         });
        //         ->orWhere(function ($q) use ($parent) {
        //             $q->where('properties->with_type', $parent::class)
        //                 ->where('properties->with_id', $parent->id);
        //         });
        // });

        // Keyword
        if ($keyword) {
            // $query->where('description', 'LIKE', '%' . $keyword . '%');
            $query->where(function ($q) use ($keyword) {
                $keywordSearch = '%' . $keyword . '%';
                // @debt implemenet a case insensitive search on the properties
                $q->where(\DB::raw('lower(description)'), 'LIKE', strtolower($keywordSearch))
                    ->orWhere('properties->title', 'LIKE', $keywordSearch)
                    ->orWhere('properties->type', 'LIKE', $keywordSearch)
                    ->orWhere('properties->content', 'LIKE', $keywordSearch);
            });
        }

        // Type
        if ($type) {
            $query->where('properties->type', $type);
        }

        // Pinned
        if ($pinned) {
            $query->where('properties->pinned', $pinned);
        }

        // Important
        if ($important) {
            $query->where('properties->important', $important);
        }

        // Has Attachment
        if ($attachment) {
            $query->where('properties->attachment', $attachment);
        }

        // Order
        if ($orderPinnedFirst) {
            $query->orderBy('properties->pinned', 'desc');
        }
        $query->orderBy('created_at', $orderLatestFirst ? 'desc' : 'asc');

        // Paginated Results (all results if limit is not set)
        $limit = $limit ?? $query->count();
        $activity = $query->paginate(
            perPage: $limit,
            columns: ['*'],
            pageName: $pageName,
        );

        // Transform into ActivityData DTO
        if ($asData) {
            $activity->getCollection()->transform(function ($activity) {
                return ActivityData::from($activity);
            });
        }

        // Return
        return $activity;
    }

    /**
     * Update the properties on an existing Activity, and then save the changes.
     *
     * @param  Activity $activity   The existing Activity to update
     * @param  array    $properties The new properties to apply
     */
    public static function updateActivityProperties(Activity $activity, array $properties): Activity
    {
        $activity->properties = $activity->properties->merge($properties);
        $activity->save();
        return $activity;
    }

    /**
     * Soft delete the Activity by changing the log name.
     *
     * @param  Activity $activity The existing Activity to soft delete
     */
    public static function deleteActivity(Activity $activity): void
    {
        $activity->log_name = self::DELETEDLOGNAME;
        $activity->save();
    }

    /**
     * Get the id from the given int or Object accordingly.
     */
    public static function getObjectId(null|int|Object $object = null): ?int
    {
        return is_object($object)
            ? $object->id
            : $object;
    }

    /**
     * Get a clickable PageLink associated with the given Activity, if one is available.
     *
     * @param  Activity|null $activity The Activity to generate a PageLink from
     */
    public static function getPageLink(?Activity $activity = null): ?PageLinkData
    {
        if (! $activity) {
            return null;
        }

        // Grab the associatd $with model that this Activity is linked to
        $with = new ModelRelationData(class: $activity->properties['with_type'], id: $activity->properties['with_id']);

        // Define
        $name = null;
        $url = null;

        // Generate the link
        switch($with->class ?? null) {
            case Organisation::class:
                $name = 'View Organisation';
                $url = route('organisations.show', ['organisation' => $with->id]);
                break;
            case User::class:
                $name = 'View User';
                $url = route('users.show', ['user' => $with->id]);
                break;
        }

        return new PageLinkData(name: $name, url: $url);
    }

}
