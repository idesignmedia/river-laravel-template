<?php

namespace App\Actions\Admin;

// use App\Support\Behaviours\WithInertiaResponses;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * This is a test action that can return fail on demand.
 */
class StoreAdmin
{
    use AsAction;
    // use WithInertiaResponses;

    public function asController(ActionRequest $request)
    {
        // Handle
        $result = $this->handle();

        // Validate
        if (! $result) {
            throw new \DomainException('Invalid data');
        }

        // Success
        return true;
    }

    public function handle(bool $result = true): bool
    {
        // Sample Action
        return $result;
    }

}
