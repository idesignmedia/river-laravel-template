<?php

namespace App\Actions\Media;

use App\Models\Admin;
use App\Models\TemporaryUpload;
use App\Support\Behaviours\WithInertiaResponses;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Validator;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * This Action is used to store a temporary upload.
 * Note, this currently works with a single file.
 *
 * Architecture:
 * https://docs.google.com/document/d/1H6dMJBJsAuyOxgr_lrtLKlclWcFPROO3h19JfSyiqHY/edit#heading=h.atiz4kpdy33w
 */
class HandleTemporaryUpload
{
    use AsAction;
    use WithInertiaResponses;

    public function asController(ActionRequest $request)
    {
        // Validate
        Validator::make($request->all(), [
            'file' => ['required', 'file'],
        ])->validate();

        // Handle
        try {
            // Upload Temporary File
            $temporaryUpload = $this->handle($request->file, $request->user());

        } catch (\Exception $e) {
            // dd($e);
            return self::handleInertiaError($e);
        }

        // Return
        return [
            'code' => $temporaryUpload->code,
        ];
    }

    public function handle(
        UploadedFile $file,
        ?Admin $admin,
    ): TemporaryUpload {

        // Create Temporary Upload Object
        $temporaryUpload = new TemporaryUpload;
        $temporaryUpload->user_id = $admin?->id;
        $temporaryUpload->save();

        // Attach file
        $temporaryUpload
            ->addMediaFromRequest('file')
            ->toMediaCollection();

        // Return
        return $temporaryUpload;
    }

}
