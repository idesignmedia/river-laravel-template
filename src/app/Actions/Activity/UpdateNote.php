<?php

namespace App\Actions\Activity;

use App\Data\Activity\StoreUpdateNoteData;
use App\Models\Admin;
use App\Models\Note;
use App\Services\ActivityService;
use App\Support\Behaviours\WithInertiaResponses;
use Illuminate\Support\Facades\Validator;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;
use River\Core\Contracts\CommonUser;

/**
 * This Action is used to update an existing Note.
 *
 * Architecture:
 * https://docs.google.com/document/d/1H6dMJBJsAuyOxgr_lrtLKlclWcFPROO3h19JfSyiqHY/edit#heading=h.atiz4kpdy33w
 */
class UpdateNote
{
    use AsAction;
    use WithInertiaResponses;

    public function asController(ActionRequest $request)
    {
        // Validate
        Validator::make($request->all(), [
            'note_id' => ['required', 'exists:' . Note::class . ',id'],
        ])->validate();

        // Create DTO from Request Data (and Validate)
        $noteData = StoreUpdateNoteData::from($request);

        // Define
        $note = Note::findOrFail($request->get('note_id'));

        // Handle
        try {
            // Update Note
            $note = $this->handle(
                note: $note,
                noteData: $noteData,
                author: $request->user(), // admin() ?? user() ?? null,
            );
        } catch (\Exception $e) {
            return self::handleInertiaError($e);
        }

        // Return
        return self::inertiaSuccess(
            message: "Note '{$note->name}' successfully updated.",
        );
    }

    public function handle(
        Note $note,
        StoreUpdateNoteData $noteData,
        ?CommonUser $author,
    ): Note {
        // Example validation if required...
        if (! $note || empty($noteData)) {
            throw new \DomainException('Invalid data');
        }

        // History PENDING

        // Update Note (approved fields only)
        $note->fill([
            'name' => $noteData->name,
            'content' => $noteData->content,
            'option_pinned' => $noteData->option_pinned,
            'option_important' => $noteData->option_important,
        ]);

        // Update associated Activity log entry
        $activity = ActivityService::listActivity(with: $note, asData: false)->first();
        if ($activity) {
            ActivityService::updateActivityProperties($activity, [
                'edited' => true,
                'content' => $noteData->content,
                'pinned' => $noteData->option_pinned,
                'important' => $noteData->option_important,
            ]);
        }

        // Note Relations
        // We skip relations when updating a note (we change content only)...

        // Update
        $note->save();

        // Activity = Note Updated
        // We DO NOT add a new Activity, as this would get loaded as another Note on the Client

        // Return
        return $note;
    }

}
