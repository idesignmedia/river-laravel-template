<?php

namespace App\Actions\Activity;

use App\Models\Admin;
use App\Models\Note;
use App\Services\ActivityService;
use App\Support\Behaviours\WithInertiaResponses;
use Illuminate\Support\Facades\Validator;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;
use River\Core\Contracts\CommonUser;

/**
 * This Action is used to (soft) delete an existing Note.
 *
 * Architecture:
 * https://docs.google.com/document/d/1H6dMJBJsAuyOxgr_lrtLKlclWcFPROO3h19JfSyiqHY/edit#heading=h.atiz4kpdy33w
 */
class DestroyNote
{
    use AsAction;
    use WithInertiaResponses;

    public function asController(ActionRequest $request)
    {
        // Validate
        Validator::make($request->all(), [
            'note_id' => ['required', 'exists:' . Note::class . ',id'],
        ])->validate();

        // Define
        $note = Note::findOrFail($request->get('note_id'));

        // Handle
        try {
            // Delete Note
            $note = $this->handle(
                note: $note,
                author: $request->user(), // admin() ?? user() ?? null,
            );
        } catch (\Exception $e) {
            throw $e;
            return self::handleInertiaError($e);
        }

        // Return
        return self::inertiaSuccess(
            message: "Note '{$note->name}' successfully deleted.",
        );
    }

    public function handle(
        Note $note,
        ?CommonUser $author,
    ): Note {
        // Example validation if required...
        if (! $note) {
            throw new \DomainException('Invalid data');
        }

        // History PENDING

        // Soft delete associated Activity log entry
        $activity = ActivityService::listActivity(with: $note, asData: false)->first();
        if ($activity) {
            ActivityService::deleteActivity($activity);
        }

        // Soft delete the Note
        $note->delete();

        // Activity = Note Deleted
        // We DO NOT add a new Activity, as this would get loaded as another Note on the Client

        // Return
        return $note;
    }

}
