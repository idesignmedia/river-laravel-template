<?php

namespace App\Actions\Activity;

use App\Data\Activity\StoreUpdateNoteData;
use App\Enums\ActivityType;
use App\Models\Admin;
use App\Models\Note;
use App\Models\TemporaryUpload;
use App\Services\ActivityService;
use App\Services\FormatService;
use App\Support\Behaviours\WithInertiaResponses;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;
use River\Core\Contracts\CommonUser;

/**
 * This Action is used to store a new Note.
 */
class StoreNote
{
    use AsAction;
    use WithInertiaResponses;

    public function asController(ActionRequest $request)
    {
        // Custom Validation if needed
        // Validator::make($request->all(), [
        //     'name' => ['string', 'nullable'],
        //     'content' => ['required', 'string'],
        //     'option_pinned' => ['sometimes', 'boolean'],
        //     'option_important' => ['sometimes', 'boolean'],
        //     'attachments' => ['sometimes', 'nullable', 'string'],
        // ])->validate();

        // Create DTO from Request Data (and Validate)
        $noteData = StoreUpdateNoteData::from($request);

        // Handle
        try {
            // Create Note
            $note = $this->handle(
                noteData: $noteData,
                author: $request->user(), // admin() ?? user() ?? null,
            );
        } catch (\Exception $e) {
            return self::handleInertiaError($e);
        }

        // Return
        return self::inertiaSuccess(
            message: $note->name ? "Note '{$note->name}' successfully added." : 'Note successfully added.'
        );
    }

    public function handle(
        StoreUpdateNoteData $noteData,
        ?CommonUser $author,
        ?Carbon $createdAt = null,
    ): Note {
        // Example validation if required...
        if (empty($noteData)) {
            throw new \DomainException('Invalid data');
        }

        // Relations
        $author = $author && ! empty($author->id)
            ? $author
            : null;
        $subject = $noteData->subject_type && $noteData->subject_id
            ? $noteData->subject_type::find($noteData->subject_id)
            : null;
        $parent = $noteData->parent_type && $noteData->parent_id
            ? $noteData->parent_type::find($noteData->parent_id)
            : null;

        // Validate subject
        if (! $subject) {
            throw new \DomainException('A subject model is required for the Note');
        }

        // Create Note
        $remove = ['attachments', 'subject_type', 'subject_id', 'parent_type', 'parent_id'];
        $noteDataStore = array_diff_key($noteData->toArray(), array_flip($remove));
        $note = new Note($noteDataStore);

        // Note Relations
        if ($author) {
            $note->author_type = $author::class;
            $note->author_id = $author->id;
        }
        if ($subject) {
            $note->noteable_type = $subject::class;
            $note->noteable_id = $subject->id;
        }

        // Custom date (often used for seeding)
        if ($createdAt) {
            $note->created_at = $createdAt;
            $note->updated_at = $createdAt;
        }

        // Store
        $note->save();

        // Handle Attachment if provided
        $hasAttachment = false;
        if ($noteData->attachments) {
            $temporaryUpload = TemporaryUpload::findByCode($noteData->attachments, $author?->id); // @debt refactor admin? to author?
            // @debt consider error handling?
            if ($temporaryUpload) {
                $temporaryUpload->attachToModel($note, 'attachments');
                $hasAttachment = true;
            }
        }

        // Activity = Note Added
        $title = $note->name ?? 'Note';
        $message = ($note->name ? "Note '{$note->name}' added" : 'Note added')
            . ' by ' . FormatService::authorName($author) . '.';
        ActivityService::log(
            title: $title,
            message: $message,
            on: $subject,
            by: $author,
            with: $note,
            parent: $parent,
            type: ActivityType::NOTE,
            action: 'create',
            content: $note->content,
            pinned: $note->option_pinned,
            important: $note->option_important,
            attachment: $hasAttachment,
            date: $createdAt,
        );

        // Return
        return $note;
    }

}
