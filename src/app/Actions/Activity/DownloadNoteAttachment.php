<?php

namespace App\Actions\Activity;

use App\Models\Note;
use App\Support\Behaviours\WithInertiaResponses;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * This Action is used to store a new Note.
 */
class DownloadNoteAttachment
{
    use AsAction;
    use WithInertiaResponses;

    public function asController(int $id, ActionRequest $request)
    {
        // Handle
        try {
            $note = Note::findOrFail($id);

            // @debt permissions checks if needed in future

            // Retrieve download url
            $response = $this->handle(
                note: $note
            );
        } catch (\Exception $e) {
            return self::handleInertiaError($e);
        }

        return $response;
    }

    /**
     * Handle a download attachment request.
     *
     * Will return a redirect response.
     */
    public function handle(Note $note): RedirectResponse
    {
        // Sanity check
        if (! $note->hasMedia('attachments')) {
            throw new \DomainException('Note does not have an attachment');
        }

        // Retrieve media
        $media = $note->getFirstMedia('attachments');

        // If the media is stored in S3, we can create a temporary signed url.
        // Otherwise, we can just build the url to locally stored media.
        $url = ($media->getDiskDriverName() === 's3')
            ? $media->getTemporaryUrl(Carbon::now()->addMinutes(5))
            : $media->getFullUrl();

        // Return
        return redirect($url);
    }

}
