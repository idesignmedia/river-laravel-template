<?php
// RIVER Template (Web App)

namespace App\Actions\Contacts;

use App\Data\Contacts\StoreUpdateUserData;
use App\Enums\ActivityType;
use App\Enums\ContactStatus;
use App\Enums\ContactType;
use App\Models\Admin;
use App\Models\User;
use App\Services\ActivityService;
use App\Services\FormatService;
use App\Support\Behaviours\WithInertiaResponses;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Lorisleiva\Actions\Concerns\AsAction;
use River\Core\Contracts\CommonUser;

/**
 * Action to create a User.
 */
class StoreUser
{
    use AsAction;
    use WithInertiaResponses;

    public function asController(Request $request)
    {
        // Create DTO from Request Data (and Validate)
        $userData = StoreUpdateUserData::from($request);

        // Custom Validation
        if ($request->contact_type !== ContactType::USER) {
            return self::inertiaError("Invalid contact type '{$request->contact_type}'.");
        }

        // Handle
        try {
            // Create User
            $user = $this->handle(
                userData: $userData,
                author: $request->user(), // admin() ?? user() ?? null,
            );
        } catch (\Exception $e) {
            return self::handleInertiaError($e);
        }

        // Show the User
        return self::inertiaSuccess(
            message: "{$user->getNumberLabel()} '{$user->getName()}' successfully added.",
            route: 'users.show',
            params: ['user' => $user],
        );

    }

    public function handle(
        StoreUpdateUserData $userData,
        ?CommonUser $author = null,
        ?Carbon $createdAt = null,
    ): User {
        // Validate Email
        if (empty($userData->email)) {
            throw new \DomainException('An email address is required.');
        }
        if ($userData->email && ! filter_var($userData->email, FILTER_VALIDATE_EMAIL)) {
            throw new \DomainException("Invalid email address '{$userData->email}'.");
        }

        // Validate email unique
        if (User::where('email', $userData->email)->count()) {
            throw new \DomainException("The provided email '{$userData->email}' is already in use.");
        }

        // Validate email not in trash
        // if (User::withTrashed()->where('email', $userData->email)->count()) {
        //     // throw new \DomainException('The provided email belongs to a previously deleted User.');
        // }

        // Defaults
        if (! $userData->admin_id && $author && $author instanceof Admin) {
            $userData->admin_id = $admin->id;
        }
        if (! $userData->status) {
            $userData->status = ContactStatus::ACTIVE;
        }
        if (! $userData->date_added) {
            $userData->date_added = Carbon::now();
        }

        // Create User
        $user = new User($userData->toArray());
        if ($createdAt) {
            $user->created_at = $createdAt;
        }

        // Dummy password
        $password = \Str::random(20);
        $user->password = Hash::make($password);

        // Save
        $user->save();

        // Activity = User Added
        $title = 'User Added';
        $message = "{$user->getNumberLabel()} '{$user->getName()}' has been added by "
            . FormatService::authorName($author) . '.';
        ActivityService::log(
            title: $title,
            message: $message,
            on: $user,
            by: $author,
            with: null,
            parent: $user,
            type: ActivityType::USER,
            action: 'create',
            important: false,
            date: $createdAt,
        );

        // Return
        return $user;
    }

}
