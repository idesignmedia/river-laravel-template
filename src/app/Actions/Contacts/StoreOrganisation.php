<?php
// RIVER Template (Web App)

namespace App\Actions\Contacts;

use App\Data\Contacts\StoreUpdateOrganisationData;
use App\Enums\ActivityType;
use App\Enums\ContactStatus;
use App\Enums\ContactType;
use App\Models\Admin;
use App\Models\Organisation;
use App\Services\ActivityService;
use App\Services\FormatService;
use App\Support\Behaviours\WithInertiaResponses;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Lorisleiva\Actions\Concerns\AsAction;
use River\Core\Contracts\CommonUser;

/**
 * Action to create an Organisation.
 */
class StoreOrganisation
{
    use AsAction;
    use WithInertiaResponses;

    public function asController(Request $request)
    {
        // Create DTO from Request Data (and Validate)
        $organisationData = StoreUpdateOrganisationData::from($request);

        // Custom Validation
        if ($request->contact_type !== ContactType::ORGANISATION) {
            return self::inertiaError("Invalid contact type '{$request->contact_type}'.");
        }

        // Handle
        try {
            // Create Organisation
            $organisation = $this->handle(
                organisationData: $organisationData,
                author: $request->user(), // admin() ?? user() ?? null,
            );
        } catch (\Exception $e) {
            return self::handleInertiaError($e);
        }

        // Show the Organisation
        return self::inertiaSuccess(
            message: "{$organisation->getNumberLabel()} '{$organisation->getName()}' successfully added.",
            route: 'organisations.show',
            params: ['organisation' => $organisation],
        );

    }

    public function handle(
        StoreUpdateOrganisationData $organisationData,
        ?CommonUser $author = null,
        ?Carbon $createdAt = null,
    ): Organisation {
        // Validate Email
        if ($organisationData->email && ! filter_var($organisationData->email, FILTER_VALIDATE_EMAIL)) {
            throw new \DomainException("Invalid email address '{$organisationData->email}'.");
        }

        // Validate email unique
        if (Organisation::where('email', $organisationData->email)->count()) {
            // throw new \DomainException("The provided email address '{$organisationData->email}' is already in use.");
        }

        // Validate email not in trash
        if (Organisation::withTrashed()->where('email', $organisationData->email)->count()) {
            // throw new \DomainException('The provided email belongs to a previously deleted Organisation.');
        }

        // Defaults
        if (! $organisationData->admin_id && $author && $author instanceof Admin) {
            $organisationData->admin_id = $admin->id;
        }
        if (! $organisationData->status) {
            $organisationData->status = ContactStatus::ACTIVE;
        }
        if (! $organisationData->date_added) {
            $organisationData->date_added = Carbon::now();
        }

        // Create Organisation
        $organisation = new Organisation($organisationData->toArray());
        if ($createdAt) {
            $organisation->created_at = $createdAt;
        }
        $organisation->save();

        // Activity = Organisation Added
        $title = 'Organisation Added';
        $message = "{$organisation->getNumberLabel()} '{$organisation->getName()}' has been added by "
            . FormatService::authorName($author) . '.';
        ActivityService::log(
            title: $title,
            message: $message,
            on: $organisation,
            by: $author,
            with: null,
            parent: $organisation,
            type: ActivityType::ORGANISATION,
            action: 'create',
            important: false,
            date: $createdAt,
        );

        // Return
        return $organisation;
    }

}
