<?php
// RIVER Template (Web App)

namespace App\Actions\Contacts;

use App\Data\Contacts\ManageAuthUserData;
use App\Models\Admin;
use App\Models\User;
use App\Support\Behaviours\WithInertiaResponses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Lorisleiva\Actions\Concerns\AsAction;
use River\Core\Contracts\CommonUser;

/**
 * Action to manage (enable or disable) login access for a User.
 */
class ManageUserAuth
{
    use AsAction;
    use WithInertiaResponses;

    public function asController(Request $request)
    {
        // Validate
        Validator::make($request->all(), [
            'user_id' => ['required', 'exists:' . User::class . ',id'],
            'enabling_auth' => ['required', 'boolean'],
            'message' => ['sometimes', 'nullable', 'string'],
        ])->validate();

        // Define
        $user = User::find($request->user_id);

        // DTO from Request Data
        $data = ManageAuthUserData::from($request);

        // Handle
        try {
            // Update User
            $user = $this->handle(
                user: $user,
                data: $data,
                author: $request->user(), // admin() ?? user() ?? null,
            );
        } catch (\Exception $e) {
            return self::handleInertiaError($e);
        }

        // Return
        return self::inertiaSuccess(
            message:"{$user->getNumberLabel()} '{$user->getName()}' successfully updated.",
        );
    }

    public function handle(
        User $user,
        ManageAuthUserData $data,
        ?CommonUser $author = null,
    ): User {
        // Example validation if required...
        if (! $user || empty($data)) {
            throw new \DomainException('Invalid data');
        }

        // PENDING
        dd('Manage Auth PENDING', $user, $data);

    }

}
