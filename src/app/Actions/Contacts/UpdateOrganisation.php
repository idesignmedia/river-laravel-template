<?php
// RIVER Template (Web App)

namespace App\Actions\Contacts;

use App\Data\Contacts\StoreUpdateOrganisationData;
use App\Enums\ActivityType;
use App\Models\Admin;
use App\Models\Organisation;
use App\Services\ActivityService;
use App\Services\FormatService;
use App\Support\Behaviours\WithInertiaResponses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Lorisleiva\Actions\Concerns\AsAction;
use River\Core\Contracts\CommonUser;

/**
 * Action to edit an Organisation.
 */
class UpdateOrganisation
{
    use AsAction;
    use WithInertiaResponses;

    public function asController(Request $request)
    {
        // Validate
        Validator::make($request->all(), [
            'organisation_id' => ['required', 'exists:' . Organisation::class . ',id'],
        ])->validate();

        // Define
        $organisation = Organisation::find($request->organisation_id);

        // DTO from Request Data
        $organisationData = StoreUpdateOrganisationData::from($request);

        // Handle
        try {
            // Update Organisation
            $organisation = $this->handle(
                organisation: $organisation,
                organisationData: $organisationData,
                author: $request->user(), // admin() ?? user() ?? null,
            );
        } catch (\Exception $e) {
            return self::handleInertiaError($e);
        }

        // Return
        return self::inertiaSuccess(
            message:"{$organisation->getNumberLabel()} '{$organisation->getName()}' successfully updated.",
        );
    }

    public function handle(
        Organisation $organisation,
        StoreUpdateOrganisationData $organisationData,
        ?CommonUser $author = null,
    ): Organisation {
        // Example validation if required...
        if (! $organisation || empty($organisationData)) {
            throw new \DomainException('Invalid data');
        }

        // History PENDING

        // Defaults
        if (! $organisationData->status) {
            $organisationData->status = $organisation->status;
        }
        if (! $organisationData->date_added) {
            $organisationData->date_added = $organisation->date_added;
        }

        // Update Organisation
        // WARNING, this will override existing data based on the DTO exactly (including null values)
        // If the DTO has been created from a request, any fields not passed in will be null...
        $organisation->fill($organisationData->toArray());
        $organisation->save();

        // Activity = Organisation Added
        $title = 'Organisation Updated';
        $message = "{$organisation->getNumberLabel()} '{$organisation->getName()}' updated by " . FormatService::authorName($author) . '.';
        ActivityService::log(
            title: $title,
            message: $message,
            on: $organisation,
            by: $author,
            with: null,
            parent: $organisation,
            type: ActivityType::ORGANISATION,
            action: 'update',
            important: false,
        );

        // Return
        return $organisation;
    }

}
