<?php
// RIVER Template (Web App)

namespace App\Actions\Contacts;

use App\Data\Contacts\StoreUpdateUserData;
use App\Enums\ActivityType;
use App\Models\Admin;
use App\Models\User;
use App\Services\ActivityService;
use App\Services\FormatService;
use App\Support\Behaviours\WithInertiaResponses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Lorisleiva\Actions\Concerns\AsAction;
use River\Core\Contracts\CommonUser;

/**
 * Action to edit a User.
 */
class UpdateUser
{
    use AsAction;
    use WithInertiaResponses;

    public function asController(Request $request)
    {
        // Validate
        Validator::make($request->all(), [
            'user_id' => ['required', 'exists:' . User::class . ',id'],
        ])->validate();

        // Define
        $user = User::find($request->user_id);

        // DTO from Request Data
        $userData = StoreUpdateUserData::from($request);

        // Handle
        try {
            // Update User
            $user = $this->handle(
                user: $user,
                userData: $userData,
                author: $request->user(), // admin() ?? user() ?? null,
            );
        } catch (\Exception $e) {
            return self::handleInertiaError($e);
        }

        // Return
        return self::inertiaSuccess(
            message:"{$user->getNumberLabel()} '{$user->getName()}' successfully updated.",
        );
    }

    public function handle(
        User $user,
        StoreUpdateUserData $userData,
        ?CommonUser $author = null,
    ): User {
        // Example validation if required...
        if (! $user || empty($userData)) {
            throw new \DomainException('Invalid data');
        }

        // History PENDING

        // Defaults
        if (! $userData->status) {
            $userData->status = $user->status;
        }
        if (! $userData->date_added) {
            $userData->date_added = $user->date_added;
        }

        // Update User
        // WARNING, this will override existing data based on the DTO exactly (including null values)
        // If the DTO has been created from a request, any fields not passed in will be null...
        $user->fill($userData->toArray());
        $user->save();

        // TBC, send confirmation email if a new email address is provided

        // Activity = User Added
        $title = 'User Updated';
        $message = "{$user->getNumberLabel()} '{$user->getName()}' updated by " . FormatService::authorName($author) . '.';
        ActivityService::log(
            title: $title,
            message: $message,
            on: $user,
            by: $author,
            with: null,
            parent: $user,
            type: ActivityType::USER,
            action: 'update',
            important: false,
        );

        // Return
        return $user;
    }

}
