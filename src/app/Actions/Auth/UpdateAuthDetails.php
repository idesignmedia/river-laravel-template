<?php

namespace App\Actions\Auth;

use App\Models\Admin;
use App\Models\User;
use App\Support\Behaviours\WithInertiaResponses;
use Illuminate\Support\Facades\Validator;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * This Action is used to update details for an auth Admin or User.
 *
 * Architecture:
 * https://docs.google.com/document/d/1H6dMJBJsAuyOxgr_lrtLKlclWcFPROO3h19JfSyiqHY/edit#heading=h.atiz4kpdy33w
 */
class UpdateAuthDetails
{
    use AsAction;
    use WithInertiaResponses;

    public function asController(ActionRequest $request)
    {
        // Validate
        Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'company' => ['nullable', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255'],
            'phone' => ['nullable', 'string', 'max:255'],
        ])->validate();

        // Validate
        $modelClass = $request->class;
        $modelId = $request->id;
        if (! in_array($modelClass, [User::class, Admin::class])) {
            return self::inertiaError('Invalid user type.');
        }
        $auth = $modelClass::find($modelId);
        if (! $auth) {
            return self::inertiaError('Invalid user model.');
        }

        // Update Values
        $values = [
            'name' => $request->name,
            'company' => $request->company,
            // 'email' => $request->email, // We currently don't allow email updates via the front-end
            'phone' => $request->phone,
        ];

        // Handle
        try {
            // Update Details
            $this->handle(
                auth: $auth,
                values: $values
            );
        } catch (\Exception $e) {
            return self::handleInertiaError($e);
        }

        // Return
        return self::inertiaSuccess(
            message:'Your details were successfully updated.',
        );
    }

    public function handle(
        Admin|User $auth,
        array $values,
    ) {
        // NOTE, validation (valid fields being filled) should be handled prior to this

        // Update the details
        $auth->fill($values);

        // Store
        $auth->save();

        // Return
        return $auth;
    }

}
