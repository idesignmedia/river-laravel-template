<?php

namespace App\Actions\Auth;

use App\Models\Admin;
use App\Models\User;
use App\Support\Behaviours\WithInertiaResponses;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Lorisleiva\Actions\ActionRequest;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * This Action is used to update the password for an auth Admin or User.
 *
 * Architecture:
 * https://docs.google.com/document/d/1H6dMJBJsAuyOxgr_lrtLKlclWcFPROO3h19JfSyiqHY/edit#heading=h.atiz4kpdy33w
 */
class UpdateAuthPassword
{
    use AsAction;
    use WithInertiaResponses;

    public function asController(ActionRequest $request)
    {
        // Validate
        Validator::make($request->all(), [
            'password_current' => ['required', 'string'],
            'password_new' => ['required', 'string'],
            'password_confirm' => ['required', 'string'],
        ])->validate();

        // Validate
        $modelClass = $request->class;
        $modelId = $request->id;
        if (! in_array($modelClass, [User::class, Admin::class])) {
            return self::inertiaError('Invalid user type.');
        }
        $auth = $modelClass::find($modelId);
        if (! $auth) {
            return self::inertiaError('Invalid user model.');
        }

        // Confirm current password
        if (! Hash::check($request->password_current, $auth->password)) {
            return self::inertiaError('Your password was not correct');
        }

        // Confirm new password
        if ($request->password_new !== $request->password_confirm) {
            return self::inertiaError('Your new password and confirmation do not match');
        }

        // Handle
        try {
            // Update Password
            $this->handle(
                auth: $auth,
                password: $request->password_new
            );
        } catch (\Exception $e) {
            return self::handleInertiaError($e);
        }

        // Return
        return self::inertiaSuccess(
            message:'Your password was successfully updated.',
        );
    }

    public function handle(
        Admin|User $auth,
        string $password,
    ) {
        // NOTE, validation (current password, confirmation) should be handled prior to this

        // Update the password
        $passwordHash = Hash::make($password);
        $auth->password = $passwordHash;

        // Store
        $auth->save();

        // Return
        return $auth;
    }

}
