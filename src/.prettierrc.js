module.exports = {
    printWidth: 160,
    tabWidth: 4,
    singleQuote: false,
    trailingComma: "es5",
    semi: true,
};
