const path = require("path");
const execSync = require("child_process").execSync;

const exec = (cmd, options = {'encoding': 'UTF-8'}) => {
  return execSync(cmd, options)
};

const abortIf = (condition, message, code = 0) => {
  if (typeof condition === 'function') {
    condition = condition()
  }

  if (condition) {
    console.log(message)
    process.exit(code)
  }
}

const inPath = (script, message) => {
  let result;
  let error;
  try {
    result = exec(`which ${script}`)
  } catch(e) {
    error = e
  }

  if (!result || error) {
    console.error(message || `"${script}" could not be found in your path.`);
    process.exit(1)
  }
}


const basePath = function (dir) {
  return path.join(__dirname, "../..", dir);
};

const appPath = function (dir) {
  return path.join(__dirname, "../../src", dir);
};

const camelCase = function (str) {
  return str
    .replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
      return index === 0 ? word.toLowerCase() : word.toUpperCase();
    })
    .replaceAll("-", "")
    .replaceAll("_", "")
    .replace(/\s+/g, "");
};

const kebabCase = function (str) {
  return str
    .replace(/\s+/g, "-")
    .toLowerCase();
};


module.exports = {
  abortIf,
  appPath,
  basePath,
  camelCase,
  kebabCase,
  inPath,
  exec,
}
